[TOC]

# 0. Introduction # {#intro} 

*Sphinx* (Spectral Parallel High-dimensional solver for 
Incompressible Navier-Stokes-Euler Collective Simulations)
is a C++(11) spectral based incompressible
solver for computation of ensemble of solutions 
using MC/MLMC tecniques.
Sphinx is multi-threaded (OpenMP) and multi-processor (MPI) and exploits GPU acceleration 
(via CUDA) whenever possible. Distributed memory parallelisation is done using 1D slabbing.
It is based on a number of high-performance numerical libraries (`fftw` for FFT, `cufft`, `BLAS`, 
`cublas`, if available, for GPU computations and `MKL`, if available, for fast BLAS/FFT operations), 
as well as many other
external libraries, such as hdf5, boost and uses `paraview`/`xdmf`/`hdf5` for parallel I/O and visualization.
 Furthermore is compatible with the "poseidon" postprocessing routines.
Compilation and
installation is done reliably and portably using CMake. Some additional feature is written in Python/Bash script.

# 1. Compiling Sphinx # {#compiling} 

## a. Dependencies ##

The following packages are *needed* for the compilation of Sphinx:
 - `boost` (for options and other small things)
 - `cmake` (compilation unit)
 - `mpi` (any, parallel computing, will be optional)
 - `hdf5` (optionally with C++ bindings) for I/O

Additionally, one of the following is needed::
 - `cuda` (with CUFFT/CUBLAS, for device computations and DFTs)
 - `fftw` (for host DFT, will be optional) and a BLAS implementation
 - `MKL` (for alternative FFT)
 - `ACCFFT` (needs CUDA (TODO: fftw))

Optional:
 - `openmp` (thread loop parallelization)

Moreover, we use the following additional tools:
 - `paraview` (visualize ouput)
 - `xdmf` reader (output is hdf5+xdmf, `paraview` is sufficient)
 - `python` + `matplotlib` + `h5py` + `numpy` for postprocessing 
 ([Poseidon project](https://gitlab.math.ethz.ch/lfilippo/poseidon))

### Hall of shame ###

CUDA incompatibilities (nvcc):
 - fftw: problem with __float128
 - boost: problem with __builtin_ia32_monitorx when including filesystem or property tree
 - enable_if construct (in CUDA 7.0)
 - gcc < 5.2.0

CUDA Aware MPI:
 - overlapped memory on local machine

MKL issues:
 - may need to load correct GCC version (<5.1.0)
 - OpenMP may not work
 - linking: carefully link against correct library (lmkl_blacs_openmpi_ilp64 vs lmkl_blacs_ilp64)
 - linking order: set the libraries order to -lmkl_core_cdft in front of -lmkl_intel_ilp64
 - use https://software.intel.com/en-us/articles/intel-mkl-link-line-advisor
 - cdfti won't work with MPI

## b. Setup / Install ##

Setup environment: source the file located in `/scripts` corresponding to wanted machine (e.g. 
`". .<sphinx_source_path>/scripts/euler_modules.sh"` for Euler). 
This sets up some environment variable for you
and loads the necessary modules. This is not necessary, but highly recommended.

Simply run `CMake` (`cmake <path> <opts>`) in the wanted `build` path. 
The following useful CMake variables (<opts>) can be used to
configure the project:
 - `-DCMAKE_INSTALL_PREFIX=<prefix>`, all  necessary files will be installed in `<prefix>`
 - `-DOUTPUT_LEVEL=<lvl>`, suppresses/enables/choose verbosity of output
 - `-DUSE_COLORS=<true/false>`, nice color to make the place a bit mor cozy, may mess up output
 - `-DDOXYGEN_ENABLE=<ON/OFF>`, set to OFF if you do not have Doxygen
 - `-DVAMPIR_ENABLE=<ON/OFF>`, set to OFF if you do not want to trace with VampirTrace
 - `-DCMAKE_BUILD_TYPE=<Debug/Release>`, changes optimizations and strips simbols
    WARNING: make sure this is set to release for production runs (otw. O3 will be disabled)
 - `-DUSE_CUDA=<true/false>`, wanna use `CUDA`?
 - `-DUSE_MKL=<true/false>`, use MKL as backend
 - `-DUSE_FFTW=<true/false>`, use `FFTW` as backend
 - `-DFREQEVO=<true/false>`, use frequency or time evolution
 - `-DNDIMS=<2/3>`, select the dimensionality of the simulation
 - `-DUSE_TEST=<int>`, select the test to be performed
 - `-DUSE_VELOCITY=<int>`, select the test to be performed
 - `-DINITIAL_DATA=<int>`, select the initial data to use

Then compile with make (<n> number of processors):

    make -j<n>

Compile the documentation, if you want and if it was enabled:

    make doc

Install everything into `CMAKE_PREFIX_PATH`:

    make install
    
An helper script is created for automatic jobs submission on Clusters:

    ./batch_launcher.sh [options]
    
(WARNING: this overrides some options setted in the configuration file.)
    
You can run some integrity checks by changing directory to `tests` and running:

    ctest

For postprocessing, you can either use `paraview` or use the Python postprocessing tool `poseidon`.

### i. DAINT Specific informations ###

On Daint the situation is the following:
 - Cray compiler is not working (as of 25.11.2015)
 - CUDA is at v. 6.5 or 7.0 (no full C++11), CUDA sucks:
   - weird issues
   - seems problem with enable_if (std's and boost's)
   - currently fails because of wrong template kernel, __T0
 - Boost is half-broken (fixed)
   - must specify path to boost in CMake
   - boost need to compile statically
 - may need to "make" two times (deprecated?)

### ii. DORA Specific informations ###

CUDA is not available.

### iii. EULER Specific informations ###

CUDA is not available. 

Last checked 17.5.2017, everything works fine.

### iv. BRUTUS Specific informations (deprecated) ###

CUDA is not supported/old.

## c. Status ## {#status}

Cf. TODO and CHANGELOG for more info.

FFTW based:
 - 2D+3D available
 - MC + MLMC available
 - spectral viscosity
 - interlaced
 - TODO: not contiguous

CUDA:
 - 2D+3D
 - MC (MLMC: broken FFT, easy fix)
 - spectral viscosity
 - broken CUDA aware MPI

MKL:
 - 2D
 - broken MPI (fixed?)
 - 3D: broken

## d. Scaling/Performance and stuff ## {#scaling}

A ffft/bfft of size 512^3 on 3 dofs can be done in 1s using 128 cores.
512^3 requires 2GB of data per dof.

Number of evolution calls per TS routine:

| Timestepping | Evo. calls | Temporaries |
|--------------|------------|------------:|
| FE           | 1          | 2 + 0       |
| SSP RK2      | 2          | 2 + 1       |
| SSP RK3      | 3          | 2 + 2       |

Number of fft calls per `Evo` routine:

| Evolution | Forward                  | Backward | Tot. 2D | Tot. 3D   |
| --------- | ------------------------ | -------- | ------- | --------- |
| Evo freq  | sym tensor               | velocity | 5       | 9         | 
| Evo time  | velocity + sym tensor    | velocity | 7       | 11        |

## e. Debugging/Profiling/Benchmarking ##

TODO

### Debugging: ###

TODO

    nvvd

    gdb mpi

### Profiling: ###

Allinea DDT:

TODO

Totalview:

TODO

VapirTrace/Vampir:

TODO

Valgrind:

TODO

# 2. Random stuff #

Bulk of computation: currently the bulk of computations is fft/bfft.

Reason is that kernels are very efficient in CUDA and n*log(n) fft stands out.

## a. Utilities ##

### CUDA ###

Check cuda status can be done with:

    nvidia-smi

Profiler:

    nvvp

## b. MLMC parallelism ##

Each level needs at least one rank.

|  lvl 0               |  lvl 1   |   ...   |  lvl N             |
|----------------------|----------|---------|--------------------|
|  coarse              |          |         |  finer             |
|  no diff.            | diff.    |         |  diff.             |
|  send to master root |          |         |  recv. from roots  |

# 3. Data layout # {#data_layout}

See \ref indexing.

# 4. Modules: Solvers/Schemes/Initial data/Models # {#list-of-modules}

Available solvers:
 - spectral viscosity (time/frequency evolution, CUDA/FFTW ffts)

Available timestepping:
 - FE (you do not want to use this)
 - SSP RK2
 - SSP RK3

Available integration types:
 - MC
 - MLMC
 - Gauss (TODO)
 - QMC (TODO)

Available RNGs:
 - std library
 - Well512a
 
What can be computed:
 - individual solutions, mean, variance, structure functions, ...

## a. Available types of initial data: ## {#available-initialdata-brief}

This is a (maybe) incomplete list of all the available types of inital data.
You select the initial data with `--InitialData.name=<name>`. To print an updated list
of all available initial datas, run with the option `--InidialData.name=blabla`.

See \ref available-initialdata.

 - Kärünèn-Löèvé expansion (cf. http://www.xkcd.com/1647/)
 - Vortex-sheet
 - Kissing voritces
 - Blob
 - Patch
 - Taylor-Green
 - Constant
 - Brownian motion

## b. Available options: ## {#available-options}

This is a (maybe) incomplete list of all options an the effect they have.
Run `./sphinx --help` to obtain an updated list.

```
--help                                produce help message
-v [ --version ]                      return version number
-c [ --conf ] arg                     configuration file
--unit arg                            test unit chosen
-n [ --name ] arg                     simulation name
--mesh arg                            set mesh size
--T arg (=1)                          final time for simulation
--maxit arg (=1000000)                set max number of options
--cfl arg (=0.5)                      set CFL number
--adaptive arg (=1)                   set adaptive timestepping
--do_initial_cleansing arg (=1)       perform a projection on the initial data
--threshold arg                       threshold for truncation of modes
--eps arg                             eps for numerical diffusion
--aliasing arg                        aliasing modes (effective modes will be padding*mesh-aliasing
--padding arg                         zero padding amount (padded mesh be padding*mesh
--snapshot_fps arg                    fps or snapshots
--snapshot_force arg (=0)             force snapshots at exact time
--snapshot_list arg                   list of snapshots
--master_seed arg                     master seed for simulations and reproducibility
--autopad arg                         List of primes to do automatic padding to.
--fft_inplace                         use in place fft
--nsims arg (=1)                      number of MC simulations (finer level if MLMC)
--levels arg (=1)                     number of levels
--InitialData.name arg                name of our initial data
--InitialData.args arg                generic arguments passed to initial data
--InitialData.rho arg                 smoothness parameter of initial data
--InitialData.delta arg               size of perturbation
--InitialData.N arg                   dimension of random space
--InitialData.alpha arg               size of random perturbation
--InitialData.lambda arg              differentiability of initial data
--InitialData.magnitude arg           magnitude of initial data
--InitialData.H arg                   Hurst index.
--InitialData.scalingFactor arg       Scaling factor for initial data (what it does depends on actual initial data chosen).
--InitialData.noise arg               Noise type.
--InitialData.force_vorticity arg (=0)Force initial data to be of vorticity type.
--Save.individual arg (=1)            save individual simulations
--Save.mc arg (=1)                    save MC simulations
--Save.mlmc arg (=1)                  save MLMC simulations
--Save.diff arg (=0)                  save MLMC diff simulations
--Save.initial arg (=1)               save initial data
--Save.final arg (=1)                 save result at final time
--FFT.mode arg                        FFT planning mode: one of Measure, Patient, Estimate, Exhaustive, WisdomOnly
--FFT.max_time arg (=0)               Maximum planning time.
--FFT.save_wisdom arg (=0)            save wisdom to file
--FFT.load_wisdom arg (=0)            load wisdom from file
```





Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
Licensed under the Apache License, Version 2.0.