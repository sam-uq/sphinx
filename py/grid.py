#!/usr/bin/env python
# -*- coding: utf-8 -*-

#### Copyright 2016 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
####
#### Licensed under the Apache License, Version 2.0 (the "License");
#### you may not use this file except in compliance with the License.
#### You may obtain a copy of the License at
####
####     http://www.apache.org/licenses/LICENSE-2.0
####
#### Unless required by applicable law or agreed to in writing, software
#### distributed under the License is distributed on an "AS IS" BASIS,
#### WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#### See the License for the specific language governing permissions and
#### limitations under the License.

import math

import numpy as np

class grid:
    """

    """

    def __init__(self, size, dim):
        """

        """
        
        self.size = size
        self.dim = dim
        
        self.x = np.linspace(0,1,size[0]+1)[:-1]
        self.y = np.linspace(0,1,size[1]+1)[:-1]
        self.dx = self.x[1] - self.x[0]
        self.dy = self.y[1] - self.y[0]
        
        self.axis = range(self.dim)

    def feval(self, f):
        """
        
        """
        
        dof = len(f(0,0))

        u = np.zeros([len(self.x), len(self.y), dof])

        for k in range(len(self.x)):
            for j in range(len(self.y)):
                u[k,j,:] = f(self.x[k], self.y[j])

        return u
    
    def norm(self, vector, p = 2):
        """
        
        """
        #dof = vector.shape[self.dim]
        
        if p != math.inf:
            return np.linalg.norm(np.reshape(np.linalg.norm(vector, 2, axis=self.dim),(1,-1)), p) * self.dx * self.dy
        else:
            return np.max(np.linalg.norm(vector, 2, axis=self.dim))
        
