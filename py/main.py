#!/usr/bin/env python
# -*- coding: utf-8 -*-

#### Copyright 2016 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
####
#### Licensed under the Apache License, Version 2.0 (the "License");
#### you may not use this file except in compliance with the License.
#### You may obtain a copy of the License at
####
####     http://www.apache.org/licenses/LICENSE-2.0
####
#### Unless required by applicable law or agreed to in writing, software
#### distributed under the License is distributed on an "AS IS" BASIS,
#### WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#### See the License for the specific language governing permissions and
#### limitations under the License.

import math

import numpy as np
import numpy.fft as fft

import matplotlib.pyplot as plt

import timestepping as ts
import evolution as evo
import operators as op
import plot as plt
import grid as grd

if __name__ == "__main__":

    gr = grd.grid((65, 65), 2)

    mu = -0.001
    T = 1

    f = lambda x,y: np.array([
                              +np.sin(2*math.pi*x)*np.cos(2*math.pi*y),
                              -np.cos(2*math.pi*x)*np.sin(2*math.pi*y)
                            ])
    f = lambda x,y: np.array([
                              +np.sin(2*math.pi*x),
                              +np.sin(2*math.pi*y)
                            ])
    rho = .05
    delta = .05
    f = lambda x,y: np.array([  rho == 0. and \
                                ((y > 0.75 or y < 0.25) and 1 or -1) or \
                                (y > .5 and math.tanh((y-.75) / rho) or math.tanh((.25-y) / rho)),
                                delta*np.sin(2*math.pi*x)
                            ])

    u_0 = gr.feval(f)

    u_0 = fft.rfftn(u_0, None, (0,1), norm='ortho')

    rhs = lambda u: evo.nse(u, mu)

    u = ts.stepper(gr, u_0, T, rhs, ts.ssp_rk3)

    u = fft.irfftn(u, None, (0,1), norm='ortho')

    plt.pcolor(u)
    
    print(op.tm_proj)
    print(op.tm_lapl)
    print(op.tm_sym_div)
    print(op.tm_conv)
