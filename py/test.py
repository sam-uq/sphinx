#!/usr/bin/env python
# -*- coding: utf-8 -*-

#### Copyright 2016 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
####
#### Licensed under the Apache License, Version 2.0 (the "License");
#### you may not use this file except in compliance with the License.
#### You may obtain a copy of the License at
####
####     http://www.apache.org/licenses/LICENSE-2.0
####
#### Unless required by applicable law or agreed to in writing, software
#### distributed under the License is distributed on an "AS IS" BASIS,
#### WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#### See the License for the specific language governing permissions and
#### limitations under the License.

import sys

import math

import numpy as np
import numpy.fft as fft

import timestepping as ts
import evolution as evo
import operators as op
import plot as plt
import grid as grd

def test_convection():
    """
    
    """
    
    gr = grd.grid((128, 64), 2)

    f = lambda x,y: np.array([
                              +np.sin(2*math.pi*x),
                              +np.sin(2*math.pi*y)
                            ])

    u = gr.feval(f)

    u = fft.rfftn(u, None, (0,1), norm='ortho')

    u = op.convection(u)
    u = op.sym_divergence(u)
    #u = op.laplace(u)

    u = fft.irfftn(u, None, (0,1), norm='ortho')

    plt.pcolor(u)
    
    N_ex = lambda x,y: np.array([
                              +np.sin(2*math.pi*x)**2,
                              +np.sin(2*math.pi*x)*np.sin(2*math.pi*y),
                              +np.sin(2*math.pi*y)**2,
                            ])
    g_ex = lambda x,y: np.array([
                              +4*math.pi*np.cos(2*math.pi*x)*np.sin(2*math.pi*x)
                              +2*math.pi*np.sin(2*math.pi*x)*np.cos(2*math.pi*y),
                              +2*math.pi*np.sin(2*math.pi*y)*np.cos(2*math.pi*x)
                              +4*math.pi*np.cos(2*math.pi*y)*np.sin(2*math.pi*y),
                            ])
    L_ex = lambda x,y: 4*math.pi**2* np.array([
                              -np.sin(2*math.pi*x),
                              -np.sin(2*math.pi*y),
                            ])
    
    v = gr.feval(g_ex)
    
    plt.pcolor(v)
    
    plt.pcolor(u-v)
    
    return gr.norm(u - v)

if __name__ == "__main__":
    if len(sys.argv) > 1:
        if sys.argv[1] == '1':
            print(test_convection())
