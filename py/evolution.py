#!/usr/bin/env python
# -*- coding: utf-8 -*-

#### Copyright 2016 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
####
#### Licensed under the Apache License, Version 2.0 (the "License");
#### you may not use this file except in compliance with the License.
#### You may obtain a copy of the License at
####
####     http://www.apache.org/licenses/LICENSE-2.0
####
#### Unless required by applicable law or agreed to in writing, software
#### distributed under the License is distributed on an "AS IS" BASIS,
#### WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#### See the License for the specific language governing permissions and
#### limitations under the License.

import operators as op

def nse(u, mu):
    """

    """

    return op.projection(mu*op.laplace(u) + op.spectral_viscosity(u) - op.sym_divergence(op.convection(u)))
