#!/usr/bin/env python
# -*- coding: utf-8 -*-

#### Copyright 2016 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
####
#### Licensed under the Apache License, Version 2.0 (the "License");
#### you may not use this file except in compliance with the License.
#### You may obtain a copy of the License at
####
####     http://www.apache.org/licenses/LICENSE-2.0
####
#### Unless required by applicable law or agreed to in writing, software
#### distributed under the License is distributed on an "AS IS" BASIS,
#### WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#### See the License for the specific language governing permissions and
#### limitations under the License.

import math
import time

import numpy as np
import numpy.fft as fft

tm_proj = 0
tm_lapl = 0
tm_sym_div = 0
tm_conv = 0

P = None
L = None
D = None

def projection(uhat):
    """

    """
    global tm_proj
    start = time.time()
    
    N = uhat.shape
    Puhat = np.zeros_like(uhat)

    #BEGIN OLD
    #for k in range(N[0]):
        #for j in range(N[1]):
            #if k == 0 and j == 0:
                #continue
            #K = np.array([
                          #k > N[0] / 2 and k - N[0] or k,
                          #j > N[1] / 2 and j - N[1] or j
                        #])
            #Puhat[k,j,:] = (uhat[k,j,:] - np.dot(K, uhat[k,j,:]) / np.linalg.norm(K)**2 * K)
            
    #BEGIN NEW
    global P
    if P == None:
        P = np.zeros(np.hstack([N[:-1],2,2]))
        for k in range(N[0]):
            for j in range(N[1]):
                if k == 0 and j == 0:
                    continue
                K = np.array([
                            k > N[0] / 2 and k - N[0] or k,
                            j > N[1] / 2 and j - N[1] or j
                            ])
                P[k,j,:,:] = np.eye(2) - np.array([[K[0]**2, K[0]*K[1]], [K[0]*K[1], K[1]**2]]) / (K[0]**2 + K[1]**2)
    Puhat = np.einsum('...ij,...i', P, uhat)

    tm_proj += time.time() - start
    return Puhat

def laplace(uhat):
    """

    """
    global tm_lapl
    start = time.time()
    
    N = uhat.shape
    
    #Luhat = np.zeros_like(uhat)
    #for k in range(N[0]):
        #for j in range(N[1]):
            #K = np.array([
                          #k > N[0] / 2 and k - N[0] or k,
                          #j > N[1] / 2 and j - N[1] or j
                        #])
            #Luhat[k,j,:] = - np.linalg.norm(K)**2 * uhat[k,j,:] * 4 * math.pi**2

    global L
    if L == None:
        Ns = np.array(N)
        Ns[2] = 1
        L = np.zeros(Ns)
        for k in range(N[0]):
            for j in range(N[1]):
                L[k,j,:] = - ( k > N[0] / 2 and k - N[0] or k )**2 + ( j > N[1] / 2 and j - N[1] or j )**2 * 4 * math.pi**2
    Luhat = uhat * L
            
    tm_lapl += time.time() - start
    return Luhat

def sym_divergence(uhat):
    """

    """
    global tm_sym_div
    start = time.time()
    
    N = np.array(uhat.shape)
    N[-1] = 2
    
    # BEGIN OLD
    #Duhat = np.zeros(N, dtype=np.complex)
    
    #S = np.array([[[1j,0,0],[0,1j,0]],[[0,1j,0],[0,0,1j]]])
    
    #for k in range(N[0]):
        #for j in range(N[1]):
            #K = np.array([
                          #k > N[0] / 2 and k - N[0] or k,
                          #j > N[1] / 2 and j - N[1] or j
                        #])
            #Duhat[k,j,:] = S.dot(uhat[k,j,:]).dot(K) * 2 * math.pi

    # BEGIN NEW
    newshape = np.hstack(uhat.shape)
    #unew = np.zeros(newshape)
    
    global D
    if D == None:
        #print(newshape)
        Ns = np.array(newshape)[:-1]
        #print(Ns)
        D = np.zeros(Ns, dtype=complex)
        for k in range(N[0]):
            for j in range(N[1]):
                D[k,j,:] = np.array( [k > N[0] / 2 and k - N[0] or k, j > N[1] / 2 and j - N[1] or j] ) * 2j * math.pi
    #print(uhat.shape)
    #print(D.shape)
    Duhat = np.einsum('...i,...ij', D, uhat)


    tm_sym_div += time.time() - start
    return Duhat

def spectral_viscosity(uhat):
    """

    """
    start = time.time()
    
    Nscalar = np.array(uhat.shape)
    
    return np.zeros(Nscalar, dtype=np.complex)

def convection(uhat):
    """

    """
    global tm_conv
    start = time.time()
    
    perc = 1./3.
    
    M = uhat.shape
    axes = range(len(uhat.shape)-1)
    
    pad = [int((n*perc)/2) for n in uhat.shape]
    
    uhat = fft.fftshift(uhat,(0,))
    uhat = np.pad(uhat,[(pad[0],pad[0]),(0,pad[1]*2),(0,0)],'constant')
    uhat = fft.ifftshift(uhat,(0,))
    
    M2 = uhat.shape
    
    #print("before shape: ", M)
    #print("after shape: ", M2)
    #print("padding: ", pad)

    u_pad = fft.irfftn(uhat, None, axes, norm='ortho')
    
    ### BEGIN LOOP BASED STUFF
    #CNpad = np.array(u_pad.shape)
    #CNpad[2] = 3

    #Cu_pad = np.zeros(CNpad)

    #Cu_pad[:,:,0] = u_pad[:,:,0]**2
    #Cu_pad[:,:,1] = u_pad[:,:,0]*u_pad[:,:,1]
    #Cu_pad[:,:,2] = u_pad[:,:,1]**2

    #Cuhat = fft.rfftn(Cu_pad, None, axes, norm='ortho')
    
    #Cuhat = fft.fftshift(Cuhat,(0,))
    #end = Cuhat.shape - np.array([pad[0], pad[1]*2, 0])
    #Cuhat = Cuhat[pad[0]:end[0],:end[1],:]
    #Cuhat = fft.ifftshift(Cuhat,(0,))
    
    #scaling = math.sqrt(M2[0]*M2[1]/M[0]/M[1])
    ##print("scaling: ", scaling)

    #tm_conv += time.time() - start
    #return Cuhat * scaling
    
    # BEGIN VECTOR CODE
    CNpad = np.array(u_pad.shape)
    CNpad[2] = 3

    Cu_pad = np.zeros(CNpad)

    Cu_pad[:,:,0] = u_pad[:,:,0]**2
    Cu_pad[:,:,1] = u_pad[:,:,0]*u_pad[:,:,1]
    Cu_pad[:,:,2] = u_pad[:,:,1]**2

    Cuhat = fft.rfftn(Cu_pad, None, axes, norm='ortho')
    
    Cuhat = fft.fftshift(Cuhat,(0,))
    end = Cuhat.shape - np.array([pad[0], pad[1]*2, 0])
    Cuhat = Cuhat[pad[0]:end[0],:end[1],:]
    Cuhat = fft.ifftshift(Cuhat,(0,))
    
    Cuhat = np.concatenate([Cuhat, np.expand_dims(Cuhat[:,:,2],2)], axis=2)
    Cuhat[:,:,2] = Cuhat[:,:,1]
    Cuhat = Cuhat.reshape(np.hstack([Cuhat.shape[:-1],2,2]))
    
    scaling = math.sqrt(M2[0]*M2[1]/M[0]/M[1])
    #print("scaling: ", scaling)

    tm_conv += time.time() - start
    return Cuhat * scaling
