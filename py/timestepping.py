#!/usr/bin/env python
# -*- coding: utf-8 -*-

#### Copyright 2016 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
####
#### Licensed under the Apache License, Version 2.0 (the "License");
#### you may not use this file except in compliance with the License.
#### You may obtain a copy of the License at
####
####     http://www.apache.org/licenses/LICENSE-2.0
####
#### Unless required by applicable law or agreed to in writing, software
#### distributed under the License is distributed on an "AS IS" BASIS,
#### WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#### See the License for the specific language governing permissions and
#### limitations under the License.

# @file http://www.cscamm.umd.edu/tadmor/pub/linear-stability/Gottlieb-Shu-Tadmor.SIREV-01.pdf

import math

import numpy as np

import operators as op

def ssp_rk3(u_prev, L, h):
    """
    @param u_prev previous value to evolve
    @param L operator L in u_t = L(u)
    @param h step size
    """

    u_1 = u_prev + h*L(u_prev)
    u_2 = (3./4.)*u_prev + u_1/4. + (h/4.)*L(u_1)
    return u_prev/3. + (2./3.)*u_2 + (2./3.*h)*L(u_2)

def fe(u_prev, L, h):
    """
    @param u_prev previous value to evolve
    @param L operator L in u_t = L(u)
    @param h step size
    """
    
    return u_prev + h*L(u_prev)

def stepper(gr, u_0, T, L, step):
    """

    """
    
    max_count = 1000

    t = 0.
    h = 1./u_0.shape[0]
    cfl = 0.5
    print(h)
    u = u_0

    count = 0
    while t < T and count < max_count:
        #sup = gr.norm(u, math.inf)
        sup = 1
        dt = cfl * h / sup
        print("New time step no. {1} at t = {0:.4}, sup = {2}".format(t, count, sup))
        
        
        t += dt
        u = step(u, L, dt)
        count += 1

    np.save("final_time.py", u)

    return u
