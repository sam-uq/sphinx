#!/usr/bin/env python
# -*- coding: utf-8 -*-

#### Copyright 2016 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
####
#### Licensed under the Apache License, Version 2.0 (the "License");
#### you may not use this file except in compliance with the License.
#### You may obtain a copy of the License at
####
####     http://www.apache.org/licenses/LICENSE-2.0
####
#### Unless required by applicable law or agreed to in writing, software
#### distributed under the License is distributed on an "AS IS" BASIS,
#### WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#### See the License for the specific language governing permissions and
#### limitations under the License.

import matplotlib.pyplot as plt

def pcolor(vector):
    """
    
    """
    
    dof = vector.shape[2]
    
    for d in range(dof):
        plt.subplot(1, dof, d+1)
        plt.pcolormesh(vector[:,:,d])
        plt.colorbar()
        
    plt.show(block=True)


