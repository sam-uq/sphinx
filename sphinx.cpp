/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
//! \file sphinx.cpp Contains the main executable, which automatically selects the unit test from the command line.
//!
//!

#include "sphinx.hpp"

#include "tests/all_tests.hpp"

int main(int argc, char **argv) {
    sphinx::init(argc, argv);

    std::string unit = sphinx::options.as<std::string>("unit");
    // Calls wanted test routine
    std::map<std::string, std::function<int()>> map = {
        {"convergence", conv_test},
        {"mc", mc_test},
        {"mlmc", mlmc_test},
        {"io", io_test},
        {"fftbench", fftbench_test},
        {"padding", basic_test},
        {"projection", projection_test},
        {"structure", structure_test},
        {"ghost", ghost_test},
//        {"step", step_test},
        {"solve", solve_test},
//        {"redistribute", redistribute_test},
//        {"bipolar", bipolar_test},
    };

    int ret = 0;
    if(map.count(unit) > 0) {
        debug::message("############################################################", sphinx::mpi::world);
        debug::message("########## STARTING UNIT " + unit, sphinx::mpi::world);
        debug::message("############################################################", sphinx::mpi::world);
        ret = map[unit]();
    } else {
        debug::error<true>("Choose a valid unit, one of:",
                           sphinx::mpi::world);
        for(auto keyval: map) {
            std::stringstream ss;
            ss << "-" << keyval.first;
            debug::error<true>(ss.str(), sphinx::mpi::world);
        }
        debug::error("...or else.",
                     sphinx::mpi::world);
        ret = -1;
    }

    return sphinx::terminate(ret);
}
