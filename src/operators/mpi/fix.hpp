#pragma once

#include "../../containers/fftw_freq_vec.hpp"

#include "../../utilities/fftw_tools.hpp"

template <class T>
inline void fix_slice(const FreqVec<T, 3, false, false> & in, pos_t<3> indexes) {
    profiler::start_stage("fix");
    std::vector<MPI_Request> send_req, recv_req;

    if(in.nk % 2 == 0) {
        const unsigned int complex_size = 2;
        const unsigned int row_size = in.ni*in.nj;

        if( in.kstart <= indexes[0] && indexes[0] < in.kend + in.kstart ) {
            const unsigned int dest = in.find_rank(in.nk-indexes[0]);
            const unsigned int relative_start = indexes[0] - in.kstart;
            const unsigned int offset = row_size*relative_start;
            std::stringstream ss;
            ss << "Sending missing conjugated data of size " << row_size*complex_size
               << " at row " << indexes[0] << " to rank " << dest
               << " and at offset " << offset << " (rstart = " << relative_start << ").";
            debug::verbose(ss.str(), in.grid_p->comm, sphinx::mpi::Policy::Self, debug::mpi);
            for(unsigned int f = 0; f < in.dof; ++f) {
                send_req.push_back(MPI_Request());
                mpiErrchk(MPI_Isend(in._data[f] + offset,
                                    row_size*complex_size,
                                    MPI_DOUBLE, dest, sphinx::fix_tag+f, in.grid_p->comm.comm, &send_req.back()));
            }
        }
        if( in.kstart <= in.nk-indexes[0] && in.nk-indexes[0] < in.kend + in.kstart ) {
            const unsigned int src = in.find_rank(indexes[0]);
            const unsigned int relative_start = in.nk-indexes[0]-in.kstart;
            const unsigned int offset = row_size*relative_start;
            std::stringstream ss;
            ss << "Receiving missing conjugated data of size " << row_size*complex_size
               << " at row " << in.nk-indexes[0] << " from rank " << src
               << " and placing it at offset " << offset << " (rstart = " << relative_start << ").";
            debug::verbose(ss.str(), in.grid_p->comm, sphinx::mpi::Policy::Self, debug::mpi);
            for(unsigned int f = 0; f < in.dof; ++f) {
                recv_req.push_back(MPI_Request());
                mpiErrchk(MPI_Irecv(in._data[f] + offset,
                                    row_size*complex_size, MPI_DOUBLE, src, sphinx::fix_tag+f,
                                    in.grid_p->comm.comm, &recv_req.back()));
            }
        }
    }

    if(in.nj % 2 == 0) {
        #pragma omp parallel for collapse(2)
        for(int k = 0; k < in.kend; ++k) {
            for(int i = 0; i < in.iend; ++i) {
                pos_t<3> from = {k,indexes[1],i};
                pos_t<3> to = {k,in.nj-indexes[1],i};
                index_t Ix_from = in.outerIndex(from);
                index_t Ix_to = in.outerIndex(to);
                for(unsigned int f = 0; f < in.dof; ++f) {
                    in(f,Ix_to) = conj(in(f, Ix_from));
                }
            }
        }
    }

    waitall(recv_req);

    if(in.nk % 2 == 0 && in.kstart <= in.nk-indexes[0] && in.nk-indexes[0] < in.kend + in.kstart ) {
        if(in.nj % 2 == 0) {
            #pragma omp parallel for
            for(int i = 0; i < in.iend; ++i) {
                pos_t<3> from = {in.nk-indexes[0]-in.kstart,indexes[1],i};
                pos_t<3> to = {in.nk-indexes[0]-in.kstart,in.nj-indexes[1],i};
                index_t Ix_from = in.outerIndex(from);
                index_t Ix_to = in.outerIndex(to);
                for(unsigned int f = 0; f < in.dof; ++f) {
                    in(f,Ix_to) = conj(in(f, Ix_from));
                }
            }
        }
    }

    waitall(send_req);

    profiler::end_stage();
}

template <class T, bool contiguous, bool interlraced>
inline void fix_slice(const FreqVec<T, 2, contiguous, interlraced> & in, pos_t<2> indexes) {
    profiler::start_stage("fix");

    if(in.nj % 2 == 1) {
        profiler::end_stage();
        return;
    }

    #pragma omp parallel for
    for(int k = 0; k < in.kend; ++k) {
        pos_t<2> from = {k,indexes[1]};
        pos_t<2> to= {k,in.nj-indexes[1]};
        index_t Ix_from = in.outerIndex(from);
        index_t Ix_to = in.outerIndex(to);
        for(unsigned int f = 0; f < in.dof; ++f) {
            in(f,Ix_to) = conj(in(f, Ix_from));
        }
    }

    profiler::end_stage();
}
