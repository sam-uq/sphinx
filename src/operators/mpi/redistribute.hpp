#pragma once

// TODO: rename to kern

#include <type_traits>

#include "../../sphinx_internal.hpp"

// Zero Pads arg 0 onto arg 1
template <class Vec, unsigned int d>
struct redistribute {
    static constexpr unsigned int dim = d;

    struct std_buffer : public std::vector<std::vector<double>> {
        inline double operator()(unsigned int f, unsigned int Idx) const {
            return (*this)[f][Idx];
        }

        inline double& operator()(unsigned int f, unsigned int Idx) {
            return (*this)[f][Idx];
        }
    };

    struct irl_buffer : public std::vector<double> {
        inline double operator()(unsigned int f, unsigned int Idx) const {
            return (*this)[dof*Idx+f];
        }

        inline double& operator()(unsigned int f, unsigned int Idx) {
            return (*this)[dof*Idx+f];
        }

        unsigned int dof;
    };

    using buffer_t = typename std::conditional<
        Vec::base_t::interlaced_data == true,
        irl_buffer,
        std_buffer
    >::type;

    static void start(const Vec & in, const Vec & out) {

        profiler::start_stage("redistribute");

        const unsigned int complex_size = 2;
        const unsigned int row_size = in.dim == 3 ? in.nj*in.ni : in.nj;
        const unsigned int N = in.dim == 3 ? in.nk : in.nk*2;
        const unsigned int Nout = out.dim == 3 ? out.nk : out.nk*2;
        const unsigned int Nmin = std::min(N, Nout);
        const int padsize = (int) Nout - (int) N;
        const int p0 = std::max((int) N - (int) Nout, 0);

        alloc(in, out, complex_size, row_size);

        send_req.clear();
        recv_req.clear();
        // redistribute(in, out, recvbuffer);
        // Sending
        if(in.kstart < N/2+1) { // Left side of padding
            for(int r = 0; r < in.grid_p->comm.size; ++r) {
                const unsigned int in_start = std::min(Nmin/2+1, (unsigned int) in.kstart);
                const unsigned int in_end = std::min(Nmin/2+1, (unsigned int) (in.kstart+in.kend));
                const unsigned int out_start = out.grid_p->partitioning[4*r+2];
                const unsigned int out_end = out.grid_p->partitioning[4*r+2] + out.grid_p->partitioning[4*r+3];
                if( out_start < in_end && out_end > in_start ) {
                    // Output of Rank r does intersect Input of this rank
                    const unsigned int left = std::max(in_start, out_start);
                    const unsigned int right = std::min(in_end, out_end);
                    const unsigned int interval_size = right - left;
                    const unsigned int size = interval_size*row_size*complex_size; // Size of data to send
                    const unsigned int relative_start = left - in_start;
                    const unsigned int offset = relative_start*row_size; // derp no complex size

                    if( size == 0 ) continue;

                    mpi_send_line(in, size, offset, r, sphinx::padding_tag_left);

                    std::stringstream ss;
                    ss << "Sending left data to rank " << r << ": in interval = [" << in_start << "," << in_end
                       << "], out interval = [" << out_start << "," << out_end
                       << "], row size is = " << row_size << ", sending " << size << " data ("
                       << interval_size << " rows) (offset = "
                       << offset << ", rstart = " << relative_start << ").";
                    debug::verbose(ss.str(),in.grid_p->comm, sphinx::mpi::Policy::Self, debug::mpi);
                } else {
                    std::stringstream ss;
                    ss << "No left send intersection with rank " << r << ": in interval  = ["
                       << in_start << "," << in_end
                       << "], out interval = [" << out_start << "," << out_end << "].";
                    debug::verbose(ss.str(),in.grid_p->comm, sphinx::mpi::Policy::Self, debug::mpi);
                }
            }
        }

        if(in.kstart + in.kend >= N/2+1) { // Right side of padding// Not done in 2D
            for(int r = 0; r < in.grid_p->comm.size; ++r) {
                const unsigned int in_start = std::max(Nmin/2+1+p0,
                                                       (unsigned int) in.kstart) + padsize;
                const unsigned int in_end = std::max(Nmin/2+1+p0,
                                                     (unsigned int) (in.kstart+in.kend ))+ padsize;
                const unsigned int out_start = out.grid_p->partitioning[4*r+2];
                const unsigned int out_end = out.grid_p->partitioning[4*r+2] + out.grid_p->partitioning[4*r+3];
                if( out_start < in_end && out_end > in_start ) {
                    // Output of Rank r does intersect Input of this rank
                    const unsigned int left = std::max(in_start, out_start);
                    const unsigned int right = std::min(in_end, out_end);
                    const unsigned int interval_size = right - left;
                    const unsigned int size = interval_size*row_size*complex_size;
                    const unsigned int relative_start = left - in.kstart - padsize;
                    const unsigned int offset = relative_start*row_size;

                    if( size == 0 ) continue;

                    mpi_send_line(in, size, offset, r, sphinx::padding_tag_right);

                    std::stringstream ss;
                    ss << "Sending right data to rank " << r << ": in interval = [" << in_start << "," <<in_end
                       << "], out interval = [" << out_start << "," << out_end
                       << "], row size is = " << row_size << ", sending " << size << " data (" <<
                          interval_size << " rows) (offset = "
                       << offset << ", rstart = " << relative_start << ").";
                    debug::verbose(ss.str(),in.grid_p->comm, sphinx::mpi::Policy::Self, debug::mpi);
                } else {
                    std::stringstream ss;
                    ss << "No right send intersection with rank " << r
                       << ": in interval  = [" << in_start << "," << in_end
                       << "], out interval = [" << out_start << "," << out_end << "].";
                    debug::verbose(ss.str(),in.grid_p->comm, sphinx::mpi::Policy::Self, debug::mpi);
                }
            }
        }

        // Receiving
        if(out.kstart < Nout/2+1) { // Left side of padding
            for(int r = 0; r < in.grid_p->comm.size; ++r) {
                const unsigned int in_start = std::min(Nmin/2+1,
                                                       (unsigned int) (in.grid_p->partitioning[4*r+2]));
                const unsigned int in_end = std::min(Nmin/2+1,
                                                     (unsigned int) (in.grid_p->partitioning[4*r+2]
                                                     + in.grid_p->partitioning[4*r+3]));
                const unsigned int out_start = out.kstart;
                const unsigned int out_end = out.kstart + out.kend;
                if( in_start < out_end && in_end > out_start ) {
                    // Output of Rank r does intersect Input of this rank
                    const unsigned int left = std::max(out_start, in_start);
                    const unsigned int right = std::min(out_end, in_end);
                    const unsigned int interval_size = right - left;
                    const unsigned int size = interval_size*row_size*complex_size;
                    const unsigned int relative_start = left - out_start;
                    const unsigned int offset = relative_start*row_size*complex_size;

                    if( size == 0 ) continue;

                    mpi_recv_line(in, size, offset, r, sphinx::padding_tag_left);

                    std::stringstream ss;
                    ss << "Receiving left data from rank " << r << ": in interval = ["
                       << in_start << "," << in_end
                       << "], out interval = [" << out_start << "," << out_end
                       << "], row size is = " << row_size << ", receiving " << size << " data ("
                       << interval_size << " rows) (offset = "
                       << offset << ", rstart = " << relative_start << ").";
                    debug::verbose(ss.str(), in.grid_p->comm, sphinx::mpi::Policy::Self, debug::mpi);
                } else {
                    std::stringstream ss;
                    ss << "No left recv intersection with rank " << r
                       << ": in interval  = [" << in_start << "," << in_end
                       << "], out interval = [" << out_start << "," << out_end << "].";
                    debug::verbose(ss.str(), in.grid_p->comm, sphinx::mpi::Policy::Self, debug::mpi);
                }
            }
        }
        // ??????????????????????????????????????????????????

        // Can cut away some padding
        if(out.kend + out.kstart >= Nout/2+1) { // Right side of padding // Not done in 2D
            for(int r = 0; r < in.grid_p->comm.size; ++r) {
                if( (unsigned int) (in.grid_p->partitioning[4*r+2]
                                    + in.grid_p->partitioning[4*r+3]) < N/2+1 ) continue;
                const unsigned int in_start = std::max(Nmin/2+1+p0,
                                                       (unsigned int) (in.grid_p->partitioning[4*r+2]))
                                                     + padsize;
                const unsigned int in_end = std::max(Nmin/2+1+p0,
                                                     (unsigned int) (in.grid_p->partitioning[4*r+2]
                                                     + in.grid_p->partitioning[4*r+3])) + padsize;
                const unsigned int out_start = out.kstart;
                const unsigned int out_end = out.kstart + out.kend;
                if( in_start < out_end && in_end > out_start ) {
                    // Output of Rank r does intersect Input of this rank
                    const unsigned int left = std::max(out_start, in_start);
                    const unsigned int right = std::min(out_end, in_end);
                    const unsigned int interval_size = right - left;
                    const unsigned int size = interval_size*row_size*complex_size;
                    const unsigned int relative_start = left - out_start;
                    const unsigned int offset = relative_start*row_size*complex_size;

                    if( size == 0 ) continue;

                    mpi_recv_line(in, size, offset, r, sphinx::padding_tag_right);

                    std::stringstream ss;
                    ss << "Receiving right data from rank " << r
                       << ": in interval = [" << in_start << "," << in_end
                       << "], out interval = [" << out_start << "," << out_end
                       << "], row size is = " << row_size << ", receiving " << size << " data ("
                       << interval_size << " rows) (offset = "
                       << offset << ", rstart = " << relative_start << ").";
                    debug::verbose(ss.str(), in.grid_p->comm, sphinx::mpi::Policy::Self, debug::mpi);
                } else {
                    std::stringstream ss;
                    ss << "No right recv intersection with rank " << r
                       << ": in interval = [" << in_start << "," << in_end
                       << "], out interval = [" << out_start << "," << out_end << "].";
                    debug::verbose(ss.str(), in.grid_p->comm, sphinx::mpi::Policy::Self, debug::mpi);
                }
            }
        }

        profiler::end_stage();
    }

    template <class Vec_ = Vec, typename std::enable_if<Vec_::interlaced_data == true, bool>::type = true>
    static inline void alloc(const Vec_ & in, const Vec_ & out,
                             unsigned int complex_size, unsigned int row_size) {
        assert(in.dof == out.dof && "Not yet supported!");

        recvbuffer.dof = out.dof;
        recvbuffer.resize(out.kend*row_size*complex_size*in.dof);

        std::fill(recvbuffer.begin(), recvbuffer.end(), 0); // put here other number to expose padding
    }

    template <class Vec_, typename std::enable_if<Vec_::interlaced_data == false, bool>::type = false>
    static inline void alloc(const Vec_ & in, const Vec_ & out,
                             unsigned int complex_size, unsigned int row_size) {
        recvbuffer.resize(std::max((int) recvbuffer.size(), (int) in.dof));
        for(auto& v: recvbuffer) {
            v.resize(out.kend*row_size*complex_size);
        }
        // Add padding
        // TODO
        for(auto& v: recvbuffer) {
            std::fill(v.begin(), v.end(), 0); // put here other number to expose padding
        }
    }

    template <class Vec_, typename std::enable_if<Vec_::interlaced_data == false, bool>::type = false>
    static inline void mpi_send_line(const Vec_ & in,
                                     unsigned int size, unsigned int offset,
                                     unsigned int rank, unsigned int padding_tag) {
        for(unsigned int f = 0; f < in.dof; ++f) {
            MPI_Request req;
            mpiErrchk(MPI_Isend(in._data[f] + offset,
                      size, MPI_DOUBLE, rank,
                      padding_tag+f, in.grid_p->comm.comm, &req));
            send_req.push_back(req);
        }
    }

    template <class Vec_, typename std::enable_if<Vec_::interlaced_data == true, bool>::type = true>
    static inline void mpi_send_line(const Vec_ & in,
                                     unsigned int size, unsigned int offset,
                                     unsigned int rank, unsigned int padding_tag) {
        MPI_Request req;
        mpiErrchk(MPI_Isend(in._data + offset,
                            size*in.dof, MPI_DOUBLE, rank,
                            padding_tag, in.grid_p->comm.comm, &req));
        send_req.push_back(req);
    }

    template <class Vec_, typename std::enable_if<Vec_::interlaced_data == false, bool>::type = false>
    static inline void mpi_recv_line(const Vec_ & in,
                                     unsigned int size, unsigned int offset,
                                     unsigned int rank, unsigned int padding_tag) {
        for(unsigned int f = 0; f < in.dof; ++f) {
            MPI_Request req;
            mpiErrchk(MPI_Irecv(recvbuffer[f].data() + offset,
                                size, MPI_DOUBLE, rank,
                                padding_tag+f, in.grid_p->comm.comm, &req));
            recv_req.push_back(req);
        }
    }

    template <class Vec_, typename std::enable_if<Vec_::interlaced_data == true, bool>::type = true>
    static inline void mpi_recv_line(const Vec_ & in,
                                     unsigned int size, unsigned int offset,
                                     unsigned int rank, unsigned int padding_tag) {
        MPI_Request req;
        mpiErrchk(MPI_Irecv(recvbuffer.data() + offset,
                            size*in.dof, MPI_DOUBLE, rank,
                            padding_tag, in.grid_p->comm.comm, &req));
        recv_req.push_back(req);
    }

    static void send_wait() {
        waitall(send_req);
    }

    static void recv_wait() {
        waitall(recv_req);
    }

    static buffer_t recvbuffer;

    static std::vector<MPI_Request> send_req, recv_req;
};

template <class Vec, unsigned int d>
typename redistribute<Vec, d>::buffer_t redistribute<Vec, d>::recvbuffer;

template <class Vec, unsigned int d>
std::vector<MPI_Request> redistribute<Vec, d>::send_req;

template <class Vec, unsigned int d>
std::vector<MPI_Request> redistribute<Vec, d>::recv_req;
