/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include <array>

#include "config.hpp"

#include "../utilities/mpi_tools.hpp"

#include "../containers/vec.hpp"

#include "../containers/host_vec.hpp"

#include "../geometry/remote_grid.hpp"

namespace sphinx {
namespace operators {

template <class T, class U, unsigned int d, bool contiguous, bool interlaced>
HostVec<U, d, contiguous, interlaced>& DeviceToHost(const HostVec<T,d, contiguous, interlaced> &,
                                                    HostVec<U, d, contiguous, interlaced> & dv) { return dv; }

template <class T, class U, unsigned int d, bool contiguous, bool interlaced>
HostVec<U, d, contiguous, interlaced>& HostToDevice(const HostVec<T,d, contiguous, interlaced> &,
                                                    HostVec<U, d, contiguous, interlaced> &hv) { return hv; }

}
}

template <class T, unsigned int d, bool contiguous, bool interlaced>
void send(const Vec<T, d, contiguous, interlaced> & V, std::shared_ptr<RemoteGrid<d>> rgr, int subtag) {
    static_assert(!contiguous && !interlaced, "Not yet implemented!");

    profiler::start_stage("send");

    const unsigned int size = 4;
    unsigned int pad = V.parpad;

    const unsigned int rowsize = (V.dim == 3 ? V.nj*V.ni : V.nj);

    const int tag = sphinx::send_vec_tag + subtag;

    for(int r = 0; r < rgr->comm.size; ++r) {
        unsigned int Istart = std::max(rgr->partitioning[size*r + pad],
                (int)(V.kstart));
        unsigned int Iend = std::min(rgr->partitioning[size*r + pad]
                + rgr->partitioning[size*r + pad + 1],
                (int)(V.kend+V.kstart));

        if(Istart < Iend) {

            unsigned int count = rowsize*(Iend-Istart);
            unsigned int f = 0;

            std::stringstream ss;
            ss << "Sending V to rk " << r+rgr->comm.start_rank_in_parent
               << " (tag: " << tag << "), data start k = "
               << Istart - V.kstart << " (row size " << rowsize << "), send " << count << "data.";
            debug::verbose(ss.str(), V.grid_p->comm, mpi::Policy::Self);

            for(auto dt: V._data) {
                std::stringstream ss1;
                ss1 << "Send from rk " << r+rgr->comm.start_rank_in_parent
                    << " (tag: " << f << ").";
                debug::verbose(ss1.str(), V.grid_p->comm, mpi::Policy::Self);
                mpiErrchk(MPI_Send(dt  + (Istart - V.kstart) * rowsize, count, MPI_DOUBLE,
                         r+rgr->comm.start_rank_in_parent, tag + f,
                         rgr->intra_level_comm.comm));
                ++f;
            }
        }
    }

    profiler::end_stage();
}

template <class T, unsigned int d, bool contiguous, bool interlaced>
void recv(const Vec<T, d, contiguous, interlaced> & V, std::shared_ptr<RemoteGrid<d>> rgr, int subtag) {
    static_assert(contiguous == false && interlaced == false, "Not yet implemented!");

    profiler::start_stage("recv");

    const unsigned int size = 4;
    unsigned int pad = V.parpad;

    const unsigned int rowsize = (V.dim == 3 ? V.nj*V.ni : V.nj);

    const int tag = sphinx::send_vec_tag + subtag;

    MPI_Status status;

    for(int r = 0; r < rgr->comm.size; ++r) {
        unsigned int Istart = std::max(rgr->partitioning[size*r + pad], (int)(V.kstart));
        unsigned int Iend = std::min(rgr->partitioning[size*r + pad]
                + rgr->partitioning[size*r + pad + 1], (int)(V.kend+V.kstart));

        if(Istart < Iend) {

            unsigned int count = rowsize*(Iend-Istart);
            unsigned int f = 0;

            std::stringstream ss;
            ss << "Recieving from rk " << r+rgr->comm.start_rank_in_parent
               << " (tag: " << tag << "), data start k = "
               << Istart - V.kstart << " (row size " << rowsize << "), send " << count << "data.";
            debug::verbose(ss.str(), V.grid_p->comm, mpi::Policy::Self);

            for(auto dt: V._data) {
                std::stringstream ss1;
                ss1 << "Recieving from rk " << r+rgr->comm.start_rank_in_parent << " (tag: " << f << ").";
                debug::verbose(ss1.str(), V.grid_p->comm, mpi::Policy::Self);
                mpiErrchk(MPI_Recv(dt + (Istart - V.kstart) * rowsize, count, MPI_DOUBLE,
                         r+rgr->comm.start_rank_in_parent,
                         tag + f,
//                         MPI_ANY_TAG,
                         rgr->intra_level_comm.comm, &status));
                mpiStatuschk(status);
                ++f;
            }
        }
    }

    profiler::end_stage();
}
