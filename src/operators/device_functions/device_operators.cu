/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include <cuComplex.h>

#include "utilities/profiler.hpp"

#include "device_operators.hpp"

#include "operators/device_kernels/all_kern.hpp"

#include "utilities/cuda_tools.hpp"

template <unsigned int dim, bool contiguous, bool interlaced>
void projection(CUDADeviceFreqVec<dim, contiguous, interlaced> & in, CUDADeviceFreqVec<dim, contiguous, interlaced> & out) {
    profiler::start_stage("device::projection");
    assert( in.dof >= dim && out.dof >= dim && "Incompatible dofs!");

    projection_kern<CUDADeviceFreqVec<dim, contiguous, interlaced>>
            <<<in.dimGrid, in.dimBlock>>>(in.device_data, out.device_data,
                                          in.device_size);
    gpuErrchk( cudaPeekAtLastError() );

    out.scaling_factor = in.scaling_factor;
    profiler::end_stage();
}

template void projection<2, false, false>(CUDADeviceFreqVec<2, false, false> & in,
CUDADeviceFreqVec<2, false, false> & out);
template void projection<3, false, false>(CUDADeviceFreqVec<3, false, false> & in,
CUDADeviceFreqVec<3, false, false> & out);

//template <unsigned int dim, bool contiguous, bool interlaced>
//void eval(CUDADeviceTimeVec<dim, contiguous, interlaced> & in, const initialdata<Grid<dim, contiguous, interlaced>, dim> & f) {
//    profiler::start_stage("eval");
//    assert( in.dof >= 1 && "Incompatible dofs!");

//    eval_kern<CUDADeviceTimeVec<dim, contiguous, interlaced>, initialdata<Grid<dim, contiguous, interlaced>, dim>>
//            <<<in.dimGrid, in.dimBlock>>>(f.apply, in.device_data,
//                                          in.device_size, in.device_size_pad, in.dof);
//    gpuErrchk( cudaPeekAtLastError() );

//    in.scaling_factor = 1;
//    profiler::end_stage();
//}

//template void eval(CUDADeviceTimeVec<2u> & in, const initialdata<Grid<2u>, 2u> & f);
//template void eval(CUDADeviceTimeVec<3u> & in, const initialdata<Grid<3u>, 3u> & f);

template <unsigned int dim, bool contiguous, bool interlaced>
void vector_laplace(CUDADeviceFreqVec<dim, contiguous, interlaced> & in,
                    CUDADeviceFreqVec<dim, contiguous, interlaced> & out,
                    int m, double eps) {
    profiler::start_stage("device::vector_laplace");
    assert( in.dof >= dim && out.dof >= dim && "Incompatible dofs!");

    vector_laplace_kern<CUDADeviceFreqVec<dim, contiguous, interlaced>>
            <<<in.dimGrid, in.dimBlock>>>(in.device_data, out.device_data,
                                          in.device_size, m, eps);
    gpuErrchk( cudaPeekAtLastError() );

    out.scaling_factor = in.scaling_factor;
    profiler::end_stage();
}

template void vector_laplace<2, false, false>(CUDADeviceFreqVec<2, false, false> & in,
                                              CUDADeviceFreqVec<2, false, false> & out,
                    int m, double eps);
template void vector_laplace<3, false, false>(CUDADeviceFreqVec<3, false, false> & in,
                                              CUDADeviceFreqVec<3, false, false> & out,
                    int m, double eps);


template <unsigned int dim, bool contiguous, bool interlaced>
void divergence(CUDADeviceFreqVec<dim, contiguous, interlaced> & in,
                CUDADeviceFreqVec<dim, contiguous, interlaced> & out) {
    profiler::start_stage("device::divergence");
    assert( in.dof >= dim && out.dof >= 1 && "Incompatible dofs!");

    divergence_kern<CUDADeviceFreqVec<dim, contiguous, interlaced>>
            <<<in.dimGrid, in.dimBlock>>>(in.device_data,
                                          out.device_data, in.device_size);
    gpuErrchk( cudaPeekAtLastError() );

    out.scaling_factor = in.scaling_factor;
    profiler::end_stage();
};

// Instantiate all templates
template
void divergence<2, false, false>(CUDADeviceFreqVec<2, false, false> & in,
                              CUDADeviceFreqVec<2, false, false> & out);
template
void divergence<3, false, false>(CUDADeviceFreqVec<3, false, false> & in,
                              CUDADeviceFreqVec<3, false, false> & out);

template <unsigned int dim, bool contiguous, bool interlaced>
void sym_tensorize(CUDADeviceTimeVec<dim, contiguous, interlaced> &in, CUDADeviceTimeVec<dim, contiguous, interlaced> &out) {
    profiler::start_stage("device::sym_tensorize");
    assert( in.dof >= dim && out.dof >= sym_tensor_dim(dim) && "Incompatible dofs!");

    double fac = in.scaling_factor * in.scaling_factor;
    sym_tensorize_kern<CUDADeviceTimeVec<dim, contiguous, interlaced>>
            <<<in.dimGrid, in.dimBlock>>>(in.device_data, out.device_data,
                                          in.device_size, in.device_size_pad, fac);
    gpuErrchk( cudaPeekAtLastError() );

    out.scaling_factor = 1;
    profiler::end_stage();
}
template void sym_tensorize<2, false, false>(CUDADeviceTimeVec<2, false, false> &in, CUDADeviceTimeVec<2, false, false> &out);
template void sym_tensorize<3, false, false>(CUDADeviceTimeVec<3, false, false> &in, CUDADeviceTimeVec<3, false, false> &out);

template <unsigned int dim, bool contiguous, bool interlaced>
void sym_vector_divergence(CUDADeviceFreqVec<dim, contiguous, interlaced> & in, CUDADeviceFreqVec<dim, contiguous, interlaced> & out) {
    profiler::start_stage("device::sym_vector_divergence");
    assert( in.dof >= sym_tensor_dim(dim) && out.dof >= dim && "Incompatible dofs!");

    sym_vector_divergence_kern<CUDADeviceFreqVec<dim, contiguous, interlaced>>
            <<<in.dimGrid, in.dimBlock>>>(in.device_data, out.device_data,
                                          in.device_size);
    gpuErrchk( cudaPeekAtLastError() );

//    out.scaling_factor = 1; // FIXME
    profiler::end_stage();
}

template void sym_vector_divergence<2, false, false>(CUDADeviceFreqVec<2, false, false> & in, CUDADeviceFreqVec<2, false, false> & out);
template void sym_vector_divergence<3, false, false>(CUDADeviceFreqVec<3, false, false> & in, CUDADeviceFreqVec<3, false, false> & out);

template <unsigned int dim, bool contiguous, bool interlaced>
void alias(CUDADeviceFreqVec<dim, contiguous, interlaced> & in, int m) {
    profiler::start_stage("device::alias");
    assert( in.dof >= 1 && "Incompatible dofs!");
    if( m == -1 ) return;

    alias_kern<CUDADeviceFreqVec<dim, contiguous, interlaced>>
            <<<in.dimGrid, in.dimBlock>>>(in.device_data, in.device_size, in.dof, m);

    // FIX SCALING
    profiler::end_stage();
}

template void alias<2, false, false>(CUDADeviceFreqVec<2, false, false> & in, int m);
template void alias<3, false, false>(CUDADeviceFreqVec<3, false, false> & in, int m);

template <unsigned int dim, bool contiguous, bool interlaced>
void curl(CUDADeviceFreqVec<dim, contiguous, interlaced> & in, CUDADeviceFreqVec<dim, contiguous, interlaced> & out) {
    profiler::start_stage("device::curl");
    assert( in.dof >= dim && out.dof >= curl_dim(dim) && "Incompatible dofs!");

    curl_kern<CUDADeviceFreqVec<dim, contiguous, interlaced>>
            <<<in.dimGrid, in.dimBlock>>>(in.device_data, out.device_data, in.device_size);
    gpuErrchk( cudaPeekAtLastError() );

    out.scaling_factor = in.scaling_factor;
    profiler::end_stage();
}

template void curl<2, false, false>(CUDADeviceFreqVec<2, false, false> & in, CUDADeviceFreqVec<2, false, false> & out);
template void curl<3, false, false>(CUDADeviceFreqVec<3, false, false> & in, CUDADeviceFreqVec<3, false, false> & out);

template <unsigned int dim, bool contiguous, bool interlaced>
void anti_curl(CUDADeviceFreqVec<dim, contiguous, interlaced> & in, CUDADeviceFreqVec<dim, contiguous, interlaced> & out) {
    profiler::start_stage("device::anti_curl");
    assert( in.dof >= curl_dim(dim) && out.dof >= dim && "Incompatible dofs!");

    anti_curl_kern<CUDADeviceFreqVec<dim, contiguous, interlaced>>
            <<<in.dimGrid, in.dimBlock>>>(in.device_data, out.device_data, in.device_size);
    gpuErrchk( cudaPeekAtLastError() );

    // FIX SCALING
    out.scaling_factor = in.scaling_factor;
    profiler::end_stage();
}

template void anti_curl<2, false, false>(CUDADeviceFreqVec<2, false, false> & in, CUDADeviceFreqVec<2, false, false> & out);
template void anti_curl<3, false, false>(CUDADeviceFreqVec<3, false, false> & in, CUDADeviceFreqVec<3, false, false> & out);

template <typename T, unsigned int dim, bool contiguous, bool interlaced>
double norm(DeviceVec<T, dim, contiguous, interlaced> & in, double p) {
    profiler::start_stage("device::norm");

    double * glob;
    gpuErrchk(cudaMalloc(&glob, in.dimGrid.x*in.dimGrid.y*in.dimGrid.z*sizeof(double)));

    double fac = in.scaling_factor * in.scaling_factor;
    int shared_size = in.dimBlock.x*in.dimBlock.y*in.dimBlock.z*sizeof(double);
    if( p != INFINITY ) {
        norm_kern<DeviceVec<T, dim, contiguous, interlaced>>
                <<<in.dimGrid, in.dimBlock, shared_size>>>(in.device_data,
                                                           glob, in.device_size, in.device_size_pad,
                                                           in.dof, fac, p );
    } else {
        inf_norm_kern<DeviceVec<T, dim, contiguous, interlaced>>
                <<<in.dimGrid, in.dimBlock, shared_size>>>(in.device_data,
                                                           glob, in.device_size, in.device_size_pad,
                                                           in.dof, fac );
    }
    gpuErrchk( cudaPeekAtLastError() );

    double * glob2;
    gpuErrchk(cudaMallocHost(&glob2, in.dimGrid.x*in.dimGrid.y*in.dimGrid.z*sizeof(double)));
    gpuErrchk(cudaMemcpy(glob2, glob,
                         in.dimGrid.x*in.dimGrid.y*in.dimGrid.z*sizeof(double),
                         cudaMemcpyDeviceToHost));
    double norm = 0;
    if( p != INFINITY ) {
        for( unsigned int i = 0; i < in.dimGrid.x*in.dimGrid.y*in.dimGrid.z; ++i) {
            norm += glob2[i];
        }
    } else {
        for( unsigned int i = 0; i < in.dimGrid.x*in.dimGrid.y*in.dimGrid.z; ++i) {
            norm = std::max(norm, std::abs(glob2[i]));
        }
    }
    gpuErrchk( cudaFree(glob) );
    gpuErrchk( cudaFreeHost(glob2) );

    profiler::end_stage();
    return p == INFINITY ? norm : pow(norm * in.grid_p->dx * in.grid_p->dy * in.grid_p->dz, 1 / p);
}

template double norm<double, 2, false, false>(DeviceVec<double, 2, false, false> & in, double p);
template double norm<double, 3, false, false>(DeviceVec<double, 3, false, false> & in, double p);

template double norm<double2, 2, false, false>(DeviceVec<double2, 2, false, false> & in, double p);
template double norm<double2, 3, false, false>(DeviceVec<double2, 3, false, false> & in, double p);

template <typename T, unsigned int dim, bool contiguous, bool interlaced>
void set(DeviceVec<T, dim, contiguous, interlaced> & in, scalar_t alpha) {
    profiler::start_stage("device::set");
    // FIXME: check: same grid

    for(auto dt: in._data) {
        cudaMemset(dt, alpha, in.size*sizeof(typename DeviceVec<T, dim, contiguous, interlaced>::data_t));
    }

    in.scaling_factor = 1;
    profiler::end_stage();
}

template void set<double, 2, false, false>(DeviceVec<double, 2, false, false> & in, scalar_t alpha);
template void set<double, 3, false, false>(DeviceVec<double, 3, false, false> & in, scalar_t alpha);

template <typename T, unsigned int dim, bool contiguous, bool interlaced>
void setZero(DeviceVec<T, dim, contiguous, interlaced> & in) {
    set(in, 0);
}

template void setZero<double, 2, false, false>(DeviceVec<double, 2, false, false> & in);
template void setZero<double, 3, false, false>(DeviceVec<double, 3, false, false> & in);

template <typename T, unsigned int dim, bool contiguous, bool interlaced>
void scale(DeviceVec<T, dim, contiguous, interlaced> & in, scalar_t alpha) {
    profiler::start_stage("device::scale");
    // FIXME: check: same grid

    alpha /= in.scaling_factor;
    for(auto dt: in._data) {
        cublasDscal(sphinx::cuda::handle, in.size*in.data_size, &alpha,
                    reinterpret_cast<double*>(dt), 1.);
    }

    in.scaling_factor = 1;
    profiler::end_stage();
}

template void scale<double, 2, false, false>(DeviceVec<double, 2, false, false> & in, scalar_t alpha);
template void scale<double, 3, false, false>(DeviceVec<double, 3, false, false> & in, scalar_t alpha);

template void scale<double2, 2, false, false>(DeviceVec<double2, 2, false, false> & in, scalar_t alpha);
template void scale<double2, 3, false, false>(DeviceVec<double2, 3, false, false> & in, scalar_t alpha);

template <unsigned int dim, bool contiguous, bool interlaced>
void normalize(CUDADeviceTimeVec<dim, contiguous, interlaced> & in) {
    profiler::start_stage("device::normalize");
    // FIXME: check: same grid

    if(in.scaling_factor != 1) scale(in, 1.);
    profiler::end_stage();
}

template void normalize<2, false, false>(CUDADeviceTimeVec<2, false, false> & in);
template void normalize<3, false, false>(CUDADeviceTimeVec<3, false, false> & in);

// y += a*x;
template <typename T, unsigned int dim, bool contiguous, bool interlaced>
void axpy(DeviceVec<T, dim, contiguous, interlaced> & y,
          const DeviceVec<T, dim, contiguous, interlaced> & x, scalar_t alpha) {
    profiler::start_stage("device::axpy");
    // FIXME: check: same grid

    alpha *= y.scaling_factor / x.scaling_factor;
    for(unsigned int f = 0; f < std::min(x.dof,y.dof); ++f) {
        cublasDaxpy(sphinx::cuda::handle, x.size*x.data_size, &alpha,
                    reinterpret_cast<double*>(x._data[f]), 1, reinterpret_cast<double*>(y._data[f]), 1);
    }
    profiler::end_stage();
}

template void axpy<double, 2, false, false>(DeviceVec<double, 2, false, false> & y, const DeviceVec<double, 2, false, false> & x, scalar_t alpha);
template void axpy<double, 3, false, false>(DeviceVec<double, 3, false, false> & y, const DeviceVec<double, 3, false, false> & x, scalar_t alpha);
template void axpy<double2, 2, false, false>(DeviceVec<double2, 2, false, false> & y, const DeviceVec<double2, 2, false, false> & x, scalar_t alpha);
template void axpy<double2, 3, false, false>(DeviceVec<double2, 3, false, false> & y, const DeviceVec<double2, 3, false, false> & x, scalar_t alpha);

template <typename T, unsigned int dim, bool contiguous, bool interlaced>
void axpbypz(DeviceVec<T, dim, contiguous, interlaced> & z,
             const DeviceVec<T, dim, contiguous, interlaced> & y,
             const DeviceVec<T, dim, contiguous, interlaced> & x,
             scalar_t alpha, scalar_t beta) {
    profiler::start_stage("device::axpbypz");
    // FIXME: check: same grid

    alpha *= z.scaling_factor / x.scaling_factor;
    beta *= z.scaling_factor / y.scaling_factor;
    for(unsigned int f = 0; f < std::min(x.dof,std::min(y.dof,z.dof)); ++f) {
        cublasDaxpy(sphinx::cuda::handle, x.size*x.data_size, &alpha,
                    reinterpret_cast<double*>(x._data[f]), 1, reinterpret_cast<double*>(z._data[f]), 1);
        cublasDaxpy(sphinx::cuda::handle, y.size*y.data_size, &beta,
                    reinterpret_cast<double*>(y._data[f]), 1, reinterpret_cast<double*>(z._data[f]), 1);
    }
    profiler::end_stage();
}

template void axpbypz<double, 2, false, false>(DeviceVec<double, 2, false, false> & z,
const DeviceVec<double, 2, false, false> & y, const DeviceVec<double, 2, false, false> & x,
                      scalar_t alpha, scalar_t beta);
template void axpbypz<double, 3, false, false>(DeviceVec<double, 3, false, false> & z,
const DeviceVec<double, 3, false, false> & y, const DeviceVec<double, 3, false, false> & x,
                      scalar_t alpha, scalar_t beta);

//template <typename T, unsigned int dim, bool contiguous, bool interlaced>
//void xmytz(DeviceVec<T, d, contiguous, interlaced> & z, const DeviceVec<T, d, contiguous, interlaced> & y, const DeviceVec<T, d, contiguous, interlaced> & x) {
//    apply_kern_inline<xmytz_kern<d>>(
//         std::forward_as_tuple(z, y, x), "device::xmytz"
//    );
//}

template <typename T, unsigned int dim, bool contiguous, bool interlaced>
void copy(const DeviceVec<T, dim, contiguous, interlaced> &in,
          DeviceVec<T, dim, contiguous, interlaced> &other) {
    profiler::start_stage("device::copy");
    // FIXME: check: same grid

    for(unsigned int f = 0; f < std::min(in.dof,other.dof); ++f) {
        cublasDcopy(sphinx::cuda::handle, in.size*in.data_size,
                    reinterpret_cast<double*>(in._data[f]), 1, reinterpret_cast<double*>(other._data[f]), 1);
    }
    other.scaling_factor = in.scaling_factor;
    profiler::end_stage();
}

template void copy<double, 2, false, false>(const DeviceVec<double, 2, false, false> & in,
DeviceVec<double, 2, false, false> & other);
template void copy<double, 3, false, false>(const DeviceVec<double, 3, false, false> & in,
DeviceVec<double, 3, false, false> & other);

template void copy<double2, 2, false, false>(const DeviceVec<double2, 2, false, false> & in,
DeviceVec<double2, 2, false, false> & other);
template void copy<double2, 3, false, false>(const DeviceVec<double2, 3, false, false> & in,
DeviceVec<double2, 3, false, false> & other);

template <unsigned int dim, bool contiguous, bool interlaced>
void zero_padding(const CUDADeviceFreqVec<dim, contiguous, interlaced> & in, CUDADeviceFreqVec<dim, contiguous, interlaced> & out) {
    profiler::start_stage("device::zero_padding");
    assert( out.dof >= 1 && in.dof >= 1 && "Incompatible dofs!");

    double factor =(double) prod( out.grid_p->timeN ) / (double) prod( in.grid_p->timeN );

    // If input is smaller than output
    if(in.nk < out.nk) {
        set(out, 0);

        zero_padding_kern<CUDADeviceFreqVec<dim, contiguous, interlaced>>
                <<<in.dimGrid, in.dimBlock>>>(in.device_data, out.device_data,
                                              in.device_size, out.device_size, std::min(in.dof, out.dof), factor);
    } else {
        truncate_kern<CUDADeviceFreqVec<dim, contiguous, interlaced>>
                <<<in.dimGrid, in.dimBlock>>>(in.device_data, out.device_data,
                                              in.device_size, out.device_size, std::min(in.dof, out.dof), factor);
    }
    gpuErrchk( cudaPeekAtLastError() );

    out.scaling_factor = in.scaling_factor;
    profiler::end_stage();
}

template void zero_padding<2, false, false>(const CUDADeviceFreqVec<2, false, false> & in, CUDADeviceFreqVec<2, false, false> & out);
template void zero_padding<3, false, false>(const CUDADeviceFreqVec<3, false, false> & in, CUDADeviceFreqVec<3, false, false> & out);

template <unsigned int dim, bool contiguous, bool interlaced>
void truncate(const CUDADeviceFreqVec<dim, contiguous, interlaced> & in,
              CUDADeviceFreqVec<dim, contiguous, interlaced> & out) {
    profiler::start_stage("device::truncate");
    assert( out.dof >= 1 && in.dof >= 1 && "Incompatible dofs!");

    double factor = (double) prod( out.grid_p->timeN ) / (double) prod( in.grid_p->timeN );

    truncate_kern<CUDADeviceFreqVec<dim, contiguous, interlaced>>
                <<<in.dimGrid, in.dimBlock>>>(in.device_data, out.device_data,
                                              in.device_size, out.device_size, std::min(in.dof, out.dof), factor);
    gpuErrchk( cudaPeekAtLastError() );

    out.scaling_factor = in.scaling_factor;
    profiler::end_stage();
}

template void truncate<2, false, false>(const CUDADeviceFreqVec<2, false, false> & in, CUDADeviceFreqVec<2, false, false> & out);
template void truncate<3, false, false>(const CUDADeviceFreqVec<3, false, false> & in, CUDADeviceFreqVec<3, false, false> & out);

template <unsigned int dof, unsigned int d, class Func,
        bool contiguous, bool interlaced>
void eval_func(FFTWTimeVec<d, contiguous, interlaced> & in, const Func & f);
