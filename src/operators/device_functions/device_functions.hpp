/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "config.hpp"

#include "containers/device_vec.hpp"
#include "containers/host_vec.hpp"

#ifdef USE_CUDA
#  include "mpi-ext.h" /* Needed for CUDA-aware check */
#endif

namespace sphinx {
namespace operators {

enum class Direction { Forward, Backward };

template <class T, class U, unsigned int d, bool contiguous, bool interlaced>
DeviceVec<U, d, contiguous, interlaced>&
HostToDevice(const HostVec<T,d, contiguous, interlaced> &hostv, DeviceVec<U, d, contiguous, interlaced> &devv) {
        profiler::start_stage("htd");

        for(unsigned int f = 0; f < std::min(hostv.dof,devv.dof); ++f) {
            std::stringstream ss_copy;
            ss_copy << "Copying HTD " << f
                    << " size (" << hostv.size *
                       sizeof(typename DeviceVec<T,d, contiguous, interlaced>::data_t) << ")"
                    << " from " << hostv._data[f] << " to " << devv._data[f];
            debug::verbose(ss_copy.str(), hostv.grid_p->comm);
            gpuErrchk( cudaMemcpy(devv._data[f], hostv._data[f],
                hostv.size *
               sizeof(typename DeviceVec<T,d, contiguous, interlaced>::data_t),
                                  cudaMemcpyHostToDevice) );
        }
        devv.scaling_factor = hostv.scaling_factor;

        profiler::end_stage();

        return devv;
}

template <class T, class U, unsigned int d, bool contiguous, bool interlaced>
HostVec<U, d, contiguous, interlaced>& DeviceToHost(
        const DeviceVec<T,d, contiguous, interlaced> &devv,
        HostVec<U, d, contiguous, interlaced> &hostv) {
        profiler::start_stage("dth");

        for(unsigned int f = 0; f < std::min(hostv.dof,devv.dof); ++f) {
            std::stringstream ss_copy;
            ss_copy << "Copying DTH " << f
                    << " size ("
                    <<  hostv.size
            * sizeof(typename DeviceVec<T,d, contiguous, interlaced>::data_t)
                    << ")"
                    << " from " << devv._data[f]
                       << " to " << hostv._data[f];
            debug::verbose(ss_copy.str(), hostv.grid_p->comm);
            gpuErrchk( cudaMemcpy(hostv._data[f], devv._data[f],
                hostv.size * sizeof(typename DeviceVec<T,d, contiguous, interlaced>::data_t),
                                  cudaMemcpyDeviceToHost) );
        }
        hostv.scaling_factor = devv.scaling_factor;

        profiler::end_stage();

        return hostv;
}

void local_transpose(double2 * in, double2 * out,
                     unsigned int* device_size,
                     dim3& dimGrid, const dim3& dimBlock);
void grid_transpose(double2 * in, double2 * out,
                    unsigned int* device_size, unsigned int csize,
                    unsigned int* block_lengths, unsigned int* cumu_block_lengths,
                    dim3& dimGrid, const dim3& dimBlock);

template <class T, unsigned int d>
void transpose(DeviceVec<T, d, false, false> & V, Direction /* dir */) {
    profiler::start_stage("transpose");

    double2 *sbuffer, *rbuffer;
    std::ptrdiff_t buffersize = std::max(V.grid_p->local_n0
                                         * V.grid_p->freqN[1],
                                         V.grid_p->local_n0_trsp
                                         * V.grid_p->freqN[0]); // FX
    gpuErrchk(cudaMalloc((void**)&sbuffer, buffersize*sizeof(double2)));
    gpuErrchk(cudaMalloc((void**)&rbuffer, buffersize*sizeof(double2)));

    //sbuffer = (double2 *) malloc(buffersize*sizeof(double2));
    //rbuffer = (double2 *) malloc(buffersize*sizeof(double2));

    std::cout << "Buffer adress: " << sbuffer << " " << rbuffer << std::endl;

    unsigned int csize = V.grid_p->comm.size;
    unsigned int rank = V.grid_p->comm.rank;

    unsigned int* local_shape;
    gpuErrchk(cudaMalloc(&local_shape, 3*sizeof(unsigned int)));
    unsigned int* cumu_block_lengths, *block_lengths;
    gpuErrchk(cudaMalloc(&cumu_block_lengths, (csize+1)*sizeof(unsigned int)));
    gpuErrchk(cudaMalloc(&block_lengths, csize*sizeof(unsigned int)));

    for(unsigned int f = 0; f < V.dof; ++f) {
        std::vector<int> sendcounts(csize);
        std::vector<int> recvcounts(csize);
        std::vector<int> sdisplacements(csize);
        std::vector<int> rdisplacements(csize);
        std::vector<unsigned int> local_shape_hst(3);
        std::vector<unsigned int> cumu_block_lengths_hst(csize+1);
        std::vector<unsigned int> block_lengths_hst(csize);

        for(unsigned int i = 0; i < csize; ++i) {
            sendcounts[i] = V.grid_p->partitioning[4*rank+1] * V.grid_p->partitioning[4*i+3] * 2; // FX
            recvcounts[i] = V.grid_p->partitioning[4*i+1] * V.grid_p->partitioning[4*rank+3] * 2; // FX

            if(i > 0)
              cumu_block_lengths_hst[i] = cumu_block_lengths_hst[i-1]
                    + V.grid_p->partitioning[4*(i-1)+1]; // FX
            else
                cumu_block_lengths_hst[i] = 0;
            block_lengths_hst[i] = V.grid_p->partitioning[4*i+1]; // FX

            if(i > 0) sdisplacements[i] = sdisplacements[i-1] + sendcounts[i-1]; // FX
            else sdisplacements[i] = 0;
            if(i > 0) rdisplacements[i] = rdisplacements[i-1] + recvcounts[i-1]; //+ maxrecvcounts; // FX
            else rdisplacements[i] = 0;
        }
        cumu_block_lengths_hst[csize] = cumu_block_lengths_hst[csize-1]
              + V.grid_p->partitioning[4*(csize-1)+1]; // FX

        local_shape_hst[0] = V.grid_p->local_n0;
        local_shape_hst[1] = V.grid_p->freqN[1]; // first dimension
        cudaMemcpy(local_shape, local_shape_hst.data(),
                   3*sizeof(unsigned int), cudaMemcpyHostToDevice);

        local_transpose((double2*) V._data[f], (double2*) sbuffer, local_shape,
                        V.dimGrid, V.dimBlock);
//        gpuErrchk(cudaMemcpy(V._data[f], sbuffer, buffersize*sizeof(double2),
//                                     cudaMemcpyDeviceToDevice ));

        MPI_Alltoallv(sbuffer, sendcounts.data(), sdisplacements.data(),
                     MPI_DOUBLE,
                     rbuffer, recvcounts.data(), rdisplacements.data(),
                     MPI_DOUBLE, V.grid_p->comm.comm);
        MPI_Barrier(V.grid_p->comm.comm);
//        gpuErrchk(cudaMemcpy(V._data[f], rbuffer, buffersize*sizeof(double2),
//                                     cudaMemcpyDeviceToDevice ));

//        local_shape[0] = V.grid_p->local_n0_trsp;
//        local_shape[1] = V.grid_p->freqN[0];
        local_shape_hst[0] = V.grid_p->local_n0_trsp;
        local_shape_hst[1] = V.grid_p->timeN[0];
//        std::cout << local_shape_hst[0] << local_shape_hst[1];
        cudaMemcpy(local_shape, local_shape_hst.data(),
                   3*sizeof(unsigned int), cudaMemcpyHostToDevice);
        cudaMemcpy(cumu_block_lengths, cumu_block_lengths_hst.data(),
                   csize*sizeof(unsigned int), cudaMemcpyHostToDevice);
        cudaMemcpy(block_lengths, block_lengths_hst.data(),
                   csize*sizeof(unsigned int), cudaMemcpyHostToDevice);

//        if(rank == 1 )continue;
        grid_transpose((double2*) rbuffer, (double2*) V._data[f],
                       local_shape, csize, block_lengths, cumu_block_lengths,
                       V.dimGrid, V.dimBlock);
    }

    gpuErrchk(cudaFree(sbuffer));
    gpuErrchk(cudaFree(rbuffer));
    gpuErrchk(cudaFree(local_shape));

    profiler::end_stage();
}

template <class T, unsigned int d>
void transpose2(DeviceVec<T, d, false, false> & V, Direction /* dir */) {
    profiler::start_stage("transpose");

    double2 *sbuffer, *rbuffer;
    std::ptrdiff_t buffersize = std::max(V.grid_p->local_n0 * V.grid_p->freqN[1],
                                         V.grid_p->local_n0_trsp * V.grid_p->freqN[0]); // FX
    gpuErrchk(cudaMalloc((void**)&sbuffer, buffersize*sizeof(double2)));
    gpuErrchk(cudaMalloc((void**)&rbuffer, buffersize*sizeof(double2)));

    //sbuffer = (double2 *) malloc(buffersize*sizeof(double2));
    //rbuffer = (double2 *) malloc(buffersize*sizeof(double2));

//    std::cout << "Buffer adress: " << sbuffer << " " << rbuffer << std::endl;

    unsigned int csize = V.grid_p->comm.size;
    unsigned int rank = V.grid_p->comm.rank;

    unsigned int* local_shape;
    gpuErrchk(cudaMalloc(&local_shape, 3*sizeof(unsigned int)));
    unsigned int* cumu_block_lengths, *block_lengths;
    gpuErrchk(cudaMalloc(&cumu_block_lengths, (csize+1)*sizeof(unsigned int)));
    gpuErrchk(cudaMalloc(&block_lengths, csize*sizeof(unsigned int)));

    for(unsigned int f = 0; f < V.dof; ++f) {
        std::vector<int> sendcounts(csize);
        std::vector<int> recvcounts(csize);
        std::vector<int> sdisplacements(csize);
        std::vector<int> rdisplacements(csize);
        std::vector<unsigned int> local_shape_hst(3);
        std::vector<unsigned int> cumu_block_lengths_hst(csize+1);
        std::vector<unsigned int> block_lengths_hst(csize);

        for(unsigned int i = 0; i < csize; ++i) {
            sendcounts[i] = V.grid_p->partitioning[4*rank+3] * V.grid_p->partitioning[4*i+1] * 2; // FX
            recvcounts[i] = V.grid_p->partitioning[4*i+3] * V.grid_p->partitioning[4*rank+1] * 2; // FX

            if(i > 0)
              cumu_block_lengths_hst[i] = cumu_block_lengths_hst[i-1]
                    + V.grid_p->partitioning[4*(i-1)+3]; // FX
            else
                cumu_block_lengths_hst[i] = 0;
            block_lengths_hst[i] = V.grid_p->partitioning[4*i+3]; // FX

            if(i > 0) sdisplacements[i] = sdisplacements[i-1] + sendcounts[i-1]; // FX
            else sdisplacements[i] = 0;
            if(i > 0) rdisplacements[i] = rdisplacements[i-1] + recvcounts[i-1]; //+ maxrecvcounts; // FX
            else rdisplacements[i] = 0;
        }
        cumu_block_lengths_hst[csize] = cumu_block_lengths_hst[csize-1]
              + V.grid_p->partitioning[4*(csize-1)+3]; // FX

        local_shape_hst[0] = V.grid_p->local_n0_trsp;
        local_shape_hst[1] = V.grid_p->timeN[0]; // first dimension
        cudaMemcpy(local_shape, local_shape_hst.data(),
                   3*sizeof(unsigned int), cudaMemcpyHostToDevice);

        local_transpose((double2*) V._data[f], (double2*) sbuffer, local_shape,
                        V.dimGrid, V.dimBlock);
//        gpuErrchk(cudaMemcpy(V._data[f], sbuffer, buffersize*sizeof(double2),
//                                     cudaMemcpyDeviceToDevice ));

        MPI_Alltoallv(sbuffer, sendcounts.data(), sdisplacements.data(),
                     MPI_DOUBLE,
                     rbuffer, recvcounts.data(), rdisplacements.data(),
                     MPI_DOUBLE, V.grid_p->comm.comm);
        MPI_Barrier(V.grid_p->comm.comm);
        gpuErrchk(cudaMemcpy(V._data[f], rbuffer, buffersize*sizeof(double2),
                                     cudaMemcpyDeviceToDevice ));

//        local_shape[0] = V.grid_p->local_n0_trsp;
//        local_shape[1] = V.grid_p->freqN[0];
        local_shape_hst[0] = V.grid_p->local_n0;
        local_shape_hst[1] = V.grid_p->freqN[1];
//        std::cout << local_shape_hst[0] << local_shape_hst[1];
        cudaMemcpy(local_shape, local_shape_hst.data(),
                   3*sizeof(unsigned int), cudaMemcpyHostToDevice);
        cudaMemcpy(cumu_block_lengths, cumu_block_lengths_hst.data(),
                   csize*sizeof(unsigned int), cudaMemcpyHostToDevice);
        cudaMemcpy(block_lengths, block_lengths_hst.data(),
                   csize*sizeof(unsigned int), cudaMemcpyHostToDevice);

//        if(rank == 1 )continue;
        grid_transpose((double2*) rbuffer, (double2*) V._data[f],
                       local_shape, csize, block_lengths, cumu_block_lengths,
                       V.dimGrid, V.dimBlock);
    }

    gpuErrchk(cudaFree(sbuffer));
    gpuErrchk(cudaFree(rbuffer));
    gpuErrchk(cudaFree(local_shape));

    profiler::end_stage();
}

} // END namespace operators
} // END namespace sphinx
