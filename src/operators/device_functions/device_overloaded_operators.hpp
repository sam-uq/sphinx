/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "device_operators.hpp"

#include "operators/host_functions/host_operators.hpp"

//! \brief Variant of eval kerel that intakes a simple function instead of
//! a complex class.
//!
//!
//!
//! \tparam dof
//! \tparam d
//! \tparam Func
//! \tparam contiguous
//! \tparam interlaced
//! \param in
//! \param f
template <unsigned int dof, unsigned int d, class Func,
        bool contiguous, bool interlaced>
void eval_func(CUDADeviceTimeVec<d, contiguous, interlaced> & in, const Func & f) {
    FFTWTimeVec<d, contiguous, interlaced> in_host(in);

    eval_func<dof>(in_host, f);

    sphinx::operators::HostToDevice(in_host, in);
}

template <class T, unsigned int d, bool contiguous, bool interlaced>
DeviceVec<T, d, contiguous, interlaced>&
DeviceVec<T, d, contiguous, interlaced>::operator+=(const DeviceVec<T, d, contiguous, interlaced> & right) {
    axpy(*this, right, 1);
    return *this;
}

template <class T, unsigned int d, bool contiguous, bool interlaced>
DeviceVec<T, d, contiguous, interlaced>&
DeviceVec<T, d, contiguous, interlaced>::operator-=(const DeviceVec<T, d, contiguous, interlaced> & right) {
    axpy(*this, right, -1);
    return *this;
}

// TODO: operator*
