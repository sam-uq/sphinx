/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "config.hpp"

#include "containers/cuda_device_time_vec.hpp"
#include "containers/cuda_device_freq_vec.hpp"

//! \brief Caller for CUDA kernel performing Leray projection of in onto out
//! Operates on frequency/device, result: out is up to date
//! \tparam dim dimensionality of data
//! \tparam dof1 degrees of freedom of in
//! \tparam dof2 degrees of freedom of out
//! \param[in] in Input vector, dof >= 2
//! \param[out] out Output vector, dof >= 2 out = P(in)
template <unsigned int dim, bool contiguous, bool interlaced>
void projection(CUDADeviceFreqVec<dim, contiguous, interlaced> & in,
                CUDADeviceFreqVec<dim, contiguous, interlaced> & out);

////! \brief Caller for CUDA kernel performing Leray projection of in onto out
////! Operates on frequency/device, result: out is up to date
////! \tparam dim dimensionality of data
////! \tparam dof1 degrees of freedom of in
////! \tparam dof2 degrees of freedom of out
////! \param[in] in Input vector, dof >= 2
////! \param[out] out Output vector, dof >= 2 out = P(in)
//template <unsigned int dim, bool contiguous, bool interlaced>
//void eval(CUDADeviceTimeVec<dim, contiguous, interlaced> & in, const initialdata<Grid<dim, contiguous, interlaced>, dim> & f);

//! \brief Caller for CUDA kernel performing vector laplace of in onto out
//! Operates on frequency/device, result: out is up to date
//! Additionally performs truncation of lower modes and scaling by epsilon
//! \tparam dim dimensionality of data
//! \tparam dof1 degrees of freedom of in
//! \tparam dof2 degrees of freedom of out
//! \param[in] in Input vector, dof >= 2
//! \param[out] out Output vector, dof >= 2 out = \eps \div P_m \grad(in)
//! \param[in] m Fourier modes to truncate, freq set to zero to lower modes (for each dim)
//! \param[in] eps Scaling factor for laplace operator ("artificial viscosity")
template <unsigned int dim, bool contiguous, bool interlaced>
void vector_laplace(CUDADeviceFreqVec<dim, contiguous, interlaced> & in,
                    CUDADeviceFreqVec<dim, contiguous, interlaced> & out,
                    int m, double eps);

//! Calls CUDA Kernel computing the divergence of a vector.
//! \tparam dim
//! \tparam contiguous
//! \tparam interlaced
//! \param in
//! \param out
template <unsigned int dim, bool contiguous, bool interlaced>
void divergence(CUDADeviceFreqVec<dim, contiguous, interlaced> & in,
                CUDADeviceFreqVec<dim, contiguous, interlaced> & out);

//! \brief Caller for CUDA kernel performing symmetric tensor of in onto out
//! Operates on time/device, result: out is up to date
//! \tparam dim dimensionality of data
//! \tparam dof degrees of freedom of in
//! \param[in] in Input vector, dof >= 2
//! \param[out] out Output vector, dof*(dof-1)/2, out = in \otimes in
template <unsigned int dim, bool contiguous, bool interlaced>
void sym_tensorize(CUDADeviceTimeVec<dim, contiguous, interlaced> & in, CUDADeviceTimeVec<dim, contiguous, interlaced> & out);

//! \brief Caller for CUDA kernel performing divergence of sym vector of in onto out
//! Operates on frequency/device, result: out is up to date
//! \tparam dim dimensionality of data
//! \tparam dof1 degrees of freedom of in
//! \tparam dof2 degrees of freedom of out
//! \param[in] in Input vector, dof >= 3
//! \param[out] out Output vector, dof >= 2 out = \div U (U assumed symmetric tensor)
template <unsigned int dim, bool contiguous, bool interlaced>
void sym_vector_divergence(CUDADeviceFreqVec<dim, contiguous, interlaced> & in, CUDADeviceFreqVec<dim, contiguous, interlaced> & out);

//! \brief Caller for CUDA kernel performing inplace aliasing of in
//! Operates on frequency/device, result: in is up to date (used in 3/2 rule)
//! \tparam dim dimensionality of data
//! \tparam dof1 degrees of freedom of in
//! \param[in] in Input vector, dof >= 1
//! \param[in] m modes to truncate (truncate if any mode is > m)
template <unsigned int dim, bool contiguous, bool interlaced>
void alias(CUDADeviceFreqVec<dim, contiguous, interlaced> & in, int m);

//! \brief Caller for CUDA kernel performing 2d curl of in onto out
//! Operates on frequency/device, result: out is up to date
//! \tparam dim dimensionality of data
//! \tparam dof1 degrees of freedom of in
//! \tparam dof2 degrees of freedom of out
//! \param[in] in Input vector, dof >= 2
//! \param[out] out Output vector, dof >= 1, out = \curl in
template <unsigned int dim, bool contiguous, bool interlaced>
void curl(CUDADeviceFreqVec<dim, contiguous, interlaced> & in, CUDADeviceFreqVec<dim, contiguous, interlaced> & out);

//! \brief Caller for CUDA kernel performing inverse curl of in onto out
//! Operates on frequency/device, result: out is up to date
//! \tparam dim dimensionality of data
//! \tparam dof1 degrees of freedom of in
//! \tparam dof2 degrees of freedom of out
//! \param[in] in Input vector, dof >= 1
//! \param[out] out Output vector, dof >= 2 \curl out = in
template <unsigned int dim, bool contiguous, bool interlaced>
void anti_curl(CUDADeviceFreqVec<dim, contiguous, interlaced> & in,
               CUDADeviceFreqVec<dim, contiguous, interlaced> & out);

//! \brief Caller for CUDA kernel performing computation of p-norm of in
//! Operates on frequency/device
//! \tparam dim dimensionality of data
//! \tparam dof degrees of freedom of in
//! \param[in] in Input vector, dof >= 2
//! \param[in] p factor of p-norm (INFINITY for L^\infty-norm)
template <typename T, unsigned int dim, bool contiguous, bool interlaced>
double norm(DeviceVec<T, dim, contiguous, interlaced> & in, double p = 2.);

template <typename T, unsigned int dim, bool contiguous, bool interlaced>
void set(DeviceVec<T, dim, contiguous, interlaced> & in, scalar_t alpha);

template <typename T, unsigned int dim, bool contiguous, bool interlaced>
void setZero(DeviceVec<T, dim, contiguous, interlaced> & in);

template <typename T, unsigned int dim, bool contiguous, bool interlaced>
void scale(DeviceVec<T, dim, contiguous, interlaced> & in, scalar_t alpha);

template <unsigned int dim, bool contiguous, bool interlaced>
void normalize(CUDADeviceTimeVec<dim, contiguous, interlaced> & in);

template <typename T, unsigned int dim, bool contiguous, bool interlaced>
void axpy(DeviceVec<T, dim, contiguous, interlaced> & y,
          const DeviceVec<T, dim, contiguous, interlaced> & x, scalar_t alpha);

template <typename T, unsigned int dim, bool contiguous, bool interlaced>
void axpbypz(DeviceVec<T, dim, contiguous, interlaced> & z, const DeviceVec<T, dim, contiguous, interlaced> & y, const DeviceVec<T, dim, contiguous, interlaced> & x, scalar_t alpha, scalar_t beta);

template <typename T, unsigned int dim, bool contiguous, bool interlaced>
void copy(const DeviceVec<T,dim, contiguous, interlaced> & in, DeviceVec<T,dim, contiguous, interlaced> & other);

template <unsigned int dim, bool contiguous, bool interlaced>
void zero_padding(const CUDADeviceFreqVec<dim, contiguous, interlaced> & in, CUDADeviceFreqVec<dim, contiguous, interlaced> & out);

template <unsigned int dim, bool contiguous, bool interlaced>
void truncate(const CUDADeviceFreqVec<dim, contiguous, interlaced> & in, CUDADeviceFreqVec<dim, contiguous, interlaced> & out);
