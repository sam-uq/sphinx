/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include "device_functions.hpp"

namespace sphinx {
namespace operators {
namespace kernels {

//! Transpose performed on local data
//! \param[in,out] A memory space of vector (transformed along N1, not along N0, already complex)
//! \param[in,out] B buffer, used to read/write temp. data (not part of vector)
//! \param[in] local_size physical size of data in input space A, 0: splitted, 1: non-splitted (pad?)
//!
template <class T>
__global__ void local_transpose_kern(T* A, T* B,
                                     unsigned int* local_size_A) {
    // Get local index
    int k = blockIdx.x * blockDim.x + threadIdx.x; // BIG DIMENSION, bigger
    int j = blockIdx.y * blockDim.y + threadIdx.y; // SPLITTED DIMENSION, smaller

    // If outside of box (in A), return
    // local size: 0 = splitted dim, 1 = freq [1]
    if( j >= local_size_A[1] || k >= local_size_A[0] ) return;

    // Find out outer index in A (in time space)
    int IA = k * local_size_A[1] + j;
    // Find out outer index in B (in time space)
    int IB = j * local_size_A[0] + k;

    // Find actual modes (k_,j) are the true modes
    B[IB] = A[IA];
}

//! Transposed performed gridwise
//! \param[in,out] A buffer, used to read/write temp. data (not part of vector)
//! \param[in,out] B memory space of vector (transformed along N1, not along N0, already complex)
//! \param[in] local_size physical size of data in input space A, 0: splitted, 1: non-splitted (pad?)
//!
template <class T>
__global__ void grid_transpose_kern(T* A, T* B,
                                    unsigned int* local_size_A, unsigned int nblocks,
                                    unsigned int* block_length, unsigned int* cumu_block_length) {
    // Get local index
    int k = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;

    // If outside of box (in A), return
    if( j >= local_size_A[0] || k >= local_size_A[1] ) return;

    unsigned int n = 0; // BLOCK TO WHICH THIS BELONG
    for(; n < nblocks; ++n) { // REPLACE BINARY SEARCH
//        printf(">> %d %d %d %d\n", j, n, cumu_block_length[n], cumu_block_length[n+1]);
        if(cumu_block_length[n] <= k && k < cumu_block_length[n+1]) break;
    }
    unsigned int S = block_length[n];
    unsigned int C = cumu_block_length[n];
//    printf("%d %d %d %d\n", j, n, S, C);

    // Find out outer index in A (in time space)
    int IA = C * local_size_A[0] + j * S + (k - C);
    // Find out outer index in B (in time space)
    int IB = j * local_size_A[1] + k;

    if(j > 400 || k  > 400) printf("%d %d %d --> %d %d\n", k, j, n, IA, IB);
    if(C > 0) printf("%d %d --> %d GOTCHA %d %d\n", k, j, C, IA, IB);
    // Find actual modes (k_,j) are the true modes
    B[IB] = A[IA];
}

}  // END namespace kernel

void local_transpose(double2 * in, double2 * out, unsigned int* device_size,
                     dim3& dimGrid, const dim3& dimBlock) {

    dimGrid.x = dimGrid.x * 2;
    dimGrid.y = dimGrid.y * 2;
    std::cout << "Kenrnel1: " << dimGrid.x << " " << dimGrid.y << " " << dimGrid.z << " " << dimBlock.x << " " << dimBlock.y  << " " << dimBlock.z << std::endl;
    kernels::local_transpose_kern<double2>
            <<<dimGrid, dimBlock>>>(in, out, device_size);
    gpuErrchk( cudaPeekAtLastError() );

}

void grid_transpose(double2 * in, double2 * out, unsigned int* device_size, unsigned int nblocks,
                    unsigned int* block_length, unsigned int* cumu_block_length,
                    dim3& dimGrid, const dim3& dimBlock) {
dimGrid.x = dimGrid.x * 2;
dimGrid.y = dimGrid.y * 2;
    std::cout << "Kenrnel: " << dimGrid.x << " " << dimGrid.y << " " << dimGrid.z << " " << dimBlock.x << " " << dimBlock.y  << " " << dimBlock.z << std::endl;
    kernels::grid_transpose_kern<double2>
            <<<dimGrid, dimBlock>>>(in, out, device_size, nblocks, block_length, cumu_block_length);
    gpuErrchk( cudaPeekAtLastError() );

}

} // END namespace operators
} // END namespace sphinx
