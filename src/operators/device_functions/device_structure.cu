/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

//
// Created by filippo on 17/11/17.
//

#include "operators/host_functions/host_structure.hpp"
#include "operators/device_functions/device_structure.hpp"

#include "operators/device_kernels/structure_kern.hpp"

#include "path_utilities.hpp"

#include <thrust/device_vector.h>

namespace sphinx {
namespace operators {
namespace extra {

//! \brief Internal management for strucutre data types.
//!
//! This class caches the data into a global variable. This hides CUDA and thrust from the rest
//! of sphinx.
//!
//!
class path_cache {
public:
    path_cache() = default;

    path_cache(int R, bool pixely, int maxstep = std::numeric_limits<int>::max(), bool verbose = false)
            : R(R), pixely(pixely), maxstep(maxstep), verbose(verbose) {
        setted_up = true;
    }

    void make_sure_is_valid() {
        if (cache_valid) {
            return;
        }
        if (!setted_up) {
            std::cout << "Options for structure not setted up!\n";
        }
        // TEST of distance
        std::tie(pathx, pathy) = get_lattice_approx<std::vector<int>>(R, pixely, maxstep, verbose);

        pathx_d = std::unique_ptr<thrust::device_vector<int>>(new thrust::device_vector<int>(pathx.size()));
        pathy_d = std::unique_ptr<thrust::device_vector<int>>(new thrust::device_vector<int>(pathy.size()));

        thrust::copy(pathx.begin(), pathx.end(), pathx_d->begin());
        thrust::copy(pathy.begin(), pathy.end(), pathy_d->begin());

        cache_valid = true;
    }

    void reset() {
        pathx_d.reset();
        pathy_d.reset();
    }

    std::vector<int> pathx, pathy;

    //! Our cache is stored inside a pointer since we require manual memory management
    //! due to cache being a global variable due to the requirement of abstracting CUDA away in the code.
    std::unique_ptr<thrust::device_vector<int>> pathx_d, pathy_d;
private:
    bool cache_valid = false;
public:
    int R;
    bool pixely;
    int maxstep;
    bool verbose;
    bool setted_up = false;
} cache;

void set_strucutre_options(int R, bool pixely, int maxstep, bool verbose) {
    cache.setted_up = true;
    cache.R = R;
    cache.pixely = pixely;
    cache.maxstep = maxstep;
    cache.verbose = verbose;
}

void reset_structure() {
    cache.reset();
}

template <unsigned int dim, bool contiguous, bool interlaced>
double compute_structure(CUDADeviceTimeVec<dim, contiguous, interlaced> &in) {
    profiler::start_stage("device::compute_structure");

    assert(in.dof >= dim && "Incompatible Dofs.!");

    double p = 2;
    int h = 6;

    thrust::device_vector<double> output((unsigned long) prod(in.grid_p->timeN));

//        Structure type = Structure::Cube;
    Structure type = Structure::Path;

    // Number of points where h is integrated in
    int size;
    switch (type) {
        case Structure::Cube:
            computeStructureCube2D<CUDADeviceTimeVec<dim, contiguous, interlaced>>
                    << < in.dimGrid, in.dimBlock >> > (output.data().get(), in.device_data, h,
                    in.device_size, in.device_size_pad, p);
            size = 8 * h;
            break;
        case Structure::Path:

            cache.make_sure_is_valid();

            computeStructurePath<CUDADeviceTimeVec<dim, contiguous, interlaced>>
                    << < in.dimGrid, in.dimBlock >> > (output.data().get(), in.device_data,
                    cache.pathx_d->data().get(), cache.pathy_d->data().get(), cache.pathx_d->size(),
                    in.device_size, in.device_size_pad, p);

            size = (int) cache.pathx_d->size();


            // TODO

            break;
    }

    gpuErrchk(cudaPeekAtLastError() );

    // Rescale and stuff

    // Integrate in space
    double D = thrust::reduce(output.begin(), output.end());
    // Normalize integral
    D /= prod(in.grid_p->timeN);

    // Average all cells in int-dh
    D /= size;

    // Handle scaling factor difference
    D /= std::pow(in.scaling_factor, p);

    profiler::end_stage();

    return D;
}

template double compute_structure(CUDADeviceTimeVec<2, false, false> &in);

} // END namespace extra
} // END namespace operators
} // END namespace sphinx