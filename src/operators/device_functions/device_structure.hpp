/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

//
// Created by filippo on 17/11/17.
//

#include "containers/cuda_device_time_vec.hpp"

namespace sphinx {
namespace operators {
namespace extra {
//! \brief Calls the appropriate kernel for the computation of the structure functions.
//!
//! Obtions are set through the \fun set_structure_options handle. You should call \fun reset_strucutre
//! before finalizing sphinx.
//!
//! \tparam dim
//! \tparam contiguous
//! \tparam interlaced
//! \param in
//! \return
template <unsigned int dim, bool contiguous, bool interlaced>
double compute_structure(CUDADeviceTimeVec<dim, contiguous, interlaced> &in);

//! \brief Configure the structure kernel launcher from external linkage.
//! \param R
//! \param pixely
//! \param maxstep
//! \param verbose
void set_strucutre_options(int R, bool pixely,
                           int maxstep = std::numeric_limits<int>::max(), bool verbose = false);

//! \brief Clears memory alocated for the strucutre computation.
//!
//! Should be called before program terminates for proper cleanup of thrust vectors.
void reset_structure();

} // END namespace extra
} // END namespace operators
} // END namespace sphinx
