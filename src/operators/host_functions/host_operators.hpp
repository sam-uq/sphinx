/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

//! \dir host_kernels
//! \brief Directory containing all CPU kernels.
//!
//! All kernels are called by including "host_operators.hpp" (see \ref host_operators.hpp) and
//! should not be called not directly.
//!
//! \author Filippo Leonardi
//! \date 2017-01-01

//! \file host_operators.hpp
//! \brief Contains the vector operators that are applied on the CPU, and use the CPU kernels
//! of \ref /host_kernels/ .
//!
//! Operators are generally called on pre-allocated input vectors of type \ref HostVec. Outputs must
//! be, in general, be already preallocated and, in general, must have the same size and shape of the input
//! vectors.
//!
//! \author Filippo Leonardi
//! \date 2017-01-01

#pragma once

#include "config.hpp"

#if myUSE_MKL
#  include <mkl_cblas.h>
#else // NO USE_MKL

#  include <cblas.h>
#endif

#include "operators/host_kernels/all_kern.hpp"

#include "containers/fftw_time_vec.hpp"

using namespace sphinx::kernel;

//////////////////////////////////////////////////////////
///////////// KERNEL APPLICATION ROUTINES ////////////////
//////////////////////////////////////////////////////////

//! \brief Applies kernels that do not require access to the real mode.
//!
//! Used for operations that are independent from the mode \f$\vec{k}\f$, or
//! from the grid position in time space.
//! The function loops over all physical (contiguous) indices as specified
//! by the first argument args[0], which must be a grid-vector type).
//! The Kernel must be compatible with such application, in particular it must have
//! an "eval(int, Tuple)" function.
//! This function is usable for any dimension, since the loop is performed on the entire
//! data.
//!
//! \see \ref indexing
//! \tparam         Kern  The CPU kernel one is applying to args.
//! \tparam         Args  The tuple containing all arguments passed to the kernel.
//! \param[in,out]  args  The actual arguments (intputs and outputs) passed to the kernel.
//! \param[in]      name  A (unique) name for profiling purposes.
template <class Kern, class Args>
inline void apply_kern_inline(const Args & args, std::string name) {
    profiler::start_stage(name);
    Kern::pre(args);

    #pragma omp parallel for
    for(unsigned int I = 0; I < std::get<0>(args).size; ++I) {
            Kern::eval(I, args);
    }

    Kern::post(args);
    profiler::end_stage();
}

//! \brief Applies kernels that do require access to the real modes.
//!
//! Used for operations that are dependent from the mode \f$\vec{k}\f$, or
//! from the grid position in time space.
//! This function is automatically SFINAEd if called with 2-dimensional or 3-dimensional kernels.
//! The function loops over the logical indices instead of the physical ones (the loop ordering depends
//! on the specified sizes from the first argument args[0]).
//! The Kernel must be compatible with such application, in particular it must have
//! an "eval(int, modes, Tuple)" function.
//! The first element arg[0] must be a vector type that specifies the logical dimensions of the grid.
//!
//! If openMP is enabled the loops are collapsed (both).
//!
//! \see \ref indexing
//! \tparam         Kern  The CPU kernel one is applying to args.
//! \tparam         Args  The tuple containing all arguments passed to the kernel.
//! \param[in,out]  args  The actual arguments (inputs and outputs) passed to the kernel.
//! \param[in]      name  A (unique) name for profiling purposes.
template <class Kern, class Args,
          typename std::enable_if<Kern::dim == 2, bool>::type = false>
inline void apply_kern(const Args & args, std::string name) {
    constexpr unsigned int dim = 2;
    profiler::start_stage(name);
    Kern::pre(args);

    #pragma omp parallel for collapse(2)
    for(int k = 0; k < std::get<0>(args).kend; ++k) {
        for(int j = 0; j < std::get<0>(args).jend; ++j) {
            pos_t<dim> logical_modes = {k,j};
            index_t Ix = std::get<0>(args).outerIndex(logical_modes);
            pos_t<dim> real_modes = std::get<0>(args).getRealModes(logical_modes);
            Kern::eval(Ix, real_modes, args);
        }
    }

    Kern::post(args);
    profiler::end_stage();
}

// 3d version selected trough SFINAE
template <class Kern, class Args,
          typename std::enable_if<Kern::dim == 3, bool>::type = true>
inline void  apply_kern(const Args & args, std::string name) {
    constexpr unsigned int dim = 3;
    profiler::start_stage(name);
    Kern::pre(args);

    #pragma omp parallel for collapse(3)
    for(unsigned int k = 0; k < std::get<0>(args).kend; ++k) {
        for(unsigned int j = 0; j < std::get<0>(args).jend; ++j) {
            for(unsigned int i = 0; i < std::get<0>(args).iend; ++i) {
                pos_t<dim> logical_modes = {k,j,i};
                index_t Ix = std::get<0>(args).outerIndex(logical_modes);
                pos_t<dim> real_modes = std::get<0>(args).getRealModes(logical_modes);
                Kern::eval(Ix, real_modes, args);
            }
        }
    }

    Kern::post(args);
    profiler::end_stage();
}

//! \brief Applies kernels that do require access to the real modes as well as the locical indices.
//!
//! Used for operations that are dependent from the mode \f$\vec{k}\f$, or
//! from the grid position in time space, but also need to use the logical modes (this is used
//! e.g. for zero-padding \ref zero_padding and truncation \ref truncate, where the vectors have
//! different size and we need
//! to have a way to loop over two different index sets).
//!
//! This function is automatically SFINAEd if called with 2-dimensional  or 3-dimensional kernels.
//! The function loops over the logical indices instead of the physical ones (the loop ordering depends
//! on the specified sizes from the first argument args[0]).
//! The Kernel must be compatible with such application, in particular it must have
//! an "eval(int, logical_modes, modes, Tuple)" function.
//! The first element arg[0] must be a vector type that specifies the logical dimensions of the grid.
//!
//! If openMP is enabled the loops are collapsed (all of them).
//!
//! \see \ref indexing
//! \tparam         Kern  The CPU kernel one is applying to args.
//! \tparam         Args  The tuple containing all arguments passed to the kernel.
//! \param[in,out]  args  The actual arguments (inputs and outputs) passed to the kernel.
//! \param[in]      name  A (unique) name for profiling purposes.
template <class Kern, unsigned int kt = 0, unsigned int jt = 0, unsigned int it = 0, class Args,
          typename std::enable_if<Kern::dim == 2, bool>::type = false>
inline void apply_kern_unequal_shape(const Args & args, std::string name) {
    constexpr unsigned int dim = 2;
    profiler::start_stage(name);
    Kern::pre(args);

    #pragma omp parallel for collapse(2)
    for(int k = 0; k < std::get<kt>(args).kend; ++k) {
        for(int j = 0; j < std::get<jt>(args).jend; ++j) {
            pos_t<dim> logical_modes = {k,j};
            index_t Ix = std::get<0>(args).outerIndex(logical_modes);
            pos_t<dim> real_modes = std::get<0>(args).getRealModes(logical_modes);
            Kern::eval(Ix, logical_modes, real_modes, args);
        }
    }

    Kern::post(args);
    profiler::end_stage();
}

// 3d version selected trough SFINAE
template <class Kern, unsigned int kt = 0, unsigned int jt = 0, unsigned int it = 0, class Args,
          typename std::enable_if<Kern::dim == 3, bool>::type = true>
inline void apply_kern_unequal_shape(const Args & args, std::string name) {
    constexpr unsigned int dim = 3;
    profiler::start_stage(name);
    Kern::pre(args);

    #pragma omp parallel for collapse(3)
    for(unsigned int k = 0; k < std::get<kt>(args).kend; ++k) {
        for(unsigned int j = 0; j < std::get<jt>(args).jend; ++j) {
            for(unsigned int i = 0; i < std::get<it>(args).iend; ++i) {
                pos_t<dim> logical_modes = {k,j,i};
                index_t Ix = std::get<0>(args).outerIndex(logical_modes);
                pos_t<dim> real_modes = std::get<0>(args).getRealModes(logical_modes);
                Kern::eval(Ix, logical_modes, real_modes, args);
            }
        }
    }


    Kern::post(args);
    profiler::end_stage();
}

//! \brief Applies the selected kernel and perform an additive reduction.
//!
//! Useful for computation of \f$L^p\f$ norms.
//!
//! \tparam         Kern  The CPU kernel one is applying to args.
//! \tparam         Args  The tuple containing all arguments passed to the kernel.
//! \param[in,out]  args  The actual arguments (inputs and outputs) passed to the kernel.
//! \param[in]      name  A (unique) name for profiling purposes.
//! \param[in]      var   The variable onto which reduction is applied.
//! \return               Final value, after reduction, of "var".
template <class Kern, class Args, class T,
          typename std::enable_if<Kern::dim == 2, bool>::type = false>
inline T apply_kern_reduction_plus(const Args & args, T var, std::string name) {
    constexpr unsigned int dim = 2;
    profiler::start_stage(name);
    Kern::pre(args, var);

    using VecType = typename Kern::template VecType<decltype(std::get<0>(args))>;

    const VecType &vec = std::get<0>(args);

    #pragma omp parallel for collapse(2) reduction(+:var)
    for (int k = 0; k < vec.kend; ++k) {
        for (int j = 0; j < vec.jend; ++j) {
            pos_t<dim> logical_modes = {k,j};
            index_t Ix = vec.outerIndex(logical_modes);
            Kern::eval(Ix, logical_modes, args, var);
        }
    }

    T temp = Kern::post(args, var);
    profiler::end_stage();

    return temp;
}

// 3d version selected trough SFINAE
template <class Kern, class Args, class T,
          typename std::enable_if<Kern::dim == 3, bool>::type = true>
inline T apply_kern_reduction_plus(const Args & args, T var, std::string name) {
    constexpr unsigned int dim = 3;
    profiler::start_stage(name);
    Kern::pre(args, var);

    #pragma omp parallel for collapse(3) reduction(+:var)
    for(unsigned int k = 0; k < std::get<0>(args).kend; ++k) {
        for(unsigned int j = 0; j < std::get<0>(args).jend; ++j) {
            for(unsigned int i = 0; i < std::get<0>(args).iend; ++i) {
                pos_t<dim> logical_modes = {k,j,i};
                index_t Ix = std::get<0>(args).outerIndex(logical_modes);
                Kern::eval(Ix, logical_modes, args, var);
            }
        }
    }

    T temp = Kern::post(args, var);
    profiler::end_stage();
    return temp;
}

//! \brief Applies the selected kernel and perform an "max" reduction.
//!
//! Useful for computation of \f$L^\infty\f$ norms.
//!
//! \tparam         Kern  The CPU kernel one is applying to args.
//! \tparam         Args  The tuple containing all arguments passed to the kernel.
//! \param[in,out]  args  The actual arguments (inputs and outputs) passed to the kernel.
//! \param[in]      name  A (unique) name for profiling purposes.
//! \param[in]      var   The variable onto which reduction is applied.
//! \return               Final value, after reduction, of "var".
template <class Kern, class Args, class T,
          typename std::enable_if<Kern::dim == 2, bool>::type = false>
inline T apply_kern_reduction_max(const Args & args, T var, std::string name) {
    constexpr unsigned int dim = 2;
    profiler::start_stage(name);
    Kern::pre(args, var);

    #pragma omp parallel for collapse(2) reduction(max:var)
    for(int k = 0; k < std::get<0>(args).kend; ++k) {
        for(int j = 0; j < std::get<0>(args).jend; ++j) {
            pos_t<dim> logical_modes = {k,j};
            index_t Ix = std::get<0>(args).outerIndex(logical_modes);
            Kern::eval(Ix, logical_modes, args, var);
        }
    }

    T temp = Kern::post(args, var);
    profiler::end_stage();
    return temp;
}

// 3d version selected trough SFINAE
template <class Kern, class Args, class T,
          typename std::enable_if<Kern::dim == 3, bool>::type = true>
inline T apply_kern_reduction_max(const Args & args, T var, std::string name) {
    constexpr unsigned int dim = 3;
    profiler::start_stage(name);
    Kern::pre(args, var);

    #pragma omp parallel for collapse(3) reduction(max:var)
    for(unsigned int k = 0; k < std::get<0>(args).kend; ++k) {
        for(unsigned int j = 0; j < std::get<0>(args).jend; ++j) {
            for(unsigned int i = 0; i < std::get<0>(args).iend; ++i) {
                pos_t<dim> logical_modes = {k,j,i};
                index_t Ix = std::get<0>(args).outerIndex(logical_modes);
                Kern::eval(Ix, logical_modes, args, var);
            }
        }
    }

    T temp = Kern::post(args, var);
    profiler::end_stage();
    return temp;
}

//////////////////////////////////////////////////////////
////////////////////// OPERATORS /////////////////////////
//////////////////////////////////////////////////////////

//! \brief Implementation of Leray projection. CPU Version.
//!
//! The function projection computes the Leray projection of the vector
//! \p in and saves the output in the vector \p out.
//!
//! The projection is performed in Fourier space, according to the formula:
//! \f[ \mathbb{P}(\hat{\vec{u}}_{\vec{k}}) = \hat{\vec{u}}_{\vec{k}} - \frac{\hat{\vec{u}}_{\vec{k}} \cdot \vec{k}}{\lvert \vec{k} \rvert^2} \vec{k}. \f]
//! \tparam     FFTWFreqVec    Type for input and output vector: must be a frequency vector.
//! \param[in]  in             Input vector which will be projected onto \f$H(div=0)\f$.
//! \param[out] out            Output vector, which will be the projection \f$\mathbb{P}(\vec{u})\f$ in frequency space.
template <class FFTWFreqVec>
void projection(const FFTWFreqVec & in, FFTWFreqVec & out) {
    apply_kern<projection_kern<FFTWFreqVec::base_t::dim>>(
                std::forward_as_tuple(in, out), "projection"
    );
}

//! \brief Implementation of gradient. CPU Version.
//!
//! The function projection computes the gradient of the vector
//! \p in and saves the output in the vector \p out.
//!
//! The gradient is performed in Fourier space, according to the formula:
//! \f[ \hat{\nabla}(\hat{\Psi}_{\vec{k}}) = 2 \pi \iota \vec{k} \hat{\Psi}_{\vec{k}}. \f]
//! \tparam     FFTWFreqVec    Type for input and output vector: must be a frequency vector.
//! \param[in]  in             Input vector of which the gradient will be computed.
//! \param[out] out            Output vector, which will be the gradient \f$\nabla(\Psi)\f$ in frequency space.
template <class FFTWFreqVec>
void gradient(const FFTWFreqVec & in, FFTWFreqVec & out) {
    apply_kern<gradient_kern<FFTWFreqVec::base_t::dim>>(
                std::forward_as_tuple(in, out), "gradient"
    );
}

//! \brief Implementation of orthogonal gradient. CPU Version.
//!
//! The function projection computes the orthogonal gradient of the vector
//! \p in and saves the output in the vector \p out.
//!
//! Only available in 2D.
//!
//! The gradient is performed in Fourier space, according to the formula:
//! \f[ \hat{\nabla}(\hat{\Psi}_{\vec{k}})^\top = 2 \pi \iota \vec{k}^\top \hat{\Psi}_{\vec{k}}. \f]
//! \tparam     FFTWFreqVec    Type for input and output vector: must be a frequency vector.
//! \param[in]  in             Input vector of which the gradient will be computed.
//! \param[out] out            Output vector, which will be the gradient \f$\nabla(\Psi)^\top =: \nabla \wedge \Psi\f$ in frequency space.
template <class FFTWFreqVec>
void wedge(const FFTWFreqVec & in, FFTWFreqVec & out) {
    apply_kern<wedge_kern<FFTWFreqVec::base_t::dim>>(
                std::forward_as_tuple(in, out), "wedge"
    );
}

//! \brief Implementation of curl. CPU Version.
//!
//! The function projection computes the curl of the vector
//! \p in and saves the output in the vector \p out.
//!
//! In 2D the result is a scalar. Otherwise the result is a vector.
//!
//! The curl is performed in Fourier space, according to the formula:
//! \f[ \hat{\nabla} \times (\hat{\mathbf{u}}_{\mathbf{k}}) = 2 \pi \iota \mathbf{k} \times \hat{\mathbf{u}}_{\mathbf{k}}. \f]
//!
//! \see \ref curl_kern
//! \tparam     FFTWFreqVec    Type for input and output vector: must be a frequency vector.
//! \param[in]  in             Input vector of which the curl will be computed.
//! \param[out] out            Output vector, which will be the curl \f$\nabla \times \mathbf{u}\f$ in frequency space.
template <class FFTWFreqVec>
void curl(const FFTWFreqVec & in, FFTWFreqVec & out) {
    apply_kern<curl_kern<FFTWFreqVec::base_t::dim>>(
                std::forward_as_tuple(in, out), "curl"
    );
}

//! \brief Implementation of inverse of curl. CPU Version.
//!
//! The function compute the inverse of the curl of the vector
//! \p in and saves the output in the vector \p out.
//!
//! Only available in 2D. The result is a 2d vector.
//!
//! The curl is performed in Fourier space, according to the formula:
//! \f[ \hat{\nabla} \times^{-1} (\hat{\Psi}_{\mathbf{k}}) = \frac{\mathbf{k}^\top}{2 \pi \iota \lvert \mathbf{k} \rvert^2} \Psi_{\mathbf{k}}. \f]
//! In fact:
//! \f[ \hat{\nabla} \times \hat{\nabla} \times^{-1} (\hat{\Psi}_{\mathbf{k}}) = 2 \pi \iota \mathbf{k} \times \frac{\mathbf{k}^\top}{2 \pi \iota \lvert \mathbf{k} \rvert^2} \Psi_{\mathbf{k}} = \Psi_{\mathbf{k}}. \f]
//!
//! \warning The mapping is not 1-to-1: in fact the Fourier coefficient is not defined for \f$\mathbf{k} = 0\f$. Thus,
//! inverse of the curl is assumed to have zero-average.
//! \see \ref anti_curl_kern
//! \tparam     FFTWFreqVec    Type for input and output vector: must be a frequency vector.
//! \param[in]  in             Input vector \f$\Psi\f$ of which the inverse of the curl will be computed.
//! \param[out] out            Output vector \f$\mathbf{u}\f$, s.t. \f$\Psi \nabla \times \mathbf{u}\f$ in frequency space.
template <class FFTWFreqVec>
void anti_curl(const FFTWFreqVec & in, FFTWFreqVec & out) {
    apply_kern<anti_curl_kern<FFTWFreqVec::base_t::dim>>(
                std::forward_as_tuple(in, out), "anti_curl"
    );
}

//! \brief Implementation of the divergence. CPU Version.
//!
//! The function divergence computes the divergence of the vector
//! \p in and saves the output in the vector \p out.
//!
//! The result is always a scalar.
//!
//! The divergence is performed in Fourier space, according to the formula:
//! \f[ \hat{\nabla} \cdot (\hat{\mathbf{u}}_{\mathbf{k}}) = 2 \pi \iota \mathbf{k} \cot \hat{\mathbf{u}}_{\mathbf{k}}. \f]
//!
//! \see \ref divergence_kern
//! \tparam     FFTWFreqVec    Type for input and output vector: must be a frequency vector.
//! \param[in]  in             Input vector \f$\mathbf{u}\f$ of which the divergence will be computed.
//! \param[out] out            Output vector, the divergence of \in: \f$\nabla \cdot \mathbf{u}\f$ in frequency space.
template <class FFTWFreqVec>
void divergence(const FFTWFreqVec & in, FFTWFreqVec & out) {
    apply_kern<divergence_kern<FFTWFreqVec::base_t::dim>>(
                std::forward_as_tuple(in, out), "divergence"
    );
}

//! \brief Implementation of the (vector) divergence of a symmetric tensor. CPU Version.
//!
//! The function sym_vector_divergence computes the divergence of the vector
//! \p in and saves the output in the vector \p out.
//!
//! The input vector is assumed to be stored as symmetric tensor, meaning that it is a symmetric matrix with
//! \f$\binom{n}{2}\f$ components (dofs).
//!
//! The divergence is performed in Fourier space and componentswise, according to the formula:
//! \f[ \hat{\nabla} \cdot (\hat{\mathbf{u}}_{\mathbf{k}}) = 2 \pi \iota \mathbf{k} \cot \hat{\mathbf{u}}_{\mathbf{k}}. \f]
//!
//! In 2d, we store the components as
//! \f[ [\mathbf{u}_1, \mathbf{u}_2, \mathbf{u}_3] = \begin{pmatrix} \mathbf{u}_1 & \mathbf{u}_2 \\ \mathbf{u}_2 & \mathbf{u}_3 \end{pmatrix} \f]
//! In 3d, we store the components as
//! \f[ [\mathbf{u}_1, \mathbf{u}_2, \mathbf{u}_3, \mathbf{u}_4, \mathbf{u}_5, \mathbf{u}_6] = \begin{pmatrix} \mathbf{u}_1 & \mathbf{u}_2  & \mathbf{u}_4 \\ \mathbf{u}_2 & \mathbf{u}_3  & \mathbf{u}_5 \\ \mathbf{u}_4 & \mathbf{u}_5  & \mathbf{u}_6 \end{pmatrix} \f]
//! This way, we save some space and we spare some FFT.
//! \see \ref sym_vector_divergence_kern
//! \tparam     FFTWFreqVec    Type for input and output vector: must be a frequency vector.
//! \param[in]  in             Input vector \f$\mathbf{u}\f$ of which the vector divergence will be computed. Vector has \f$\binom{n}{2}\f$ dofs.
//! \param[out] out            Output vector, the divergence of \in: \f$\nabla \cdot \mathbf{u}\f$ in frequency space, component wise.
template <class FFTWFreqVec>
void sym_vector_divergence(const FFTWFreqVec & in, FFTWFreqVec & out) {
    apply_kern<sym_vector_divergence_kern<FFTWFreqVec::base_t::dim>>(
                std::forward_as_tuple(in, out), "sym_vector_divergence"
    );
}

//! \brief Implementation of the (vector) Laplace operator. CPU Version.
//!
//! The function vector_laplace computes the Laplace applied to the vector
//! \p in and saves the output in the vector \p out.
//!
//! The result is always a vector.
//!
//! The Laplace operator is computed in Fourier space and componentwise, according to the formula:
//! \f[ \hat{\Delta} (\hat{\mathbf{u}}_{\mathbf{k}}) = - 4 \pi^2 \lvert \mathbf{k} \rvert^2 \hat{\mathbf{u}}_{\mathbf{k}}. \f]
//!
//! \see \ref vector_laplace_kern
//! \tparam     FFTWFreqVec    Type for input and output vector: must be a frequency vector.
//! \param[in]  in             Input vector \f$\mathbf{u}\f$ of which the Laplace will be computed.
//! \param[out] out            Output vector, the divergence of \in: \f$\Delta \mathbf{u}\f$ in frequency space.
template <class FFTWFreqVec>
void vector_laplace(const FFTWFreqVec & in, FFTWFreqVec & out, int m, double eps) {
    apply_kern<vector_laplace_kern<FFTWFreqVec::base_t::dim>>(
                std::forward_as_tuple(in, out, m, eps), "vector_laplace"
    );
}

//! \brief Implementation of the inverse of (vector) Laplace operation. CPU Version.
//!
//! The function ecalpal (Laplace read backwards) computes the solution of the
//! Poisson equation \f$\Delta \psi = \phi\f$, where the r.h.s. is \f$\phi\f$ is the scalar
//! \p in and  solution \f$\psi\f$ is saved in \p out.
//!
//! Vor vectors this is done component-wise.
//! The result has always the same dofs as the input vector.
//!
//! The inverse of the Laplace operator is computed in Fourier space and componentwise, according to the formula:
//! \f[ \hat{\Delta}^{-1} (\hat{\mathbf{u}}_{\mathbf{k}}) = - \frac{1}{4 \pi^2 \lvert \mathbf{k} \rvert^2} \hat{\mathbf{u}}_{\mathbf{k}}. \f]
//!
//! \warning The Fourier coefficient for \f$\mathbf{v} = 0\f$ is not defined, as the Laplace operator is not 1-to-1.
//! Thus we make the additional assumption that the solution has zero average.
//!
//! \see \ref ecalpal_kern
//! \tparam     FFTWFreqVec    Type for input and output vector: must be a frequency vector.
//! \param[in]  in             Input vector \f$\mathbf{u}\f$ of which the inverse Laplace will be computed.
//! \param[out] out            Output vector, the solution \f$\mathbf{v}\f$ of \in: \f$\Delta \mathbf{v} =  \mathbf{u}\f$ in frequency space.
template <class FFTWFreqVec>
void ecalpal(const FFTWFreqVec & in, FFTWFreqVec & out) {
    apply_kern<ecalpal_kern<FFTWFreqVec::base_t::dim>>(
            std::forward_as_tuple(in, out), "vector_ecalpla"
    );
}

//! \brief Set value of the vector to be the specified constant. CPU Version.
//!
//! The input vector \p in is set to be constant, with values equal to the constant \p value.
//! \tparam         Vec     The vector type for \p in.
//! \tparam         T       The scalar type for Vec.
//! \param[in,out]  in      The input vector: will be overwrtittn.
//! \param          value   A value of scalar type compatible with \p in.
template <class Vec, class T>
void set(Vec & in, const T & value) {
    apply_kern<set_kern<Vec::base_t::dim>>(
                std::forward_as_tuple(in, value), "host::set"
    );
}

//! \brief Apply anti-aliasing kernel to data.
//!
//! Removes the high frequency portion of the spectrum for values of \f$\mathbf{k}\f$ with
//! \f$\lvert \mathbf{k} \rvert_\infty > a\f$ where \f$a\f$ is \p value.
//! Applies \ref alias_kern to the data.
//!
//! \see \ref alias_kern \ref indexing
//! \tparam            T           Scalar type fo threshold r \p value.
//! \tparam            d           Space dimensions.
//! \tparam            contiguous  true if Dofs are stored in a contiguous chunk of memory. (Only if deinterlaced.)
//! \tparam            interlaced  true if Dofs are stored next to each other.
//! \param[in,out]     in          Vector (in frequency space) that will be de-aliased.
//! \param[in]         value       Threshold \f$a\f$ for the removal of high frequencies.
template <class T, unsigned int d, bool contiguous, bool interlaced>
void alias(FFTWFreqVec<d, contiguous, interlaced> & in, const T & value) {
    apply_kern<alias_kern<d>>(
                std::forward_as_tuple(in, value), "host::alias"
    );
}

//! \brief Extends the input vector to be of the size of the output vector.
//!
//! The spectrum of \p in is extended to be of the same size as the vector \p out (\p in is untouched).
//! Data from \p in is copied/transferred to \p out. The missing data is set to zero (zero-padding).
//!
//! Notice that the coefficients of \p in and \p out approximate the same integral, however the quadrature
//! is performed with more points (for \p put), that is, it is not *exactly* equivalent.
//!
//! This operation is the opposite of \ref zero_padding.
//!
//! \see \ref zero_padding_kern
//!
//! \tparam            d           Space dimensions.
//! \tparam            contiguous  true if Dofs are stored in a contiguous chunk of memory. (Only if deinterlaced.)
//! \tparam            interlaced  true if Dofs are stored next to each other.
//! \param[in]         in          Input vector, will be left unmodified.
//! \param[in,out]     out         Padded vector with new size. Used as input only for size. Will approximate same vector as \p in.
//! \param             zeroset     Set to false if you do not want to override the missing data of \p out with zeros.
template <unsigned int d, bool contiguous, bool interlaced>
void zero_padding(const FFTWFreqVec<d, contiguous, interlaced> & in,
                  FFTWFreqVec<d, contiguous, interlaced> & out,
                  bool zeroset = true) {
    apply_kern_unequal_shape<zero_padding_kern<d>,1>(
                std::forward_as_tuple(in, out, zeroset), "host::zero_padding"
    );
}

//! \brief Shortens the input vector data to be of the size of the output vector.
//!
//! The spectrum of \p in is truncated to be of the same size as the vector \p out (\p in is untouched).
//! Data from \p in is copied/transferred to \p out. The excess data is forgotten.
//!
//! Notice that the coefficients of \p in and \p out approximate the same integral, however the quadrature
//! is performed with more points (for \p put), that is, it is not *exactly* equivalent.
//!
//! This operation is the opposite of \ref zero_padding.
//!
//! \see \ref truncate_kern
//!
//! \tparam            d           Space dimensions.
//! \tparam            contiguous  true if Dofs are stored in a contiguous chunk of memory. (Only if deinterlaced.)
//! \tparam            interlaced  true if Dofs are stored next to each other.
//! \param[in]         in          Input vector, will be left unmodified.
//! \param[in,out]     out         Truncated vector with new size. Used as input only for size. Will approximate same vector as \p in.
template <unsigned int d, bool contiguous, bool interlaced>
void truncate(const FFTWFreqVec<d, contiguous, interlaced> & in,
              FFTWFreqVec<d, contiguous, interlaced> & out) {
    apply_kern_unequal_shape<truncate_kern<d>>(
                std::forward_as_tuple(out, in), "host::truncate"
    );
}

//! \brief Evaluates a function and sets the values into the input vector. CPU version.
//!
//! The vector is intended to be in time-space and will be filled using a function \f$f\f$
//! of functional type \t Func that has the following members:
//! - bool indexed(): return true if the function evaluates at indices and not at cordinates
//! - void evalat(coord_t<d> coord, data_t<dof> out): evaluates the function at coord, output is in out
//!
//! The virtual class \ref sphinx::Data::initialdata is the prototype for the functional type \t Func.
//!
//! \see \ref eval_kern, \ref indexing, \ref sphinx::Data::initialdata
//!
//! \tparam            dof         Number of degrees of freedom of the vector and of the function.
//! \tparam            d           Space dimensions.
//! \tparam            Func        Functional type for the input \p f.
//! \tparam            contiguous  true if Dofs are stored in a contiguous chunk of memory. (Only if deinterlaced.)
//! \tparam            interlaced  true if Dofs are stored next to each other.
//! \param[in,out]     in          Vector that will be filled with values of \p f.
//! \param[in]         f           Input function, will be used to fill the vector with values of the function.
template <unsigned int dof, unsigned int d, class Func,
          bool contiguous, bool interlaced>
void eval(FFTWTimeVec<d, contiguous, interlaced> & in, const Func & f) {
    if(!f.is_init()) {
        debug::warning("Data is not initialized, maybe a bug? Call init() before eval!!!", mpi::world);
    }

    if(f.indexed() == false) {
        apply_kern<eval_kern<d, dof, false>>(
                std::forward_as_tuple(in, f), "host::eval"
        );
    } else {
        apply_kern<eval_kern<d, dof, true>>(
                std::forward_as_tuple(in, f), "host::eval"
        );
    }
}

//! \brief Variant of eval kerel that intakes a simple function instead of
//! a complex class.
//!
//!
//!
//! \tparam dof
//! \tparam d
//! \tparam Func
//! \tparam contiguous
//! \tparam interlaced
//! \param in
//! \param f
template <unsigned int dof, unsigned int d, class Func,
          bool contiguous, bool interlaced>
void eval_func(FFTWTimeVec<d, contiguous, interlaced> & in, const Func & f) {
    apply_kern<eval_functional_kern<d, dof, false>>(
            std::forward_as_tuple(in, f), "host::eval"
    );
}

//! \brief Applies a scalar valued function to a vector. CPU version.
//!
//! Applies a function \f$f: \mathbb{R}^m \rightarrow \mathbb{R}\f$ to the input vector.
//! The value is overridden in the input vector: \f$\mathbf{u} = f(\mathbf{u})\f$.
//! The operation is done componentwise and using
//! the same function for all components.
//!
//! \see Do not confuse with \ref eval.
//!
//! \tparam            FFTWTimeVec Type of the vector \p in.
//! \tparam            Func        Functional type for the input \p f.
//! \param[in,out]     in          Vector that will be filled with values of \p f.
//! \param[in]         f           Input function \f$f\f$.
template <class FFTWTimeVec, class Func>
void applyf(FFTWTimeVec & in, const Func & f) {
    apply_kern<applyf_kern<FFTWTimeVec::base_t::dim>>(
                std::forward_as_tuple(in, f), "host::applyf"
    );
}

//! \brief Compute the tensor product of a vector with itself. CPU Version.
//!
//! The function computes \f$\mathbf{u} \otimes \mathbf{u}\f$ given a vector input \f$\mathbf{u}\f$.
//! If the input vector is in \f$\mathbf{R}^m\f$ then the output is in \f$\mathbf{R}^{\binom{m}{2}} \simeq Sym_n(\mathbf{R})\f$.
//! The latter isomorphism is chosen to be compatible with \ref sym_vector_divergence.
//!
//! \warning The output is in compressed symmetric form, namely we only compute the lower triangular
//! part of the tensor and sort the components in row-major format.
//!
//! \see \ref sym_vector_divergence
//!
//! \tparam      FFTWTimeVec  Type of the input and output vector, must be in time-space.
//! \param[in]   in           Input vector \f$\mathbf{u}\f$.
//! \param[out]  out          Tensor product \f$\mathbf{u} \otimes \mathbf{u}\f$.
template <class FFTWTimeVec>
void sym_tensorize(const FFTWTimeVec & in, FFTWTimeVec & out) {
    apply_kern<symtensorize_kern<FFTWTimeVec::base_t::dim>>(
                std::forward_as_tuple(in, out), "host::symtensorize"
    );
}

//! \brief Compute the discrete \f$L^p\f$ norm of a vector field. CPU Version.
//!
//! The discrete norm is:
//! \f[\lVert \mathbf{u} \rVert_p := \left( \sum_{i,j,k} \lvert \mathbf{u}_{i,j,k} \rvert^p \Delta x \Delta y \Delta z\right)^{\frac{1}{p}}.\f]
//! The value of \p p can also be \ref INFINITY, then the maximum is computed.
//!
//! The norm is computed on all Dofs.
//!
//! \see \ref inf_norm_kern, \ref norm_kern.
//!
//! \warning Note to future self, there is no bug in the norm: the norm computes
//! the vector l^2 norm of each cell point, and then computes the discrete L^p
//! norm of the grid vector. A vector with dof \ref Dofs and constant value \f$c\f$
//! will have norm \f$\sqrt(dof) c\f$.
//!
//! TODO: potentially introduce component wise or other l^p norm for Dofs.
//!
//! \tparam            T           Type of scalar for the vector \p in.
//! \tparam            d           Space dimensions.
//! \tparam            contiguous  true if Dofs are stored in a contiguous chunk of memory. (Only if deinterlaced.)
//! \tparam            interlaced  true if Dofs are stored next to each other.
//! \param             in          Vector of which the norm is computed.
//! \param             p           Either a scalar \f$p \geq 0\f$ or \ref INFINITY.
//! \return                        The \f$L^p\f$ norm.
template <typename T, unsigned int d, bool contiguous, bool interlaced>
scalar_t norm(const HostVec<T, d, contiguous, interlaced> & in, scalar_t p = 2) {
    double var = 0;
    if( p != INFINITY ) {
        return apply_kern_reduction_plus<norm_kern<d>>(
                    std::forward_as_tuple(in, p), var, "host::norm_kern"
                    );
    } else {
        return apply_kern_reduction_max<inf_norm_kern<d>>(
                    std::forward_as_tuple(in), var, "host::inf_norm_kern"
                    );
    }
}

//! \brief Set value of the vector to be the specified constant. CPU Version.
//!
//! The input vector \p in is set to be constant, with values equal to the constant \p value.
//!
//! This variant uses fill which should use memset or other very efficient routines.
//!
//! \tparam            T           Type of scalar for the vector \p in.
//! \tparam            d           Space dimensions.
//! \tparam            contiguous  true if Dofs are stored in a contiguous chunk of memory. (Only if deinterlaced.)
//! \tparam            interlaced  true if Dofs are stored next to each other.
//! \param[in,out]     in          The input vector: will be overrtidden.
//! \param[in]         alpha       A value of scalar type compatible with \p in.
template <typename T, unsigned int d, bool contiguous, bool interlaced>
void set(HostVec<T, d, contiguous, interlaced> & in, scalar_t alpha) {
    profiler::start_stage("host::set");
    // FIXME: check: same grid

    //    #pragma omp parallel for collapse (2)
    for(auto dt: in._data) {
        std::fill(dt, dt + in.size, alpha);
    }
    in.scaling_factor = 1;
    profiler::end_stage();
}

//! \brief Set vector to zero. CPU Version.
//!
//! Equivalent to set(in, 0);
//!
//! \see \ref set.
//!
//! \tparam            T           Type of scalar for the vector \p in.
//! \tparam            d           Space dimensions.
//! \tparam            contiguous  true if Dofs are stored in a contiguous chunk of memory. (Only if deinterlaced.)
//! \tparam            interlaced  true if Dofs are stored next to each other.
//! \param[in,out]     in          The input vector: will be overridden.
template <typename T, unsigned int d, bool contiguous, bool interlaced>
void setZero(HostVec<T, d, contiguous, interlaced> & in) {
    set(in, 0);
}

//! \brief Scale a vector by constant factor. CPU Version.
//!
//! Computes (and overrides input) \f$\mathbf{u} = \alpha \mathbf{u}\f$.
//!
//! Uses BLAS.
//!
//! Variant for deinterlaced and discontiguous vectors.
//!
//! \tparam            T           Type of scalar for the vector \p in.
//! \tparam            d           Space dimensions.
//! \param[in,out]     in          Vector to be scaled.
//! \param[in]         alpha       Scales a vector.
template <typename T, unsigned int d>
void scale(HostVec<T, d, false, false> & in, scalar_t alpha) {
    profiler::start_stage("host::scale");
    // FIXME: check: same grid

    alpha /= in.scaling_factor;
    //    #pragma omp parallel for collapse (2)
    for(auto dt: in._data) {
        cblas_dscal(in.size*in.data_size, alpha, reinterpret_cast<double*>(dt), 1.);
    }
    in.scaling_factor = 1;
    profiler::end_stage();
}

//! \brief Scale a vector by constant factor. CPU Version.
//!
//! Computes (and overrides input) \f$\mathbf{u} = \alpha \mathbf{u}\f$.
//!
//! Uses BLAS.
//!
//! Variant for contiguous vectors.
//!
//! \tparam            T           Type of scalar for the vector \p in.
//! \tparam            d           Space dimensions.
//! \tparam            interlaced  True if vector Dofs are interleaved.
//! \param[in,out]     in          Vector to be scaled.
//! \param[in]         alpha       Scales a vector.
template <typename T, unsigned int d, bool interlaced>
void scale(HostVec<T, d, true, interlaced> & in, scalar_t alpha) {
    profiler::start_stage("host::scale");
    // FIXME: check: same grid

    alpha /= in.scaling_factor;
    cblas_dscal(in.size*in.data_size*in.dof, alpha, reinterpret_cast<double*>(in._data), 1.);
    in.scaling_factor = 1;
    profiler::end_stage();
}

//! \brief Scale the vector s.t. the components have the correct value.
//!
//! Each vector contains a scalar scaling_factor that represent how much the vector should actually be scaled to.
//! This scaling factor is generally changed when performing FFTs, and has to be taken into account when we
//! require the actual data from the vector.
//! This function reset scaling_factor to one by setting \f$\mathbf{u} = s \mathbf{u}\f$, where the scaling_factor is\f$s\f$.
//!
//! \see \ref HostVec \ref FFTWFreqVec \ref FFTWTimeVec, \ref DeviceVec, \ref CUDADeviceFreqVec, \ref CUDADeviceTimeVec
//! \tparam            d           Space dimensions.
//! \tparam            contiguous  True if Dofs are contiguous (only if deinterlaced).
//! \tparam            interlaced  True if vector Dofs are interleaved.
//! \param[in]         in          Overridden.
template <unsigned int d, bool contiguous, bool interlaced>
void normalize(FFTWTimeVec<d, contiguous, interlaced> & in) {
    profiler::start_stage("host::normalize");
    // FIXME: check: same grid

    if(in.scaling_factor != 1) scale(in, 1.);
    profiler::end_stage();
}

//! \brief Computes AXPY. CPU version.
//!
//! Computes \f$\mathbf{y} += \alpha \mathbf{x}\f$ for all Dofs that are in common.
//! Uses BLAS.
//!
//! Discontiguous variant (deinterlaced only).
//!
//! \p alpha is a scalar, AXPY is performed component-wise.
//!
//! \tparam         T           Scalar type for the vector.
//! \tparam         d           Space dimensions.
//! \tparam         interlaced  True if vector Dofs are interleaved (always false).
//! \param[in,out]  y           Modified vector \f$\mathbf{y}\f$.
//! \param[in]      x           Vector \f$x\f$, remains unmodified.
//! \param[in]      alpha       Scaling factor \f$\alpha\f$.
template <typename T, unsigned int d, bool interlaced>
void axpy(HostVec<T, d, false, interlaced> & y,
          const HostVec<T, d, false, interlaced> & x, scalar_t alpha) {
    profiler::start_stage("host::axpy");
    // FIXME: check: same grid

//    std::cout << x.size<< " " <<  x.data_size << std::endl;
    alpha *= y.scaling_factor / x.scaling_factor;
//    #pragma omp parallel for collapse (2)
    for(unsigned int f = 0; f < std::min(x.dof,y.dof); ++f) {
        cblas_daxpy(x.size*x.data_size, alpha,
                    reinterpret_cast<double*>(x._data[f]), 1, reinterpret_cast<double*>(y._data[f]), 1);
    }
    profiler::end_stage();
}

//! \brief Computes AXPY. CPU version.
//!
//! Computes \f$\mathbf{y} += \alpha \mathbf{x}\f$ for all Dofs that are in common.
//! Uses BLAS.
//!
//! Contiguous variant, interlaced or not.
//!
//! \p alpha is a scalar, AXPY is performed component-wise.
//!
//! \tparam         T           Scalar type for the vector.
//! \tparam         d           Space dimensions.
//! \tparam         interlaced  True if vector Dofs are interleaved.
//! \param[in,out]  y           Modified vector \f$\mathbf{x}\f$.
//! \param[in]      x           Vector \f$x\f$, remains unmodified.
//! \param[in]      alpha       Scaling factor \f$\alpha\f$.
template <typename T, unsigned int d, bool interlaced>
void axpy(HostVec<T, d, true, interlaced> & y,
          const HostVec<T, d, true, interlaced> & x, scalar_t alpha) {
    profiler::start_stage("host::axpy");
    // FIXME: check: same grid

    assert(x.dof == y.dof && "Must have same dof!");
    alpha *= y.scaling_factor / x.scaling_factor;
    cblas_daxpy(x.size*x.data_size*x.dof, alpha,
                reinterpret_cast<double*>(x._data), 1, reinterpret_cast<double*>(y._data), 1);
    profiler::end_stage();
}

//! \brief Computes AXPBYPZ. CPU version.
//!
//! Computes \f$\mathbf{z} += \alpha \mathbf{x} + \beta \mathbf{y}\f$ for all Dofs that are in common.
//! Uses BLAS.
//!
//! Discontiguous variant (deinterlaced only).
//!
//! \p alpha and \p beta are scalar, AXPBYPZ is performed component-wise.
//!
//! \tparam         T           Scalar type for the vector.
//! \tparam         d           Space dimensions.
//! \tparam         interlaced  True if vector Dofs are interleaved (always false).
//! \param[in,out]  z           Modified vector \f$\mathbf{z}\f$.
//! \param[in]      x           Vector \f$x\f$, remains unmodified.
//! \param[in]      y           Vector \f$y\f$, remains unmodified.
//! \param[in]      alpha       Scaling factor \f$\alpha\f$.
//! \param[in]      beta        Scaling factor \f$\beta\f$.
template <typename T, unsigned int d, bool interlaced>
void axpbypz(HostVec<T, d, false, interlaced> & z,
             const HostVec<T, d, false, interlaced> & y,
             const HostVec<T, d, false, interlaced> & x,
             scalar_t alpha, scalar_t beta) {
    profiler::start_stage("host::axpbypz");
    // FIXME: check: same grid

    alpha *= z.scaling_factor / x.scaling_factor;
    beta *= z.scaling_factor / y.scaling_factor;
//    #pragma omp parallel for collapse (2)
    for(unsigned int f = 0; f < std::min(x.dof,std::min(y.dof,z.dof)); ++f) {
        cblas_daxpy(x.size*x.data_size, alpha,
                    reinterpret_cast<double*>(x._data[f]), 1,
                    reinterpret_cast<double*>(z._data[f]), 1);
        cblas_daxpy(y.size*y.data_size, beta,
                    reinterpret_cast<double*>(y._data[f]), 1,
                    reinterpret_cast<double*>(z._data[f]), 1);
    }
    profiler::end_stage();
}

//! \brief Computes AXPBYPZ. CPU version.
//!
//! Computes \f$\mathbf{z} += \alpha \mathbf{x} + \beta \mathbf{y}\f$ for all Dofs that are in common.
//! Uses BLAS.
//!
//! Contiguous variant, interlaced or not.
//!
//! \p alpha and \p beta are scalar, AXPBYPZ is performed component-wise.
//!
//! \tparam         T           Scalar type for the vector.
//! \tparam         d           Space dimensions.
//! \tparam         interlaced  True if vector Dofs are interleaved.
//! \param[in,out]  z           Modified vector \f$\mathbf{z}\f$.
//! \param[in]      x           Vector \f$x\f$, remains unmodified.
//! \param[in]      y           Vector \f$y\f$, remains unmodified.
//! \param[in]      alpha       Scaling factor \f$\alpha\f$.
//! \param[in]      beta        Scaling factor \f$\beta\f$.
template <typename T, unsigned int d, bool interlaced>
void axpbypz(HostVec<T, d, true, interlaced> & z,
             const HostVec<T, d, true, interlaced> & y,
             const HostVec<T, d, true, interlaced> & x,
             scalar_t alpha, scalar_t beta) {
    profiler::start_stage("host::axpbypz");
    // FIXME: check: same grid
    assert(x.dof == y.dof && x.dof == z.dof && "Must have same dof!");

    alpha *= z.scaling_factor / x.scaling_factor;
    beta *= z.scaling_factor / y.scaling_factor;
    cblas_daxpy(x.size*x.data_size*x.dof, alpha,
                reinterpret_cast<double*>(x._data), 1,
                reinterpret_cast<double*>(z._data), 1);
    cblas_daxpy(y.size*y.data_size*x.dof, beta,
                reinterpret_cast<double*>(y._data), 1,
                reinterpret_cast<double*>(z._data), 1);
    profiler::end_stage();
}

//! \brief Computes XMYTZ. CPU version.
//!
//! Computes \f$\mathbf{z} *= \mathbf{x} - \mathbf{y}\f$ for all Dofs that are in common.
//! Uses BLAS.
//!
//! XMYTZ is performed component-wise. Scaling is taken into account.
//!
//! \see \ref xmytz_kern.
//!
//! \tparam         T           Scalar type for the vector.
//! \tparam         d           Space dimensions.
//! \tparam         contiguous  True if Dofs are contiguous (only if deinterlaced).
//! \tparam         interlaced  True if vector Dofs are interleaved.
//! \param[in,out]  z           Modified vector \f$\mathbf{z}\f$.
//! \param[in]      x           Vector \f$x\f$, remains unmodified.
//! \param[in]      y           Vector \f$y\f$, remains unmodified.
template <typename T, unsigned int d, bool contiguous, bool interlaced>
void xmytz(HostVec<T, d, contiguous, interlaced> & z,
           const HostVec<T, d, contiguous, interlaced> & y,
           const HostVec<T, d, contiguous, interlaced> & x) {
    apply_kern_inline<xmytz_kern<d>>(
         std::forward_as_tuple(z, y, x), "host::xmytz"
    );
}

//! \brief Performs memcopy of vectors. CPU Variant.
//!
//! Copies \p in into \p other, leaves \p in unchanged.
//!
//! The output vector must be preallocated and the shapes must coincide.
//!
//! Uses BLAS.
//!
//! Discontiguous variant.
//!
//! \tparam         T           Scalar type for the vector.
//! \tparam         d           Space dimensions.
//! \tparam         interlaced  True if vector Dofs are interleaved (always false).
//! \param[in]      in          Vector to be copied from.
//! \param[in,out]  other       Vector to be copied to (dimensions must match).
template <typename T, unsigned int d, bool interlaced>
void copy(const HostVec<T,d, false, interlaced> & in,
          HostVec<T,d, false, interlaced> & other) {
    profiler::start_stage("host::copy");
    // FIXME: check: same grid

//    #pragma omp parallel for
    for(unsigned int f = 0; f < std::min(in.dof,other.dof); ++f) {
        cblas_dcopy (in.size*in.data_size,
                    reinterpret_cast<double*>(in._data[f]), 1, reinterpret_cast<double*>(other._data[f]), 1);
//        memcpy(in.data[f], other.data[f], in.size*sizeof(Vec::data_t));
    }
    other.scaling_factor = in.scaling_factor;
    profiler::end_stage();
}

//! \brief Performs memcopy of vectors. CPU Variant.
//!
//! Copies \p in into \p other, leaves \p in unchanged.
//!
//! The output vector must be preallocated and the shapes must coincide.
//!
//! Uses BLAS.
//!
//! Contiguous variant (interlaced or not).
//!
//! \tparam         T           Scalar type for the vector.
//! \tparam         d           Space dimensions.
//! \tparam         interlaced  True if vector Dofs are interleaved.
//! \param[in]      in          Vector to be copied from.
//! \param[in,out]  other       Vector to be copied to (dimensions must match).
template <typename T, unsigned int d, bool interlaced>
void copy(const HostVec<T,d, true, interlaced> & in,
          HostVec<T,d, true, interlaced> & other) {
    profiler::start_stage("host::copy");
    // FIXME: check: same grid

    assert(in.dof == other.dof && "Must have same dof!");
    cblas_dcopy (in.size*in.data_size*in.dof,
                 reinterpret_cast<double*>(in._data), 1, reinterpret_cast<double*>(other._data), 1);
    other.scaling_factor = in.scaling_factor;
    profiler::end_stage();
}

