/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "host_structure.hpp"

//
// Created by filippo on 23/11/17.
//

namespace sphinx {
namespace operators {
namespace extra {

template<unsigned int dim, bool contiguous, bool interlaced>
double compute_structure(const FFTWTimeVec<dim, contiguous, interlaced> &in, double p) {
    double var = 0;

    return apply_kern_reduction_plus<structure_kern<dim>>(
            std::forward_as_tuple(in, p), var, "host::sructure_kern"
    );
};

template<unsigned int dim, bool contiguous, bool interlaced>
double compute_structure(containers::Ghost<FFTWTimeVec<dim, contiguous, interlaced>> &in, double p) {
    double var = 0;

    return apply_kern_reduction_plus<structure_ghost_kern<dim>>(
            std::forward_as_tuple(in, p), var, "host::sructure_kern"
    );
};

template double
compute_structure<2, false, false>(const FFTWTimeVec<2, false, false> &in, double p);

template double
compute_structure<2, false, false>(containers::Ghost<FFTWTimeVec<2, false, false>> &in, double p);

#if !USE_CUDA

void set_strucutre_options(int R, bool pixely,
                           int maxstep, bool verbose) {
    structure_kern<2>::cache.R = R;
    structure_kern<2>::cache.verbose = verbose;
    structure_kern<2>::cache.pixely = pixely;
    structure_kern<2>::cache.maxstep = maxstep;
}

void reset_structure() {

}

#endif // !USE_CUDA

} // END namespace extra
} // END namespace operators
} // END namespace sphinx