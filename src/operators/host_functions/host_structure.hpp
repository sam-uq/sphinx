/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

//
// Created by filippo on 23/11/17.
//

#include "config.hpp"

#include "containers/fftw_time_vec.hpp"
#include "containers/ghost.hpp"

namespace sphinx {
namespace operators {
namespace extra {

//! Enum for selection of structure type.
enum class Structure {
    Cube, Path
};

//! \brief
//!
//! Not implemented yet!
//!
//! \tparam dim
//! \tparam contiguous
//! \tparam interlaced
//! \param in
//! \return
template<unsigned int dim, bool contiguous, bool interlaced>
double compute_structure(const FFTWTimeVec<dim, contiguous, interlaced> &in, double p = 2);

template<unsigned int dim, bool contiguous, bool interlaced>
double compute_structure(containers::Ghost<FFTWTimeVec<dim, contiguous, interlaced>> &in, double p = 2);

#if !USE_CUDA
//! \brief Configure the structure kernel launcher from external linkage.
//! \param R
//! \param pixely
//! \param maxstep
//! \param verbose
void set_strucutre_options(int R, bool pixely,
                           int maxstep = std::numeric_limits<int>::max(), bool verbose = false);

//! \brief Clears memory alocated for the strucutre computation.
//!
//! Should be called before program terminates for proper cleanup of thrust vectors.
void reset_structure();

#endif // !USE_CUDA

} // END namespace extra
} // END namespace operators
} // END namespace sphinx
