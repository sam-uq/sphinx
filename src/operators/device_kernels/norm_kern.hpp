/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

//! \file norm_kern.hpp Implements all norms kernels on device

#include <cuda_runtime.h>
#include <cufft.h>

//! \brief Compute the 2-norm squared of a 2D vector (or complex)
//! Available on both host and device
//! \param[in] in vector for which compute the norm
//! \return norm squared
inline __host__ __device__ double normsq(double2 in) {
    return in.x*in.x + in.y*in.y;
}

//! \brief Compute the 2-norm squared of a 1D vector (aka square)
//! Available on both host and device
//! \param[in] in vector for which compute the norm
//! \return norm squared
inline __host__ __device__ double normsq(double in) {
    return in*in;
}

//! \brief Kernel computing arbitrary p-norm of a vector using smart reduction
//!
//!
template <class T, typename std::enable_if<T::dim == 2, bool>::type = false>
__global__ void norm_kern(typename T::data_t** in, typename T::real_t* temp_norm,
                          unsigned int* device_size, unsigned int* device_size_pad,
                          unsigned int dof, double fac, double p) {
    int k = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;
    int I = k * device_size_pad[1] + j;
    unsigned int tid = blockDim.x * threadIdx.y + threadIdx.x;

    extern __shared__ double sdata[];

    if( k >= device_size[0] || j >= device_size[1] ) {
        sdata[tid] = 0;
    } else {
        typename T::real_t temp = 0.;
        for(unsigned int f = 0; f < dof; ++f) {
            temp += normsq(in[f][I]);
        }

        sdata[tid] = pow(temp / fac, p / 2);
    }

    __syncthreads();

    for (unsigned int s=(blockDim.x*blockDim.y)/2; s>0; s>>=1) {
        if (tid < s) {
            sdata[tid] += sdata[tid + s];
        }
        __syncthreads();
    }

    if (tid == 0) {
        temp_norm[gridDim.x * blockIdx.y + blockIdx.x] = sdata[0];
    }
}

//! \brief 3D Kernel computing arbitrary p-norm of a vector using smart reduction
//!
//!
template <class T, typename std::enable_if<T::dim == 3, bool>::type = true>
__global__ void norm_kern(typename T::data_t** in, typename T::real_t* temp_norm,
                          unsigned int* device_size, unsigned int* device_size_pad,
                          unsigned int dof, double fac, double p) {
    // This threads's local index
    int k = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;
    int i = blockIdx.z * blockDim.z + threadIdx.z;
    // Outer index
    int I = device_size_pad[2] * (k * device_size[1] + j) + i;
    // If out of bounds don't bother
//    if( k >= device_size[0] || j >= device_size[1] || i >= device_size[2] ) return;

    // Temporary where to reduce data
    extern __shared__ double sdata[];

    // Figure out the thread index
    unsigned int tid = blockDim.x * (blockDim.y * threadIdx.z + threadIdx.y) + threadIdx.x;

    // If out of bounds then set as vector was zero
    if( k >= device_size[0] || j >= device_size[1] || i >= device_size[2] ) {
        sdata[tid] = 0;
    } else {
        typename T::real_t temp = 0.;
        for(unsigned int f = 0; f < dof; ++f) {
            temp += normsq(in[f][I]);
        }

        sdata[tid] = pow(temp / fac, p / 2);
    }

    __syncthreads();

    for (unsigned int s=(blockDim.x*blockDim.y*blockDim.z)/2; s>0; s>>=1) {
        if (tid < s)
            sdata[tid] += sdata[tid + s];
        __syncthreads();
    }

    if (tid == 0) {
        temp_norm[gridDim.x * (gridDim.y * blockIdx.z + blockIdx.y) + blockIdx.x] = sdata[0];
    }
}

//! \brief Kernel computing infinity-norm of a vector using smart reduction
//!
//!
template <class T, typename std::enable_if<T::dim == 2, bool>::type = false>
__global__ void inf_norm_kern(typename T::data_t** in, typename T::real_t* temp_norm,
                              unsigned int* device_size, unsigned int* device_size_pad,
                              unsigned int dof, double fac) {
    int k = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;
    int I = k * device_size_pad[1] + j;
    extern __shared__ double sdata[];
    unsigned int tid = blockDim.x * threadIdx.y + threadIdx.x;
    if( k >= device_size[0] || j >= device_size[1] ) {
        sdata[tid] = 0;
    } else {
        typename T::real_t temp = 0.;
        for(unsigned int f = 0; f < dof; ++f) {
            temp += normsq(in[f][I]);
        }

        sdata[tid] = sqrt(temp / fac);
    }

    __syncthreads();

    for (unsigned int s=(blockDim.x*blockDim.y)/2; s>0; s>>=1) {
        if (tid < s)
            sdata[tid] = max(sdata[tid], sdata[tid + s]);
        __syncthreads();
    }
    if (tid == 0) {
        temp_norm[gridDim.x * blockIdx.y + blockIdx.x] = sdata[0];
    }
}

//! \brief 3D Kernel computing infinity-norm of a vector using smart reduction
//!
//!
template <class T, typename std::enable_if<T::dim == 3, bool>::type = true>
__global__ void inf_norm_kern(typename T::data_t** in, typename T::real_t* temp_norm,
                              unsigned int* device_size, unsigned int* device_size_pad,
                              unsigned int dof, double fac) {
    int k = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;
    int i = blockIdx.z * blockDim.z + threadIdx.z;

    int I = device_size_pad[2] * (k * device_size[1] + j) + i;

    extern __shared__ double sdata[];
    unsigned int tid = blockDim.x * (blockDim.y * threadIdx.z + threadIdx.y) + threadIdx.x;
    if( k >= device_size[0] || j >= device_size[1] || j >= device_size[2] ) {
        sdata[tid] = 0;
    } else {
        typename T::real_t temp = 0.;
        for(unsigned int f = 0; f < dof; ++f) {
            temp += normsq(in[f][I]);
        }

        sdata[tid] = sqrt(temp / fac);
    }

    __syncthreads();

    for (unsigned int s=(blockDim.x*blockDim.y)/2; s>0; s>>=1) {
        if (tid < s)
            sdata[tid] = max(sdata[tid], sdata[tid + s]);
        __syncthreads();
    }
    if (tid == 0) {
        temp_norm[gridDim.x * (gridDim.y * blockIdx.z + blockIdx.y) + blockIdx.x] = sdata[0];
    }
}
