/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

//
// Created by filippo on 17/11/17.
//

#include <cmath>

#include "utilities/fake_cuda.hpp"

//! \file sym_tensorize_kern.hpp Contains kernels performing \f$U \otimes U\f$ on time space.

//! \brief Device kernel: Performs tensor product \f$U \otimes U\f$ on time space, 2d version.
//!
//! Used to make nonlinear product. Time device kernel. Vector must have a sufficient number of dofs.
//!
//! \param[in]  in                 (device) Vector data to which apply the product
//! \param[out] out                (device) Vector data to which the product is overwrtitten
//! \param[in]  device_size        (device) 2-vector containing the layout size of the data (logical size)
//! \param[in]  device_size_pad    (device) 2-vector containing the layout size of the data with padding (true size)
//! \param[in]  fac                multiplicative factor^2, gets removed from vector
template <class T, typename std::enable_if<T::dim == 2, bool>::type = false>
__global__ void sym_tensorize_kern(typename T::data_t** in, typename T::data_t** out,
                                   unsigned int* device_size, unsigned int* device_size_pad, double fac) {
    // Find out kernel coordinates
    int k = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;
    // Find out outer index
    int I = k * device_size_pad[1] + j;
    // If out of box, return
    if( k >= device_size[0] || j >= device_size[1] ) return;

    auto r1 = in[0][I];
    auto r2 = in[1][I];

    out[0][I] = r1*r1 / fac;
    out[1][I] = r1*r2 / fac;
    out[2][I] = r2*r2 / fac;
}

//! \brief Device kernel: Performs tensor product \f$U \otimes U\f$ on time space, 3d version.
//!
//! Used to make nonlinear product. Time device kernel. Vector must have a sufficient number of dofs.
//!
//! \param[in]  in              (device) Vector data to which apply the product
//! \param[out] out             (device) Vector data to which the product is overwritten.
//! \param[in]  device_size     (device) 3-vector containing the layout site of the data
//! \param[in]  device_size_pad (device) 3-vector containing the layout site of the data with padding
//! \param[in]  fac             multiplicative factor^3, gets removed from vector
template <class T, typename std::enable_if<T::dim == 3, bool>::type = true>
__global__ void sym_tensorize_kern(typename T::data_t** in, typename T::data_t** out,
                                   unsigned int* device_size, unsigned int* device_size_pad, double fac) {
    // Find out kernel coordinates
    int k = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;
    int i = blockIdx.z * blockDim.z + threadIdx.z;
    // Find out ouzter index
    int I = device_size_pad[2] * (k * device_size[1] + j) + i;
    // If out of box, return
    if( k >= device_size[0] || j >= device_size[1] || i >= device_size[2] ) return;

    auto r1 = in[0][I];
    auto r2 = in[1][I];
    auto r3 = in[2][I];

    out[0][I] = r1*r1 / fac;
    out[1][I] = r1*r2 / fac;
    out[2][I] = r1*r3 / fac;
    out[3][I] = r2*r2 / fac;
    out[4][I] = r2*r3 / fac;
    out[5][I] = r3*r3 / fac;
}
