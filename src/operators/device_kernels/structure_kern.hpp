/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include <cmath>
#include <type_traits>

#include "utilities/fake_cuda.hpp"

//
// Created by filippo on 17/11/17.
//
// Forked from https://github.com/kjetil-lye/alsvinn
//

//! \brief Uses periodicity to change the index to be within boundaries.
//!
//!
//!
//! \param index         The index for remapping
//! \param size          Max. Dimension along which the index is remapped.
//! \return              Remapped index
inline __device__ int periodic_remap(int index, int size) {
    return (index >= size)
           ? (index - size)
           : ((index < 0)
              ? index + size
              : index
           );
}

//! \brief Computes the local integrand of the structure function.
//!
//! Given are two vectors and selectors for the norm.
//! Computes the quantity \f$  | u(x) - u(x+h) |^p_2 \f$
//!
//! \param u_1           First component of first vector
//! \param u_2           First component of first vector
//! \param uh_1          First component of second vector
//! \param uh_2          Second component of second vector
//! \param q             Selector for finite-dimensional norm for l^q(R^d) - norm in phase space
//! \param p             Scaling exponent for structure funcion L^p(domain)
//! \return              Local value of S_p(u(x), u(x+h))
inline __device__ double structure_normp(double u_1, double u_2,
                                         double uh_1, double uh_2, double q, double p) {
    return std::pow((u_1 - uh_1) * (u_1 - uh_1) + (u_2 - uh_2) * (u_2 - uh_2), p / q); // only q = 2
}

//!
//! Computes the quantity \f$  | u(x) - u(x+h) |^p_2 \f$ given u(x), x, and h
//!
//! \tparam    T
//! \param[in] in
//! \param[in] u_1
//! \param[in] u_2
//! \param[in] k
//! \param[in] j
//! \param[in] h1                  Offset in x-direction
//! \param[in] h2                  Offset in y-direction
//! \param[in] p                   Scaling exponent for structure function
//! \param[in] device_size         Real size of data, excluding padding
//! \param[in] device_size_pad     Logical size of data, including padding
//! \return
template<class T>
inline __device__ double local_structure(typename T::data_t **in,
                                         double u_1, double u_2,
                                         int k, int j,
                                         int h1, int h2,
                                         double p, unsigned int *device_size, unsigned int *device_size_pad) {
    int Ih = periodic_remap(k + h1, device_size[0]) * device_size_pad[1] + periodic_remap(j + h2, device_size[1]);

    const auto uh_1 = in[0][Ih];
    const auto uh_2 = in[1][Ih];

    return structure_normp(u_1, u_2, uh_1, uh_2, 2., p);
}

//! \brief Computes the structure function for FIXED h in a square around each point x.
//!
//! Computes the quantity:
//! \f[ S_p(x) = \int_{Box_h} | u(x) - u(x+h) |^p dh \f]
//!
//! The output will be stored in a linear array.
//!
//! The goal is to compute the structure function, then reduce (sum) over space
//! then go on to next h
//!
//! \tparam T
//! \param output
//! \param in
//! \param h
//! \param device_size
//! \param device_size_pad
//! \param p
template <class T, typename std::enable_if<T::dim == 2, bool>::type = false>
__global__ void computeStructureCube2D(typename T::data_t* output,
                                       typename T::data_t **in,
                                       int h,
                                       unsigned int *device_size,
                                       unsigned int *device_size_pad,
                                       double p) {
    // Find out kernel coordinates
    int k = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;
    // Find out outer index
    int I = k * device_size_pad[1] + j;
    int I_small = k * device_size[1] + j;
    // If out of box, return
    if( k >= device_size[0] || j >= device_size[1] ) return;

    // Reduced structure function to be further scaled/integrated
    output[I_small] = 0;

    // Local value
    const auto u_1 = in[0][I];
    const auto u_2 = in[1][I];

    // Vertical boundaries
    for (int hx = -h; hx <= h; ++hx) {
        // Left
        output[I_small] += local_structure<T>(in, u_1, u_2, k, j, hx, -h, p, device_size, device_size_pad);
        // Right
        output[I_small] += local_structure<T>(in, u_1, u_2, k, j, hx, h, p, device_size, device_size_pad);
    }
    // Horizontal boundaries (don't do corners)
    for (int hy = -h + 1; hy < h; ++hy) {
        // Bottom
        output[I_small] += local_structure<T>(in, u_1, u_2, k, j, -h, hy, p, device_size, device_size_pad);
        // Top
        output[I_small] += local_structure<T>(in, u_1, u_2, k, j, h, hy, p, device_size, device_size_pad);
    }

    // After this:
    // - divide by number of cells in the two fors
    // - must be integrated in domain (summed and mult. dx)
    // - must be integrated in proba (summed and averaged)
}


//! \brief Computes the structure function in a path around each point x.
//!
//! Computes the quantity:
//! \f[ S_p(x) = \int_{Box_h} | u(x) - u(x+h) |^p dh \f]
//!
//! The output will be stored in a linear array.
//!
//! The goal is to compute the structure function, then reduce (sum) over space
//! then go on to next h
//!
//! \tparam T
//! \param output
//! \param in
//! \param pathx
//! \param pathy
//! \param pathsize
//! \param device_size
//! \param device_size_pad
//! \param p
template<class T, typename std::enable_if<T::dim == 2, bool>::type = false>
__global__ void computeStructurePath(typename T::data_t *output,
                                     typename T::data_t **in,
                                     int *pathx, int *pathy, int pathsize,
                                     unsigned int *device_size,
                                     unsigned int *device_size_pad,
                                     double p) {
    // Find out kernel coordinates
    int k = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;
    // Find out outer index
    int I = k * device_size_pad[1] + j;
    int I_small = k * device_size[1] + j;
    // If out of box, return
    if (k >= device_size[0] || j >= device_size[1]) { return; }

    // Reduced structure function to be further scaled/integrated
    output[I_small] = 0;

    // Local value
    const auto u_1 = in[0][I];
    const auto u_2 = in[1][I];

    // Vertical boundaries
    for (int ix = 0; ix < pathsize; ++ix) {
        // Get point
        int px = pathx[ix];
        int py = pathy[ix];
        // Local structure
//        if(I_small == 0)
//        printf("%d %d\n", px, py);
        output[I_small] += local_structure<T>(in, u_1, u_2, k, j, px, py, p, device_size, device_size_pad);
    }

    // After this:
    // - divide by number of cells in the two fors
    // - must be integrated in domain (summed and mult. dx)
    // - must be integrated in proba (summed and averaged)
}
