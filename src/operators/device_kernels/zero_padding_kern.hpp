/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include <cuComplex.h>

#include "utilities/cutilities.hpp"

template <class T, typename std::enable_if<T::dim == 2, bool>::type = false>
__global__ void zero_padding_kern(typename T::data_t** in, typename T::data_t** out,
                                  unsigned int* device_size, unsigned int* device_new_size,
                                  unsigned int dof, double factor = 1) {
    // Retrieve global coordinates (looping over smaller vector)
    // k loops along x-axis (not halved)
    // j loops along y-axis (halved)
    int k = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;

    // Out of array indexes
    if( k >= device_size[0] || j >= device_size[1] ) return;

    // Outer index in smaller vector
    int Ismall = k * device_size[1] + j;

    int k_ = k;
    if( k > device_size[0]/2 ) k_ += device_new_size[0] - device_size[0];

    int Ibig = k_ * device_new_size[1] + j; //FIXME
    for(unsigned int f = 0; f < dof; ++f) {
        out[f][Ibig] = in[f][Ismall] * factor;
    }
}

template <class T, typename std::enable_if<T::dim == 3, bool>::type = false>
__global__ void zero_padding_kern(typename T::data_t** in, typename T::data_t** out,
                                  unsigned int* device_size, unsigned int* device_new_size,
                                  unsigned int dof, double factor = 1) {
    // Retrieve global coordinates (looping over smaller vector)
    // k loops along x-axis (not halved)
    // j loops along y-axis (not halved)
    // i loops along z-axis (halved)
    int k = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;
    int i = blockIdx.z * blockDim.z + threadIdx.z;

    // Out of array indexes
    if( k >= device_size[0] || j >= device_size[1] || i >= device_size[2] ) return;

    // Outer index in smaller vector
    int Ismall = (k * device_size[1] + j) * device_size[2] + i;

    int k_ = k;
    if( k > device_size[0]/2 ) k_ += device_new_size[0] - device_size[0];
    int j_ = j;
    if( j > device_size[1]/2 ) j_ += device_new_size[1] - device_size[1];

    int Ibig = (k_ * device_new_size[1] + j_) * device_new_size[2] + i;
    for(unsigned int f = 0; f < dof; ++f) {
        out[f][Ibig] = in[f][Ismall] * factor;
    }
}

template <class T, typename std::enable_if<T::dim == 2, bool>::type = false>
__global__ void truncate_kern(typename T::data_t** in, typename T::data_t** out,
                              unsigned int* device_size, unsigned int* device_new_size,
                              unsigned int dof, double factor = 1) {
    // Retrieve global coordinates (looping over smaller vector)
    // k loops along x-axis (not halved)
    // j loops along y-axis (halved)
    int k = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;

    // Out of array indexes
    if( k >= device_new_size[0] || j >= device_new_size[1] ) return;

    // Outer index in smaller vector
    int Ismall = k * device_new_size[1] + j;

    // If you are past the half of the k-indexes, need to retrieve data from the end of the bigger vector
    int k_ = k;
    if( k > device_new_size[0]/2 ) k_ += device_size[0] - device_new_size[0];

    // Get outer index in bigger vector and perform the copy
    int Ibig = k_ * device_size[1] + j;
    for(unsigned int f = 0; f < dof; ++f) {
        out[f][Ismall] = in[f][Ibig] * factor;
    }
}

template <class T, typename std::enable_if<T::dim == 3, bool>::type = false>
__global__ void truncate_kern(typename T::data_t** in, typename T::data_t** out,
                              unsigned int* device_size, unsigned int* device_new_size,
                              unsigned int dof, double factor = 1) {
    // Retrieve global coordinates (looping over smaller vector)
    // k loops along x-axis (not halved)
    // j loops along y-axis (not halved)
    // i loops along z-axis (halved)
    int k = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;
    int i = blockIdx.z * blockDim.z + threadIdx.z;

    // Out of array indexes
    if( k >= device_new_size[0] || j >= device_new_size[1] || i >= device_new_size[2] ) return;

    // Outer index in smaller vector
    int Ismall = (k * device_new_size[1] + j) * device_new_size[2] + i;

    // If you are past the half of the k-indexes, need to retrieve data from the end of the bigger vector
    int k_ = k;
    if( k > device_new_size[0]/2 ) k_ += device_size[0] - device_new_size[0];
    int j_ = j;
    if( j > device_new_size[1]/2 ) j_ += device_size[1] - device_new_size[1];

    // Get outer index in bigger vector and perform the copy
    int Ibig = (k_ * device_size[1] + j_) * device_size[2] + i;
    for(unsigned int f = 0; f < dof; ++f) {
        out[f][Ismall] = in[f][Ibig] * factor;
    }
}
