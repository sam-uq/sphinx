/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

//
// Created by filippo on 17/11/17.
//

#include <cmath>

#include "utilities/fake_cuda.hpp"

// \file alias_kern.hpp Contains kernels performing aliasing operator on device.

//! \brief Device kernel: Removes high-frequency noise (2D version)
//!
//! Used to remove high-frequency oscillations. This is a CUDA kernel.
//!
//! TODO: use a smmoth truncation function
//!
//! \param[in,out] in (device) Vector data to which apply the aliasing
//! (for each dof)
//! \param[in] device_size (device) 2-vector containing the layout
//! site of the data
//! \param[in] dof number of dof of vector, in size must be at least dof
//! \param[in] m thresold mode after which the vector is zero setted
template <class T, typename std::enable_if<T::dim == 2, bool>::type = false>
__global__ void alias_kern(typename T::data_t** in,
                           unsigned int* device_size, unsigned int dof, int m) {
    // Get local index
    int k = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;
    // Find out outer index
    int I = k * device_size[1] + j;

    // If outside of box, return
    if( k >= device_size[0] || j >= device_size[1] ) return;

    // Find actual modes (k_,j) are the true modes
    double k_ = k;
    if(k > device_size[0]/2+1) k_ -= device_size[0];

    // If this compoonent needs to be aliased
    if(std::abs(k_) > m || std::abs(j) > m) {
        for(int f = 0; f < dof; ++f) {
            in[f][I] = {0,0}; // set to zero
        }
        return;
    }
}

//! \brief Device kernel: Removes high-frequency noise (3D version)
//! Used to remove high-frequency oscillations. This is a CUDA kernel.
//! TODO: use a smmoth truncation function
//! \param[in,out] in (device) Vector data to which apply the aliasing
//! (for each dof)
//! \param[in] device_size (device) 3-vector containing the layout site of
//! the data
//! \param[in] dof number of dof of vector, in size must be at least dof
//! \param[in] m thresold mode after which the vector is zero setted
template <class T, typename std::enable_if<T::dim == 3, bool>::type = true>
__global__ void alias_kern(typename T::data_t** in,
                           unsigned int* device_size, int dof, int m) {
    // Get local index
    int k = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;
    int i = blockIdx.z * blockDim.z + threadIdx.z;
    // Find out outer index
    int I = device_size[2] * (k * device_size[1] + j) + i;

    // If outside of box, return
    if( k >= device_size[0] ||
            j >= device_size[1] ||
            i >= device_size[2] ) return;

    // Find actual modes (k_,j_,i) are the true modes
    double k_ = k;
    if(k > device_size[0]/2+1) k_ -= device_size[0];
    double j_ = j;
    if(j > device_size[1]/2+1) j_ -= device_size[1];

    // If this component needs to be aliased
    if(std::abs(k_) > m || std::abs(j_) > m || std::abs(i) > m) {
        for(int f = 0; f < dof; ++f) {
            in[f][I] = {0,0}; // set to zero
        }
        return;
    }
}

