/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

//! \file all_kern.hpp Includes all available device operators kernels.

#include <type_traits>

// Input operators
//#include "eval_kern.hpp" // broken

// Reduction kernels
#include "norm_kern.hpp"

// Interpolation kernels
#include "zero_padding_kern.hpp"

// Frequency operators
#include "curl_kern.hpp"
#include "anti_curl_kern.hpp"
#include "sym_vector_divergence_kern.hpp"
#include "projection_kern.hpp"
#include "vector_laplace_kern.hpp"
#include "divergence_kern.hpp"

// Time operators
#include "sym_tensorize_kern.hpp"

// Other frequency operators
#include "alias_kern.hpp"
