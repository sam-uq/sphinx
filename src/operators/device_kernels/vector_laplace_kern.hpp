/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

template <class T, typename std::enable_if<T::dim == 2, bool>::type = true>
__global__ void vector_laplace_kern(typename T::data_t** in, typename T::data_t** out,
                                    unsigned int* device_size, int m, double eps) {
    // Find out kernel coordinates
    int k = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;
    // Compute outer index
    int I = k * device_size[1] + j;
    // Return if out of box
    if( k >= device_size[0] || j >= device_size[1] ) return;
    // Find true modes
    double k_ = k;
    if(k > device_size[0]/2+1) k_ -= device_size[0];

    // If modes are small, we are not applying the numberical diffusion
//    if(abs(k_) < m || abs(j) < m) { // TODO: CHECK, is || or &&
//        out[0][I] = {0,0};
//        out[1][I] = {0,0};
//        return;
//    }

    // Size of transform (1-dim)
    double N = device_size[0] / 2 * (2./3.);
    // A factor behavin' like |\xi|*h^2 - h
    double M = eps*(std::pow(double(std::sqrt(k_*k_+j*j))/N, 2.) - 1./N);

    // New numerical viscosity
    typename T::data_t mult = make_cuDoubleComplex(-N*(M < 0 ? 0 : M), 0.);
    // Old numerical viscosity
//    typename T::data_t mult = make_cuDoubleComplex(- eps*4*M_PI*M_PI*(k_*k_+j*j), 0);

    out[0][I] = cuCmul(mult, in[0][I]);
    out[1][I] = cuCmul(mult, in[1][I]);
}

template <class T, typename std::enable_if<T::dim == 3, bool>::type = true>
__global__ void vector_laplace_kern(typename T::data_t** in, typename T::data_t** out,
                                    unsigned int* device_size, int m, double eps) {
    // Find local coordinates
    int k = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;
    int i = blockIdx.z * blockDim.z + threadIdx.z;
    // Find outer index
    int I = device_size[2] * (k * device_size[1] + j) + i;
    // Return if out of box
    if( k >= device_size[0] || j >= device_size[1] || i >= device_size[2] ) return;
    // Find true modes
    double k_ = k;
    if(k > device_size[0]/2+1) k_ -= device_size[0];
    double j_ = j;
    if(j > device_size[1]/2+1) j_ -= device_size[1];

    // If modes are small, we are not applying the numerical diffusion
//    if(abs(k_) < m || abs(j_) < m || abs(i) < m) { // TODO: CHECK, is || or &&
//        out[0][I] = {0,0};
//        out[1][I] = {0,0};
//        out[2][I] = {0,0};
//        return;
//    }

    // Size of transform (1-dim)
    double N = device_size[0] / 2;
    // A factor behavin' like |\xi|*h^2 - h
    double M = eps*(std::pow(double(std::sqrt(k_*k_+j_*j_+i*i))/N, 2.) - 1./N);

    // New numerical viscosity
    typename T::data_t mult = make_cuDoubleComplex(-N*(M < 0 ? 0 : M), 0.);
    // Old numerical viscosity
//    typename T::data_t mult = make_cuDoubleComplex(- eps*4*M_PI*M_PI*(k_*k_+j_*j_+i*i), 0.);

    out[0][I] = cuCmul(mult, in[0][I]);
    out[1][I] = cuCmul(mult, in[1][I]);
    out[2][I] = cuCmul(mult, in[2][I]);
}

