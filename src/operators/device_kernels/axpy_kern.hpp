/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

template <class T, typename std::enable_if<T::dim == 2, bool>::type = false>
__global__ void axpy_kern(typename T::data_t** in, typename T::data_t** out, unsigned int* device_size, double a) {
    int k = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;
    int I = k * device_size[1] + j;
    if( k >= device_size[0] || j >= device_size[1] ) return;

    out[0][I] = cuCadd(out[0][I], cuCmul(make_cuDoubleComplex(a,0), in[0][I]));
    out[1][I] = cuCadd(out[1][I], cuCmul(make_cuDoubleComplex(a,0), in[1][I]));
}

template <class T, typename std::enable_if<T::dim == 3, bool>::type = true>
__global__ void axpy_kern(typename T::data_t** in, typename T::data_t** out, unsigned int* device_size, double a) {
    int k = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;
    int i = blockIdx.z * blockDim.z + threadIdx.z;
    int I = device_size[2] * (k * device_size[1] + j) + i;
    if( k >= device_size[0] || j >= device_size[1] || j >= device_size[2] ) return;

    out[0][I] = cuCadd(out[0][I], cuCmul(make_cuDoubleComplex(a,0), in[0][I]));
    out[1][I] = cuCadd(out[1][I], cuCmul(make_cuDoubleComplex(a,0), in[1][I]));
    out[2][I] = cuCadd(out[2][I], cuCmul(make_cuDoubleComplex(a,0), in[2][I]));
}
