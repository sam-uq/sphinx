/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

// \file projection_kern.hpp Contains kernels performing Leray projection operator on device.

//! \brief Device kernel: Performs projection onto input vector, 3d version
//! Used to make vector divergence free. Frequency device kernel. Vector must have a sufficient number of dofs.
//! \param[in] in (device) Vector data to which apply the prjection
//! \param[out] out (device) Vector data to which the projection is overwrtitten
//! \param[in] device_size (device) 2-vector containing the layout site of the data
template <class T, typename std::enable_if<T::dim == 2, bool>::type = false>
__global__ void projection_kern(typename T::data_t** in, typename T::data_t** out, unsigned int* device_size) {
    // Local coordinates of thread
    int k = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;
    // Outer index of thread
    int I = k * device_size[1] + j;

    // If outside return
    if( k >= device_size[0] || j >= device_size[1] ) return;

    // Find out true modes
    double k_ = k;
    if(k > device_size[0]/2+1) k_ -= device_size[0];

    // Zero mode is set to zero (remove average!)
    if( k_ == 0 && j == 0 ) {
        out[0][I] = {0.,0.};
        out[1][I] = {0.,0.};
        return;
    }

    // Read data
    typename T::data_t c1 = in[0][I];
    typename T::data_t c2 = in[1][I];

    // Compute dots = (c1*k_ + c2*j_)/(k_^2+j^2)
    typename T::data_t dots = cuCdiv(cuCadd(cuCmul(c1, make_cuDoubleComplex(k_,0)),
                 cuCmul(c2, make_cuDoubleComplex(j,0))),
                 make_cuDoubleComplex(k_*k_ + j*j,0));
    // Write data, out1 = c1 * dots * k_, out2 = c2 * dots * j
    out[0][I] = cuCsub(c1, cuCmul(dots, make_cuDoubleComplex(k_,0)));
    out[1][I] = cuCsub(c2, cuCmul(dots, make_cuDoubleComplex(j,0)));
}

//! \brief Device kernel: Performs projection onto input vector, 3d version
//! Used to make vector divergence free. Frequency device kernel. Vector must have a sufficient number of dofs.
//! \param[in] in (device) Vector data to which apply the prjection
//! \param[out] out (device) Vector data to which the projection is overwrtitten
//! \param[in] device_size (device) 2-vector containing the layout site of the data
template <class T, typename std::enable_if<T::dim == 3, bool>::type = true>
__global__ void projection_kern(typename T::data_t** in, typename T::data_t** out, unsigned int* device_size) {
    // Local coordinates of thread
    int k = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;
    int i = blockIdx.z * blockDim.z + threadIdx.z;
    // Outer index of thread
    int I = device_size[2] * (k * device_size[1] + j) + i;

    // If outside return
    if( k >= device_size[0] || j >= device_size[1] || i >= device_size[2] ) return;

    // Find out true modes
    double k_ = k;
    if(k > device_size[0]/2+1) k_ -= device_size[0];
    double j_ = j;
    if(j > device_size[1]/2+1) j_ -= device_size[1];

    // Zero mode is set to zero (remove average!)
    if( k_ == 0 && j_ == 0 && i == 0 ) {
        out[0][I] = {0.,0.};
        out[1][I] = {0.,0.};
        out[2][I] = {0.,0.};
        return;
    }

    // Read data
    typename T::data_t c1 = in[0][I];
    typename T::data_t c2 = in[1][I];
    typename T::data_t c3 = in[2][I];

    // Compute dots = (c1*k_ + c2*j_ + c3*i)/(k_^2+j_^2+i^2)
    typename T::data_t dots =
            cuCdiv(
                cuCadd(
                    cuCadd(
                        cuCmul(c1, make_cuDoubleComplex(k_,0)),
                        cuCmul(c2, make_cuDoubleComplex(j_,0))
                        ),
                    cuCmul(c3, make_cuDoubleComplex(i, 0))
                    ),
                make_cuDoubleComplex(k_*k_ + j_*j_ + i*i,0)
                );

    // Write data, out1 = c1 * dots * k_, out2 = c2 * dots * j, out3 = c3 * dots * i
    out[0][I] = cuCsub(c1, cuCmul(dots, make_cuDoubleComplex(k_, 0)));
    out[1][I] = cuCsub(c2, cuCmul(dots, make_cuDoubleComplex(j_, 0)));
    out[2][I] = cuCsub(c3, cuCmul(dots, make_cuDoubleComplex(i,  0)));
}
