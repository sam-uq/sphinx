/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "../../utilities/cutilities.hpp"
#include "../../utilities/types.hpp"

template <class T, class Func, typename std::enable_if<T::dim == 2, bool>::type = false>
__global__ void eval_kern(Func F,
                          typename T::data_t** device_data, unsigned int* device_size, unsigned int* device_size_pad, unsigned int dof) {
    unsigned int* idx = new unsigned int[2];
    idx[0] = blockIdx.x * blockDim.x + threadIdx.x;
    idx[1] = blockIdx.y * blockDim.y + threadIdx.y;
    unsigned int I = idx[0] * device_size_pad[1] + idx[1];
    if( idx[0] >= device_size[0] || idx[1] >= device_size[1] ) return;


    double* c = new real_t[2];
    coord<2>(idx, c, device_size, 0u);

//    printf("a %d, %d, %d asd %d, %d, %d asd %f, %f, %f\n", device_size[0], device_size[1], device_size[2], device_size_pad[0], device_size_pad[1], device_size_pad[2],c[0], c[1], c[2]);
    printf("a %d, %d, %d asd %d    %p   %p\n", idx[0], idx[1], idx[1], I, device_data, device_data[0]);
    for(unsigned int f = 0; f < dof; ++f) {
        device_data[f][I] = 9.;
        F(c, f, device_data[f][0]);
    }
}

template <class T, class Func, typename std::enable_if<T::dim == 3, bool>::type = true>
__global__ void eval_kern(const Func & F,
                          typename T::data_t** device_data, unsigned int* device_size, unsigned int* device_size_pad, unsigned int dof) {
    unsigned int* idx = new unsigned int[3];
    idx[0] = blockIdx.x * blockDim.x + threadIdx.x;
    idx[1] = blockIdx.y * blockDim.y + threadIdx.y;
    idx[2] = blockIdx.z * blockDim.z + threadIdx.z;
    unsigned int I = device_size_pad[2] * (idx[0] * device_size[1] + idx[1]) + idx[2];
    if( idx[0] >= device_size[0] || idx[1] >= device_size[1] || idx[2] >= device_size[2] ) return;

    double* c = new real_t[3];
    coord<3>(idx, c, device_size, 0u);
    for(unsigned int f = 0; f < dof; ++f) {
        F.apply(c, f, device_data[f][I]);
    }
}
