/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

template <class T, typename std::enable_if<T::dim == 2, bool>::type = false>
__global__ void sym_vector_divergence_kern(typename T::data_t** in, typename T::data_t** out,
                                           unsigned int* device_size) {
    int k = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;
    int I = k * device_size[1] + j;
    if( k >= device_size[0] || j >= device_size[1] ) return;

    // Compute actual modes
    double k_ = k;
    if(k > device_size[0]/2+1) k_ -= device_size[0];

    typename T::data_t c1 = in[0][I];
    typename T::data_t c2 = in[1][I];
    typename T::data_t c3 = in[2][I];

    typename T::data_t l = make_cuDoubleComplex(0,2*M_PI*k_);
    typename T::data_t r = make_cuDoubleComplex(0,2*M_PI*j);

    out[0][I] = cuCadd(cuCmul(l, c1), cuCmul(r, c2));
    out[1][I] = cuCadd(cuCmul(l, c2), cuCmul(r, c3));
}

template <class T, typename std::enable_if<T::dim == 3, bool>::type = true>
__global__ void sym_vector_divergence_kern(typename T::data_t** in, typename T::data_t** out,
                                           unsigned int* device_size) {
    // Find out kernel coordinates
    int k = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;
    int i = blockIdx.z * blockDim.z + threadIdx.z;
    // Compute outer index
    int I = device_size[2] * (k * device_size[1] + j) + i;

    if( k >= device_size[0] || j >= device_size[1] || i >= device_size[2] ) return;

    // Compute actual modes
    double k_ = k;
    if(k > device_size[0]/2+1) k_ -= device_size[0];
    double j_ = j;
    if(j > device_size[1]/2+1) j_ -= device_size[1];

    typename T::data_t c1 = in[0][I];
    typename T::data_t c2 = in[1][I];
    typename T::data_t c3 = in[2][I];
    typename T::data_t c4 = in[3][I];
    typename T::data_t c5 = in[4][I];
    typename T::data_t c6 = in[5][I];

    typename T::data_t l = make_cuDoubleComplex(0,2*M_PI*k_);
    typename T::data_t r = make_cuDoubleComplex(0,2*M_PI*j_);
    typename T::data_t t = make_cuDoubleComplex(0,2*M_PI*i);

    out[0][I] = cuCadd(cuCadd(cuCmul(l, c1), cuCmul(r, c2)), cuCmul(t, c3));
    out[1][I] = cuCadd(cuCadd(cuCmul(l, c2), cuCmul(r, c4)), cuCmul(t, c5));
    out[2][I] = cuCadd(cuCadd(cuCmul(l, c3), cuCmul(r, c5)), cuCmul(t, c6));
}
