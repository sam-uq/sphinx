/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

template <class T, typename std::enable_if<T::dim == 2, bool>::type = false>
__global__ void anti_curl_kern(typename T::data_t** in,
                               typename T::data_t** out, unsigned int* device_size) {
    int k = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;
    int I = k * device_size[1] + j;
    if( k >= device_size[0] || j >= device_size[1] ) return;

    typename T::data_t c1 = in[0][I];

    double k_ = k;
    if(k > device_size[0]/2+1) k_ -= device_size[0];
    if(k_ == 0 && j == 0) {
        out[0][I] = {0,0};
        out[1][I] = {0,0};
        return;
    }

    typename T::data_t l = make_cuDoubleComplex(0, k_ / ((k_*k_+j*j)*2*M_PI));
    typename T::data_t r = make_cuDoubleComplex(0, -j / ((k_*k_+j*j)*2*M_PI));
    out[0][I] = cuCmul(r, c1);
    out[1][I] = cuCmul(l, c1);
}

template <class T, typename std::enable_if<T::dim == 3, bool>::type = true>
__global__ void anti_curl_kern(typename T::data_t** in,
                               typename T::data_t** out, unsigned int* device_size) {
    assert(false && "3D anti-curl not implemented!");

    int k = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;
    int i = blockIdx.z * blockDim.z + threadIdx.z;
    int I = device_size[2] * (k * device_size[1] + j) + i;

//    printf("(%d %d) ==> %d %d %d\n", blockIdx.x, blockIdx.y, k, j, I);
    if( k >= device_size[0] || j >= device_size[1] || i >= device_size[2] ) return;
    double k_ = k;
    if(k > device_size[0]/2+1) k_ -= device_size[0];
    double j_ = j;
    if(j > device_size[1]/2+1) j_ -= device_size[1];

    using Td = typename T::data_t;
    Td c1 = in[0][I];
//    Td c2 = in[1][I];
//    Td c3 = in[2][I];

    if(k_ == 0 && j_ == 0 && i == 0) {
        out[0][I] = {0,0};
        out[1][I] = {0,0};
        out[2][I] = {0,0};
        return;
    }

    Td l = make_cuDoubleComplex(0, -k_ / ((k_*k_+j*j)*2*M_PI));
    Td r = make_cuDoubleComplex(0, j / ((k_*k_+j*j)*2*M_PI));
    Td t = make_cuDoubleComplex(0, j / ((k_*k_+j*j)*2*M_PI));
    out[0][I] = cuCmul(r, c1);
    out[1][I] = cuCmul(l, c1);
    out[2][I] = cuCmul(l, c1);
}
