/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "sphinx_includes.hpp"

#include "utilities/utilities.hpp"

namespace sphinx {
namespace kernel {

using namespace sphinx::types;


template <unsigned int d>
struct ecalpal_kern {
    static constexpr unsigned int dim = d;

    //! Compute inverse Laplace operator. Mode 0 is set to 0 (average is set to 0).
    //! \tparam Args a tuple (or compatible) type of size 2
    //! \param Ix The internal index for the loop
    //! \param real_modes The fourier modes of the current loop iteation
    //! \param args tuple of fequency vector (in, out)
    template <class Args>
    static inline void eval(index_t Ix, const pos_t<d> & real_modes,
                            const Args & args) {
//        double N = std::get<1>(args).grid_p->timeN[0];

        double factor;
        if(real_modes[0] == 0 && real_modes[1] == 0 && (d == 2 || real_modes[2] == 0)) {
            for(unsigned int f = 0; f < std::min(std::get<1>(args).dof, std::get<0>(args).dof); ++f) {
                std::get<1>(args)(f,Ix) = 0;
            }
            return;
        } else {
            factor = -normp(real_modes)*4*M_PI*M_PI;
        }

        for(unsigned int f = 0; f < std::min(std::get<1>(args).dof, std::get<0>(args).dof); ++f) {
            std::get<1>(args)(f,Ix) = std::get<0>(args)(f, Ix) / factor;
        }
    }

    template <class Args>
    static void pre(const Args & args) {
        static_assert(std::tuple_size<Args>::value == 2, "Tuple must have size 2!");

        assert(std::get<0>(args).dof >= 1 && "Input vector has not enough dof");
        assert(std::get<1>(args).dof >= 1 && "Ouput vector has not enough dof");
    }

    template <class Args>
    static void post(const Args & args) {
        std::get<1>(args).scaling_factor = std::get<0>(args).scaling_factor;
    }
};

} // END NAMESPACE KERNEL
} // END NAMESPACE SPHINX