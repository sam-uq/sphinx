/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

//! \file all_kern.hpp Includes all host kernels.

// Frequency based operators (differentiation bases)
#include "projection_kern.hpp"
//#include "stream_kern.hpp"
#include "curl_kern.hpp"
#include "anti_curl_kern.hpp"
#include "gradient_kern.hpp"
#include "wedge_kern.hpp"
#include "divergence_kern.hpp"

// Frequency base operators exploiting tensor symmetry (differentiation bases)
#include "sym_vector_divergence_kern.hpp"
#include "vector_laplace_kern.hpp"

// Time domain based operators (non-linear)
#include "symtensorize_kern.hpp"

// Frequency based operators, non diff. based (smoothing/interpolation/etc.)
#include "alias_kern.hpp"
#include "zero_padding_kern.hpp"
#include "truncate_kern.hpp"
#include "ecalpal_kern.hpp"

// General BLAS-like operators
#include "xmytz_kern.hpp"

// Reduction operations
#include "norm_kern.hpp"

// Setters/Evaluators/Appliers operators
#include "set_kern.hpp"
#include "eval_kern.hpp"
#include "applyf_kern.hpp"

// Structure function
#include "structure_kern.hpp"
