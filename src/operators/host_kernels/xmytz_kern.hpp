/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "sphinx_includes.hpp"

namespace sphinx {
namespace kernel {

using namespace sphinx::types;

template <unsigned int d>
struct xmytz_kern {
    static constexpr unsigned int dim = d;

    template <class Args>
    static inline void eval(index_t I, const Args & args) {
        for(unsigned int f = 0; f < std::get<0>(args).dof; ++f) {
            std::get<0>(args)(f, I) /= std::get<0>(args).scaling_factor;
            std::get<0>(args)(f, I) *= (std::get<2>(args)(f, I) / std::get<2>(args).scaling_factor
                                        - std::get<1>(args)(f, I) / std::get<1>(args).scaling_factor );
        }
    }

    template <class Args>
    static void pre(const Args & args) {
        static_assert(std::tuple_size<Args>::value == 3, "Tuple must have size 3");

        assert(std::get<0>(args).dof >= 1 && "Need at least one dof");
        assert(std::get<1>(args).dof >= 1 && "Need at least one dof");
        assert(std::get<2>(args).dof >= 1 && "Need at least one dof");
    }

    template <class Args>
    static void post(const Args &) {

    }
};

} // END NAMESPACE KERNEL
} // END NAMESPACE SPHINX