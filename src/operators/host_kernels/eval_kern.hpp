/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "sphinx_includes.hpp"

namespace sphinx {
namespace kernel {

using namespace sphinx::types;

template <unsigned int d, unsigned int dof, bool indexed = false>
struct eval_kern {
    static constexpr unsigned int dim = d;

    template <class Args, bool indexed_ = indexed,
                        typename std::enable_if<!indexed_, bool>::type
                           is_indexed = false
             >
    static inline void eval(index_t Ix, const pos_t<d> & modes,
                            const Args & args) {
//        using id_t = typename std::remove_reference<
//        typename std::tuple_element<1,Args>::type
//        >::type;
        using vec_t = typename std::remove_reference<
        typename std::tuple_element<0,Args>::type
        >::type;
        using coord_t = typename vec_t::grid_t::coord_t;

        coord_t coord = std::get<0>(args).grid_p->coord(modes);

        data_t<dof> val;
        std::get<1>(args).evalat(coord, val);
        for(unsigned int f = 0; f < std::get<0>(args).dof; ++f) {
            std::get<0>(args)(f,Ix) = val[f];
//            assert(!std::isnan(std::get<0>(args)(f,Ix)) && "Nan encountered while checking data.");
        }
    }

    template <class Args, bool indexed_ = indexed, typename std::enable_if<
                                   indexed_, bool>::type
                                   is_indexed = true
             >
    static inline void eval(index_t Ix, const pos_t<d> & modes,
                            const Args & args) {
        data_t<dof> val;
        std::get<1>(args).evalat_idx(modes, val);
        for(unsigned int f = 0; f < std::get<0>(args).dof; ++f) {
            std::get<0>(args)(f,Ix) = val[f];
        }
    }

    template <class Args>
    static void pre(const Args & args) {
        static_assert(std::tuple_size<Args>::value == 2,
                      "Tuple must have size 2!");

        assert(std::get<0>(args).dof >= 1 && "Unallocated Vec.");

    }

    template <class Args>
    static void post(const Args & args) {
        std::get<0>(args).scaling_factor = 1.;
    }
};


template <unsigned int d, unsigned int dof, bool indexed = false>
struct eval_functional_kern {
    static constexpr unsigned int dim = d;

    template <class Args, bool indexed_ = indexed>
    static inline void eval(index_t Ix, const pos_t<d> & modes,
                            const Args & args) {
        using vec_t = typename std::remove_reference<
                typename std::tuple_element<0,Args>::type
        >::type;
        using coord_t = typename vec_t::grid_t::coord_t;

        coord_t coord = std::get<0>(args).grid_p->coord(modes);

        data_t<dof> val;
        val = std::get<1>(args)(coord);
        for(unsigned int f = 0; f < std::get<0>(args).dof; ++f) {
            std::get<0>(args)(f,Ix) = val[f];
        }
    }

    template <class Args>
    static void pre(const Args & args) {
        static_assert(std::tuple_size<Args>::value == 2,
                      "Tuple must have size 2!");

        assert(std::get<0>(args).dof >= 1 && "Unallocated Vec.");

    }

    template <class Args>
    static void post(const Args & args) {
        std::get<0>(args).scaling_factor = 1.;
    }
};


} // END NAMESPACE KERNEL
} // END NAMESPACE SPHINX
