/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "sphinx_includes.hpp"


template <typename T, unsigned int d>
void scale(HostVec<T, d, false, false> & in, scalar_t alpha);

namespace sphinx {
namespace kernel {

using namespace sphinx::types;

#include "operators/mpi/redistribute.hpp"

// Zero Pads arg 0 onto arg 1
template <unsigned int d>
struct truncate_kern {
    static constexpr unsigned int dim = d;

    template <class Args>
    static inline void eval(index_t Ix, const pos_t<dim> & logical_modes,
                            const pos_t<dim> &,
                            const Args & args) {
        using Vec_t = typename std::remove_reference<typename std::tuple_element<0, Args>::type>::type;

        index_t Ix_buf = std::get<0>(args).outerIndexTruncateBuffer(logical_modes, std::get<1>(args));

        for(unsigned int f = 0; f < dim; ++f) {
            std::get<0>(args)(f, Ix) = std::complex<double>(redistribute<Vec_t, d>::recvbuffer(f,2*Ix_buf),
                    redistribute<Vec_t, d>::recvbuffer(f,2*Ix_buf+1));
        }
    }

    template <class Args>
    static void pre(const Args & args) {
        static_assert(std::tuple_size<Args>::value == 2, "Tuple must have size 2!");
//        using complex_t = std::complex<double>;
        using Vec_t = typename std::remove_reference<typename std::tuple_element<0, Args>::type>::type;

        assert(std::get<1>(args).dof <= std::get<0>(args).dof
               && "Not eough dofs in receiving vector");
        assert(std::get<1>(args).dof >= 1 && "Need at least one dof");

        const Vec_t& in = std::get<1>(args);
        Vec_t& out = std::get<0>(args);

        redistribute<Vec_t, d>::start(in, out);

        redistribute<Vec_t, d>::recv_wait();

    }

    template <class Args>
    static void post(const Args & args) {
        using Vec_t = typename std::remove_reference<typename std::tuple_element<0, Args>::type>::type;

        std::get<0>(args).scaling_factor = std::get<1>(args).scaling_factor;

        redistribute<Vec_t, d>::send_wait();

        // This makes sure that the padded data is consistent
        auto factor =(double) prod( std::get<0>(args).grid_p->timeN ) / (double) prod( std::get<1>(args).grid_p->timeN );
        scale( std::get<0>(args), factor);
    }

};

} // END NAMESPACE KERNEL
} // END NAMESPACE SPHINX