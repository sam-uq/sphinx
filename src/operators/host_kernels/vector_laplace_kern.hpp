/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "sphinx_includes.hpp"

#include "utilities/utilities.hpp"

namespace sphinx {
namespace kernel {

using namespace sphinx::types;


template <unsigned int d>
struct vector_laplace_kern {
    static constexpr unsigned int dim = d;

    template <class Args>
    static inline void eval(index_t Ix, const pos_t<d> & real_modes,
                            const Args & args) {
        // Remove low frequencies from laplace
//        if(abs(real_modes) < std::get<2>(args)) {
//            for(unsigned int f = 0; f < std::get<1>(args).dof; ++f) {
//                std::get<1>(args)(f,Ix) = std::complex<double>(0,0);
//            }
//            return;
//        }

        double N = std::get<1>(args).grid_p->timeN[0];

        double factor = -std::get<3>(args) * N * std::max(
                    std::pow(static_cast<double>(std::sqrt(normp(real_modes)))/N,2.) -1./N,0.);
//        double factor = -std::get<3>(args)*4.*M_PI*M_PI*normp(real_modes);

        for(unsigned int f = 0; f < std::get<1>(args).dof; ++f) {
            std::get<1>(args)(f,Ix) = factor * std::get<0>(args)(f,Ix);
        }
    }

    template <class Args>
    static void pre(const Args & args) {
        static_assert(std::tuple_size<Args>::value == 4, "Tuple must have size 4!");

        static_assert(std::numeric_limits<arg_type<2,Args>>::is_specialized,
                "Argument 2 of Args must be an integer");
        static_assert(std::numeric_limits<arg_type<3,Args>>::is_specialized,
                "Argument 3 of Args must be a real number");

        assert(std::get<0>(args).dof >= dim && "Input vector has not enough dof");
        assert(std::get<1>(args).dof >= dim && "Ouput vector has not enough dof");
    }

    template <class Args>
    static void post(const Args & args) {
        std::get<1>(args).scaling_factor = std::get<0>(args).scaling_factor;
    }
};

} // END NAMESPACE KERNEL
} // END NAMESPACE SPHINX