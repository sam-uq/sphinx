/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
//! \file alias_kern.hpp
//! \author Filippo Leonardi
//! \date 2017-01-01
//! \brief Contains CPU kernel for anti-aliasing.
//!
//!

#pragma once

#include "sphinx_includes.hpp"

namespace sphinx {
namespace kernel {

using namespace sphinx::types;

//! \brief Kernel specifying the anti-aliasing operation.
//!
//! This Kernel sets to zero the portion of the spectrum that has frequencies higher than the
//! requested one.
//! This kernel takes two arguments from `Args`:
//! - args[0]: a grid-vector: input spectrum \f$\{{\vec{\hat{u}}}_{\vec{k}}\}_{\vec{k}}\f$,
//! to be aliased, overwritten as output;
//! - args[1]: frequency (a scalar) \f$K\f$ after which every mode is set to zero
//! (i.e. if \f$\vec{k}\f$ is the current Fourier mode
//! then \f$ \hat{u}_{\vec{k}} = 0\f$ if \f$| \vec{k} |_\infty > K\f$).
//!
//! The Tuple of `Args` is of size at least 2, arguments with index >= 2 (if any) are ignored.
//! \tparam d Number of space dimensions.
template <unsigned int d>
struct alias_kern {
    static constexpr unsigned int dim = d; //!< Grab the dimension

    //! \brief Actual operation performed for each loop iteration.
    //!
    //!
    //!
    //! TODO: make sure loop is unrolled.
    //! TODO: improve smoothing with cutoff function.
    //! \tparam        Args        Tuple as in class description.
    //! \param[in]     Ix          Outer-index to access values inside array, used to grab correct mode from arguments.
    //! \param[in]     real_modes  Real modes: vector of size `dim` containing the current Fourier mode.
    //! \param[in,out] args        Arguments as in class description.
    template <class Args>
    static inline void eval(index_t Ix,
                            const pos_t<dim> & real_modes,
                            const Args & args) {
        // truncate is true if the vector real_modes is outside the box [-k,k]^dim
        bool truncate = false;
        for(unsigned int f = 0; f < dim; ++f) {
            truncate |= std::abs(real_modes[f]) > std::get<1>(args);
        }

        if( truncate  ) {
            // Set each component to zero if we are outside of the box [-k,k]^d
            for(unsigned int f = 0; f < std::get<0>(args).dof; ++f) {
                std::get<0>(args)(f, Ix) = std::complex<double>(0,0);
            }
        }
    }

    //! \brief Perform pre-checks.
    //!
    //! Minimal check that Args is valid and args works.
    //! TODO: some static assert to prevent misuse of kernel.
    //! \tparam  Args Tuple as in class description.
    //! \param   args Arguments as in class description.
    template <class Args>
    static void pre(const Args & args) {
        static_assert(std::tuple_size<Args>::value == 2,
                      "Tuple must have size 2");

        assert(std::get<0>(args).dof >= 1 && "Need at least one dof");
    }

    //! \brief Perform post routines.
    //!
    //! Does nothing.
    //! \tparam  Args Tuple as in class description.
    //! \param   args Arguments as in class description.
    template <class Args>
    static void post(const Args &) {

    }
};

} // END NAMESPACE KERNEL
} // END NAMESPACE SPHINX