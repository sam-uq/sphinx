/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "sphinx_includes.hpp"

namespace sphinx {
namespace kernel {

using namespace sphinx::types;

template <unsigned int d>
struct anti_curl_kern {
    static constexpr unsigned int dim = d;

    template <class Args>
    static inline void eval(index_t Ix, const pos_t<2> & real_modes,
                            const Args & args) {
        using complex_t = std::complex<double>;

        if( real_modes == 0l ) {
            std::get<1>(args)(0,Ix) = 0;
            std::get<1>(args)(1,Ix) = 0;
            return;
        }
        double factor = 0;
        for(unsigned int f = 0; f < dim; ++f) {
            factor += real_modes[f]*real_modes[f];
        }
        factor *= 2*M_PI;

        std::get<1>(args)(0,Ix) = complex_t(0, -real_modes[1] / factor) * std::get<0>(args)(0,Ix);
        std::get<1>(args)(1,Ix) = complex_t(0, real_modes[0] / factor) * std::get<0>(args)(0,Ix);
    }

    template <class Args>
    static inline void eval(index_t, const pos_t<3> &,
                            const Args &) {
//        using complex_t = std::complex<double>;
        throw NotImplementedException();
    }

    template <class Args>
    static void pre(const Args & args) {
        static_assert(std::tuple_size<Args>::value == 2, "Tuple must have size 2!");

        assert(std::get<0>(args).dof >= curl_dim(dim) && "Input vector has not enough dof");
        assert(std::get<1>(args).dof >= dim && "Ouput vector has not enough dof");
    }

    template <class Args>
    static void post(const Args & args) {
        std::get<1>(args).scaling_factor = std::get<0>(args).scaling_factor;
    }
};

} // END NAMESPACE KERNEL
} // END NAMESPACE SPHINX