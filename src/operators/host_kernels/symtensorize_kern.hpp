/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "sphinx_includes.hpp"

namespace sphinx {
namespace kernel {

using namespace sphinx::types;

#include "../../utilities/utilities.hpp"

template <unsigned int d>
struct symtensorize_kern {
    static constexpr unsigned int dim = d;

    template <class Args>
    static inline void eval(index_t Ix, const pos_t<2> &,
                            const Args & args) {
        auto r1 = std::get<0>(args)(0, Ix);
        auto r2 = std::get<0>(args)(1, Ix);

        double fac = std::get<0>(args).scaling_factor * std::get<0>(args).scaling_factor;
        std::get<1>(args)(0, Ix) = r1*r1 / fac;
        std::get<1>(args)(1, Ix) = r1*r2 / fac;
        std::get<1>(args)(2, Ix) = r2*r2 / fac;
    }

    template <class Args>
    static inline void eval(index_t Ix, const pos_t<3> &,
                            const Args & args) {
        auto r1 = std::get<0>(args)(0, Ix) / std::get<0>(args).scaling_factor;
        auto r2 = std::get<0>(args)(1, Ix) / std::get<0>(args).scaling_factor;
        auto r3 = std::get<0>(args)(2, Ix) / std::get<0>(args).scaling_factor;

        std::get<1>(args)(0, Ix) = r1*r1;
        std::get<1>(args)(1, Ix) = r1*r2;
        std::get<1>(args)(2, Ix) = r2*r2;
        std::get<1>(args)(3, Ix) = r1*r3;
        std::get<1>(args)(4, Ix) = r2*r3;
        std::get<1>(args)(5, Ix) = r3*r3;
    }

    template <class Args>
    static void pre(const Args & args) {
        static_assert(std::tuple_size<Args>::value == 2, "Tuple must have size 2!");

        assert(std::get<0>(args).dof >= dim && "Incompatible dimensions!");
        assert(std::get<1>(args).dof >= sym_tensor_dim(dim) && "Incompatible dimensions!");

    }

    template <class Args>
    static void post(const Args & args) {
        std::get<1>(args).scaling_factor = 1.;
    }

//    double fac;
};

} // END NAMESPACE KERNEL
} // END NAMESPACE SPHINX