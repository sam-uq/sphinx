/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "sphinx_includes.hpp"

namespace sphinx {
namespace kernel {

using namespace sphinx::types;

//! \file Implements all norms based on openmp reduction and MPI reduction

//! \brief Class implementing a standard lp norm
template <unsigned int d>
struct norm_kern {
    static constexpr unsigned int dim = d;

    //! Expose vector type used by the kernel
    template<class SuperType>
    using VecType = SuperType;

    template <class Args, class T>
    static inline void eval(index_t Ix, const pos_t<dim> &,
                            const Args & args, T & var) {
        double temp = 0;
        for(unsigned int f = 0; f < std::get<0>(args).dof; ++f) {
            auto comp = std::abs(std::get<0>(args)(f, Ix));
            temp += comp * comp;
        }
        var += std::pow(temp / fac, std::get<1>(args) / 2.);

    }

    template <class Args, class T>
    static void pre(const Args & args, T &) {
        static_assert(std::tuple_size<Args>::value == 2, "Tuple must have size 2!");

        fac = std::get<0>(args).scaling_factor*std::get<0>(args).scaling_factor;
    }

    template <class Args, class T>
    static T post(const Args & args, T & loc_norm) {

        auto& in = std::get<0>(args);

//#if USE_MPI
        scalar_t glob_norm;
        MPI_Allreduce(&loc_norm, &glob_norm, 1,
                      MPI_DOUBLE, MPI_SUM, std::get<0>(args).grid_p->comm.comm);

        return pow(glob_norm * in.grid_p->dx * in.grid_p->dy * in.grid_p->dz,
                   1 / std::get<1>(args));
//#else
//        return pow(loc_norm * in.grid_p->dx * in.grid_p->dy, 1 / std::get<1>(args));
//#endif
    }

    static double fac;
};

//! \brief Class implementing a standard l infty norm
//! Contains static members eval, pre and post and is applied using apply_kern_redutcion_max
//!
//!
template <unsigned int d>
struct inf_norm_kern {
    static constexpr unsigned int dim = d;

    template <class Args, class T>
    static inline void eval(index_t Ix, const pos_t<dim> &,
                            const Args & args, T & var) {
        double temp = 0;
        for(unsigned int f = 0; f < std::get<0>(args).dof; ++f) {
            auto comp = std::abs(std::get<0>(args)(f, Ix));
            temp += comp;
        }
        var = std::max(std::sqrt(temp), var);
    }

    template <class Args, class T>
    static void pre(const Args & args, T &) {
        static_assert(std::tuple_size<Args>::value == 1, "Tuple must have size 1!");

        fac = std::get<0>(args).scaling_factor;
    }

    template <class Args, class T>
    static T post(const Args & args, T & loc_norm) {
//#if USE_MPI
        scalar_t glob_norm;
        MPI_Allreduce(&loc_norm, &glob_norm, 1,
                      MPI_DOUBLE, MPI_MAX, std::get<0>(args).grid_p->comm.comm);

        return glob_norm / fac;
//#else
//        return sqrt(var / fac);
//#endif
    }

    static double fac;
};

template <unsigned int d>
double norm_kern<d>::fac = 1;

template <unsigned int d>
double inf_norm_kern<d>::fac = 1;

} // END NAMESPACE KERNEL
} // END NAMESPACE SPHINX