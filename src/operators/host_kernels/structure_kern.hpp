/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "sphinx_includes.hpp"

#include "containers/fftw_time_vec.hpp"

#include "path_utilities.hpp"

namespace sphinx {
namespace kernel {

using namespace sphinx::types;

//! \file Implements computation of structure functions based on OpenMP reduction and MPI reduction.

template<unsigned int dof, class V>
inline int periodic_remap_cpu(const V &vec, int index) {
    int size = vec.grid_p->timeN[dof];

    return (index >= size)
           ? (index - size)
           : ((index < 0)
              ? index + size
              : index
           );
}

inline double structure_normp_cpu(double u_1, double u_2,
                                  double uh_1, double uh_2, double q, double p) {
    return std::pow((u_1 - uh_1) * (u_1 - uh_1) + (u_2 - uh_2) * (u_2 - uh_2), p / q); // only q = 2
}

struct structure_options {
    int R;
    int maxstep;
    bool verbose;
    bool pixely;
};

template<unsigned int d>
class structure_super_kern {
public:
    static std::vector<int> pathx, pathy;

    static structure_options cache;
};

//! \brief Class implementing the computation of a structure function.
//!
//! \tparam d
template<unsigned int d>
struct structure_kern : public structure_super_kern<d> {
    //! Alias for dimensions of the kernel.
    static constexpr unsigned int dim = d;

    //! Expose vector type used by the kernel
    template<class SuperType>
    using VecType = SuperType;

    template<class Args, class T>
    static inline void eval(index_t Ix, const pos_t<dim> &pos,
                            const Args &args, T &var) {
        using super = structure_super_kern<d>;

//        double temp = 0;
//        for(unsigned int f = 0; f < std::get<0>(args).dof; ++f) {
//            auto comp = std::abs(std::get<0>(args)(f, Ix));
//            temp += comp * comp;
//        }
//        var += std::pow(temp / fac, std::get<1>(args) / 2.);
        const auto &in = std::get<0>(args);
        const auto p = std::get<1>(args);

        auto u_1 = in(0, Ix);
        auto u_2 = in(1, Ix);

        // Vertical boundaries
        for (int ix = 0; ix < (int) super::pathx.size(); ++ix) {
            // Get point
            int h1 = super::pathx[ix];
            int h2 = super::pathy[ix];

            pos_t<dim> offset = {
                    periodic_remap_cpu<0>(in, pos[0] + h1),
                    periodic_remap_cpu<1>(in, pos[1] + h2)
            };

            // Local structure
            int Ixh = in.outerIndex(offset);

            auto uh_1 = in(0, Ixh);
            auto uh_2 = in(1, Ixh);

            var += structure_normp_cpu(u_1, u_2, uh_1, uh_2, 2., p);
//            std::cout << u_1 << "," << u_2 << "   :  " << uh_1 << "," << uh_2
//                      << " ##  "<< var << "\n";
        }
    }

    //!
    //! \tparam Args
    //! \tparam T
    //! \param args
    template<class Args, class T>
    static void pre(const Args &args, T &) {

        using super = structure_super_kern<d>;

        std::tie(super::pathx, super::pathy) = get_lattice_approx<std::vector<int>>(super::cache.R, super::cache.pixely,
                                                                                    super::cache.maxstep,
                                                                                    super::cache.verbose);

        static_assert(std::tuple_size<Args>::value == 2, "Tuple must have size 2!");

//        bool x = std::remove_reference<decltype(std::get<0>(args))>::type::is_time_vector();
//        static_assert(sphinx::is_time_vector<typename std::remove_reference<decltype(std::get<0>(args))>::type>::value,
//                      "First argument must be a phase space vector.");

        static_assert(std::is_arithmetic<typename std::remove_reference<decltype(std::get<1>(args))>::type>::value,
                      "Second argument must be arithmetic.");
    }

    //!
    //! \tparam Args
    //! \tparam T
    //! \param args
    //! \param loc_norm
    //! \return
    template<class Args, class T>
    static T post(const Args &args, T &loc_norm) {

        auto &in = std::get<0>(args);

        double fac = std::get<0>(args).scaling_factor;
        double p = std::get<1>(args);

        int size = (int) structure_super_kern<d>::pathx.size();

        scalar_t glob_norm;
#if USE_MPI
        MPI_Allreduce(&loc_norm, &glob_norm, 1,
                      MPI_DOUBLE, MPI_SUM, std::get<0>(args).grid_p->comm.comm);

#else
        glob_norm = loc_norm;
#endif

        glob_norm /= prod(in.grid_p->timeN);

        // Average all cells in int-dh
        glob_norm /= size;

        // Handle scaling factor difference
        glob_norm /= std::pow(fac, p);

        return glob_norm;
    }
};


//! \brief Class implementing the computation of a structure function.
//!
//! \tparam d
template<unsigned int d>
struct structure_ghost_kern : public structure_super_kern<d> {
    //! Alias for dimensions of the kernel.
    static constexpr unsigned int dim = d;

    //! Exposes super type form arguments
    template<class SuperType>
    using VecType = typename std::remove_reference<SuperType>::type::VectorType;

    template<class Args, class T>
    static inline void eval(index_t Ix, const pos_t<dim> &pos,
                            const Args &args, T &var) {
        auto &in = std::get<0>(args);
        const auto p = std::get<1>(args);

        using V = VecType<decltype(in)>;

        V &vec = in;

        auto u_1 = vec(0, Ix);
        auto u_2 = vec(1, Ix);

        // Vertical boundaries
        for (int ix = 0; ix < (int) structure_super_kern<d>::pathx.size(); ++ix) {
            // Get point
            int h1 = structure_super_kern<d>::pathx[ix];
            int h2 = structure_super_kern<d>::pathy[ix];

            pos_t<2> offset = {
                    pos[0] + h1,
                    pos[1] + h2
            };

            // Local structure
//            int Ixh = vec.outerIndex(offset);

            auto uh_1 = in.ghost_access_throw(0, offset);
            auto uh_2 = in.ghost_access_throw(1, offset);

            var += structure_normp_cpu(u_1, u_2, uh_1, uh_2, 2., p);
        }
    }

    //!
    //! \tparam Args
    //! \tparam T
    //! \param args
    template<class Args, class T>
    static void pre(const Args &args, T &) {
        using super = structure_super_kern<d>;

        auto & ghost = std::get<0>(args);

        ghost.set_size(super::cache.R);
        std::cout << "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa   " << super::cache.R;

        ghost.update_begin();

        std::tie(super::pathx, super::pathy) = get_lattice_approx<std::vector<int>>(super::cache.R, super::cache.pixely,
                                                                                    super::cache.maxstep,
                                                                                    super::cache.verbose);

        static_assert(std::tuple_size<Args>::value == 2, "Tuple must have size 2!");

//        bool x = std::remove_reference<decltype(std::get<0>(args))>::type::is_time_vector();
//        static_assert(sphinx::is_time_vector<typename std::remove_reference<decltype(std::get<0>(args))>::type>::value,
//                      "First argument must be a phase space vector.");

        static_assert(std::is_arithmetic<typename std::remove_reference<decltype(std::get<1>(args))>::type>::value,
                      "Second argument must be arithmetic.");

        ghost.update_end();
    }

    //!
    //! \tparam Args
    //! \tparam T
    //! \param args
    //! \param loc_norm
    //! \return
    template<class Args, class T>
    static T post(const Args &args, T &loc_norm) {

        auto &in = std::get<0>(args);
        const auto p = std::get<1>(args);

        using V = VecType<decltype(in)>;
        V &vec = in;

        double fac = vec.scaling_factor;

        int size = (int) structure_super_kern<d>::pathx.size();

        scalar_t glob_norm;
#if USE_MPI
        MPI_Allreduce(&loc_norm, &glob_norm, 1,
                      MPI_DOUBLE, MPI_SUM, vec.grid_p->comm.comm);

#else
        glob_norm = loc_norm;
#endif

        glob_norm /= prod(vec.grid_p->timeN);

        // Average all cells in int-dh
        glob_norm /= size;

        // Handle scaling factor difference
        glob_norm /= std::pow(fac, p);

        return glob_norm;
    }

};

template<unsigned int d>
std::vector<int> structure_super_kern<d>::pathx;

template<unsigned int d>
std::vector<int> structure_super_kern<d>::pathy;

template<unsigned int d>
structure_options structure_super_kern<d>::cache;

} // END NAMESPACE KERNEL
} // END NAMESPACE SPHINX