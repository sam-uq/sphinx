/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "utilities/mpi.hpp"
#include "utilities/debug.hpp"

using namespace sphinx;

namespace sphinx {
namespace operators {

//! \namespace operators Namespace containing all operators.

//! \brief Virtual base class providing interface for fft
//! Must be inherited to implement specific fft operator.
//! \tparam T type of plan if required
//! \tparam d dimensions of FFT
template <class T, unsigned int d, bool contiguous, bool interlaced>
class FFT {
public:

    //! Plan type: vector of plans (for interlaced, or just a plan?=
    using plan_t = typename std::conditional<
        interlaced,
        std::vector<T>,
        T
    >::type;
    //! Has plan flag type: vector of plans (for interlaced, or just a plan?=
    using has_plan_t = typename std::conditional<
        interlaced,
        std::vector<bool>,
        bool
    >::type;

    //! \brief Constructor from a shared pointer, available for backward compatibility
    //! Grabs pointer to grid and initialize basic data from grid
    FFT(std::shared_ptr<const Grid<d, contiguous, interlaced>> grid_p) : grid_p(grid_p.get()){ // EVIL
        transform_size = prod(this->grid_p->timeN);
        comm = grid_p->comm;
        init();
    }

    //! \brief Construct fundamental variables
    //! Grabs a C-pointer and construct some data from the grid pointer
    FFT(Grid<d, contiguous, interlaced> * grid_p) : grid_p(grid_p){
        transform_size = prod(this->grid_p->timeN);
        comm = grid_p->comm;
        init();
    }

    virtual void set_planning_time(double /* time */) {

    }

    virtual void load_wisdom() {
        debug::warning("Using a FFT without Wisdom, and asking to load Wisdom...",
                mpi::world, mpi::Policy::Collective, debug::option);
    }

    virtual void save_wisdom() {
        debug::warning("Using a FFT without Wisdom, and asking to save Wisdom...",
                mpi::world, mpi::Policy::Collective, debug::option);
    }

    //!
    //!
    //! A string representing the type of planning
    void set_planning_mode(std::string /* mode_ */) {
        debug::warning("Using a FFT without planning mode setup...",
                mpi::world, mpi::Policy::Collective, debug::option);
    }

    //! \brief Initialize FFT. Contiguous variant.
    //!
    //! Set up the FFT.
    //!
    //! \tparam contiguous_
    template <bool contiguous_ = contiguous, typename std::enable_if<contiguous_ == true, bool>::type = false>
    void init(void) {

    }

    //! \brief Initialize FFT. Uncontiguous variant.
    //!
    //! Set up the FFT.
    //!
    //! \tparam contiguous_
    template <bool contiguous_ = contiguous, typename std::enable_if<contiguous_ == false, int>::type = 1>
    void init(void) {
        this->has_plan = false;
    }

protected:
    //! Grid associated to this FFT, non-owned pointer.
    const Grid<d, contiguous, interlaced>* grid_p;

    //! Forward plan, backward plan, must be created before usage
    plan_t forward_plan, backward_plan;

    //! Is the FFT acting in place (not implemented)
    bool in_place = false;
    //! Has the plan been generated?
    has_plan_t has_plan;

    //! Number of modes (total) of the transform
    index_t transform_size = -1;

    //! Communicator used (for parallel FFT)
    mpi::IntraDomainComm comm;
};

} // END namespace sphinx
} // END namespace operators
