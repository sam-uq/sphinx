/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "containers/cuda_device_time_vec.hpp"
#include "containers/cuda_device_freq_vec.hpp"

#include "fft.hpp"

#include "utilities/types.hpp"
#include "utilities/utilities.hpp"

#include "operators/device_functions/device_functions.hpp"

namespace sphinx {
namespace operators {

template <unsigned int d, bool contiguous, bool interlaced>
class CUDAFFT : public FFT<cufftHandle, d, contiguous, interlaced> {
public:
    using Grid_t = Grid<d, contiguous, interlaced>;
    using FFT_t = FFT<cufftHandle, d, contiguous, interlaced>;

    CUDAFFT(std::shared_ptr<const Grid_t> grid_p) : FFT_t(grid_p) { }

    CUDAFFT(Grid_t * grid_p) : FFT_t(grid_p) { }

    //!
    //!
    //! See also http://docs.nvidia.com/cuda/cufft/#axzz3saoIvCWa
    void plan(const CUDADeviceTimeVec<d, contiguous, interlaced> &,
              const CUDADeviceFreqVec<d, contiguous, interlaced> &) {
        static_assert(contiguous == false && interlaced == false, "Not yet implemented!");

        if(this->has_plan) return;

        profiler::start_stage("CUDAFFT::plan");

        if( this->grid_p->comm.size > 1 ) {
            debug::warning("Creating a CUDA+MPI plan, highly experimental, inefficient or not available"
                           "at all.", this->grid_p->comm);
            plan_mpi();
        } else {
            if(d == 2) {
                fftErrchk(cufftPlan2d(&this->forward_plan,
                                      this->grid_p->timeN[0], this->grid_p->timeN[1], CUFFT_D2Z) );
                fftErrchk(cufftPlan2d(&this->backward_plan,
                                      this->grid_p->timeN[0], this->grid_p->timeN[1], CUFFT_Z2D) );
            }else {
                fftErrchk(cufftPlan3d(&this->forward_plan,
                                      this->grid_p->timeN[0],
                          this->grid_p->timeN[1], this->grid_p->timeN[2],
                        CUFFT_D2Z) );
                fftErrchk(cufftPlan3d(&this->backward_plan,
                                      this->grid_p->timeN[0],
                          this->grid_p->timeN[1], this->grid_p->timeN[2],
                        CUFFT_Z2D) );
            }

            this->has_plan = true;
        }


        profiler::end_stage();
    }

    //! Plan a CUFFT transform in distributed domain context
    //!
    //!
    void plan_mpi() {
        if(d == 2) {
            fftErrchk(cufftPlan1d(&this->forward_plan,
                                  this->grid_p->timeN[1], CUFFT_D2Z, 1) );
            fftErrchk(cufftPlan1d(&this->backward_plan,
                                  this->grid_p->timeN[1], CUFFT_Z2D, 1) );
        }else {
            fftErrchk(cufftPlan2d(&this->forward_plan,
                                  this->grid_p->timeN[1], this->grid_p->timeN[2],
                    CUFFT_D2Z) );
            fftErrchk(cufftPlan2d(&this->backward_plan,
                                  this->grid_p->timeN[1], this->grid_p->timeN[2],
                    CUFFT_Z2D) );
        }
        fftErrchk(cufftPlan1d(&this->forward_plan_transpose,
                              this->grid_p->timeN[0], CUFFT_Z2Z, 1) );
        fftErrchk(cufftPlan1d(&this->backward_plan_transpose,
                              this->grid_p->timeN[0], CUFFT_Z2Z, 1) );

        this->has_plan = true;
    }

    //! \brief Performs forward FFT
    //! If there is no plan plan is made, data may be destroyed.
    //! \param[in] uv vector in time space, may be destroyed
    //! \param[out] fr vector in frequency space, transform of uv
    //! TODO: add safety check
    void forward(const CUDADeviceTimeVec<d, contiguous, interlaced> & uv,
                 CUDADeviceFreqVec<d, contiguous, interlaced> & fr) {
        profiler::start_stage("CUDAFFT::forward");

        if(!this->has_plan) {
            debug::warning("Plan was missing, making it for you, WANRNING: data WILL be destroyed!",
                           this->comm);
            plan(uv, fr);
        }

        if( this->grid_p->comm.size > 1 ) {
            mpi_forward(uv, fr);
        } else {
            for(unsigned int f = 0; f < std::min(uv.dof, fr.dof); ++f) {
                fftErrchk(cufftExecD2Z(this->forward_plan, uv._data[f],
                                       (cuda_device_freq_t*)(fr._data[f]) ));
            }
            fr.scaling_factor = uv.scaling_factor;
        }

        profiler::end_stage();
    }


    //!
    //! \param fr
    //! \param uv
    void backward(const CUDADeviceFreqVec<d, contiguous, interlaced> & fr,
                  CUDADeviceTimeVec<d, contiguous, interlaced> & uv) {
        profiler::start_stage("CUDAFFT::backward");

        if(!this->has_plan) {
            debug::warning("Plan was missing, making it for you, WANRNING: data WILL be destroyed!",
                           this->comm);
            plan(uv, fr);
        }


        if( this->grid_p->comm.size > 1 ) {
            mpi_backward(fr, uv);
        } else {
            for(unsigned int f = 0; f < std::min(uv.dof, fr.dof); ++f) {
                fftErrchk(cufftExecZ2D(this->backward_plan,
                                       (cuda_device_freq_t*)(fr._data[f]), uv._data[f]));
            }
            uv.scaling_factor = fr.scaling_factor * this->transform_size;
        }

        profiler::end_stage();
    }

    //!
    void mpi_forward(const CUDADeviceTimeVec<d, contiguous, interlaced> & uv,
                     CUDADeviceFreqVec<d, contiguous, interlaced> & fr) {

        assert(d == 2 && "2D only!");

        for(unsigned int f = 0; f < std::min(uv.dof, fr.dof); ++f) {
            std::ptrdiff_t rowsize = this->grid_p->timeNpad[1]+2; // 2d in 3d
            unsigned rowp = 0;
            for(unsigned row = 0; row < this->grid_p->local_n0; row+=rowsize) {
                fftErrchk(cufftExecD2Z(this->forward_plan, (cuda_device_time_t*)&(uv._data[f][row]),
                          (cuda_device_freq_t*)&(fr._data[f][rowp]) ));
                rowp += rowsize + 1;
            }
        }

       operators::transpose(fr, operators::Direction::Forward);

        for(unsigned int f = 0; f < std::min(uv.dof, fr.dof); ++f) {
            std::ptrdiff_t rowsize = this->grid_p->freqN[0]; // 1d in 3d
            for(unsigned row = 0; row < this->grid_p->local_n0_trsp; row+=rowsize) {
                fftErrchk(cufftExecZ2Z(this->forward_plan_transpose,
                                       (cuda_device_freq_t*)&(fr._data[f][row]),
                                       (cuda_device_freq_t*)&(fr._data[f][row]),
                                       CUFFT_FORWARD));
            }
        }

        fr.scaling_factor = uv.scaling_factor;

    }

    //!
    //! \param fr
    //! \param uv
    void mpi_backward(const CUDADeviceFreqVec<d, contiguous, interlaced> & fr,
                      CUDADeviceTimeVec<d, contiguous, interlaced> & uv) {

        for(unsigned int f = 0; f < std::min(uv.dof, fr.dof); ++f) {
            std::ptrdiff_t rowsize = this->grid_p->freqN[0]; // 1d in 3d
            for(unsigned row = 0; row < this->grid_p->local_n0_trsp; row+=rowsize) {
                fftErrchk(cufftExecZ2Z(this->backward_plan_transpose,
                                       (cuda_device_freq_t*)&(fr._data[f][row]),
                          (cuda_device_freq_t*)&(fr._data[f][row]),
                        CUFFT_INVERSE));
            }
        }

        operators::transpose2(const_cast< CUDADeviceFreqVec<d, contiguous, interlaced> &>(fr), operators::Direction::Backward);

        for(unsigned int f = 0; f < std::min(uv.dof, fr.dof); ++f) {
            std::ptrdiff_t rowsize = this->grid_p->timeNpad[1]+2; // 2d in 3d
            unsigned rowp = 0;
            for(unsigned row = 0; row < this->grid_p->local_n0; row+=rowsize) {
                fftErrchk(cufftExecZ2D(this->backward_plan,
                                       (cuda_device_freq_t*)&(fr._data[f][rowp]),&(uv._data[f][row])));
                rowp += rowsize + 1;
            }
        }

        uv.scaling_factor = fr.scaling_factor * this->transform_size;
    }

private:
    //! Forward plan, backward plan, must be created before usage, for second step (ie transposed data)
    typename FFT_t::plan_t forward_plan_transpose, backward_plan_transpose;
};

} // END namespace operators
} // END namespace sphinx
