/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include <bitset>

#include "operators/fft/fft.hpp"

#include "containers/fftw_time_vec.hpp"
#include "containers/fftw_freq_vec.hpp"

#include "utilities/include_accfft.hpp"

namespace sphinx {
namespace operators {

//! \brief Interface to FFTW
//! Generates appropriate plans and allows forward/backward transforms
//! \tparam d dimension of the transform
template <unsigned int d, bool contiguous, bool interlaced>
class ACCFFT : public FFT<accfft_plan_gpu*, d, contiguous, interlaced> {
public:
    //! \brief Construct an FFTW solver by taking a reference to a grid, over
    //! which the solver is define
    //! \param[in] grid_p Shared pointer to a grid over which the FFT will
    //! operate
    ACCFFT(std::shared_ptr<const Grid<d, contiguous, interlaced>> grid_p)
        : FFT<accfft_plan_gpu*, d, contiguous, interlaced>(grid_p) { }

    ACCFFT(Grid<d, contiguous, interlaced> * grid_p)
        : FFT<accfft_plan_gpu*, d, contiguous, interlaced>(grid_p) {
    }

    ACCFFT(ACCFFT & other) = delete;

    //! \brief Delete plans (broken)
    //!
    ~ACCFFT() {
        dealloc();
    }

    template <bool interlaced_ = interlaced,
             typename std::enable_if<interlaced_, bool>::type = true>
    void dealloc() {
        for(unsigned int idx = 0; idx < this->has_plan.size(); ++idx) {
            this->has_plan[idx] = false;
            if(this->forward_plan[idx])
                accfft_destroy_plan_gpu(this->forward_plan[idx]);
        }

    }

    template <bool interlaced_ = interlaced,
              typename std::enable_if<!interlaced_, int>::type = 1>
    void dealloc() {
        if(!this->has_plan) return;
        this->backward_plan = nullptr;
        if(this->forward_plan)
            accfft_destroy_plan_gpu(this->forward_plan);
    }

    template <bool interlaced_ = interlaced,
             typename std::enable_if<interlaced_, bool>::type = true>
    void plan(const FFTWTimeVec<d, contiguous, interlaced> & tme,
              const FFTWFreqVec<d, contiguous, interlaced> & frq) {
        assert(tme.dof >= 1 && frq.dof >= 1 &&
               "Empty vectors cannot be use to plan FFT.");

        assert(tme.dof == frq.dof && "Not available.");

        long unsigned int idx = frq.dof - 1;

        if(this->has_plan.size() > idx && this->has_plan[idx]) {
            return;
        }

        this->forward_plan.resize(std::max(idx+1,
            this->forward_plan.size()), nullptr);
        this->backward_plan.resize(std::max(idx+1,
            this->backward_plan.size()), nullptr);
        this->has_plan.resize(std::max(idx+1,this->has_plan.size()), false);

        profiler::start_stage("ACCFFT::make_plans");

        assert(tme.dof == frq.dof &&
               "Interlaced data requires same dof on all vectors.");
        this->forward_plan[idx] =
                accfft_plan_many_dft_3d_r2c_gpu(this->grid_p->timeN.data(),
                   reinterpret_cast<double*>(tme._data),
                   reinterpret_cast<fftw_complex*>(frq._data),
                   this->grid_p->mm, nullptr);
        this->backward_plan[idx] = this->forward_plan[idx];


        if( this->forward_plan[idx] == NULL ||
                this->backward_plan[idx] == NULL ) {
            debug::error("Could not create a ACCFFT plan.", this->comm);
        } else {
            this->has_plan[idx] = true;
        }
        profiler::end_stage();
    }

    template <bool interlaced_ = interlaced,
              typename std::enable_if<!interlaced_, int>::type = 1>
    void plan(const CUDADeviceTimeVec<d, contiguous, interlaced> & tme,
              const CUDADeviceFreqVec<d, contiguous, interlaced> & frq) {
        assert(tme.dof >= 1 && frq.dof >= 1 &&
               "Empty vectors cannot be use to plan FFT.");
        if(this->has_plan) {
            return;
        }

        profiler::start_stage("ACCFFT::make_plans");

        std::array<std::ptrdiff_t, 3> S_ = this->grid_p->timeN;
        int S[] = {S_[0], S_[1], S_[2]};
        std::cout << S_[0] << " " << S_[1] << " " << S_[2] << std::endl;

        this->forward_plan = accfft_plan_dft_3d_r2c_gpu(S,
                    reinterpret_cast<double*>(tme._data[0]),
                    reinterpret_cast<double*>(frq._data[0]),
                    this->grid_p->acc_comm);
        this->backward_plan = this->forward_plan;

        if( this->forward_plan == NULL || this->backward_plan == NULL ) {
            debug::error("Could not create a FFTW plan.", this->comm);
        } else {
            this->has_plan = true;
        }
        profiler::end_stage();
    }

    template <bool interlaced_ = interlaced,
             typename std::enable_if<interlaced_, bool>::type = true>
    void forward(const CUDADeviceTimeVec<d, contiguous, interlaced> & tme,
                 CUDADeviceFreqVec<d, contiguous, interlaced> & frq){
        int idx = frq.dof - 1;

        if(this->has_plan.size() <= idx || !this->has_plan[idx]) {
            debug::warning(
            "Plan was missing, making it for you, "
            "WANRNING: data WILL be destroyed!",
                           this->comm);
            plan(tme, frq);
        }

        profiler::start_stage("ACCFFT::ffft");

        profiler::sync(tme.grid_p->comm.comm);
        assert(false && "Not implemented!");
        fftw_execute_dft_r2c(this->forward_plan[idx],
                             reinterpret_cast<double*>(tme._data),
                                 reinterpret_cast<fftw_complex*>(frq._data));
        frq.scaling_factor = tme.scaling_factor;
        profiler::end_stage();
    }

    template <bool interlaced_ = interlaced,
              typename std::enable_if<!interlaced_, int>::type = 1>
    void forward(const CUDADeviceTimeVec<d, contiguous, interlaced> & tme,
                 CUDADeviceFreqVec<d, contiguous, interlaced> & frq){
        if(!this->has_plan) {
            debug::warning("Plan was missing, making it for you, "
                           "WANRNING: data WILL be destroyed!",
                           this->comm);
            plan(tme, frq);
        }

        profiler::start_stage("ACCFFT::ffft");

        profiler::sync(tme.grid_p->comm.comm);

//        int f = 0;
        for(unsigned int f = 0; f < std::min(tme.dof, frq.dof); ++f) {
            accfft_execute_r2c_gpu(this->forward_plan,
                              reinterpret_cast<double*>(tme._data[f]),
                              reinterpret_cast<fftw_complex*>(frq._data[f]));
        }

        frq.scaling_factor = tme.scaling_factor;
        profiler::end_stage();
    }

    template <bool interlaced_ = interlaced,
             typename std::enable_if<interlaced_, bool>::type = true>
    void backward(const CUDADeviceFreqVec<d, contiguous, interlaced> & frq,
                  CUDADeviceTimeVec<d, contiguous, interlaced> & tme) {
        int idx = frq.dof - 1;

        if(this->has_plan.size() <= idx || !this->has_plan[idx]) {
            debug::warning("Plan was missing, making it for you, "
                            "WANRNING: data WILL be destroyed!", this->comm);
            plan(tme, frq);
        }
        profiler::start_stage("ACCFFT::bfft");

        profiler::sync(frq.grid_p->comm.comm);

        assert(false && "NYI");
        fftw_mpi_execute_dft_c2r(this->backward_plan[idx],
            reinterpret_cast<fftw_complex*>(frq._data),
                                 reinterpret_cast<double*>(tme._data));

        tme.scaling_factor = frq.scaling_factor * this->transform_size;
        profiler::end_stage();
    }

    template <bool interlaced_ = interlaced,
              typename std::enable_if<interlaced_ , int>::type = 1>
    void backward(const CUDADeviceFreqVec<d, contiguous, interlaced> & frq,
                  CUDADeviceTimeVec<d, contiguous, interlaced> & tme) {
        if(!this->has_plan) {
            debug::warning("Plan was missing, making it for you,"
                    "WANRNING: data WILL be destroyed!", this->comm);
            plan(tme, frq);
        }
        profiler::start_stage("ACCFFT::bfft");

        profiler::sync(frq.grid_p->comm.comm);

//        int f = 0;
        for(unsigned int f = 0; f < std::min(tme.dof, frq.dof); ++f) {
            accfft_execute_c2r_gpu(this->forward_plan,
                 reinterpret_cast<fftw_complex*>(frq._data[f]),
                 reinterpret_cast<double*>(tme._data[f]));
        }
        tme.scaling_factor = frq.scaling_factor * this->transform_size;
        profiler::end_stage();
    }
};

} // END namespace sphinx
} // END namespace operators

