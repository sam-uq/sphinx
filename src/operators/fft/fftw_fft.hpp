/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "config.hpp"
#include "fft.hpp"

#include "containers/fftw_time_vec.hpp"
#include "containers/fftw_freq_vec.hpp"

#include "utilities/include_fftw.hpp"

namespace sphinx {
namespace operators {

enum class FFTPlanMode {
    MEASURE = FFTW_MEASURE,
    PATIENT = FFTW_PATIENT,
    ESTIMATE = FFTW_ESTIMATE,
    EXHAUSTIVE = FFTW_EXHAUSTIVE,
    WISDOM_ONLY = FFTW_WISDOM_ONLY
};

inline std::string to_string(FFTPlanMode mode) {
    switch(mode) {
    case FFTPlanMode::MEASURE:
        return "Measure";
    case FFTPlanMode::PATIENT:
        return "Patient";
    case FFTPlanMode::ESTIMATE:
        return "Estimate";
    case FFTPlanMode::EXHAUSTIVE:
        return "Exhaustive";
    case FFTPlanMode::WISDOM_ONLY:
        return "Wisdom Only";
    }
}


inline unsigned int to_uint(FFTPlanMode mode) {
    return (unsigned int) mode;
}

inline FFTPlanMode fft_mode_from_string(std::string s) {
    if(s == "Measure") return FFTPlanMode::MEASURE;
    else if(s == "Patient") return FFTPlanMode::PATIENT;
    else if(s == "Estimate") return FFTPlanMode::ESTIMATE;
    else if(s == "Exhaustive") return FFTPlanMode::EXHAUSTIVE;
    else if(s == "WisdomOnly") return FFTPlanMode::WISDOM_ONLY;
    else return FFTPlanMode::MEASURE;
}

//! \brief Interface to FFTW
//! Generates appropriate plans and allows forward/backward transforms
//! \tparam d dimension of the transform
template <unsigned int d, bool contiguous, bool interlaced>
class FFTWFFT : public FFT<fftw_plan, d, contiguous, interlaced> {
public:
    //! \brief Construct an FFTW solver by taking a reference to a grid, over
    //! which the solver is define
    //! \param[in] grid_p Shared pointer to a grid over which the FFT will
    //! operate
    FFTWFFT(std::shared_ptr<const Grid<d, contiguous, interlaced>> grid_p)
        : FFT<fftw_plan, d, contiguous, interlaced>(grid_p) { }

    FFTWFFT(Grid<d, contiguous, interlaced> * grid_p)
        : FFT<fftw_plan, d, contiguous, interlaced>(grid_p) { }

    FFTWFFT(FFTWFFT & other) = delete;

    //! \brief Delete plans (broken)
    //!
    virtual ~FFTWFFT() {
        dealloc();
    }

    //!
    //! \tparam interlaced_
    template <bool interlaced_ = interlaced, typename std::enable_if<interlaced_ == true, bool>::type = true>
    void dealloc() {
        for(unsigned int idx = 0; idx < this->has_plan.size(); ++idx) {
            this->has_plan[idx] = false;
            if(this->forward_plan[idx]) fftw_destroy_plan(this->forward_plan[idx]);
            if(this->backward_plan[idx]) fftw_destroy_plan(this->backward_plan[idx]);
            this->has_plan = false;
        }

    }

    //!
    //! \tparam interlaced_
    template <bool interlaced_ = interlaced, typename std::enable_if<interlaced_ == false, int>::type = 1>
    void dealloc() {
        if(this->forward_plan && this->has_plan) fftw_destroy_plan(this->forward_plan);
        if(this->backward_plan && this->has_plan) fftw_destroy_plan(this->backward_plan);
        this->has_plan = false;
    }

    //!
    //! \param time
    virtual void set_planning_time(double time) {
        if(time > 0.0) {
            debug::warning("Limiting planning time to "
                           + std::to_string(time) + ".",
                           mpi::world);
            fftw_set_timelimit(time);
        }
    }

    //!
    //!
    virtual void load_wisdom() {
        debug::message("Importing wisdom...", mpi::world);
        int ret = fftw_import_wisdom_from_filename("fft.wisdom");
        if(ret != 1) {
            debug::warning("Failed importing Wisdom.", mpi::world);
        }
    }

    //!
    //!
    virtual void save_wisdom() {
        debug::message("Exporting wisdom...", mpi::world);
        int ret = fftw_export_wisdom_to_filename("fft.wisdom");
        if(ret != 1) {
            debug::warning("Failed exporting Wisdom.", mpi::world);
        }
    }

    //!
    //! \param mode_
    virtual void set_planning_mode(std::string mode_) {
//        static_assert(typeof(FFTW_MEASURE) == unsigned int,
//                      "Invalid Plan Type");
        debug::message("Setting plan mode to " + mode_, mpi::world);
        mode = fft_mode_from_string(mode_);
    }

    //!
    //! \tparam interlaced_
    //! \param tme
    //! \param frq
    template <bool interlaced_ = interlaced,
              typename std::enable_if<interlaced_ == true, bool>::type = true>
    void plan(const FFTWTimeVec<d, contiguous, interlaced> & tme,
              const FFTWFreqVec<d, contiguous, interlaced> & frq) {
        assert(tme.dof >= 1 && frq.dof >= 1 && "Empty vectors cannot be use to plan FFT.");

        assert(tme.dof == frq.dof && "Not avaiable.");

        debug::message("Planning FFTW with mode " + to_string(mode),
                       mpi::world);

        long unsigned int idx = frq.dof - 1;

        if(this->has_plan.size() > idx && this->has_plan[idx]) {
            return;
        }

        this->forward_plan.resize(std::max(idx+1,this->forward_plan.size()), nullptr);
        this->backward_plan.resize(std::max(idx+1,this->backward_plan.size()), nullptr);
        this->has_plan.resize(std::max(idx+1,this->has_plan.size()), false);

        profiler::start_stage("FFTWFFT::make_plans");
#if HAVE_MPI
        assert(tme.dof == frq.dof && "Interlaced data requires same dof on all vectors.");
        this->forward_plan[idx] =
                fftw_mpi_plan_many_dft_r2c(d, this->grid_p->timeN.data(),
                                           tme.dof, FFTW_MPI_DEFAULT_BLOCK, FFTW_MPI_DEFAULT_BLOCK,
                                                        reinterpret_cast<double*>(tme._data),
                                                        reinterpret_cast<fftw_complex*>(frq._data),
                                                        this->comm.comm,
                                          to_uint(mode) | FFTW_MPI_TRANSPOSED_OUT | FFTW_DESTROY_INPUT);
        this->backward_plan[idx] =
                fftw_mpi_plan_many_dft_c2r(d, this->grid_p->timeN.data(),
                                           tme.dof, FFTW_MPI_DEFAULT_BLOCK, FFTW_MPI_DEFAULT_BLOCK,
                                                         reinterpret_cast<fftw_complex*>(frq._data),
                                                         reinterpret_cast<double*>(tme._data),
                                                         this->comm.comm,
                                           to_uint(mode) | FFTW_MPI_TRANSPOSED_IN | FFTW_DESTROY_INPUT);
#else
        fftw_plan fftw_plan_many_dft_r2c(int rank,
                                         const int *n, int howmany,
                                         double *in, const int *inembed,
                                         int istride, int idist,
                                         fftw_complex *out, const int *onembed,
                                         int ostride, int odist,
                                         unsigned flags);
        fftw_plan fftw_plan_many_dft_c2r(int rank,
                                         const int *n, int howmany,
                                         fftw_complex *in, const int *inembed,
                                         int istride, int idist,
                                         double *out, const int *onembed,
                                         int ostride, int odist,
                                         unsigned flags);
        this->forward_plan[idx] =
                fftw_plan_many_dft_r2c(d, this->grid_p->timeN.data(),
                                       tme.dof,
                                       FFTW_MPI_DEFAULT_BLOCK, FFTW_MPI_DEFAULT_BLOCK,
                                       reinterpret_cast<double*>(u._data),
                                       reinterpret_cast<fftw_complex*>(f._data),
                                       mode);
        this->backward_plan[idx] =
                fftw_plan_many_dft_c2r(d, this->grid_p->timeN.data(),
                                       tme.dof,
                                       FFTW_MPI_DEFAULT_BLOCK, FFTW_MPI_DEFAULT_BLOCK,
                                       reinterpret_cast<fftw_complex*>(f._data),
                                       reinterpret_cast<double*>(u._data),
                                       mode);
#endif
        if( this->forward_plan[idx] == NULL || this->backward_plan[idx] == NULL ) {
            debug::error("Could not create a FFTW plan.", this->comm);
        } else {
            this->has_plan[idx] = true;
        }
        profiler::end_stage();
    }

    //!
    //! \tparam interlaced_
    //! \param tme
    //! \param frq
    template <bool interlaced_ = interlaced,
              typename std::enable_if<interlaced_ == false, int>::type = 1>
    void plan(const FFTWTimeVec<d, contiguous, interlaced> & tme,
              const FFTWFreqVec<d, contiguous, interlaced> & frq) {
        assert(tme.dof >= 1 && frq.dof >= 1 &&
               "Empty vectors cannot be use to plan FFT.");
        if(this->has_plan) {
            return;
        }

        profiler::start_stage("FFTWFFT::make_plans");
#if HAVE_MPI
        this->forward_plan = fftw_mpi_plan_dft_r2c(d, this->grid_p->timeN.data(),
                    reinterpret_cast<double*>(tme._data[0]),
                    reinterpret_cast<fftw_complex*>(frq._data[0]),
                    this->comm.comm, to_uint(mode) | FFTW_MPI_TRANSPOSED_OUT | FFTW_DESTROY_INPUT);
        this->backward_plan = fftw_mpi_plan_dft_c2r(d, this->grid_p->timeN.data(),
                    reinterpret_cast<fftw_complex*>(frq._data[0]),
                    reinterpret_cast<double*>(tme._data[0]),
                    this->comm.comm, to_uint(mode) | FFTW_MPI_TRANSPOSED_IN | FFTW_DESTROY_INPUT);
#else // END HAVE_MPI
        this->forward_plan = fftw_plan_dft_r2c(d, this->grid_p->timeN.data(),
                    reinterpret_cast<double*>(u._data[0]),
                    reinterpret_cast<fftw_complex*>(f._data[0]), to_uint(mode));
        this->backward_plan = fftw_plan_dft_c2r(d, this->grid_p->timeN.data(),
                reinterpret_cast<fftw_complex*>(f._data[0]),
                reinterpret_cast<double*>(u._data[0]), to_uint(mode));
#endif // END HAVE_MPI
        if( this->forward_plan == NULL || this->backward_plan == NULL ) {
            debug::error("Could not create a FFTW plan.", this->comm);
        } else {
            this->has_plan = true;
        }
        profiler::end_stage();
    }

    //!
    //! \tparam interlaced_
    //! \param tme
    //! \param frq
    template <bool interlaced_ = interlaced, typename std::enable_if<interlaced_ == true, bool>::type = true>
    void forward(const FFTWTimeVec<d, contiguous, interlaced> & tme,
                 FFTWFreqVec<d, contiguous, interlaced> & frq){
        int idx = frq.dof - 1;

        if(this->has_plan.size() <= idx || !this->has_plan[idx]) {
            debug::warning("Plan was missing, making it for you, WANRNING: data WILL be destroyed!",
                           this->comm);
            plan(tme, frq);
        }

        profiler::start_stage("FFTWFFT::ffft");

        profiler::sync(tme.grid_p->comm.comm);
#if HAVE_MPI
        fftw_mpi_execute_dft_r2c(this->forward_plan[idx],
                                     reinterpret_cast<double*>(tme._data),
                                     reinterpret_cast<fftw_complex*>(frq._data));
#else // END HAVE_MPI
        fftw_execute_dft_r2c(this->forward_plan[idx], reinterpret_cast<double*>(tme._data),
                                 reinterpret_cast<fftw_complex*>(frq._data));
#endif // END HAVE_MPI
        frq.scaling_factor = tme.scaling_factor;
        profiler::end_stage();
    }

    //!
    //! \tparam interlaced_
    //! \param tme
    //! \param frq
    template <bool interlaced_ = interlaced, typename std::enable_if<interlaced_ == false, int>::type = 1>
    void forward(const FFTWTimeVec<d, contiguous, interlaced> & tme,
                 FFTWFreqVec<d, contiguous, interlaced> & frq){
        if(!this->has_plan) {
            debug::warning("Plan was missing, making it for you, WANRNING: data WILL be destroyed!",
                           this->comm);
            plan(tme, frq);
        }

        profiler::start_stage("FFTWFFT::ffft");

        profiler::sync(tme.grid_p->comm.comm);

        for(unsigned int f = 0; f < std::min(tme.dof, frq.dof); ++f) {
#if HAVE_MPI
            fftw_mpi_execute_dft_r2c(this->forward_plan,
                                     reinterpret_cast<double*>(tme._data[f]),
                                     reinterpret_cast<fftw_complex*>(frq._data[f]));
#else // END HAVE_MPI
            fftw_execute_dft_r2c(this->forward_plan, reinterpret_cast<double*>(tme._data[f]),
                                 reinterpret_cast<fftw_complex*>(frq._data[f]));
#endif // END HAVE_MPI
        }
        frq.scaling_factor = tme.scaling_factor;
        profiler::end_stage();
    }

    //!
    //! \tparam interlaced_
    //! \param frq
    //! \param tme
    template <bool interlaced_ = interlaced, typename std::enable_if<interlaced_ == true, bool>::type = true>
    void backward(const FFTWFreqVec<d, contiguous, interlaced> & frq,
                  FFTWTimeVec<d, contiguous, interlaced> & tme) {
        int idx = frq.dof - 1;

        if(this->has_plan.size() <= idx || !this->has_plan[idx]) {
            debug::warning("Plan was missing, making it for you, WANRNING: data WILL be destroyed!", this->comm);
            plan(tme, frq);
        }
        profiler::start_stage("FFTWFFT::bfft");

        profiler::sync(frq.grid_p->comm.comm);

#if HAVE_MPI
        fftw_mpi_execute_dft_c2r(this->backward_plan[idx], reinterpret_cast<fftw_complex*>(frq._data),
                                 reinterpret_cast<double*>(tme._data));
#else // END HAVE_MPI
        fftw_execute_dft_c2r(this->backward_plan[idx], reinterpret_cast<fftw_complex*>(frq._data),
                             reinterpret_cast<double*>(tme._data));
#endif // END HAVE_MPI
        tme.scaling_factor = frq.scaling_factor * this->transform_size;
        profiler::end_stage();
    }

    //!
    //! \tparam interlaced_
    //! \param frq
    //! \param tme
    template <bool interlaced_ = interlaced, typename std::enable_if<interlaced_ == false, int>::type = 1>
    void backward(const FFTWFreqVec<d, contiguous, interlaced> & frq,
                  FFTWTimeVec<d, contiguous, interlaced> & tme) {
        if(!this->has_plan) {
            debug::warning("Plan was missing, making it for you, WANRNING: data WILL be destroyed!", this->comm);
            plan(tme, frq);
        }
        profiler::start_stage("FFTWFFT::bfft");

        profiler::sync(frq.grid_p->comm.comm);

        for(unsigned int f = 0; f < std::min(tme.dof, frq.dof); ++f) {
#if HAVE_MPI
            fftw_mpi_execute_dft_c2r(this->backward_plan, reinterpret_cast<fftw_complex*>(frq._data[f]),
                                     reinterpret_cast<double*>(tme._data[f]));
#else
            fftw_execute_dft_c2r(this->backward_plan, reinterpret_cast<fftw_complex*>(frq._data[f]),
                                 reinterpret_cast<double*>(tme._data[f]));
#endif
        }
        tme.scaling_factor = frq.scaling_factor * this->transform_size;
        profiler::end_stage();
    }
private:
    FFTPlanMode mode = FFTPlanMode::MEASURE;
};

} // END namespace sphinx
} // END namespace operators
