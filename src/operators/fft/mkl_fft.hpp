/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "fft.hpp"

#include "containers/fftw_time_vec.hpp"
#include "containers/fftw_freq_vec.hpp"

#include <mkl.h>

#if HAVE_MPI
#include <mkl_cdft.h>
#endif

#include "utilities/mkl_tools.hpp"

namespace sphinx {
namespace operators {

using mkl_plan = DFTI_DESCRIPTOR_DM_HANDLE; //DFTI_DESCRIPTOR_HANDLE;

//! \brief Interface to FFTW
//! Generates appropriate plans and allows forward/backward transforms
//! \tparam d dimension of the transform
template <unsigned int d, bool contiguous, bool interlaced>
class MKLFFT : public FFT<mkl_plan, d, contiguous, interlaced> {
public:
    MKLFFT(std::shared_ptr<const Grid<d, contiguous, interlaced>> grid_p)
        : FFT<mkl_plan, d, contiguous, interlaced>(grid_p) { }

    MKLFFT(Grid<d, contiguous, interlaced> * grid_p)
        : FFT<mkl_plan, d, contiguous, interlaced>(grid_p) { }

    MKLFFT(MKLFFT & other) = delete;

    //! \brief Delete plans (broken)
    //!
    ~MKLFFT() {
        // FIXME: broken destructors (how come?)
//        status = DftiFreeDescriptor(&desc_handle);
    }

    void plan(const FFTWTimeVec<d, contiguous, interlaced> & tme,
              const FFTWFreqVec<d, contiguous, interlaced> & frq) {
        static_assert(contiguous == false && interlaced == false, "Not yet implemented!");

        assert(tme.dof >= 1 && frq.dof >= 1 && "Empty vectors cannot be use to plan FFT.");
        if(this->has_plan) {
            return;
        }

        profiler::start_stage("MKLFFT::make_plans");

        // FIX this
        this->forward_plan = this->grid_p->mkl_descriptor;
        this->backward_plan = this->grid_p->mkl_descriptor;

        if( this->forward_plan == NULL ) {
            debug::error("Could not create a FFT MKL plan.", this->comm);
        } else {
            this->has_plan = true;
        }
        profiler::end_stage();
    }

    void forward(const FFTWTimeVec<d, contiguous, interlaced> & tme,
                 FFTWFreqVec<d, contiguous, interlaced> & frq){
        if(!this->has_plan) {
            plan(tme, frq);
        }

        profiler::start_stage("MKLFFT::ffft");

        profiler::sync(tme.grid_p->comm.comm);

        for(unsigned int f = 0; f < std::min(tme.dof, frq.dof); ++f) {
            dftiErrchk(DftiComputeForwardDM(this->forward_plan,
                                        reinterpret_cast<double*>(tme._data[f]),
                                        reinterpret_cast<fftw_complex*>(frq._data[f])));
        }
        frq.scaling_factor = tme.scaling_factor;
        profiler::end_stage();
    }

    void backward(const FFTWFreqVec<d, contiguous, interlaced> & frq,
                  FFTWTimeVec<d, contiguous, interlaced> & tme) {
        if(!this->has_plan) {
            plan(tme,frq);
        }
        profiler::start_stage("MKLFFT::bfft");

        profiler::sync(frq.grid_p->comm.comm);

        for(unsigned int f = 0; f < std::min(tme.dof, frq.dof); ++f) {
            dftiErrchk(DftiComputeBackwardDM(this->backward_plan,
                                             reinterpret_cast<fftw_complex*>(frq._data[f]),
                                             reinterpret_cast<double*>(tme._data[f])));
        }
        tme.scaling_factor = frq.scaling_factor * this->transform_size;
        profiler::end_stage();
    }

};

} // END namespace sphinx
} // END namespace operators
