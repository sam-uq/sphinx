/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

//
// Copied from https://stackoverflow.com/questions/39980645/enable-code-indexing-of-cuda-in-clion
//

#ifdef __JETBRAINS_IDE__

#define __CUDACC__ 1
#define __host__
#define __device__
#define __global__
#define __forceinline__
#define __shared__

inline void __syncthreads() {}
inline void __threadfence_block() {}

template<class T> inline T __clz(const T val) { return val; }

struct __cuda_fake_struct { int x,y,z; };

extern __cuda_fake_struct blockDim;

extern __cuda_fake_struct threadIdx;

extern __cuda_fake_struct blockIdx;

#endif // __JETBRAINS_IDE__