/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include <array>
#include <complex>

#include "config.hpp"

namespace sphinx {
namespace types {

using index_t = std::ptrdiff_t;

// DEPRECADED
template <unsigned int dim>
using pos_t = std::array<index_t, dim>;

typedef double    real_t;
typedef double    complex_t;
typedef real_t    scalar_t;

template<long unsigned dim>
using coord_t = std::array<real_t ,dim>;

template <unsigned int dof>
using data_t = std::array<scalar_t, dof>;

// DEPRECATED
#if HAVE_FFTW
typedef double               fftw_time_t;
typedef std::complex<double> fftw_freq_t;
#endif // HAVE_FFTW

} // END namespace types

template<class V>
class is_time_vector {
public:
    template<typename E>
    static std::true_type check(decltype(&E::is_time_vector())) {
        return std::true_type();
    }

    template<typename>
    static std::false_type check(...) {
        return std::false_type();
    }

    typedef decltype(check<V>(nullptr)) type;

    static const bool value = type::value;
};

} // END namespace sphinx
