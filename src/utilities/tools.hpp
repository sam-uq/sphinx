/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

template <int i>
class Set;

template <int i>
std::ostream & operator<<(std::ostream & o, const Set<i> & S) {
    o << S.n;
    return o;
}

template <int i>
class Set {
public:
    Set() : Set<i>(counter) { counter = counter<<1; }
    Set(const Set<i> & other) : n(other.n) {}
    Set(int n) : n(n) {}
//    operator int() const { return n; }
    Set<i> & operator=(int n_) { n = n_; return *this; }
    Set<i> operator|(const Set<i> & other) const { return n | other.n; }
    Set<i> operator&(const Set<i> & other) const { return n & other.n; }
    Set<i> & operator|=(const Set<i> & other) { n |= other.n; return *this; }
    Set<i> & operator&=(const Set<i> & other) { n &= other.n; return *this; }
    Set<i> operator~() const { return Set<i>(~n); }
    friend std::ostream & operator<< <>(std::ostream & o, const Set<i> & S);
    bool operator==(const Set<i> & other) const { return n == other.n; }
    bool operator!=(const Set<i> & other) const { return n != other.n; }
    bool operator<=(const Set<i> & other) const { return (~n | other.n) == ~0; }
    bool operator>=(const Set<i> & other) const { return (n | ~other.n) == ~0; }
    bool operator<(const Set<i> & other) const { return (*this <= other) && (*this != other); }
    bool operator>(const Set<i> & other) const { return (*this >= other) && (*this != other); }

    static const Set<i> None;
    static const Set<i> All;
private:
    int n = 0;
    static int counter;
};

template <int i>
const Set<i> Set<i>::None(0);
template <int i>
const Set<i> Set<i>::All(~0);
template <int i>
int Set<i>::counter = 1;
