/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include "cuda_tools.hpp"

#include <iostream>
#include <sstream>

#include "./debug.hpp"

namespace sphinx {
namespace cuda {

cublasHandle_t handle;

cublasStatus_t cuda_init(int, char**) {

    cudaDeviceProp prop;
    gpuErrchk(cudaGetDeviceProperties	(&prop, 0));
    std::stringstream ss;
    ss << "Device: " << prop.name << " (capability " << prop.major << "." << prop.minor << ")";
    debug::status(ss.str(), sphinx::mpi::world); ss.str(""); ss.clear();
    ss << "Memory:               " << prop.totalGlobalMem << " bytes";
    debug::status(ss.str(), sphinx::mpi::world); ss.str(""); ss.clear();
    ss << "Max grid size:        " << prop.maxGridSize[0]
       << " " << prop.maxGridSize[1] << " " << prop.maxGridSize[2];
    debug::status(ss.str(), sphinx::mpi::world); ss.str(""); ss.clear();
    ss << "Max threads/dim.:     " << prop.maxThreadsDim[0]
       << " " << prop.maxThreadsDim[1] << " " << prop.maxThreadsDim[2];
    debug::status(ss.str(), sphinx::mpi::world); ss.str(""); ss.clear();
    ss << "Max threads per block:  " << prop.maxThreadsPerBlock;
    debug::status(ss.str(), sphinx::mpi::world); ss.str(""); ss.clear();
    ss << "Max registers/block:  " << prop.regsPerBlock;
    debug::status(ss.str(), sphinx::mpi::world); ss.str(""); ss.clear();

    return cublasCreate(&handle);
}

cublasStatus_t cuda_term() {
    return cublasDestroy(handle);
}

const char* _cudaGetErrorEnum(cufftResult error) {
    switch (error) {
    CASE_RETURN_STR(CUFFT_SUCCESS);
    CASE_RETURN_STR(CUFFT_INVALID_PLAN);
    CASE_RETURN_STR(CUFFT_ALLOC_FAILED);
    CASE_RETURN_STR(CUFFT_INVALID_TYPE);
    CASE_RETURN_STR(CUFFT_INVALID_VALUE);
    CASE_RETURN_STR(CUFFT_INTERNAL_ERROR);
    CASE_RETURN_STR(CUFFT_EXEC_FAILED);
    CASE_RETURN_STR(CUFFT_SETUP_FAILED);
    CASE_RETURN_STR(CUFFT_INVALID_SIZE);
    CASE_RETURN_STR(CUFFT_UNALIGNED_DATA);
    CASE_RETURN_STR(CUFFT_INCOMPLETE_PARAMETER_LIST);
    CASE_RETURN_STR(CUFFT_INVALID_DEVICE);
    CASE_RETURN_STR(CUFFT_PARSE_ERROR);
    CASE_RETURN_STR(CUFFT_NO_WORKSPACE);
    CASE_RETURN_STR(CUFFT_NOT_IMPLEMENTED);
    CASE_RETURN_STR(CUFFT_LICENSE_ERROR);
    CASE_RETURN_STR(CUFFT_NOT_SUPPORTED);
        default:
        return "<unknown>";
    }
}

const char* _cublasGetErrorEnum(cublasStatus_t error) {
    switch (error) {
    // The operation completed successfully.
    CASE_RETURN_STR(CUBLAS_STATUS_SUCCESS);
    // The cuBLAS library was not initialized. This is usually caused by the lack of a prior cublasCreate() call,
    // an error in the CUDA Runtime API called by the cuBLAS routine, or an error in the hardware setup.
    // To correct: call cublasCreate() prior to the function call; and check that the hardware, an appropriate
    // version of the driver, and the cuBLAS library are correctly installed.
    CASE_RETURN_STR(CUBLAS_STATUS_NOT_INITIALIZED);
    // Resource allocation failed inside the cuBLAS library. This is usually caused by a cudaMalloc() failure.
    // To correct: prior to the function call, deallocate previously allocated memory as much as possible.
    CASE_RETURN_STR(CUBLAS_STATUS_ALLOC_FAILED);
    // An unsupported value or parameter was passed to the function (a negative vector size, for example).
    // To correct: ensure that all the parameters being passed have valid values.
    CASE_RETURN_STR(CUBLAS_STATUS_INVALID_VALUE);
    // The function requires a feature absent from the device architecture; usually caused by the lack of support
    // for double precision.
    // To correct: compile and run the application on a device with appropriate compute capability, which is 1.3 for
    // double precision.
    CASE_RETURN_STR(CUBLAS_STATUS_ARCH_MISMATCH);
    // An access to GPU memory space failed, which is usually caused by a failure to bind a texture.
    // To correct: prior to the function call, unbind any previously bound textures.
    CASE_RETURN_STR(CUBLAS_STATUS_MAPPING_ERROR);
    // The GPU program failed to execute. This is often caused by a launch failure of the kernel on the GPU,
    // which can be caused by multiple reasons.
    // To correct: check that the hardware, an appropriate version of the driver, and the cuBLAS library are
    // correctly installed.
    CASE_RETURN_STR(CUBLAS_STATUS_EXECUTION_FAILED);
    // An internal cuBLAS operation failed. This error is usually caused by a cudaMemcpyAsync() failure.
    // To correct: check that the hardware, an appropriate version of the driver, and the cuBLAS library are correctly
    // installed. Also, check that the memory passed as a parameter to the routine is not being deallocated prior
    // to the routine’s completion.
    CASE_RETURN_STR(CUBLAS_STATUS_INTERNAL_ERROR);
    // The functionnality requested is not supported
    CASE_RETURN_STR(CUBLAS_STATUS_NOT_SUPPORTED);
    CASE_RETURN_STR(CUBLAS_STATUS_LICENSE_ERROR);
    default:
        return "<unknown>";
    }
}

int __cudaSafeCall(cudaError_t err, const char *file, int line, bool abort) {
#if APPREHENSIVE_CUDA
    cudaDeviceSynchronize();
#endif // APPREHENSIVE_CUDA
    if ( err != cudaSuccess ) {
        std::stringstream ss;
        ss << debug::cuda_error << " " << err << ": " << cudaGetErrorString(err)
           << " in file " << file << " at line " << line << "." << std::endl;
        debug::error(ss.str(), sphinx::mpi::world, sphinx::mpi::Policy::Self, debug::cuda);
        if (abort) exit(err);
    }
    return err;
}

int __cufftSafeCall(cufftResult err, const char *file, const int line, bool abort) {
#if APPREHENSIVE_CUDA
    cudaDeviceSynchronize();
#endif // APPREHENSIVE_CUDA
    if( CUFFT_SUCCESS != err) {
        std::stringstream ss;
        ss << debug::cufft_error << " " << err << ": " << _cudaGetErrorEnum(err)
           << " in file " << file << " at line " << line << "." << std::endl;
        debug::error(ss.str(), sphinx::mpi::world, sphinx::mpi::Policy::Self, debug::cufft);
        cudaDeviceReset();
        if (abort) exit(err);
    }
    return err;
}


int __cublasSafeCall(cublasStatus_t err, const char *file, int line, bool abort) {
#if APPREHENSIVE_CUDA
    cudaDeviceSynchronize();
#endif // APPREHENSIVE_CUDA
    if ( err != CUBLAS_STATUS_SUCCESS ) {
        std::stringstream ss;
        ss << debug::cuda_error << " " << err << ": " << _cublasGetErrorEnum(err)
           << " in file " << file << " at line " << line << "." << std::endl;
        debug::error(ss.str(), sphinx::mpi::world, sphinx::mpi::Policy::Self, debug::cublas);
        if (abort) exit(err);
    }
    return err;
}

} // END namespace sphinx
} // END namespace cuda
