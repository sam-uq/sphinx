/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include <string>
#include <vector>
#include <sstream>

#include "config.hpp"
#include "mpi_tools.hpp"

#if HAVE_MPI
#include <mpi.h> // remov eon crqy
#else
using MPI_Comm = void;
#endif

namespace sphinx {
namespace mpi {

int mpi_init(int argc, char**argv);
int mpi_term();

enum class Policy { Collective, Self };

struct Comm {
    Comm() {}

    Comm(MPI_Comm comm, int rank, int size, std::string name)
        : comm(comm), rank(rank), size(size), name(name) {}

    virtual ~Comm() {}

    void print() {
        std::stringstream ss;
        ss << "> Communicator " << name
           << " (" << comm << ") " << rank
           << "/" << size
           << std::endl;
        std::cout << ss.str();
    }

    MPI_Comm comm;
    int rank;
    int size;
    std::string name;
};

//! IntraComm manages an intra-communicator, a set of disjoint sets
//! ranks that communicate with each other.
struct IntraComm : public Comm {
    //! Does not set any information about the communicaotr.
    IntraComm() {}

    //! Creates a communicator
    IntraComm(MPI_Comm comm, int rank,
              int size, std::string name,
              int color, int ncolors,
              int start_rank_in_parent = -1,
              int size_of_parent = -1)
        : Comm(comm, rank, size,name)
        , color(color), ncolors(ncolors)
        , start_rank_in_parent(start_rank_in_parent)
        , size_of_parent(size_of_parent) {}

    virtual ~IntraComm() {}

    void print() {
        Comm::print();

        std::stringstream ss;
        ss << ">> Intra communicator color:"
           << color << "/" << ncolors
           << ", in parent: " << start_rank_in_parent
           << " size " << size_of_parent
           << std::endl;
        std::cout << ss.str();
    }
    //! Intra-communicator has a color associated with it
    int color;
    //! Total number of intra communicators.
    int ncolors;

    int start_rank_in_parent;
    int size_of_parent;
};

using IntraDomainComm = IntraComm;

struct IntraLevelComm : public  IntraComm {
    IntraLevelComm() {}

    IntraLevelComm(MPI_Comm comm, int rank,
                   int size, std::string name,
                   int color, int ncolors)
        : IntraComm(comm, rank, size, name, color, ncolors) {}


    virtual ~IntraLevelComm() {}

    void addDomain(const IntraDomainComm & intraDomainComm) {
        intra_domain_comms.push_back(intraDomainComm);
    }

    void print() {
        IntraComm::print();

        std::stringstream ss;
        ss << ">>> Intra level communicator:" << std::endl;
        std::cout << ss.str();
        for(IntraDomainComm cm: intra_domain_comms) {
            cm.print();
        }
    }

    void handshake() {
        for(unsigned int other_color = 0; other_color < intra_domain_comms.size(); ++other_color) {
            auto& d_comm = intra_domain_comms.at(other_color);

            int ranksize[2] = {intra_domain_comms.at(color).rank, intra_domain_comms.at(color).size};

            mpiErrchk(MPI_Bcast(ranksize, 2, MPI_INTEGER, d_comm.start_rank_in_parent, comm));

            d_comm.rank = ranksize[0];
            d_comm.size = ranksize[1];
        }
    }

public:
    std::vector<IntraDomainComm> intra_domain_comms;
};

extern IntraComm world;
extern IntraComm self;

inline bool is_selected(Comm comm, Policy pol) {
    return pol == Policy::Self || comm.rank == 0;
}

template <class P>
std::string identifier(const Comm& comm, P pol) {
    if( pol == P::Self ) {
        return "[" + std::to_string(comm.rank)
                + "/" + std::to_string(comm.size)
                + "@" + comm.name
                + "]";
    } else {
        return "<" + comm.name
                + ">";
    }
}

template <class P>
std::string identifier(const IntraComm& comm, P pol) {
    if( pol == P::Self ) {
        return "[" + std::to_string(comm.rank)
                + "/" + std::to_string(comm.size)
                + "@" + comm.name
                + ":" + std::to_string(comm.color)
                + "/" + std::to_string(comm.ncolors)
                + "]";
    } else {
        return "<" + comm.name
                + ":" + std::to_string(comm.color)
                + "/" + std::to_string(comm.ncolors)
                + ">";
    }
}

} // END namespace sphinx
} // END namespace mpi
