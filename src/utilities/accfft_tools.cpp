/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include "accfft_tools.hpp"

#include "include_accfft.hpp"

//template <int dim>
void accfft_get_alloc(std::ptrdiff_t &alloc_time, std::ptrdiff_t &alloc_freq,
                      std::array<ptrdiff_t, 3> &local_0_start,
                      std::array<ptrdiff_t, 3> &local_n0,
                      std::array<ptrdiff_t, 3> &local_0_start_trsp,
                      std::array<ptrdiff_t, 3> &local_n0_trsp,
                      const std::array<std::ptrdiff_t, 3> &N,
                      MPI_Comm  comm) {

    int S[] = {N[0], N[1], N[2]};
    int loc[3], start[3], loc_trsp[3], start_trsp[3];
    std::ptrdiff_t alloc_max = accfft_local_size_dft_r2c_gpu(S,
            loc, start, loc_trsp, start_trsp,
            comm, false);
    for(int i = 0; i < 3; ++i) {
        local_n0[i] = loc[i];
        local_0_start[i] = start[i];
        local_n0_trsp[i] = loc_trsp[i];
        local_0_start_trsp[i] = start_trsp[i];
    }

    alloc_time=loc[0]*loc[1]*loc[2];
    alloc_freq=alloc_max/sizeof(double)/2;
    std::cout << alloc_time << "  XXXXXXXXXXXXXXXXXXXXXXXXXXX " << alloc_freq;
}

void accfft_create_comm_wrapper(MPI_Comm comm,
                        std::array<int, 2> split,
                        MPI_Comm * newcomm) {

    std::cout << split[0] << "      " << split[1] << std::endl << std::flush;
    int dims[2] = {1,1};
    accfft_create_comm (MPI_COMM_WORLD,
                        dims, //split.data(), // TODO
                        newcomm);
}
