/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "../sphinx_internal.hpp"

#include <mkl.h>

#if HAVE_MPI
#include <mkl_cdft.h>
#endif

#include <array>
#include <vector>

typedef int mklError_t;
typedef int dftiError_t;

//template <unsigned int d>
void mkl_get_alloc(std::ptrdiff_t &alloc_time, std::ptrdiff_t &alloc_freq,
                    std::ptrdiff_t &local_0_start, std::ptrdiff_t &local_n0,
                    std::ptrdiff_t &local_0_start_trsp, std::ptrdiff_t &local_n0_trsp,
                    const std::array<std::ptrdiff_t, 2> &N, MPI_Comm comm, DFTI_DESCRIPTOR_DM_HANDLE& hand);

void mkl_get_alloc(std::ptrdiff_t &alloc_time, std::ptrdiff_t &alloc_freq,
                    std::ptrdiff_t &local_0_start, std::ptrdiff_t &local_n0,
                    std::ptrdiff_t &local_0_start_trsp, std::ptrdiff_t &local_n0_trsp,
                    const std::array<std::ptrdiff_t, 3> &N, MPI_Comm comm, DFTI_DESCRIPTOR_DM_HANDLE& hand);

int __dftiSafeCall(dftiError_t, const char *, int, bool = true);

#define dftiErrchk(ans) { __dftiSafeCall((ans), __FILE__, __LINE__); }


inline int __mklSafeCall(mklError_t, const char *, int, bool) {
    return 0;
}

#define mklErrchk(ans) { __mklSafeCall((ans), __FILE__, __LINE__); }

