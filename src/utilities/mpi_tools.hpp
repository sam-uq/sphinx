/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include <array>
#include <vector>

#include <mpi.h>

typedef MPI_Status  mpiStatus_t;
typedef int  mpiError_t;

int __mpiStatusCheck(const mpiStatus_t& status, const char *file, int line);

int __mpiSafeCall(mpiError_t err, const char *file, int line, bool abort = true);

#define mpiStatuschk(ans) { __mpiStatusCheck((ans), __FILE__, __LINE__); }
#define mpiErrchk(ans) { __mpiSafeCall((ans), __FILE__, __LINE__); }

int waitall(std::vector<MPI_Request> reqs);
