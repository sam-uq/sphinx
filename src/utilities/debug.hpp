/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

//! \file debug.hpp Implementation of output for debugging purposes, colorized, tagged and classified.

#include <mpi.h>

#include <string>
#include <iostream>

#include "sphinx_internal.hpp"
#include "mpi.hpp"

#ifndef UNUSED
#define UNUSED(x)     (void)x
#endif

#define CASE_RETURN_STR(x) \
    case x: \
         return #x

//! Colorize output (debug)
namespace color {
// Turn off colors if stdout is redirected to file
#if USE_COLORS
    const char red[] = "\x1b[31;1m";
    const char yellow[] = "\x1b[33m";
    const char green[] = "\x1b[32;1m";
    const char blue[] = "\x1b[34;1m";
    const char magenta[] = "\x1b[35;1m";
    const char restore[] = "\x1b[0m";
#else // DONT USE COLORS
    const char red[] = "";
    const char yellow[] = "";
    const char green[] = "";
    const char blue[] = "";
    const char magenta[] = "";
    const char restore[] = "";
#endif // NO USE_COLORS
}

//! Namespace for debugging routines
namespace debug {

//! String for a CUDA related error
const char cuda_error[] = "CUDA Error";
//! String for a CUFFT related error
const char cufft_error[] = "CUFFT Error";
//! String for a MPI related error
const char mpi_error[] = "MPI Error";
//! String for a BLAS related error
const char blas_error[] = "BLAS Error";
//! String for a CUBLAS related error
const char cublas_error[] = "CUBLAS Error";
//! String for a MKL related error
const char mkl_error[] = "MKL Error";

//! String to prepend to options related message
const char option_prestring[] = "<option> ";
const char profiler_prestring[] = "<profiler> ";
const char fftw_prestring[] = "<fftw> ";
const char cuda_prestring[] = "<cuda> ";
const char cufft_prestring[] = "<cufft> ";
const char cublas_prestring[] = "<cublas> ";
const char io_prestring[] = "<io> ";
const char ts_prestring[] = "<ts> ";
const char grid_prestring[] = "<grid> ";
const char mpi_prestring[] = "<mpi> ";
const char mkl_prestring[] = "<mkl> ";
const char mc_prestring[] = "<mc> ";
const char mlmc_prestring[] = "<mlmc> ";
const char none_prestring[] = " ";

//! We split debugging in components, like profiler, i/o, timestepping, etc...
enum componentEnum { option, profiler, fftw, mpi, grid, cuda, cufft, cublas, io, ts, mc, mlmc, mkl, none };

//! Components name for each enum component
inline const char* prestring(componentEnum cpt = none) {
    switch(cpt) {
    case option:
        return option_prestring;
    case profiler:
        return profiler_prestring;
    case fftw:
        return fftw_prestring;
    case cuda:
        return cuda_prestring;
    case cublas:
        return cublas_prestring;
    case cufft:
        return cufft_prestring;
    case mc:
        return mc_prestring;
    case grid:
        return grid_prestring;
    case mlmc:
        return mlmc_prestring;
    case io:
        return io_prestring;
    case ts:
        return ts_prestring;
    case mkl:
        return mkl_prestring;
    default:
        return none_prestring;
    }
}

//inline std::string sphinx::mpi::identifier(MPI_Comm comm) {
//    if( comm == MPI_COMM_SELF ) {
//        int rank, size;
//        MPI_Comm_rank( MPI_COMM_WORLD, &rank );
//        MPI_Comm_size( MPI_COMM_WORLD, &size );
//        return "[" + std::to_string(rank) + "/" + std::to_string(size) + "]";
//    } else if( comm == MPI_COMM_WORLD ) {
//        return "(WORLD)";
//    } else {
//        return "";
//    }
//}

//! Output an error if OUTPUT_LEVEL > 1, aborts if flag ABORT_ON_ERROR
//! \param[in] wts string to output
//! \param[in] cpt optional component (prepend a string)
//! \param[in] comm optional communicator
//! \param[in] end optional end character
template <bool no_abort = false, class C>
void error(const std::string & wst,
                  const C& comm, sphinx::mpi::Policy policy = sphinx::mpi::Policy::Collective,
                  componentEnum cpt = none,
                  std::string end = "\n") {
    UNUSED(wst); UNUSED(comm); UNUSED(policy); UNUSED(cpt); UNUSED(end);
#if OUTPUT_LEVEL > 1
    if( sphinx::mpi::is_selected(comm, policy) ) {
        std::cout << color::red << "[-ERROR-] " << color::restore
                  << sphinx::mpi::identifier(comm, policy)
                  << prestring(cpt) << wst << end;
    }
    if(!no_abort && ABORT_ON_ERROR) {
#if HAVE_MPI
        MPI_Abort(comm.comm, -1);
#else
        abort(); // TODO: replace with sphinx::abort
#endif
    }
#endif
}

//! Output warning if OUTPUT_LEVEL > 2 (used for warnings)
//! \param[in] wts string to output
//! \param[in] cpt optional component (prepend a string)
//! \param[in] comm optional communicator
//! \param[in] end optional end character
template <class C>
void warning(const std::string & wst,
             const C& comm,
             sphinx::mpi::Policy policy = sphinx::mpi::Policy::Collective,
             componentEnum cpt = none,
             std::string end = "\n") {
#if OUTPUT_LEVEL > 2
    if( sphinx::mpi::is_selected(comm, policy) ) {
        std::cout << color::yellow << "[WARNING] " << color::restore
                  << sphinx::mpi::identifier(comm, policy)
                  << prestring(cpt) << wst << end;
    }
#else
    UNUSED(wst); UNUSED(comm); UNUSED(policy); UNUSED(cpt); UNUSED(end);
#endif
}

//! Output if OUTPUT_LEVEL > 3 (used for light output)
//! \param[in] wts string to output
//! \param[in] cpt optional component (prepend a string)
//! \param[in] comm optional communicator
//! \param[in] end optional end character
template <class C>
void message(const std::string & wst,
                    const C& comm, sphinx::mpi::Policy policy = sphinx::mpi::Policy::Collective,
                    componentEnum cpt = none,
                    std::string end = "\n") {
#if OUTPUT_LEVEL > 3
    if( sphinx::mpi::is_selected(comm, policy) ) {
        std::cout << color::blue << "[-INFO--] " << color::restore
                  << sphinx::mpi::identifier(comm, policy)
                  << prestring(cpt) << wst
                  << end;
    }
#else
    UNUSED(wst); UNUSED(comm); UNUSED(policy); UNUSED(cpt); UNUSED(end);
#endif
}

//! Output if OUTPUT_LEVEL > 4 (used for heavy status messages)
//! \param[in] wts string to output
//! \param[in] cpt optional component (prepend a string)
//! \param[in] comm optional communicator
//! \param[in] end optional end character
template <class C>
void status(const std::string & wst,
                   const C& comm, sphinx::mpi::Policy policy = sphinx::mpi::Policy::Collective,
                   componentEnum cpt = none,
                   std::string end = "\n") {
#if OUTPUT_LEVEL > 4
    if( sphinx::mpi::is_selected(comm, policy) ) {
        std::cout << color::magenta << "[STATUS-] " << color::restore << sphinx::mpi::identifier(comm, policy) << prestring(cpt) << wst << end;
    }
#else
    UNUSED(wst); UNUSED(comm); UNUSED(policy); UNUSED(cpt); UNUSED(end);
#endif
}

//! Output if OUTPUT_LEVEL > 5 (used for very verbose content)
//! \param[in] wts string to output
//! \param[in] cpt optional component (prepend a string)
//! \param[in] comm optional communicator
//! \param[in] end optional end character
template <class C>
void verbose(const std::string & wst,
                    const C& comm, sphinx::mpi::Policy policy = sphinx::mpi::Policy::Collective,
                    componentEnum cpt = none,
                    std::string end = "\n") {
    UNUSED(wst); UNUSED(comm); UNUSED(policy); UNUSED(cpt); UNUSED(end);
#if OUTPUT_LEVEL > 5
    if( sphinx::mpi::is_selected(comm, policy) ) {
        std::cout << "[VERBOSE] " << sphinx::mpi::identifier(comm, policy) << prestring(cpt) << wst << end;
    }
#endif
}

/// UGLY STUFF FOR C++ < 17


template <class T>
inline std::string stringify(T s) {
    return std::to_string(s);
}

template <>
inline std::string stringify(sphinx::mpi::Policy) {
    return "";
}

template <>
inline std::string stringify(componentEnum s) {
    return prestring(s);
}

template <>
inline std::string stringify(std::string s) {
    return s;
}

template <>
inline std::string stringify(const char * s) {
    return s;
}


template <class C, class Policy, class T>
void print_test_message(const C&, Policy&& policy,
                        const T& cpt) {
    std::cout << stringify(policy);
    std::cout << stringify(cpt);
}

template <class C, class Policy, class E, class T>
void print_test_message(const C&, Policy&& policy,
                        E&& cpt, const T& first) {
    std::cout << stringify(policy);
    std::cout << stringify(cpt);
    std::cout << first;
}

template <class C, class Policy, class E, class T, class ...Args>
void print_test_message(const C& comm, Policy&& policy,
                  E&& cpt, const T& first, Args&&... args) {

    std::cout << stringify(policy);
    std::cout << stringify(cpt);
    std::cout << first;
    print_test_message(comm, args...);
}

/// END UGLY STUFF

//! \brief Emit a message specific for unit tests.
//!
//! Print a fomatted message with some information about the rank and the context.
//!
//! Used to output message specific of a test.
//!
//! \tparam C           Type for Communicator, e.g. of type mpi::Comm
//! \tparam Policy      Either the output policy or a string
//! \tparam E           Either a componentEnum or a string, must be a string if Policy is a string
//! \tparam Args        Remainder std::string that will be printed
//! \param comm         The communitactor, e.g. mpi::Comm::world, i.e. the group of ranks that will output the message.
//! \param policy       The policy for printing (all print, only one rank prints), or a string to prefix args.
//! \param cpt          The component (will add an identifier as predix),
//!                     or a string to prefix args (only if policy is a string).
//! \param args         String or streamable content, will be joined and outputted.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wunused-but-set-parameter"
template <class C, class Policy, class E, class... Args>
void test_message(const C& comm, Policy&& policy,
                  E&& cpt, Args&&... args) {
    std::cout << color::magenta << "[-TEST--] " << color::restore;
#if __cpp_if_constexpr
    if constexpr ( std::is_same<Policy, sphinx::mpi::Policy>::value ) {
        std::cout << sphinx::mpi::identifier(comm, policy);
        if constexpr ( std::is_same<E, componentEnum>::value ) {
            if (sphinx::mpi::is_selected(comm, policy)) {
                std::cout << prestring(cpt);
            }
        } else {
            if (sphinx::mpi::is_selected(comm, policy)) {
                std::cout << prestring(none) << cpt;
            }
        }
        (std::cout  << ... << args);
    } else {
        if (sphinx::mpi::is_selected(comm, sphinx::mpi::Policy::Collective)) {
            std::cout << sphinx::mpi::identifier(comm, sphinx::mpi::Policy::Collective)
                      << prestring(none) << policy << cpt;
            (std::cout << ... << args);
        }
    }
    std::cout << std::endl;
#else
    UNUSED(comm);
    UNUSED(policy);
    UNUSED(cpt);


    print_test_message(comm, policy, cpt, args...);

    std::cout << std::endl;
#endif
}
#pragma GCC diagnostic pop

//! \brief Output a test message with Collective policy and no context.
//!
//! Overload to make the 2 argument test_message work.
//!
//! \tparam C           Type for Communicator, e.g. of type mpi::Comm
//! \tparam E           A streamable format.
//! \param comm         The communicator on which to output.
//! \param arg          The content to stream.
template <class C, class E>
void test_message(const C& comm, E&& arg) {
    test_message(comm, sphinx::mpi::Policy::Collective, arg);
}

} // end namespace DEBUG
