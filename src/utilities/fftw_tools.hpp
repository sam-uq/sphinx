/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "sphinx_internal.hpp"

#include <array>
#include <vector>

#include <mpi.h>

typedef int  fftwError_t;

//template <int dim>
void fftw_get_alloc(std::ptrdiff_t &alloc_time, std::ptrdiff_t &alloc_freq,
                    std::ptrdiff_t &local_0_start, std::ptrdiff_t &local_n0,
                    std::ptrdiff_t &local_0_start_trsp, std::ptrdiff_t &local_n0_trsp,
                    const std::array<std::ptrdiff_t, 2> &N, MPI_Comm comm);

//template <>
void fftw_get_alloc(std::ptrdiff_t &alloc_time, std::ptrdiff_t &alloc_freq,
                    std::ptrdiff_t &local_0_start, std::ptrdiff_t &local_n0,
                    std::ptrdiff_t &local_0_start_trsp, std::ptrdiff_t &local_n0_trsp,
                    const std::array<std::ptrdiff_t, 3> &N, MPI_Comm comm);

inline int __fftwSafeCall(fftwError_t, const char *, int, bool = true) {
    return 0;
}

#define fftwErrchk(ans) { __fftwSafeCall((ans), __FILE__, __LINE__); }
