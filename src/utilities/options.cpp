/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include "options.hpp"

namespace sphinx {

namespace po = boost::program_options;

// Definition of global variable Options (Super nasty)
Options options;

int Options::read(int argc, char** argv) {
    // Declare the supported options.
    desc.add_options()
            (
                    "help",
                    "produce help message"
            )
            (
                    "version,v",
                    "return version number"
            )
            (
                    "conf,c",
                    po::value<std::string>(),
                    "configuration file"
            )

            (
                    "unit",
                    po::value<std::string>(),
                    "test unit chosen"
            )
            (
                    "name,n",
                    po::value<std::string>(),
                    "simulation name"
            )
            ("mesh", po::value<std::vector<int>>()->multitoken(), "set mesh size")
            ("T", po::value<double>()->default_value(1.), "final time for simulation")

            ("maxit", po::value<int>()->default_value(1000000), "set max number of options")
            ("cfl", po::value<double>()->default_value(0.5), "set CFL number")
            ("adaptive", po::value<bool>()->default_value(true), "set adaptive timestepping")
            ("do_initial_cleansing", po::value<bool>()->default_value(true),
             "perform a projection on the initial data")

            ("threshold", po::value<int>(), "threshold for truncation of modes")
            ("eps", po::value<double>(), "eps for numerical diffusion")
            ("aliasing", po::value<int>(),
             "aliasing modes (effective modes will be padding*mesh-aliasing")
            ("padding", po::value<double>(),
             "zero padding amount (padded mesh will be padding*mesh")

            ("dump_xdmf", po::value<bool>(), "Dump an xdmf at each frame.")
            ("snapshot_fps", po::value<double>(), "fps or snapshots")
            ("snapshot_force", po::value<bool>()->default_value(false),
             "force snapshots at exact time")
            ("snapshot_list", po::value<std::vector<int>>()->multitoken(), "list of snapshots")


            (
                    "master_seed",
                    po::value<int>(),
                    "master seed for simulations and reproducibility"
            )
            (
                    "autopad",
                    po::value<std::vector<long long int>>()->multitoken(),
                    "List of primes to do automatic padding to."
            )
            ("fft_inplace", po::bool_switch()->default_value(false), "use in place fft")
            ("nsims", po::value<int>()->default_value(1),
             "number of MC simulations (finer level if MLMC)")
            (
                    "levels",
                    po::value<unsigned int>()->default_value(1),
                    "number of levels"
            )
            (
                    "use_forcing",
                    po::value<bool>()->default_value(true),
                    "Enable usage of forcing for certain types of initial data."
            )
            ////// INITIAL DATA OPTIONS
            (
                    "InitialData.name",
                    po::value<std::string>(),
                    "name of our initial data"
            )
            (
                    "InitialData.args",
                    po::value<std::vector<std::string>>()->multitoken(),
                    "generic arguments passed to initial data"
            )
            ("InitialData.rho", po::value<double>(), "smoothness parameter of initial data")
            ("InitialData.delta", po::value<double>(), "size of perturbation")
            ("InitialData.N", po::value<int>(), "dimension of random space")
            ("InitialData.alpha", po::value<double>(), "size of random perturbation")
            ("InitialData.lambda", po::value<double>(), "differentiability of initial data")
            ("InitialData.magnitude", po::value<double>(), "magnitude of initial data")
            (
                    "InitialData.H",
                    po::value<double>(),
                    "Hurst index."
            )
            (
                    "InitialData.input",
                    po::value<std::string>(),
                    "Input filename for loading-based initialdata."
            )
            (
                    "InitialData.scalingFactor",
                    po::value<double>(),
                    "Scaling factor for initial data (what it does depends "
                            "on actual initial data chosen)."
            )
            (
                    "InitialData.shift",
                    po::value<double>()->default_value(0.0), "Shift of the data."
            )
            (
                    "InitialData.ashift",
                    po::value<std::vector<double>>()->multitoken(), "Amplitude vector-shift of the data."
            )
            (
                    "InitialData.vshift",
                    po::value<std::vector<double>>()->multitoken(), "Horizontal vector-shift of the data."
            )
            (
                    "InitialData.noise",
                    po::value<std::string>(),
                    "Noise type."
            )
            (
                    "InitialData.force_vorticity",
                    po::value<bool>()->default_value(false),
                    "Force initial data to be of vorticity type."
            )

            //// IO OPTIONS
            (
                    "Save.individual",
                    po::value<bool>()->default_value(true),
                    "save individual simulations"
            )
            ("Save.mc", po::value<bool>()->default_value(true), "save MC simulations")
            ("Save.mlmc", po::value<bool>()->default_value(true), "save MLMC simulations")
            ("Save.diff", po::value<bool>()->default_value(false), "save MLMC diff simulations")
            ("Save.initial", po::value<bool>()->default_value(true), "save initial data")
            ("Save.final", po::value<bool>()->default_value(true), "save result at final time")

            (
                    "FFT.mode",
                    po::value<std::string>(),
                    "FFT planning mode: one of "
                            "Measure, Patient, Estimate, Exhaustive, WisdomOnly"
            )
            (
                    "FFT.max_time",
                    po::value<double>()->default_value(0.0),
                    "Maximum planning time."
            )
            (
                    "FFT.save_wisdom",
                    po::value<bool>()->default_value(false),
                    "Save wisdom to file."
            )
            (
                    "FFT.load_wisdom",
                    po::value<bool>()->default_value(false),
                    "Load wisdom from file."
            );

    // Parse options

    try {
        po::store(po::parse_command_line(argc, argv, desc), vm);
    } catch(boost::program_options::unknown_option e) {
        std::cout << "BOOST ERROR: " << e.what() << std::endl;
        help(false);
    }

    po::notify(vm);

    // Read and notify config file
    std::string conf_file_path = DEFAULT_CONFIG_FILE;
    if( vm.count("conf") ) {
        conf_file_path = vm["conf"].as<std::string>();
    }
    std::ifstream file(conf_file_path);
    if( file ) {
        po::store(po::parse_config_file(file, desc, true), vm);
        po::notify(vm);
    }

    // If help requested, exit
    if ( vm.count("help") ) {
        help();
    }

    if ( vm.count("version") ) {
        exit(0);
    }


    message("Compiled for " + std::to_string(NDIMS) + " dimentions.",
            mpi::world, mpi::Policy::Collective, debug::option);

    // Options sanity check
    if ( vm.count("mesh") ) {
        auto v = vm["mesh"].as<std::vector<int>>();
        std::stringstream ss;
        ss << "Mesh size is "
                << v[0]
                << "," << v[1]
#if NDIMS == 3
            << "," << v[2]
#endif
                ;
        if(v.size() < NDIMS) {
            error("Specified mesh size "
                  + std::to_string(v.size())
                  + "has not correct length (>= "
                  + std::to_string(NDIMS) + ").",
                  mpi::world, mpi::Policy::Collective, debug::option);
        }
        message(ss.str(), mpi::world, mpi::Policy::Collective, debug::option);
    } else {
        std::stringstream ss;
        ss << "Mesh size was not set.";
        message(ss.str(), mpi::world, mpi::Policy::Collective, debug::option);
    }

    if ( has("maxit") ) {
        std::stringstream ss;
        ss << "Maximum number of iterations capped at " << as<int>("maxit") << ".";
        message(ss.str(), mpi::world, mpi::Policy::Collective, debug::option);
    }

    return 0;
}

void Options::help(bool abort) const {
    std::cout << "Sphinx v. " << VERSION << std::endl;
    std::cout << " - MKL support:                 " << myUSE_MKL << std::endl;
    std::cout << " - MKL fttw interface support:  " << USE_MKL_FFTW << std::endl;
    std::cout << " - FFTW support:                " << USE_FFTW << std::endl;
    std::cout << " - CUDA support:                " << USE_CUDA << std::endl;
    std::cout << " - MPI support:                 " << USE_MPI << std::endl;
    std::cout << " - OMP support:                 " << USE_OMP << std::endl;
    std::cout << " - Compiled with:               " << COMPILER << std::endl;

    std::cout << desc << "\n";
    if(abort) exit(0);
}

}
