/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

//! \file utilities.hpp Contains a bunch of mathematical and other utilities.

#include "types.hpp"

#include "debug.hpp"
#include "mpi.hpp"

#ifdef __GNUC__
#define likely(x)       __builtin_expect(!!(x), 1)
#define unlikely(x)     __builtin_expect(!!(x), 0)
#else
#define likely(x)       (x)
#define unlikely(x)     (x)
#endif

class NotImplementedException : std::exception {
public:
    NotImplementedException() {

    }
};

class TestFailureException : std::exception {
public:
    TestFailureException(std::string msg = "") : msg(msg) {
        if(msg != "") {
            debug::test_message(sphinx::mpi::world, msg);
        }
    }

    const char * what() const throw() {
        return msg.c_str();
    }

    std::string msg;
};

//! Get type of the n-th element of a tuple of type Args (no reference type)
template <unsigned int n, class Args>
using arg_type = typename std::remove_reference<
                    typename std::tuple_element<n,Args>::type
                >::type;

//! \brief Return dimension of symmetric tensor given space dimension
//! \tparam T any numeric type
//! \param[in] dim dimention of input space
//! \return dimension of tensor
template <class T>
constexpr T sym_tensor_dim(T dim) {
    return dim * (dim+1) / 2;
}

template <class T>
constexpr T curl_dim(T dim) {
    return dim == 2 ? 1 : 3;
}

template <template <class T, unsigned long int dim> class Array,
          unsigned long int dim, class T1, class T2>
inline bool operator==(const Array<T1, dim> & p, T2 i) {
    for(unsigned long int d = 0; d < dim; ++d) {
        if(p[d] != i) return false;
    }
    return true;
}

template <template <class T, unsigned long int dim> class Array,
          unsigned long int dim, class T>
inline bool operator<(const Array<T, dim> & p, T i) {
    for(unsigned long int d = 0; d < dim; ++d) {
        if(p[d] >= i) return false;
    }
    return true;
}


template <template <class T, unsigned long int dim> class Array,
          unsigned long int dim, class T>
inline Array<T, dim> abs(const Array<T, dim> & p) {
    Array<T, dim> ret;
    for(unsigned long int d = 0; d < dim; ++d) {
        ret[d] = std::abs(p[d]);
    }
    return ret;
}

//! Return the 2-norm of the array to the power p
//!
//! TODO: implement any norm
//! \warning Currently p = 2 for any p.
//! \warning The returned value is the \f$l^p\f$ norm to the power of p.
//!
//! \tparam Array
//! \tparam dim
//! \tparam T
//! \param p
//! \return
template <template <class T, unsigned long int dim> class Array,
          unsigned long int dim, class T, unsigned int p = 2>
inline T normp(const Array<T, dim> & arr) {
    T nr = 0;
    for(unsigned long int d = 0; d < dim; ++d) {
        nr += arr[d] * arr[d];
    }
    return nr;
}

inline std::vector<int> range(unsigned int start, unsigned int end) {
    std::vector<int> ret(end-start);
    for(unsigned long int v = start; v < end; ++v) {
        ret[v-start] = v;
    }
    return ret;
}

template <unsigned long int dim>
inline std::vector<int> range(void) {
    std::vector<int> ret(dim);
    for(unsigned long int f = 0; f < dim; ++f) {
        ret[f] = f;
    }
    return ret;
}

inline std::vector<int> range(unsigned long int dim) {
    std::vector<int> ret(dim);
    for(unsigned long int f = 0; f < dim; ++f) {
        ret[f] = f;
    }
    return ret;
}

template <class T, long unsigned int dim>
inline T prod(const std::array<T, dim> & a) {
    T p = 1;
    for(T v: a) {
        p *= v;
    }
    return p;
}

template <class T, long unsigned int dim, class Func>
inline void apply(std::array<T, dim> & a, const Func & f) {
    for(T& v: a) {
        f(v);
    }
}

template <class T, long unsigned int dim, typename S>
inline std::array<T, dim>  operator+(const std::array<T, dim> & a, S scalar) {
    std::array<T, dim> b = a;
    auto f = [&scalar] (T& v) { v += scalar; };
    apply(b, f);
    return b;
}

template <class T, long unsigned int dim, typename S>
inline std::array<T, dim>  operator*(const std::array<T, dim> & a, S scalar) {
    std::array<T, dim> b = a;
    auto f = [&scalar] (T& v) { v *= scalar; };
    apply(b, f);
    return b;
}

template <class T, long unsigned int dim, typename S>
inline std::array<T, dim>& operator*=(std::array<T, dim> & a, S scalar) {
    auto f = [&scalar] (T& v) { v *= scalar; };
    apply(a, f);
    return a;
}

template <class T, long unsigned int dim>
inline std::array<T, dim> operator-(const std::array<T, dim> & a, const std::array<T, dim> & b) {
    std::array<T,dim> ret;
    for(std::size_t i = 0; i < dim; ++i) {
        ret[i] = a[i] - b[i];
    }
    return ret;
}

template<class T, long unsigned int dim>
inline std::array<T, dim> operator+(const std::array<T, dim> &a, const std::array<T, dim> &b) {
    std::array<T, dim> ret;
    for (std::size_t i = 0; i < dim; ++i) {
        ret[i] = a[i] + b[i];
    }
    return ret;
}

template <class T, long unsigned int dim, typename S>
inline std::array<T, dim>& set(std::array<T, dim> & a, S scalar) {
    auto f = [&scalar] (T& v) { v = scalar; };
    apply(a, f);
    return a;
}

template <class T, long unsigned int dim, typename S>
inline std::array<T, dim> operator/(const std::array<T, dim> & a, S scalar) {
    std::array<T, dim> b = a;
    auto f = [&scalar] (T& v) { v /= scalar; };
    apply(b, f);
    return b;
}

inline int next_pow_of_two(int number) {
    int pow = 1;
    while(pow < number) {
        pow = (pow << 1);
    }
    return pow;
}

//! \brief Conversion from cartesian to polar coordinates in 2d
//!
//! Uses the formula:
//! \f[ r, \theta = \sqrt{x^2 + y^2}, \mathrm{atan}(y/x) \f]
//!
//! \param[in]    cart A coordinate vector (x,y)
//! \return            The polar coordinates (r, theta) corresponding to (x,y).
inline sphinx::types::coord_t<2> cart2pol(const sphinx::types::coord_t<2> &cart) {
    return {std::sqrt(cart[0] * cart[0] + cart[1] * cart[1]), std::atan2(cart[1], cart[0])};
}

//! \brief Conversion from polar to cartesian coordinates in 2d
//!
//! Uses the formula:
//! \f[x, y = r \cos(\theta), r \sin(\theta) \f]
//!
//! \param[in]    cart A coordinate vector (x, y)
//! \return            The cartesian coordinates (x, y) corresponding to (r, theta).
inline sphinx::types::coord_t<2> pol2cart(const sphinx::types::coord_t<2> &pol) {
    return {pol[0] * cos(pol[1]), pol[0] * sin(pol[1])};
}