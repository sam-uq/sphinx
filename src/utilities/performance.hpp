/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "mpi.hpp"
#include "debug.hpp"

// A function to print all prime factors of a given number n
// Adapted: http://www.geeksforgeeks.org/print-all-prime-factors-of-a-given-number/
template <typename T>
std::vector<T> primeFactors(T n)
{
    std::vector<T> V(0);
    // Print the number of 2s that divide n
    if(n%2 == 0) V.push_back(2);
    while (n%2 == 0)
    {
        // counter?
//        printf("%d ", 2);
        n = n/2;
    }

    // n must be odd at this point.  So we can skip one element (Note i = i +2)
    for (int i = 3; i <= std::sqrt(n); i = i+2)
    {
        // While i divides n, print i and divide n
        if(n%i == 0) V.push_back(i);
        while (n%i == 0)
        {
            // counter?
//            printf("%d ", i);
            n = n/i;
        }
    }

    // This condition is to handle the case whien n is a prime number
    // greater than 2
    if (n > 2)
        V.push_back(n);
//        printf ("%d ", n);

    return V;
}

template <typename T, long unsigned int d>
std::array<T,d> three_half_rule(std::array<T,d> small_grid) {

    return (small_grid*3)/2;
}

template <typename T, long unsigned int d>
void do_grid_check(std::array<T, d> grid, sphinx::mpi::Comm comm) {
    for(unsigned int dim = 0; dim < d; ++dim) {
        std::vector<T> primes = primeFactors(grid[dim]);
//        std::cout << primes.size() << " " << primes[0] <<" "<< primes[1] <<" "<< primes[2] <<" "<< primes[3];
        if( primes.size() > 4 || primes[0] != 2
                || (primes.size() >= 2 && primes[1] != 3)
                || (primes.size() >= 3 && primes[2] != 5)
                || (primes.size() >= 4 && primes[3] != 7 )) {
//            std::cout << primes[0] <<" "<< primes[1] <<" "<< primes[2] <<" "<< primes[3];
            std::stringstream ss_no2357;
            ss_no2357 << "Factorization along dimension " << dim << " has many ("
                      << primes.size() << ") and/or big prime factors.";
            debug::warning(ss_no2357.str(), comm);
        }
    }
    if( grid[0] % 4 != 0 ) {
        std::stringstream ss_nodiv4x;
        ss_nodiv4x << "Dimension x is not divisible by 4.";
        debug::warning(ss_nodiv4x.str(), comm);
    }
    if( grid[0] % comm.size != 0
            && (d == 2 ? grid[1] % comm.size != 0 : grid[1]*grid[2] % comm.size != 0 ) ) {
        std::stringstream ss_unbalanced;
        ss_unbalanced << "Load may be unbalanced.";
        debug::warning(ss_unbalanced.str(), comm);
    }
}
