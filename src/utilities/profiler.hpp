/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include <map>
#include <list>
#include <iomanip>
#include <string>
#include <sstream>
#include <cassert>
//#include <chrono> // chrono not available on all platforms
#include <boost/chrono.hpp>
#include <boost/property_tree/ptree.hpp>

#include "config.hpp"
#include "debug.hpp"
#include "mpi.hpp"

//! Maximal stage depth
#define MAX_DEPTH 14

#define PROFILING_DIR     "profiler"

using namespace sphinx;

//! \brief Class managing profiling (timing, memory, etc.)
//! Only one class should be created and inizialized
//! at the beginning, and terminated at the end.
class profiler {
public:
//    using duration_t = std::chrono::high_resolution_clock::duration;
//    using time_point = std::chrono::high_resolution_clock::time_point;
    using duration_t = boost::chrono::high_resolution_clock::duration;
    using time_point = boost::chrono::high_resolution_clock::time_point;

    struct stage {
        //! Create an empty stage
        stage() = default;
        //! Create a stage with a name and an optional parent.
        //! \param name
        //! \param parent
        stage(std::string name, const stage* parent = nullptr)
                : name(name), uuid(counter++), parent(parent) {
            elapsed = duration_t::zero();
            if(parent) depth = parent->depth + 1;
            start();
        }

        stage & operator=(const stage & other) {
            parent = &other;
            name = other.name;
            uuid = other.uuid;
            depth = other.depth;
            parent = other.parent;
            childs = other.childs;
            start_time = other.start_time;
            elapsed = other.elapsed;
            running = other.running;
            return *this;
        }

        void print() const {
            std::stringstream ss;
            if( depth == 0 ) {
                std::stringstream ss1;
                ss1 << std::setw(40+1+MAX_DEPTH) << std::left  << "name"
                    << std::setw(15)                           << "nstarts"
                    << std::setw(15+1)                         << "elapsed";
                debug::message(ss1.str(), mpi::world,
                               mpi::Policy::Self, debug::profiler);
            }
            for(int d = 0; d < depth; ++d) {
                ss << "-";
            }
            ss << ">"
               << std::setw(MAX_DEPTH-depth) << " "
               << std::setw(40) << std::left << name
               << std::setw(15)              << nstarts
               << std::setw(15)              << elapsed.count() / 1e9 << "s";
            debug::message(ss.str(), mpi::world,
                           mpi::Policy::Self, debug::profiler);
            for(const stage * child : childs ) {
                child->print();
            }
        }

        template <class tree>
        void append_to_ptree(tree & ptree) {

            tree pt_stage;
            pt_stage.put("<xmlattr>.name", name);
            pt_stage.put("<xmlattr>.count", nstarts);
            pt_stage.put("elapsed", elapsed.count() / 1e9);
            for(stage * child : childs ) {
                child->append_to_ptree(pt_stage);
            }
            ptree.add_child("Stage", pt_stage);
        }

        double runtime() const {
            return boost::chrono::duration_cast<duration_t>(
                    boost::chrono::high_resolution_clock::now() - start_time).count()
                   / 1e9;
        }

        void start() {
//            start_time =  std::chrono::high_resolution_clock::now();
            start_time =  boost::chrono::high_resolution_clock::now();
            running = true;
            ++nstarts;
        }

        void stop();

        std::string name = "";
        int uuid = 0;
        const stage* parent = nullptr;
        std::list<stage *> childs;
        int depth = 0;
        unsigned int nstarts = 1;
        time_point start_time;
        duration_t elapsed;
        bool running = false;
        static unsigned int counter;
    };

    static void init() {
        stage::counter = 0;

        start_time = boost::chrono::high_resolution_clock::now();

        start_stage("main_stage");

    }

    static void finalize() {
        debug::verbose("Terminated stage: main_stage",
                       mpi::world, mpi::Policy::Self, debug::profiler);
        stage_stack.back()->stop();
        stage_stack.pop_back();
        if( !stage_stack.empty() ) {
            debug::error("Stage stack didn't become empty, top = \""
                         + stage_stack.back()->name + "\", staging broken!",
                         mpi::world, mpi::Policy::Self, debug::profiler);
        }

        print();
    }

    static void start_stage(std::string name) {
        if(stage_map.count(name) == 0) {
            stage_map[name] = stage(name, stage_stack.empty() ? nullptr : stage_stack.back());
            if( !stage_stack.empty() ) {
                stage_stack.back()->childs.push_back(&(stage_map.at(name)));
            }
        } else {
            stage_map.at(name).start();
        }
        stage_stack.push_back(&(stage_map.at(name)));
    }

    static void sync(MPI_Comm comm);

    static void end_stage() {
        if( stage_stack.empty() ) {
            debug::error("Empty stage stack!",
                         mpi::world, mpi::Policy::Self, debug::profiler);
        }

        stage_stack.back()->stop();

        stage * top = stage_stack.back();
        stage_stack.pop_back();

        std::stringstream strs;
        strs << "Terminated stage: " << top->name
             << " (now in " << stage_stack.back()->name << ")";
        debug::verbose(strs.str(), mpi::world,
                       mpi::Policy::Self, debug::profiler);
    }

    static stage* current_stage() {
        return stage_stack.empty() ? nullptr : stage_stack.back();
    }

    static void timestamp(const std::string & tag) {
        auto now = boost::chrono::high_resolution_clock::now();
        timestamps.emplace_back(make_tuple(tag, now));
    }

    static void print();

private:
    static time_point start_time;
    static time_point end_time;

    static std::list<stage*> stage_stack;
    static std::map<std::string, stage> stage_map;

    static std::vector<std::tuple<std::string, time_point>> timestamps;

    template <class tree>
    static void add_timestamps_to_ptree(tree & ptree) {

        int I = 0;
        for(auto & entry: timestamps) {
            tree pt_timestamp;
            pt_timestamp.put("<xmlattr>.name", std::get<0>(entry));
            pt_timestamp.put("<xmlattr>.id",  I++);
            pt_timestamp.put("<xmlattr>.elapsed",
                             boost::chrono::duration_cast<duration_t>(
                                     std::get<1>(entry) - start_time).count()
                             / 1e9);
//            pt_timestamp.§
            ptree.add_child("Timestamp", pt_timestamp);
        }
    }
};
