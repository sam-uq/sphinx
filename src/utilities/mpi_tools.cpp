/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include "mpi_tools.hpp"

#include "debug.hpp"
#include "mpi.hpp"

using namespace sphinx;

const char* _mpiGetErrorEnum(mpiError_t error) {
    switch (error) {
    CASE_RETURN_STR(MPI_SUCCESS);
    CASE_RETURN_STR(MPI_ERR_COMM);
    CASE_RETURN_STR(MPI_ERR_COUNT);
    CASE_RETURN_STR(MPI_ERR_TYPE);
    CASE_RETURN_STR(MPI_ERR_BUFFER);
    CASE_RETURN_STR(MPI_ERR_ROOT);
    default:
        return "<unknown>";
    }
}

int __mpiSafeCall(mpiError_t err, const char *file, const int line, bool abort) {
    if( MPI_SUCCESS != err ) {
        std::stringstream ss;
        ss << debug::mpi_error << " " << err << ": " << _mpiGetErrorEnum(err) << " "
           << " in file " << file << " at line " << line << "." << std::endl;
        debug::error(ss.str(), mpi::world, mpi::Policy::Collective, debug::mpi);
        if (abort) exit(err);
    }
    return err;
}

int __mpiStatusCheck(const mpiStatus_t &status, const char *file, const int line) {
    if( MPI_SUCCESS != status.MPI_ERROR ) {
        std::stringstream ss;
        ss << debug::mpi_error << " " << status.MPI_ERROR << ": " << _mpiGetErrorEnum(status.MPI_ERROR) << " "
           << " in file " << file << " at line " << line << "." << std::endl;
        debug::error(ss.str(), mpi::world, mpi::Policy::Collective, debug::mpi);
    }
    return status.MPI_ERROR;
}

int waitall(std::vector<MPI_Request> reqs) {
   std::vector<MPI_Status> stats(reqs.size());
   if( reqs.size() == 0 ) return 0;
   int ret = MPI_Waitall(reqs.size(), reqs.data(), stats.data());
   for(auto stat: stats) {
       mpiStatuschk(stat);
       // TODO
   }
   return ret;
}
