/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "../sphinx_internal.hpp"

#include <array>
#include <vector>
#include <array>

#include <mpi.h>

typedef int  accfftError_t;

//template <int dim>
void accfft_get_alloc(std::ptrdiff_t &alloc_time,
                      ptrdiff_t &alloc_freq,
                      std::array<ptrdiff_t, 3> &local_0_start,
                      std::array<ptrdiff_t, 3> &local_n0,
                      std::array<ptrdiff_t, 3> &local_0_start_trsp,
                      std::array<ptrdiff_t, 3> &local_n0_trsp,
                      const std::array<std::ptrdiff_t, 3> &N, MPI_Comm comm);

inline int __accfftSafeCall(accfftError_t, const char *, int, bool = true) {
    return 0;
}

void accfft_create_comm_wrapper(MPI_Comm comm,
                        std::array<int, 2> split,
                        MPI_Comm * newcomm);

#define accfftErrchk(ans) { __accfftSafeCall((ans), __FILE__, __LINE__); }
