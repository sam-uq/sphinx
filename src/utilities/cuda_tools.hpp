/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "config.hpp"

#include <cuda_runtime.h>
#include <cufft.h>
#include <cublas_v2.h>

#define gpuErrchk(ans) { sphinx::cuda::__cudaSafeCall((ans), __FILE__, __LINE__); }
#define fftErrchk(ans) { sphinx::cuda::__cufftSafeCall((ans), __FILE__, __LINE__); }
#define cublasErrchk(ans) { sphinx::cuda::__cublasSafeCall((ans), __FILE__, __LINE__); }

namespace sphinx {
namespace cuda {

int __cufftSafeCall(cufftResult err, const char *file, int line, bool abort = true);
int __cudaSafeCall(cudaError_t err, const char *file, int line, bool abort = true);
int __cublasSafeCall(cublasStatus_t err, const char *file, int line, bool abort = true);

cublasStatus_t cuda_init(int argc, char**argv);
cublasStatus_t cuda_term();

extern cublasHandle_t handle;

} // END namespace sphinx
} // END namespace cuda
