/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include "fftw_tools.hpp"

#include "include_fftw.hpp"

//template <int dim>
void fftw_get_alloc(std::ptrdiff_t &alloc_time, std::ptrdiff_t &alloc_freq,
                    std::ptrdiff_t &local_0_start, std::ptrdiff_t &local_n0,
                    std::ptrdiff_t &local_0_start_trsp, std::ptrdiff_t &local_n0_trsp,
                    const std::array<std::ptrdiff_t, 2> &N, MPI_Comm  comm) {
//    std::ptrdiff_t alloc = fftw_mpi_local_size_2d(timeN[0], timeNpad[1], MPI_COMM_WORLD, &local_n0, &local_0_start);
    std::ptrdiff_t alloc = fftw_mpi_local_size_2d_transposed(N[0], N[1], comm,
            &local_n0, &local_0_start,
            &local_n0_trsp, &local_0_start_trsp);
    alloc_time = 2*alloc;
    alloc_freq = alloc;
}

//template <>
void fftw_get_alloc(std::ptrdiff_t &alloc_time, std::ptrdiff_t &alloc_freq,
                    std::ptrdiff_t &local_0_start, std::ptrdiff_t &local_n0,
                    std::ptrdiff_t &local_0_start_trsp, std::ptrdiff_t &local_n0_trsp,
                    const std::array<std::ptrdiff_t, 3> &N, MPI_Comm comm) {
//    std::ptrdiff_t alloc = fftw_mpi_local_size_3d(timeN[0], timeNpad[1], timeNpad[2], MPI_COMM_WORLD, &local_n0, &local_0_start);
    std::ptrdiff_t alloc = fftw_mpi_local_size_3d_transposed(N[0], N[1], N[2], comm,
            &local_n0, &local_0_start,
            &local_n0_trsp, &local_0_start_trsp);
    alloc_time = 2*alloc;
    alloc_freq = alloc;
}
