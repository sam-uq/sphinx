/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

//! \file options.hpp
//! \brief File containing the definition of the class \ref Options.
//!
//!
//!
//! \author Filippo Leonardi
//! \date 2017-01-01

#include <boost/program_options.hpp>

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

#include "debug.hpp"

#define DEFAULT_CONFIG_FILE "conf.ini"

namespace sphinx {

namespace po = boost::program_options;

//! \brief Handler for all program options.
//!
//!
//! This routine reads the command line arguments and the configuration files and parses all the
//! options that are specified and valid into an internal map.
//!
//! Upon request the class returns the requested variable with the appropriate type.
//!
//! The default configuration file is a file called "conf.ini" located in the working directory.
//!
//! You can use the option "--help" to print the most up to date configuration options.
//!
//! \see \ref available-options for the list of all available options.
//!
//! \warning The command line has the precedence on the configuration file.
//!
class Options {
public:
    //! \brief Constructor, name the option context
    Options() : desc("Allowed options") {}

    //! \brief Perform the reading of args and of conf file
    int read(int argc, char** argv);

    //! \brief Print extra help and optionally abort.
    //! \param abort    Abort execution if this is true.
    void help(bool abort = true) const;

    //! \brief Check if option is present.
    //!
    //! Looks on the variable maps and check if the variable is present.
    //!
    //! \param[in]  s    Name of the option of type \t T (may not exist).
    //! \return          True if option exists, false otherwise.
    bool has(std::string s) {
        return vm.count(s) != 0;
    }

    //! \brief Returns the value of the given option.
    //!
    //! The option must be of the requrested type.
    //! The function checks if casting is possible.
    //! \tparam     T    Type of the option, to which the option will be casted.
    //! \param[in]  s    Name of the option of type \t T (must exist).
    //! \return          The value of the option casted to \t T.
    template <class T>
    T as(std::string s) {
        if( !has(s) ) debug::error("Option " + s + " not found",
                                   mpi::world, mpi::Policy::Collective, debug::option);
        return vm[s].as<T>();
    }

    //! A description of the option context.
    po::options_description desc;

    //! Contains the entire options for the program (the variable map).
    po::variables_map vm;
};

//! Nasty global variable containing all program options. Handle with care. Defined in "options.cpp".
extern Options options;

} // END namespace sphinx
