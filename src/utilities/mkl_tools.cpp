/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include "mkl_tools.hpp"

#include <mkl.h>

#if HAVE_MPI
#include <mkl_cdft.h>
#endif

#include <sstream>

#include "debug.hpp"
#include "mpi.hpp"

using namespace sphinx;

//template <unsigned int dim>
void mkl_get_alloc(std::ptrdiff_t &alloc_time, std::ptrdiff_t &alloc_freq,
                    std::ptrdiff_t &local_0_start, std::ptrdiff_t &local_n0,
                    std::ptrdiff_t &local_0_start_trsp, std::ptrdiff_t &local_n0_trsp,
                    const std::array<std::ptrdiff_t, 2> &N, MPI_Comm comm, DFTI_DESCRIPTOR_DM_HANDLE& hand) {

    dftiErrchk(DftiCreateDescriptorDM(comm, &hand, DFTI_DOUBLE, DFTI_REAL, 2, N.data()));

//    dftiErrchk(DftiSetValueDM(hand, DFTI_CONJUGATE_EVEN_STORAGE, DFTI_COMPLEX_COMPLEX));
    //dftiErrchk(DftiSetValueDM(hand, DFTI_PACKED_FORMAT, DFTI_CCE_FORMAT));
    dftiErrchk(DftiSetValueDM(hand, DFTI_TRANSPOSE, DFTI_ALLOW));
    dftiErrchk(DftiSetValueDM(hand, DFTI_PLACEMENT, DFTI_NOT_INPLACE));
    //status = DftiSetValueDM(hand, DFTI_INPUT_STRIDES, 1);
    //status = DftiSetValueDM(hand, DFTI_OUTPUT_STRIDES, 1);

    dftiErrchk(DftiCommitDescriptorDM(hand));

    dftiErrchk(DftiGetValueDM(hand,CDFT_LOCAL_SIZE, &alloc_time));
    dftiErrchk(DftiGetValueDM(hand,CDFT_LOCAL_NX, &local_n0));
    dftiErrchk(DftiGetValueDM(hand,CDFT_LOCAL_X_START, &local_0_start));
    dftiErrchk(DftiGetValueDM(hand,CDFT_LOCAL_OUT_NX, &local_n0_trsp));
    dftiErrchk(DftiGetValueDM(hand,CDFT_LOCAL_OUT_X_START, &local_0_start_trsp));

    // WEIRD ALLOC THING, CF.  /opt/intel/mkl/interfaces/fftw3x_cdft/wrappers/mpi_transpose.c
    std::ptrdiff_t alloc = std::max(local_n0 * (N[1]+2), 2*local_n0_trsp * N[0]);
    alloc_time = alloc;
    alloc_freq = alloc/2;
}

void mkl_get_alloc(std::ptrdiff_t &alloc_time, std::ptrdiff_t &alloc_freq,
                    std::ptrdiff_t &local_0_start, std::ptrdiff_t &local_n0,
                    std::ptrdiff_t &local_0_start_trsp, std::ptrdiff_t &local_n0_trsp,
                    const std::array<std::ptrdiff_t, 3> &N, MPI_Comm comm, DFTI_DESCRIPTOR_DM_HANDLE& hand) {

    dftiErrchk(DftiCreateDescriptorDM(comm, &hand, DFTI_DOUBLE, DFTI_REAL, 3, N.data()));

//    dftiErrchk(DftiSetValueDM(hand, DFTI_CONJUGATE_EVEN_STORAGE, DFTI_COMPLEX_COMPLEX));
    //dftiErrchk(DftiSetValueDM(hand, DFTI_PACKED_FORMAT, DFTI_CCE_FORMAT));
    dftiErrchk(DftiSetValueDM(hand, DFTI_TRANSPOSE, DFTI_ALLOW));
    dftiErrchk(DftiSetValueDM(hand, DFTI_PLACEMENT, DFTI_NOT_INPLACE));
    //status = DftiSetValueDM(hand, DFTI_INPUT_STRIDES, 1);
    //status = DftiSetValueDM(hand, DFTI_OUTPUT_STRIDES, 1);

    dftiErrchk(DftiCommitDescriptorDM(hand));

    dftiErrchk(DftiGetValueDM(hand,CDFT_LOCAL_SIZE, &alloc_time));
    dftiErrchk(DftiGetValueDM(hand,CDFT_LOCAL_NX, &local_n0));
    dftiErrchk(DftiGetValueDM(hand,CDFT_LOCAL_X_START, &local_0_start));
    dftiErrchk(DftiGetValueDM(hand,CDFT_LOCAL_OUT_NX, &local_n0_trsp));
    dftiErrchk(DftiGetValueDM(hand,CDFT_LOCAL_OUT_X_START, &local_0_start_trsp));

    alloc_time = local_n0 * N[1] * (N[2]+2);
    alloc_freq = local_n0_trsp * (N[2]/2+1) * N[1];
}

const char* _dftiGetErrorEnum(dftiError_t error) {
    switch (error) {
    CASE_RETURN_STR(DFTI_NO_ERROR);
    CASE_RETURN_STR(DFTI_MEMORY_ERROR);
    CASE_RETURN_STR(DFTI_INVALID_CONFIGURATION);
    CASE_RETURN_STR(DFTI_INCONSISTENT_CONFIGURATION);
    CASE_RETURN_STR(DFTI_NUMBER_OF_THREADS_ERROR);
    CASE_RETURN_STR(DFTI_MULTITHREADED_ERROR);
    CASE_RETURN_STR(DFTI_BAD_DESCRIPTOR);
    CASE_RETURN_STR(DFTI_UNIMPLEMENTED);
    CASE_RETURN_STR(DFTI_MKL_INTERNAL_ERROR);
    CASE_RETURN_STR(DFTI_1D_LENGTH_EXCEEDS_INT32);
    CASE_RETURN_STR(CDFT_SPREAD_ERROR);
    CASE_RETURN_STR(CDFT_MPI_ERROR);
    default:
        return "<unknown>";
    }
}

int __dftiSafeCall(dftiError_t err, const char *file, const int line, bool abort) {
    if( DFTI_NO_ERROR != err ) {
        std::stringstream ss;
        ss << debug::mkl_error << " " << err << ": " << _dftiGetErrorEnum(err) << " "
           << " in file " << file << " at line " << line << "." << std::endl;
        debug::error(ss.str(), mpi::world, mpi::Policy::Collective, debug::mkl);
        if (abort) exit(err);
    }
    return err;
}
