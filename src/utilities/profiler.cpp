/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include "profiler.hpp"

#if USE_CUDA
    #include <cuda_runtime.h>
#endif // USE_CUDA

#include <boost/filesystem.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

// This file is a nightmare just to avoid compiling issues with CUDa non playing nice with boost /cf. README.md)

//! Definition of counter for stage indexing
unsigned int profiler::stage::counter = 0;

//! Declaration of stage stack
std::list<profiler::stage*> profiler::stage_stack;

//! Declaration of stage map
std::map<std::string, profiler::stage> profiler::stage_map;

std::vector<std::tuple<std::string, profiler::time_point>> profiler::timestamps;

profiler::time_point profiler::start_time;
profiler::time_point profiler::end_time;

using ptree_t = boost::property_tree::ptree;

//! Declaration of property tree (FIXME: replace with this all stage and stuff business)
static ptree_t ptree;

void profiler::sync(MPI_Comm comm) {
#if PROFILING_SYNC
    profiler::start_stage("sync");
    MPI_Barrier(comm);
    profiler::end_stage();
#endif // PROFILING_SYNC
}

void profiler::stage::stop() {
#if USE_CUDA && PROFILING_SYNC
    cudaDeviceSynchronize();
#endif // USE_CUDA
//    elapsed += std::chrono::duration_cast<duration_t>(std::chrono::high_resolution_clock::now() - start_time);
    elapsed += boost::chrono::duration_cast<duration_t>(boost::chrono::high_resolution_clock::now() - start_time);
    running = false;
}

void profiler::print() {
    boost::filesystem::create_directory(PROFILING_DIR);

    stage_map["main_stage"].print();
    stage_map["main_stage"].append_to_ptree(ptree);

    debug::message("Timestamps:", mpi::world,
                   mpi::Policy::Self, debug::profiler);
    for(auto & entry: timestamps) {
        std::stringstream ss;
        ss << std::setw(30) << std::get<0>(entry)
           << std::setw(30) << (std::get<1>(entry) - start_time).count() / 1e9;

        debug::message(ss.str(), mpi::world,
                       mpi::Policy::Self, debug::profiler);
    }
    add_timestamps_to_ptree(ptree);

#if OLD_GCC
           boost::property_tree::xml_writer_settings<char> settings(' ', 1);
#else
           boost::property_tree::xml_writer_settings<std::string> settings(' ', 1);
#endif // OLD_GCC

    std::stringstream ss_name;
    ss_name << PROFILING_DIR << "/" << sphinx::name << "_" << "profiler_rk"
            << sphinx::mpi::world.rank << ".xml";
    write_xml(ss_name.str(), ptree, std::locale(), settings);
}
