/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "types.hpp"

#include <cuda_runtime.h>
#include <cufft.h>

namespace sphinx {
namespace types {

typedef double               cuda_host_time_t;
typedef double               cuda_device_time_t;
typedef cufftDoubleComplex   cuda_device_freq_t;

}
}

//! Given index (as local index), returns the coordinate at the index
//!
//! \tparam dim
//! \param idx
//! \param coord
//! \param device_size
//! \param local_0_start
template <unsigned int dim>
inline __device__ void coord(unsigned int* idx, real_t* coord,
                      unsigned int* device_size, unsigned int local_0_start) {
    coord[0] = (real_t) (idx[0] + local_0_start + .5) / device_size[0];
    for(int d = 1; d < dim; ++d) {
        coord[d] = (real_t) (idx[d] + .5) / device_size[d];
    }
}

//!
//!
//! \param left
//! \param right
//! \return
inline __device__ __host__ cuDoubleComplex operator*(const cuDoubleComplex & left, double right) {
    return make_cuDoubleComplex(cuCreal(left) * right, cuCimag(left) * right);
}