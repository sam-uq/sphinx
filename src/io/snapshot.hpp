/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

//#include "io_utils.hpp"

#include <vector>
#include <list>
#include <string>
#include <cstring>
#include <memory>
#include <unordered_map>

#include <boost/property_tree/ptree.hpp>

#include "sphinx_internal.hpp"
//#include "operators/host_operators.hpp"

#include "hdf5writer.hpp"

//#define GRID_CENTER  "Cell"
#define GRID_CENTER "Node"
// TODO: 3d, to do continuous, adjust grid FIXME!!!!!!!

using boost::property_tree::ptree;
using namespace sphinx;

//!
//!
//!
//!
//!
//! \tparam VectorType
template<class VectorType>
class Snapshot {
public:
    using grid_t = typename VectorType::grid_t;

    static constexpr int dim = VectorType::dim;

    //!
    //! \brief Snapshot create a new snapshot (i.e. time
    //! stamp from the solution). Shall be called by
    //! a Solution instance.
    //! \param grid_p
    //! \param t
    //! \param store_components
    //! \param output_on_push
    //!
    Snapshot(std::shared_ptr<grid_t> grid_p, const double t,
             bool store_components = false, bool output_on_push = false)
            : t(t), frame(frame_counter++)
              , store_components(store_components)
              , output_on_push(output_on_push), hf(grid_p) {

    }

    //!
    //!
    //!
    //!
    //! \param grid_p
    //! \param t
    //! \param frame
    //! \param store_components
    //! \param output_on_push
    Snapshot(std::shared_ptr<grid_t> grid_p, const double t, unsigned int frame,
             bool store_components = false, bool output_on_push = false)
            : t(t), frame(frame)
              , store_components(store_components)
              , output_on_push(output_on_push), hf(grid_p) {

    }

    //!
    //!
    //!
    //!
    //! \param grid_p
    //! \param other
    Snapshot(std::shared_ptr<grid_t> grid_p, const Snapshot<VectorType> &other)
            : t(other.t), frame(other.frame)
              , store_components(other.store_components)
              , output_on_push(other.output_on_push), hf(grid_p) {

        comps = other.comps;
        names = other.names;
        num_data = other.num_data;
        if (store_components) { components = other.components; }
    }

    virtual ~Snapshot() = default;

    //!
    //!
    //!
    //!
    void setZero() {
        for (VectorType &V: components) {
            set(V, 0); // FIXNME
        }
    }

    //!
    //!
    //!
    //!
    //! \param alpha
    void rescale(double alpha) {
        for (VectorType &V: components) {
            scale(V, alpha);
        }
    }

    //!
    //!
    //!
    //!
    //! \param prefix
    void open(std::string prefix) {
        is_open = true;
        hf.open(prefix, frame);
    }

    //!
    //! \brief pushData push new data to the snapshoot
    //!
    //! You can select which data to push using the comp
    //! argument.

    //! \param V
    //! \param comp
    //!
    void pushData(const VectorType &V, std::vector<int> comp) {
        num_data++;
        comps.push_back(comp);
        names.push_back(V.name);

        if(store_components) components.push_back(V);
        if(output_on_push) hf.pushData(V, comp);

        dofs += comp.size();
        maxdof = std::max(maxdof, V.dof);
    }

    //! \brief Add extra data to the snapshot.
    //!
    //! WIP
    //!
    //! \param label
    //! \param data
    void pushExtraData(std::string label, double data) {
        extra_data[label] = data;
    }

    //! \brief Push historgram data onto the snapshot.
    //!
    //! WIP
    //!
    //! \param coord
    //! \param data
    void pushHistogram(const pos_t<dim> &coord, const data_t<dim> data) {
        histogram.emplace_back(std::make_pair(coord, data));
    }

    //! \brief Pushes all stored compoents onto the writer at once
    //!
    //!
    //!
    //!
    void write() {
        for(unsigned int i = 0; i < comps.size(); ++i) {
            dofs += comps.at(i).size();
            hf.pushData(components.at(i), comps.at(i));
        }
    }

    //! \brief Calls writedata on writer, performs actual write
    //!
    //!
    //!
    //!
    void writeData() {
        assert(is_open && "open() wasn't called, maybe forgot save_on_push?");
        std::stringstream ss;
        ss << "Saving new snapshot at time t = " << t
                << " (frame " << frame << ") with " << dofs << " dofs";
        debug::message(ss.str(), mpi::world, mpi::Policy::Collective, debug::io);
        fname = hf.writeData();
    }

    //! \brief Attach a property tree (to a given root) describing this snapshot in Xdmf format.
    //!
    //! Insert this Snapshot as a Grid into the property tree desctribing the Xml structure of
    //! the Xdmf formmat. This will be set as child element of given ptree, and uses data from
    //! the grid given as input.
    //!
    //! TODO: switch to shared_ptr for G.
    //!
    //! \tparam        G      class containing grid information
    //! \param[in,out] pt     property try, to which snapshot information is added
    //! \param[in]     grid_p grid from which obtain information about current snapshot
    template <class G, class S>
    void addToPTree(ptree & pt, G grid_p, const S & sol, bool freq = false) {

        unsigned int offset = 0;
        unsigned int nj = grid_p->timeN[1];
        unsigned int ni = grid_p->timeN[2];
        if( freq ) {
            offset = 2;
            nj = grid_p->freqN[0]*(G::element_type::dims == 2 ? 2 : 1);
            ni = grid_p->freqN[2]*(G::element_type::dims == 3 ? 2 : 1);
        }

        ptree pt_collection;
        // Loop over each process (aka subdomain)
        for(int p = 0; p < grid_p->comm.size; ++p) {
            // This guy was marked to be skipped
            if(sol.skip.count(p) > 0) {
                continue;
            }
            ptree pt_subdomain;
            pt_subdomain.put("<xmlattr>.Name", "p" + std::to_string(p));
            pt_subdomain.put("<xmlattr>.GridType", "Uniform");
            pt_subdomain.put("Topology.<xmlattr>.Reference",
                             "/Xdmf/Domain/Topology[" + std::to_string(p+1) + "]");
            pt_subdomain.put("Geometry.<xmlattr>.Reference",
                             "/Xdmf/Domain/Geometry[" + std::to_string(p+1) + "]");
            // Loop over each push back
            // TODO: put into function
            for(int r = 0; r < num_data; ++r) {
                // Loop over each component of each push back
                // FIXME: non-contiguous components?
                for(unsigned int c = 0; c < comps.at(r).size(); ++c) {
                    ptree pt_component;
                    std::string cname = names.at(r) + "_" + std::to_string(comps.at(r).at(c));
                    pt_component.put("<xmlattr>.Name", cname);
                    pt_component.put("<xmlattr>.Center", GRID_CENTER);
                    pt_component.put("DataItem.<xmlattr>.Format", "HDF");
                    pt_component.put("DataItem.<xmlattr>.DataType", "Double");
                    pt_component.put("DataItem.<xmlattr>.Precision", "4");
                    pt_component.put("DataItem.<xmlattr>.Dimensions",
                                     std::to_string(grid_p->partitioning[4*p+offset+1])
                                     + " " + std::to_string(nj)
                                     + (G::element_type::dims == 3 ? " " + std::to_string(ni) : " "));
                    pt_component.put("DataItem", fname.at(p) + ":/" + cname);

                    pt_subdomain.add_child("Attribute", pt_component);
                }
            }
            pt_collection.add_child("Grid", pt_subdomain);
        }

        pt_collection.put("<xmlattr>.Name", "f" + std::to_string(frame));
        pt_collection.put("<xmlattr>.GridType", "Collection");
        pt.add_child("Grid", pt_collection);
    }

    //! Time at which the snapshot is taken
    double t = -1;
    //! Access to dofs of the snapshot (sum of all dofs pushed onto the snapshot)
    int dofs = 0;
    //! Tracks the frame number
    int frame = 0;
    //! Internal storage if components ough to be stored
    std::vector<VectorType> components;

    //! Push compoents into this.
    bool store_components;
    //! When output is pushed, write with writer and close file
    bool output_on_push;
private:
    //! Number of items pushed onto the snapshot
    int num_data = 0;
    //! Tracks the total number of frames taken
    static int frame_counter;
    //! List of all file names written by the hfile
    std::vector<std::string> fname;
    //! Contains the writer to use for the data
    HDF5Writer<typename VectorType::grid_t> hf;
    //! List of the names of the components for each push back
    std::vector<std::string> names;
    //! Vector of components (numbers) for each pushed back data
    std::vector<std::vector<int>> comps;

    //! Extra labelled data (such as energy and whatnot).
    std::unordered_map<std::string, double> extra_data;
    //!
    std::vector<std::tuple<pos_t<dim>, data_t<dim>>> histogram;

    //! Is writer open by the snapshot?
    bool is_open = false;
public:
    //! Maximum dof of all components
    unsigned int maxdof = 0;
};

template <class T>
int Snapshot<T>::frame_counter = 0;

template <class T>
void axpbypz(Snapshot<T> & z, const Snapshot<T> & y, const Snapshot<T> & x,
             double alpha, double beta) {
    for(unsigned int c = 0; c < z.components.size(); ++c) {
        axpbypz(z.components[c], y.components[c], x.components[c], alpha, beta);
    }
}

template <class T>
void axpy(Snapshot<T> & y, const Snapshot<T> & x, double alpha) {
    for(unsigned int c = 0; c < y.components.size(); ++c) {
        axpy(y.components[c], x.components[c], alpha);
    }
}

template <class T>
void xmytz(Snapshot<T> & z, const Snapshot<T> & x, const Snapshot<T> & y) {
    for(unsigned int c = 0; c < z.components.size(); ++c) {
        xmytz(z.components[c], x.components[c], y.components[c]);
    }
}

template <class T, class R_p>
void glue(std::shared_ptr<Snapshot<T>> mean, std::shared_ptr<Snapshot<T>> M2, unsigned int & count,
          const mpi::IntraDomainComm & intraDomainComm, std::vector<R_p> & remote_grids, int frame) {

//    const int tag = sphinx::mav_count_tag;
    int count_copy;

    // Loop over all components of mean and variance
    for(unsigned int c = 0; c < mean->components.size(); ++c) {
        // Async MPIs
        if(intraDomainComm.color == 0) {
//            MPI_Status status;
            T& meanV = mean->components.at(c);
            T& M2V = M2->components.at(c);

            T delta(meanV.grid_p, meanV.dof);
            T Rcv(meanV.grid_p, meanV.dof);
            // delta buffer
            // Rcv buffer

            // Can be optimized
            count_copy = count;
            for(R_p rgrid: remote_grids) {
                int other_count = rgrid->tot_sim;

                recv(delta, rgrid, generate_tag(sphinx::mean_subtag, c, frame));

//                delta = meanV - delta; // axpydz
                axpy(delta, meanV, -1.); // y += a*x;

                recv(Rcv, rgrid, generate_tag(sphinx::var_subtag, c, frame));

//                meanV += delta * (other_count / (double)(count + other_count)); // axpy
                axpy(meanV, delta, (other_count / (double) (count_copy + other_count)));

//                M2 += Rcv + delta**2 * (count * other_count / (double)(count + other_count)); //axpbypdz
                applyf(delta, [] (double x) { return x*x; });
                axpbypz(M2V, Rcv, delta, (count_copy * other_count / (double) (count_copy + other_count)), 1);
                count_copy += other_count;

            }
        } else {
//            MPI_Send(&count, 1, MPI_INTEGER, /**/, tag, intraLevelComm.comm); /**//**//**/

            assert(remote_grids.size() == 1);
            send(mean->components.at(c), remote_grids.at(0), generate_tag(sphinx::mean_subtag, c, frame));
            send(M2->components.at(c), remote_grids.at(0), generate_tag(sphinx::var_subtag, c, frame));
        }
    }
    count = count_copy; //!!!!!!!!
}
