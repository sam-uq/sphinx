/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include <vector>
#include <string>
#include <list>

#include "H5Cpp.h"

#include "io.hpp"

#include "sphinx_internal.hpp"
#include "utilities/profiler.hpp"

#include "containers/host_vec.hpp"

#ifndef H5_NO_NAMESPACE
using namespace H5;
#endif

//! \brief Implements I/O of data in HDF5 format, each process
//! outputs is own data.
//!
//! Data can be pushed back once the file has bee onened,
//! this class must be finalized.
//!
//! \tparam G
template <class G>
class HDF5Writer {
public:
    explicit HDF5Writer(std::shared_ptr<G> grid_p) : grid_p(grid_p) { /* */ }

    virtual ~HDF5Writer() = default;

    //! \brief Open a file having a prescribed prefix and prescribed frame
    //!
    //! Frame is required separately in order to have ending "_f<frame>.h5"
    //!
    //! \param[in] prefix_ string prepended to filename
    //! \param[in] frame_ frame of the output
    void open(std::string prefix_, int frame_) {
        frame = frame_;
        prefix = prefix_;
        file = H5File(sphinx::io::file_name(prefix,
                                            grid_p->comm.rank, frame),
                      H5F_ACC_TRUNC);
    }

    template<class T, typename> struct type_ { typedef T type; };

    //! \brief Push a new vector into the writer, specify which
    //! components to extract.
    //!
    //!
    //!
    //! \tparam T type for which perform the output
    //! \param[in] data  Data pushed back to the writer
    //!                  (usually some scalar/vector field)
    //! \param[in] comp  Components of the vector to output
    //! TODO: implement empty comp. vector
    //! TODO: must check how this perform (in parallel and with multiple push backs)
    template <class T, unsigned int d>
    void pushData(const HostVec<T,d, false, false> & data, std::vector<int> comp) {
        profiler::start_stage("HDF5Writer::pushData");
        hsize_t     dimsf[d]; // dataset dimensions
        dimsf[0] = data.kend; //data.grid_p->local_n0;
        dimsf[1] = data.nj; //data.grid_p->timeNpad[1];
        if(d >= 3 ) dimsf[2] = data.ni; //grid_p->timeNpad[2];
        dimsf[d-1] *= data.data_size; // Enlarge dat
        long long unsigned int arr1[] = {0,0,0};
//        long long unsigned int arr2[] = {1,1,1};
//        long long unsigned int arr3[] = {1,1,1};
        long long unsigned int arr4[d];
        arr4[0] = (long long unsigned int) data.kend; //grid_p->local_n0;
        arr4[1] = (long long unsigned int) data.jend; //grid_p->timeN[1];
        if( d >= 3 ) arr4[2] = (long long unsigned int) data.iend; //grid_p->timeN[2];
        arr4[d-1] *= data.data_size; // Enlarge dat
        H5::DataSpace dataspace( d, dimsf );

//        hid_t s1_tid = H5Tcreate (H5T_COMPOUND, sizeof(typename T::data_t));
//        H5Tinsert(s1_tid, "real", 0, H5T_NATIVE_DOUBLE);
//        H5Tinsert(s1_tid, "complex", sizeof(double), H5T_NATIVE_DOUBLE);

        IntType datatype( PredType::NATIVE_DOUBLE );
        datatype.setOrder( H5T_ORDER_LE );
        dataspace.selectHyperslab(H5S_SELECT_SET, arr4, arr1);

        for(auto c: comp) {
            DataSet dataset = file.createDataSet(data.name + "_" + std::to_string(c),
                                                 datatype, dataspace );
            dataset.write(data.data()[c], PredType::NATIVE_DOUBLE );
//            DataSet dataset = file.createDataSet(data.name + "_" + std::to_string(c)
//                                                 , s1_tid, dataspace );
//            dataset.write( data.data()[c], s1_tid );
            dataset.close();
        }
        dataspace.close();
        datatype.close();
        profiler::end_stage();
    }

    //! \brief Push a new vector into the writer, specify which
    //! components to extract, interlaced version.
    //!
    //! // TODO: implement empty comp. vector
    //! TODO: must check how this perform (in parallel and with multiple push backs)
    //!
    //!
    //! \tparam T type for which perform the output
    //! \param[in] data    Data pushed back to the writer
    //!                    (usually some scalar/vector field)
    //! \param[in] comp    Components of the vector to output
    template <class T, unsigned int d>
    void pushData(const HostVec<T,d, true, true> & data, std::vector<int> comp) {
        profiler::start_stage("HDF5Writer::pushData");
        hsize_t     dimsf[d + 1]; // dataset dimensions
        dimsf[0] = data.kend; //data.grid_p->local_n0;
        dimsf[1] = data.nj; //data.grid_p->timeNpad[1];
        if(d >= 3 ) dimsf[2] = data.ni; //grid_p->timeNpad[2];
        dimsf[d] = data.data_size * data.dof; // Enlarge dat

        for(auto c: comp) {
            long long unsigned int start[] = {0,0,0,0};
            start[d] = c;
            long long unsigned int stride[] = {1,1,1,1};
            long long unsigned int block[] = {1,1,1,1};
            long long unsigned int count[d+1];
            count[0] = (long long unsigned int) data.kend; //grid_p->local_n0;
            count[1] = (long long unsigned int) data.jend; //grid_p->timeN[1];
            if( d >= 3 ) count[2] = (long long unsigned int) data.iend; //grid_p->timeN[2];
            count[d] = 1; // Enlarge dat
            H5::DataSpace mem_dataspace( d+1, dimsf );
            long long unsigned int file_size[d];
            count[0] = (long long unsigned int) data.kend; //grid_p->local_n0;
            count[1] = (long long unsigned int) data.jend; //grid_p->timeN[1];
            if( d >= 3 ) count[2] = (long long unsigned int) data.iend; //grid_p->timeN[2];
            H5::DataSpace file_dataspace( d, count );

            //        hid_t s1_tid = H5Tcreate (H5T_COMPOUND, sizeof(typename T::data_t));
            //        H5Tinsert(s1_tid, "real", 0, H5T_NATIVE_DOUBLE);
            //        H5Tinsert(s1_tid, "complex", sizeof(double), H5T_NATIVE_DOUBLE);

            IntType datatype( PredType::NATIVE_DOUBLE );
            datatype.setOrder( H5T_ORDER_LE );
            mem_dataspace.selectHyperslab(H5S_SELECT_SET, count, start, stride, block);

            DataSet file_dataset = file.createDataSet(data.name + "_" + std::to_string(c),
                                                      datatype, file_dataspace );
            file_dataset.write( data.data(), PredType::NATIVE_DOUBLE, mem_dataspace, file_dataspace );
//            DataSet dataset = file.createDataSet(data.name + "_" + std::to_string(c)
//                                                 , s1_tid, dataspace );
//            dataset.write( data.data()[c], s1_tid );
            file_dataset.close();
            file_dataspace.close();
            mem_dataspace.close();
            datatype.close();
        }
        profiler::end_stage();
    }

    //! \brief Finalize data writing and output list of written filenames (by all processes)
    //! \return List of all file names written by all processes
    std::vector<std::string> writeData() {
        std::vector<std::string> files;
        for(int p = 0; p < grid_p->comm.size; ++p) {
            files.push_back(sphinx::io::file_name(prefix, p, frame));
        }
        file.close();
        return files;
    }

private:
    //! Prefix to prepend to file names
    std::string prefix;
    //! Frame (appended at the end of file name, before extension)
    int frame = 0;
    //! Handler for the H5File
    H5File file;
    //! Extension (unused) or something
//     const std::string _ext = ".h5";
    std::shared_ptr<G> grid_p;
};

