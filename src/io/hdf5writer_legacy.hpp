/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include <vector>
#include <string>
#include <list>
#include <memory>

#include <hdf5.h>

#include "io.hpp"

#include "sphinx_internal.hpp"
#include "utilities/profiler.hpp"

#include "containers/host_vec.hpp"

//! \brief Implements I/O of data in HDF5 format,
//! each process outputs is own data
//! Data can be pushed back once the file has bee onened, this class must be finalized
template <class G>
class HDF5Writer {
public:
    explicit HDF5Writer(std::shared_ptr<G> grid_p) : grid_p(grid_p) { /* */ }

    //! \brief Open a file having a prescribed prefix and prescribed frame
    //! Frame is required separately in order to have ending "_f<frame>.h5"
    //! \param[in] prefix_ string prepended to filename
    //! \param[in] frame_ frame of the output
    void open(std::string prefix_, int frame_) {
        frame = frame_;
        prefix = prefix_;
        file = H5Fcreate(sphinx::io::file_name(prefix, grid_p->comm.rank, frame).c_str(),
                         H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
        assert(file >= 0 && "Cannot load file!");
    }

    template<class T, typename> struct type_ { typedef T type; };

    //! \brief Push a new vector into the writer, specify which components to extract
    //! \tparam T type for which perform the output
    //! \param[in] data data pushed back to the writer (usually some scalar/vector field)
    //! \param[in] comp components of the vector to output // TODO: implement empty comp. vector
    //! TODO: must check how this perform (in parallel and with multiple push backs)
    template <class T, unsigned int d>
    void pushData(const HostVec<T,d, false, false> & data, std::vector<int> comp) {
        profiler::start_stage("HDF5Writer::pushData");
        hsize_t     dimsf[HostVec<T,d,false,false>::dim]; // dataset dimensions
        dimsf[0] = data.kend; //data.grid_p->local_n0;
        dimsf[1] = data.nj; //data.grid_p->timeNpad[1];
        if( HostVec<T,d,false,false>::dim >= 3 ) dimsf[2] = data.ni; //grid_p->timeNpad[2];
        dimsf[HostVec<T,d,false,false>::dim-1] *= data.data_size; // Enlarge dat
        long long unsigned int arr1[] = {0,0,0};
        long long unsigned int arr2[] = {1,1,1};
        long long unsigned int arr3[] = {1,1,1};
        long long unsigned int arr4[HostVec<T,d,false,false>::dim];
        arr4[0] = (long long unsigned int) data.kend; //grid_p->local_n0;
        arr4[1] = (long long unsigned int) data.jend; //grid_p->timeN[1];
        if( HostVec<T,d,false,false>::dim >= 3 ) arr4[2] = (long long unsigned int) data.iend * data.data_size; //grid_p->timeN[2];
        hid_t dataspace = H5Screate_simple( HostVec<T,d,false,false>::dim, dimsf, nullptr );
        assert(dataspace >= 0 && "Cannot create dataspace!");
        herr_t err = H5Sselect_hyperslab(dataspace, H5S_SELECT_SET, arr1, arr2, arr3, arr4);
        assert(err >= 0 && "Cannot select hyperslab!");

        for(auto c: comp) {
            hid_t property = H5Pcreate(H5P_DATASET_CREATE);
            hid_t dataset = H5Dcreate1(file, (data.name + "_" + std::to_string(c)).c_str(),
                                       H5T_NATIVE_DOUBLE, dataspace, property);
            assert(dataset >= 0 && "Cannot create dataset!");

            err = H5Dwrite( dataset, H5T_NATIVE_DOUBLE, dataspace, dataspace, H5P_DEFAULT, data.data()[c] );
            assert(err >= 0 && "Cannot write dataset!");

            err = H5Dclose( dataset );
            assert(err >= 0 && "Cannot close dataset!");

            err = H5Pclose( property );
        }
        err = H5Sclose( dataspace );
        assert(err >= 0 && "Cannot close dataspace!");
        profiler::end_stage();
    }

    //! \brief Finalize data writing and output list of written filenames (by all processes)
    //! \return List of all file names written by all processes
    std::vector<std::string> writeData() {
        std::vector<std::string> files;
        for(int p = 0; p < grid_p->comm.size; ++p) {
            files.push_back(sphinx::io::file_name(prefix, p, frame));
        }
        herr_t err = H5Fclose(file );
        assert(err >= 0 && "Cannot close file!");
        return files;
    }

private:
    //! Prefix to prepend to file names
    std::string prefix;
    //! Frame (appended at the end of file name, before extension)
    int frame = 0;
    //! Handler for the H5File
    hid_t file;
    //! Extension (unused) or something
//     const std::string _ext = ".h5";

    std::shared_ptr<G> grid_p;
};

