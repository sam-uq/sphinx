/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include <list>
#include <string>
#include <cstring>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

#include <memory>
#include <set>

#include "sphinx_internal.hpp"
#include "utilities/debug.hpp"
#include "utilities/mpi.hpp"

//! \file solution.hpp Contains the specification of a
//! Solution class.

using boost::property_tree::ptree;

using namespace sphinx;

namespace sphinx {

//! \namepsace io
//! \brief Namespace containing all Input/Output routines.
namespace io {

//! \brief Class containing the representation of a solutuion (as a time range of data).
//!
//! Takes care of writing its own Xml, and manages snapshots. Data is in Xdmf format with HDF5 backend.
//!
//! TODO: each snapshots should write own xml
//! TOOD: switch G to be directly a grid, rather than a shared_ptr (do this here)
//!
//! \tparam G type for the grid
//! \tparam S type for the snapshots
template<class G, class S>
class Solution {
    using G_p = std::shared_ptr<G>;
public:
    //!
    //!
    Solution(G_p grid_p, std::string prefix,
             bool store_components = false,
             bool output_on_push = false)
        : grid_p(grid_p), prefix(prefix)
        , store_components(store_components)
        , output_on_push(output_on_push) {
        /*  */
        fr = 0;
    }

    //! Avoids polluting the ptree and duplicate
    //! XDMF entries, if the solution class is reused
    //!
    //! Call when you want to reuse the solution strucutre.
    void reset() {
        pt = ptree();
    }

    virtual ~Solution() {}

    //! Automatically creates a snapshot with correct initialization
    //! and returns it to the caller.
    //!
    //! \param t
    //! \param frame
    //! \return
    std::shared_ptr<S> newSnapshot(double t,
                                   unsigned int frame =-1U) {
        std::shared_ptr<S> s(
                    new S(grid_p, t,
                          store_components, output_on_push
                          )
                    );
        if(frame != (unsigned int) -1) s->frame = frame;
        else s->frame = fr;
        ++fr;
        snapshots_p.push_back(s);
        if( output_on_push == true ) s->open(prefix);
        return s;
    }

    std::shared_ptr<S> getSnapshot(std::shared_ptr<S> snp,
                                   unsigned int frame) {
        std::shared_ptr<S> s;
        if( snapshots_p.size() <= frame ) {
            snapshots_p.resize(frame+1);
            s = std::shared_ptr<S>(new S(*snp));
            s->setZero();
            snapshots_p.at(frame) = s;
        } else {
            return snapshots_p.at(frame);
        }
        return s;
    }

    std::shared_ptr<S> getSnapshot(unsigned int frame) {
        return snapshots_p.at(frame);
    }

    //! Reads he ptree and saves all the data to and XML strucutred file.
    void writeXml(bool freq = false, std::string postfix = "",
                  bool only_last_frame = false) {
        if (grid_p->comm.rank != 0) return;
        std::stringstream ss1;
        std::string fname = prefix + postfix + format;
        ss1 << "Writing data to \"" << fname << "\"";
        debug::message(ss1.str(), grid_p->comm, mpi::Policy::Collective, debug::io);

//        gr->addToPTree("Xdmf.Domain", pt);

        unsigned int offset = 0;
        unsigned int nj = grid_p->timeN[1];
        unsigned int ni = grid_p->timeN[2];
        if (freq) {
            offset = 2;
            nj = grid_p->freqN[0];
            ni = grid_p->freqN[2];
        }

        pt.put("Xdmf.<xmlattr>.Version", "2.0");

        for (int p = 0; p < grid_p->comm.size; ++p) {
            // if this is zero, the data is empty and we skip this rank (also in the following pushes)
            if (grid_p->partitioning[4 * p + offset + 1] == 0) {
                skip.insert(p);
                continue;
            }

            // Origin of current processor (Ox and Oy)
            std::stringstream ss_origin;
            ss_origin << grid_p->partitioning[4 * p + offset] * grid_p->dx << " " << 0; // FIXME
            if (G::dims >= 3) ss_origin << " " << 0;


            // Origin of current processor (Ox and Oy)
            std::stringstream ss_deltas;
            ss_deltas << grid_p->dx << " " << grid_p->dy; // FIXME
            if (G::dims >= 3) ss_deltas << " " << grid_p->dz;

            // Space separated values with "extent" of mesh (dimensions nx and Ny)
            std::stringstream ss_extent;
            ss_extent << grid_p->partitioning[4 * p + offset + 1] << " " << nj * (freq ? 2 : 1); // FIXME
            if (G::dims >= 3) ss_extent << " " << ni;

            ptree pt_topology, pt_geometry;
            pt_topology.put("<xmlattr>.Name", "topo" + std::to_string(p));
            if (G::dims == 2) pt_topology.put("<xmlattr>.TopologyType", "2DCoRectMesh");
            else if (G::dims == 3) pt_topology.put("<xmlattr>.TopologyType", "3DCoRectMesh");
            pt_topology.put("<xmlattr>.Dimensions", ss_extent.str().c_str());
            pt_geometry.put("<xmlattr>.Name", "geo" + std::to_string(p));
            if (G::dims >= 3) pt_geometry.put("<xmlattr>.Type", "ORIGIN_DXDYDZ");
            else pt_geometry.put("<xmlattr>.Type", "ORIGIN_DXDY");

            // Ox and Oy
            ptree pt_origin;
            pt_origin.put("<xmlattr>.Format", "XML");
            pt_origin.put("<xmlattr>.Dimensions", std::to_string(G::dims));
            pt_origin.put("", ss_origin.str().c_str());
            // Dx Dy
            ptree pt_extent;
            pt_extent.put("<xmlattr>.Format", "XML");
            pt_extent.put("<xmlattr>.Dimensions", std::to_string(G::dims));
            pt_extent.put("", ss_deltas.str().c_str());

            pt_geometry.add_child("DataItem", pt_origin);
            pt_geometry.add_child("DataItem", pt_extent);
            pt.add_child("Xdmf.Domain.Topology", pt_topology);
            pt.add_child("Xdmf.Domain.Geometry", pt_geometry);
        }

        ptree pt_times;
        pt_times.put("<xmlattr>.Name", "TimeSeries");
        pt_times.put("<xmlattr>.GridType", "Collection");
        pt_times.put("<xmlattr>.CollectionType", "Temporal");
        pt_times.put("Time.<xmlattr>.TimeType", "List");
        pt_times.put("Time.DataItem.<xmlattr>.Format", "XML");
        pt_times.put("Time.DataItem.<xmlattr>.NumberType", "Float");

        if (only_last_frame) {
            pt_times.put("Time.DataItem.<xmlattr>.Dimensions", 1);
            pt_times.put("Time.DataItem", snapshots_p.back()->t);
            snapshots_p.back()->addToPTree(pt_times, grid_p, *this, freq);
        } else {
            pt_times.put("Time.DataItem.<xmlattr>.Dimensions", snapshots_p.size());
            std::stringstream ss;
            for (auto snapshot_p: snapshots_p) {
                ss << snapshot_p->t << " ";
            }
            pt_times.put("Time.DataItem", ss.str().c_str());
            for (auto snapshot_p: snapshots_p) {
                snapshot_p->addToPTree(pt_times, grid_p, *this, freq);
            }
        }
        pt.add_child("Xdmf.Domain.Grid", pt_times);

        std::ofstream file(fname);
        boost::property_tree::write_xml(
           file, pt,
#if OLD_GCC
           boost::property_tree::xml_writer_make_settings(' ', 1));
#else
           boost::property_tree::xml_writer_make_settings<std::string>(' ', 1));
#endif // OLD_GCC
    }

    void write() {
        for(std::shared_ptr<S> sp: snapshots_p) {
            sp->open(prefix);
            sp->write();
            sp->writeData();
        }
        writeXml();
    }

    //! vector of skipped ranks
    std::set<int> skip;

    //! Stores each snapshot for later use (when xml is written)
    std::vector<std::shared_ptr<S>> snapshots_p;
    //! Pointer to the grid class
    //! TODO: switch to shared_ptr inside here
    G_p grid_p;
    //! Prefix to prepend to solutions
    std::string prefix;
private:
    //! Contains the full representation of the full solution
    ptree pt;
    //! Extension to be used (Static?)
    const std::string format = ".xdmf";

    bool store_components;
    bool output_on_push;

    unsigned int fr;
};

} // END namespace io
} // END namsepace sphinx
