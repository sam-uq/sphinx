/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "timestepping/ssprk3.hpp"
#include "timestepping/ssprk2.hpp"
#include "timestepping/fe.hpp"

#include "timestepping/stepper.hpp"

#include "io/solution.hpp"
#include "io/snapshot.hpp"

#include "sphinx.hpp"

#include "solver/genericSolver.hpp"

//! \file conv_test.hpp Implements a convergence test on a
//! hieratrchy of grids

using Solver = solver::genericSolver<SamplerZ, Grd>;

//! \brief Main function implementing a complete test for convergence
int conv_test() {

    Set<1> data_type = Interlaced | R2CFFT | DestroyInput | DoublePrecision;
    FFTType fft_type =  DeviceTimeVector::fft_type;
    if( sphinx::options.has("fft_inplace") &&
            sphinx::options.as<bool>("fft_inplace") ) {
         data_type |= InPlace;
    }

    const unsigned int dim  = NDIMS;
    std::array<std::ptrdiff_t, dim> N;

    std::copy_n(sphinx::options.as<std::vector<int>>("mesh").begin(),
                dim, N.begin());

    std::vector<std::shared_ptr<io::Solution<Grd, Snpshot>>> sols;
    std::vector<std::shared_ptr<Solver>> solvers;

    std::shared_ptr<SamplerZ> smp(new SamplerZ());

    unsigned int levels = 2;
    if( sphinx::options.has("levels") ) {
        levels = sphinx::options.as<unsigned int>("levels");
    }

    unsigned int I = 0;
    for(std::array<std::ptrdiff_t, dim> Nx = N; I < levels; Nx *= 2) {

        std::shared_ptr<Grd> grid(new Grd(Nx, fft_type, data_type, false));
        grid->setup(sphinx::mpi::world);
        // TODO: move up and pass grid
        std::shared_ptr<InitialData<dim>> data =
                sphinx::Data::Factory(smp,
                                      grid,
                                      options.as<std::string>(
                                              "InitialData.name"));

        smp->sync(sphinx::master_seed());
        smp->set_idx(0);
        std::shared_ptr<Solver> solver_p(new Solver(smp,
               grid, data,
               0, sphinx::name + "_lvl" + std::to_string(I)));
        solver_p->start();

        sols.push_back(solver_p->sol_p);
        solvers.push_back(solver_p);

        ++I;
    }

    std::stringstream ss_err;

    std::vector<std::vector<std::vector<std::pair<double,double>>>> err;
    err.resize(sols.size());

    for(unsigned int m = 0; m < sols.size()-1; ++m) {
        auto sol = sols.at(m);
        err.at(m).resize(sol->snapshots_p.size());

        ss_err << "Mesh: " << m << "\t" << std::endl;

        io::Solution<Grd, Snapshot<Vector>> errsol(sols.back()->grid_p,
                                                         sphinx::name + "_" + "err_lvl" + std::to_string(m), true, true);

        for(unsigned int frame = 0; frame < sol->snapshots_p.size(); ++frame ) {
            auto snp = sol->snapshots_p.at(frame);
            auto snp_ref = sols.back()->snapshots_p.at(frame);

            err.at(m).at(frame).resize(snp->components.size());

            ss_err << "Frame: " << frame << "\t";

            auto errsnp = errsol.newSnapshot(frame);
            for(unsigned int c = 0; c < snp->components.size(); ++c) {
                Vector& V_ref = snp_ref->components.at(c);
                Vector& V = snp->components.at(c);

                DeviceFreqVector Vhat(V.grid_p, V.dof, "Vhat");
                DeviceFreqVector
                        V_interphat(V_ref.grid_p, V_ref.dof, "Vinterphat");
                DeviceTimeVector
                        V_interp(V_ref.grid_p, V_ref.dof, "Vinterp");

                solvers.at(m)->fft_s->forward(V, Vhat);

                zero_padding(Vhat, V_interphat); // FIXME ADD SCALING

                V_interp.name = V_ref.name + "err";

                solvers.back()->fft_s->backward(V_interphat, V_interp);

                scale(V_interp, std::pow(std::pow(2,NDIMS),sols.size()-1-m)); // UGLYNESS
                axpy(V_interp, static_cast<DeviceTimeVector>(V_ref),-1); // Add grid check

                errsnp->pushData(V_interp, range(V_interp.dof));

                double err2 = norm(V_interp,2);
                double errinf = norm(V_interp,INFINITY);
                err.at(m).at(frame).at(c) = std::make_pair(err2, errinf);
                ss_err << err2 << "\t" << errinf << "\t";
            }
            errsnp->writeData();

            ss_err << std::endl;
        }
        errsol.writeXml(false); // FIXME ADD CHECK
    }

    std::cout << ss_err.str();

    return 0;
}
