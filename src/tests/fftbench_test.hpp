/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include <algorithm>

#include "sphinx.hpp"

int fftbench_test(void) {
    std::array<std::ptrdiff_t, dim> N;

    std::copy_n(sphinx::options.as<std::vector<int>>("mesh").begin(),
                dim, N.begin());

    Set<1> data_type = Interlaced | R2CFFT | DestroyInput | DoublePrecision;
    FFTType fft_type = EvolutionVector::fft_type;
    if( sphinx::options.has("fft_inplace") &&
            sphinx::options.as<bool>("fft_inplace") ) {
         data_type |= InPlace;
    }

//    double padding = 3./2.;
//    if( sphinx::options.has("padding") ) {
//        padding = sphinx::options.as<double>("padding");
//    }
    std::shared_ptr<Grd> gr(new Grd(N, fft_type, data_type));

//    std::shared_ptr<Grd> gr_pad(new Grd(gr->timeN*padding,
//                                    fft_type,
//                                    data_type, false));

    Vector U(gr, dim, "U");
    DeviceTimeVector dU(gr, dim, "dU");
    DeviceFreqVector dUhat(gr, dim, "dUhat");

    auto policy = sphinx::container::Data::ShapeOnly;
    Vector X(U, policy);
    DeviceFreqVector Xh(dUhat, policy);
    DeviceFreqVector Xh2(dUhat, policy);
    Vector X2(U, policy);

    FFTt fft(gr);

//    fft.plan(dU, dUhat);

    std::shared_ptr<SamplerZ> smp;
    std::shared_ptr<InitialData<dim>> data = sphinx::Data::Factory(smp,
            gr, options.as<std::string>("InitialData.name"));;
    Solution sol(gr,
        "fftwbenchtest" + sphinx::timestamp, true, true);

    dU.plan(Xh2);

    eval<dim>(U, *data);

    auto snp1 = sol.newSnapshot(0);
    snp1->pushData(U, range<dim>());
    snp1->writeData();

    unsigned int M = sphinx::options.as<int>("nsims");

    debug::test_message(mpi::world, "Repeating test ", M, " times.");

    copy(U,X);

    profiler::start_stage("main loop");
    for(unsigned int n = 0; n < M; ++n) {
        sphinx::operators::HostToDevice(U, dU);
//        fft.forward(dU, dUhat);
//        fft.backward(dUhat, dU);

        dU.forward(Xh2);
        projection(Xh2, Xh2);
        Xh2.backward(dU);

        sphinx::operators::DeviceToHost(dU, U);
    }
    profiler::end_stage();

    axpy(X, U,-1);
    double err = norm(X);

    debug::test_message(mpi::world, "Before/After projection error = ", err);
    if(err > testunit::tolerance()) {
        return testunit::report(false);
    }

    auto snp2 = sol.newSnapshot(1);
    snp2->pushData(U, range<dim>());
    snp2->writeData();
    auto snp3 = sol.newSnapshot(2);
    snp3->pushData(X, range<dim>());
    snp3->writeData();

    sol.writeXml();

    return testunit::report(true);
}
