/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "sphinx.hpp"

// Basic tests
#include "io_test.hpp"
#include "basic_test.hpp"
//#include "redistribute_test.hpp"

// Operators tests
#include "projection_test.hpp"
#include "structure_test.hpp"

// Solver tests
//#include "evolution_test.hpp"
//#include "step_test.hpp"
#include "solve_test.hpp"

// Convergence study
#include "conv_test.hpp"

// Statistical study
#include "mc_test.hpp"
#include "mlmc_test.hpp"

// Extra tests
//#include "bipolar_test.hpp"

// Benchmarks
#include "fftbench_test.hpp"

// MPI test
#include "ghost_test.hpp"
