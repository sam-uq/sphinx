/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "timestepping/stepper.hpp"

#include "timestepping/ssprk3.hpp"
#include "timestepping/ssprk2.hpp"
#include "timestepping/fe.hpp"

#include "sphinx.hpp"

int step_test() {

    const unsigned int dim  = NDIMS;
    std::array<std::ptrdiff_t, dim> N;

    std::copy_n(sphinx::options.as<std::vector<int>>("mesh").begin(), dim, N.begin());

    Set<1> data_type = Interlaced | R2CFFT | DestroyInput | DoublePrecision;
    FFTType fft_type = DeviceTimeVector::fft_type;
    if( sphinx::options.has("fft_inplace") && sphinx::options.as<bool>("fft_inplace") ) {
         data_type |= InPlace;
    }

    std::shared_ptr<Grid<dim>> gr(new Grid<dim>(N, fft_type, data_type));
    std::shared_ptr<Grid<dim>> gr_pad(new Grid<dim>(N*3/2, fft_type, data_type));

    DeviceFreqVector what(gr, curl_dim(dim));

    DeviceFreqVector Uhat(gr, dim), Vhat(gr, dim);
    Uhat.name = "Uhat";
    Vhat.name = "Vhat";
    what.name = "what";

    BipolarVector<Vector, DeviceTimeVector> U;
    std::shared_ptr<Vector> Uh = std::shared_ptr<Vector>( new Vector(gr, dim));

    BipolarVector<Vector, DeviceTimeVector> V;
    std::shared_ptr<DeviceTimeVector> Vd
            = std::shared_ptr<DeviceTimeVector>( new DeviceTimeVector(gr, dim));

    BipolarVector<Vector, DeviceTimeVector> w;
    std::shared_ptr<DeviceTimeVector> wd
            = std::shared_ptr<DeviceTimeVector>( new DeviceTimeVector(gr, curl_dim(dim)));

    Uh->name = "U";
    wd->name = "w";
    what.name = "what";

    U.setHost(Uh);
    w.setDevice(wd);
    V.setDevice(Vd);

    std::shared_ptr<SamplerZ> smp(new SamplerZ());
    smp->sync();
    InitialData<dim> *data = sphinx::Data::Factory(smp, gr_pad, options.as<std::string>("InitialData.name"));

    //// TEST NO 1
    int threshold = 90;
    double eps = 0.001;
    int aliasing = N[0] / 3;
    if( sphinx::options.has("threshold") ) threshold = sphinx::options.as<int>("threshold");
    if( sphinx::options.has("eps") ) eps = sphinx::options.as<double>("eps");
    if( sphinx::options.has("aliasing") ) aliasing = sphinx::options.as<int>("aliasing");

    Solution_p sol_p = Solution_p(new Solution(gr, "step" + sphinx::timestamp, true, true));

    Evo Phi(gr, sol_p, U, threshold, eps, aliasing, 0.1);

    eval(*U.getWriteHost(), data);

    U.toDevice();
    std::cout << "Norm: " << norm(*U.getReadDevice(), INFINITY) << " " << norm(*U.getReadDevice(), 2)
              << " (factor = " << U.getReadDevice()->scaling_factor << ")" << std::endl;

    ssp_rk3_step<DeviceTimeVector, decltype(Phi)>(*U.getReadDevice(), *V.getWriteDevice(), Phi, 0.1);

    Phi.fft.forward(*U.getReadDevice(), Phi.Uhat);
    curl(Phi.Uhat, what);
    Phi.fft.backward(Phi.Uhat,*U.getWriteDevice());
    Phi.fft.backward(what,*w.getWriteDevice());
    std::cout << "Norm: " << norm(*U.getReadDevice(), INFINITY) << " " << norm(*U.getReadDevice(), 2)
              << " (factor = " << U.getReadDevice()->scaling_factor << ")" << std::endl;

    U.toHost();
    w.toHost();

    auto snp = sol_p->newSnapshot(0);
    snp->pushData(*w.getReadHost(), range<curl_dim(dim)>() );
    snp->pushData(*U.getReadHost(), range<dim>() );
    snp->writeData();

    // TODO:
    // check oscillations
    // continue uncomment
    // 3D

    sol_p->writeXml(false);

    return 0;
}
