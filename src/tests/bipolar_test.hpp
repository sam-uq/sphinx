/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include <algorithm>

#include "sphinx.hpp"

int bipolar_test(void) {
    const unsigned int dim  = NDIMS;

    std::array<std::ptrdiff_t, dim> N;

    std::copy_n(sphinx::options.as<std::vector<int>>("mesh").begin(), dim, N.begin());

    Set<1> data_type = Interlaced | R2CFFT | DestroyInput | DoublePrecision;
    FFTType fft_type = EvolutionVector::fft_type;
    if( sphinx::options.has("fft_inplace") && sphinx::options.as<bool>("fft_inplace") ) {
         data_type |= InPlace;
    }

    std::shared_ptr<Grid<dim>> gr(new Grid<dim>(N, fft_type, data_type));

    BipolarVector<Vector, DeviceTimeVector> U;
    DeviceFreqVector Uhat(gr, dim), Vhat(gr, dim);
    std::shared_ptr<Vector> Uh = std::shared_ptr<Vector>( new Vector(gr, dim));
    U.setHost(Uh);

    FFTt fft(gr);

    fft.plan(U, Uhat);

    vortex_sheet<Grid<dim>, dim> data;

    eval(*U.getWriteHost(), data);

    io::Solution<Grid<dim>, Snapshot<Vector>> sol(gr, "bipolar" + sphinx::timestamp, true, true);

    auto snp1 = sol.newSnapshot(0);
    snp1->pushData(U, range<dim>());
    snp1->writeData();

//    HostToDevice(U, U_d);
    U.toDevice();
    fft.forward(U, Uhat);

    projection(Uhat, Vhat);

    fft.backward(Vhat, U);
    U.toHost();
//    DeviceToHost(U_d, U);

    auto snp2 = sol.newSnapshot(1);
    snp2->pushData(U, range<dim>());
    snp2->writeData();

    sol.writeXml();

    return 0;
}
