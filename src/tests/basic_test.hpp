/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

//! \file basic_test.hpp Contains the function for some base integrity test for sphinx.
//!
//!

#include <algorithm>
#include <memory>

#include "sphinx.hpp"

#include "sphinx_testing.hpp"

//! \brief Perform a basic test for sphinx.
//!
//! Test includes I/O, DFT, zero_padding/truncate, DeviceToHost, copy and axpy.
//!
//! \return ErrorCode, 0 for success.
int basic_test(void) {
    std::array<std::ptrdiff_t, dim> N;

    std::copy_n(sphinx::options.as<std::vector<int>>("mesh").begin(), dim, N.begin());

    Set<1> data_type = Interlaced | R2CFFT | DestroyInput | DoublePrecision;
    FFTType fft_type = EvolutionVector::fft_type;
    if( sphinx::options.has("fft_inplace") && sphinx::options.as<bool>("fft_inplace") ) {
         data_type |= InPlace;
    }

    std::shared_ptr<Grd> gr(new Grd(N, fft_type, data_type));
    std::shared_ptr<Grd> gr_alias(new Grd(N*3/2, fft_type, data_type));

    Vector U(gr, dim, "U"), Ucopy(gr, dim, "Ucopy");
    DeviceTimeVector dU(gr, dim, "dU");
    DeviceFreqVector dUhat(gr, dim, "dUhat");
    HostFreqVector dUhath(gr, dim, "dUhat");
    DeviceFreqVector dUpad(gr_alias, dim, "dUpad"), dVpad(gr_alias, dim, "dVpad");

    FFTt fft(gr);

    fft.plan(dU, dUhat);

    std::shared_ptr<SamplerZ> smp(new SamplerZ());
    smp->sync();
    std::shared_ptr<InitialData<dim>> data = sphinx::Data::Factory(smp,
                                                                   gr,
                                                                   options.as<std::string>("InitialData.name")
                                              );
    io::Solution<Grd, Snapshot<Vector>> sol(gr, sphinx::name, true, true);

    eval<dim>(U, *data);

//    auto snp1 = sol.newSnapshot(0);
//    snp1->pushData(U, range<dim>());
//    snp1->writeData();

    int repeats = 1;
    if( sphinx::options.has("nsim") ) {
        repeats = sphinx::options.as<int>("nsim");
    }
    for(int i = 0; i < repeats; ++i) {
        copy(U, Ucopy);

        operators::HostToDevice(U, dU);
        fft.forward(dU, dUhat);

        zero_padding(dUhat, dUpad);
//    projection(dUpad, dVpad);
//    set(dUpad, 5);
//    copy(dVpad, dUpad);
        truncate(dUpad, dUhat);

        fft.backward(dUhat, dU);
        operators::DeviceToHost(dU, U);
//    operators::DeviceToHost(dUhat, U);
//    operators::DeviceToHost(dUhat, dUhath);
        axpy(U, Ucopy, -1);
    }
    double err = norm(U);

    debug::test_message(mpi::world, "Residual Error: ", err);
    if(err > sphinx::testunit::tolerance()) {
        testunit::report(false);
    }

    double val = 5;
    set(U, val);
//    eval<dim>(U, *data);
    err = norm(U, 1) + norm(U, 2) + norm(U, INFINITY) - 3 * std::sqrt(2) * val;

    debug::test_message(mpi::world, "Error of \"norm\": ", err);
    if(err > sphinx::testunit::tolerance()) {
        testunit::report(false);
    }

    auto snp2 = sol.newSnapshot(1);
//    snp2->pushData(dUhath, range<dim>());
    snp2->pushData(U, range<dim>());
    snp2->writeData();

    sol.writeXml(false);

    return sphinx::testunit::success();
}
