/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

//! \file io_test.hpp Contains the function for some IO testing for sphinx.
//!
//!

#include <algorithm>

#include "config.hpp"

#include "sphinx.hpp"
#include "sphinx_testing.hpp"

//! \breif Basic IO test for sphinx.
//!
//! Tests IO and device mapping.
//!
//! \return ErrorCode, 0 for success.
int io_test(void) {
    std::array<std::ptrdiff_t, dim> N;

    std::copy_n(sphinx::options.as<std::vector<int>>("mesh").begin(),
                dim, N.begin());

    Set<1> data_type = Interlaced | R2CFFT | DestroyInput | DoublePrecision;
    FFTType fft_type = EvolutionVector::fft_type;
    if( sphinx::options.has("fft_inplace") &&
            sphinx::options.as<bool>("fft_inplace") ) {
         data_type |= InPlace;
    }

    std::shared_ptr<Grd> gr(new Grd(N, fft_type, data_type));

    DeviceTimeVector intw(gr, dim);
    Vector hintw(gr, dim);

    std::shared_ptr<SamplerZ> smp(new SamplerZ());
    smp->sync();
    std::shared_ptr<InitialData<dim>> data =
            sphinx::Data::Factory(smp, gr,
                                  options.as<std::string>(
                                      "InitialData.name"));

    eval<dim>(hintw, *data);
    operators::HostToDevice(hintw, intw);
    operators::DeviceToHost(intw, hintw);

    io::Solution<Grd, Snapshot<Vector>> sol(gr,
          "iotest" + sphinx::timestamp, true, true);

    std::vector<int> comps = range<dim>();

    auto snp1 = sol.newSnapshot(0);
    snp1->pushData(hintw, comps);
    snp1->writeData();

    auto snp2 = sol.newSnapshot(1);
    snp2->pushData(hintw, comps);
    snp2->writeData();

    sol.writeXml();

    return sphinx::testunit::report(true);
}
