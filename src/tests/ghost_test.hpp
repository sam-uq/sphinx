/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

//! \file ghost_test.hpp Contains the function for testing the Ghost class.
//!
//!

#include <algorithm>
#include <memory>

#include <fmt/format.h>

#include "sphinx.hpp"
#include "sphinx_testing.hpp"

#include "containers/ghost.hpp"

//! \brief Performs a basic test for the Ghost class in sphimx.
//!
//!
//!
//! \return ErrorCode, 0 for success.
int ghost_test(void) {
    std::array<std::ptrdiff_t, dim> N{512, 512};

    int R = 5;
    bool pixely = true;
    int maxstep = std::numeric_limits<int>::max();
    bool verbose = true;

    operators::extra::set_strucutre_options(R, pixely, maxstep, verbose);

    std::copy_n(sphinx::options.as<std::vector<int>>("mesh").begin(),
                dim, N.begin());

    Set<1> data_type = Interlaced | R2CFFT | DestroyInput | DoublePrecision;
    FFTType fft_type = EvolutionVector::fft_type;
    if( sphinx::options.has("fft_inplace") && sphinx::options.as<bool>("fft_inplace") ) {
         data_type |= InPlace;
    }

    std::shared_ptr<Grd> gr(new Grd(N, fft_type, data_type));
    Vector U(gr, dim, "U");
    DeviceTimeVector dU(gr, dim, "dU");

    FFTt fft(gr);

    std::shared_ptr<SamplerZ> smp(new SamplerZ());
    smp->sync();
    std::shared_ptr<InitialData<dim>> data = sphinx::Data::Factory(smp,
                                                                   gr,
                                                                   "debug"
                                              );
    io::Solution<Grd, Snapshot<Vector>> sol(gr, sphinx::name, true, true);

    smp->set_idx(0);
    data->init();
    eval<dim>(U, *data);

//    int repeats = 1;
//    if( sphinx::options.has("nsim") ) {
//        repeats = sphinx::options.as<int>("nsim");
//    }

    sphinx::containers::Ghost<Vector> ghost;

    int size = 4;

    ghost.set_grid(gr);
    ghost.set_container(&U);
    ghost.set_size(size);

    ghost.update_begin();
    ghost.update_end();

    auto fnc = [] (int f, int k, int j) {
        return 4*(4096 * j +  k) + f;
    };

    int err = 0;
    for(int k = -size; k < U.kend + size; ++k) {
        for(int j = 0; j < U.jend; ++j) {

            pos_t<2> logical_modes = {k, j};

            for(int f = 0; f < (int) U.dof; ++f) {
                int real_k = k + U.grid_p->local_0_start;

                int p_k = kernel::periodic_remap_cpu<0>(U, real_k);
                int p_j = kernel::periodic_remap_cpu<1>(U, j);

                if(  std::abs(ghost.ghost_access(f, logical_modes) - fnc(f, p_k, p_j)) > testunit::tolerance() ) {
                    debug::warning(
                            fmt::format(
                                    "Bad value at {},{}: value is {} but should be {}.",
                                    k, j, ghost.ghost_access_throw(f, logical_modes), fnc(f, p_k, p_j)
                            ),
                            mpi::world, mpi::Policy::Self
                    );
                    ++err;
                }
            }

            if( k >= 0 and k < U.kend ) {
                index_t Ix = U.outerIndex(logical_modes);

                for(int f = 0; f < (int) U.dof; ++f) {
                    if (std::abs(U(f, Ix) - ghost.ghost_access_throw(f, logical_modes)) > testunit::tolerance()) {
                        debug::warning("Error!", mpi::world, mpi::Policy::Self);
                        ++err;
                    }
                }
            }
        }
    }

    std::vector<int> comps = range<dim>();

    auto snp = sol.newSnapshot(1);
    snp->pushData(U, comps);
    snp->writeData();

    sol.writeXml();

    double D = operators::extra::compute_structure(ghost);

    std::cout << "aaaaaaaaaaaaaaaaaaaaaaaa            " << D << std::endl << std::endl;

    operators::extra::reset_structure();

    return testunit::report(err == 0, "Ghost test completed!");
}
