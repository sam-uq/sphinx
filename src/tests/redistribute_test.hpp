/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include <algorithm>

#include "io/solution.hpp"
#include "io/snapshot.hpp"

#include "sphinx.hpp"

int redistribute_test(void) {
    const unsigned int dim  = NDIMS;
    std::array<std::ptrdiff_t, dim> N;

    std::copy_n(sphinx::options.as<std::vector<int>>("mesh").begin(), dim, N.begin());

    Set<1> data_type = Interlaced | R2CFFT | DestroyInput | DoublePrecision;
    FFTType fft_type = DeviceTimeVector::fft_type;
    if( sphinx::options.has("fft_inplace") && sphinx::options.as<bool>("fft_inplace") ) {
         data_type |= InPlace;
    }

    std::shared_ptr<Grd> gr(new Grd(N, fft_type, data_type));
    std::shared_ptr<Grd> gr_alias(new Grd(N*3/2, fft_type, data_type));

    BipolarVector<HostFreqVector, DeviceFreqVector> BU;
    std::shared_ptr<HostFreqVector> out = std::shared_ptr<HostFreqVector>(new HostFreqVector(gr, dim));

    BU.setHost(out);

    HostFreqVector newout(gr, dim);
    DeviceFreqVector freq_padded(gr_alias, dim);

    io::Solution<Grd, Snapshot<HostFreqVector>> sol(gr, sphinx::name, false, true);

#if NDIMS == 2
    #pragma omp parallel for collapse(2)
    for(int k = 0; k < out->kend; ++k) {
        for(int j = 0; j < out->jend; ++j) {
            pos_t<dim> logical_modes = {k,j};
            index_t Ix = out->outerIndex(logical_modes);
            for(unsigned int f = 0; f < dim; ++f) {
//                (*out)(f,Ix) = std::complex<double>(f*Ix, -Ix*gr->comm.rank);
                (*out)(f,Ix) = std::complex<double>(Ix*(gr->comm.rank*2-1), 0);
//                (*out)(f,Ix) = std::complex<double>((gr->comm.rank*2-1), (gr->comm.rank*2-1));
            }
//            std::cout << sphinx::mpi_rank << " " << Ix << " "
//                      << out(0,Ix) << " " << out._data[0][Ix] << std::endl;
        }
    }
#elif NDIMS == 3
    #pragma omp parallel for collapse(3)
    for(int k = 0; k < out->kend; ++k) {
        for(int j = 0; j < out->jend; ++j) {
            for(int i = 0; i < out->iend; ++i) {
            pos_t<dim> logical_modes = {k,j,i};
            index_t Ix = out->outerIndex(logical_modes);
            for(unsigned int f = 0; f < dim; ++f) {
                (*out)(f,Ix) = std::complex<double>(f*Ix, -Ix);
            }
//            std::cout << sphinx::mpi_rank << " " << Ix << " " << out(0,Ix)
//                      << " " << out._data[0][Ix] << std::endl;
            }
        }
    }
#endif

    auto snp1 = sol.newSnapshot(0);
    snp1->pushData(*BU.getReadHost(), range<dim>());
    snp1->writeData();

    copy(*BU.getReadHost(), newout);

    // PADDING TEST
    zero_padding(*BU.getReadDevice(), freq_padded);

    truncate(freq_padded, *BU.getWriteDevice());

    newout -= *BU.getReadHost();
    // END PADDING TEST

    // TRANSPOSE TEST
//    operators::transpose(*BU.getReadWriteDevice(), operators::Direction::Forward);
//    operators::transpose2(*BU.getReadWriteDevice(), operators::Direction::Forward);

    double nrm = norm(newout);
    std::cout << "Norm: " << nrm << std::endl; // debug message

    auto snp2 = sol.newSnapshot(1);
    snp2->pushData(*BU.getReadHost(), range<dim>());
    snp2->writeData();
    auto snp3 = sol.newSnapshot(2);
    snp3->pushData(newout, range<dim>());
    snp3->writeData();

    sol.writeXml(true);

    return testunit::report(nrm < NORM_TOL);

}
