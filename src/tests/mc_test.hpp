/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "containers/fftw_time_vec.hpp"

#include "integration/mc.hpp"

#include "geometry/grid.hpp"
#include "geometry/remote_grid.hpp"

#include "sphinx.hpp"

int mc_test() {
    const unsigned int dim  = NDIMS;

    std::array<std::ptrdiff_t, dim> N;

    std::copy_n(sphinx::options.as<std::vector<int>>("mesh").begin(),
                dim, N.begin());
    Set<1> data_type = Interlaced | R2CFFT | DestroyInput | DoublePrecision;
    FFTType fft_type = DeviceTimeVector::fft_type;
    if( sphinx::options.has("fft_inplace") &&
            sphinx::options.as<bool>("fft_inplace") ) {
         data_type |= InPlace;
    }
    std::shared_ptr<Grd> grid(new Grd(N, fft_type, data_type, false));

    std::shared_ptr<SamplerZ> smp(new SamplerZ());
    smp->sync();

    int sims = 1;
    if( sphinx::options.has("nsims") ) {
         sims = sphinx::options.as<int>("nsims");
    }

    std::shared_ptr<InitialData<dim>> data =
            sphinx::Data::Factory(smp,
                                  grid,
                                  options.as<std::string>(
                                          "InitialData.name"));

    Integration::MC<SamplerZ, dim> mc(smp,
        grid, data, sphinx::mpi::world, sims, sphinx::name);
    mc.start();

    return 0;
}
