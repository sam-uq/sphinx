/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

//! \file structure_test.hpp Contains the function for some structure functions integrity test for sphinx.
//!
//!

#include <algorithm>
#include <memory>

#include "sphinx.hpp"
#include "sphinx_testing.hpp"

//! \brief Performs a basic test for the strucutre computation in sphimx.
//!
//! Test includes computation of strucutre functions.
//!
//! \return ErrorCode, 0 for success.
int structure_test(void) {
    std::array<std::ptrdiff_t, dim> N{512, 512};

    Set<1> data_type = Interlaced | R2CFFT | DestroyInput | DoublePrecision;
    FFTType fft_type = EvolutionVector::fft_type;
    if( sphinx::options.has("fft_inplace") && sphinx::options.as<bool>("fft_inplace") ) {
         data_type |= InPlace;
    }

    std::shared_ptr<Grd> gr(new Grd(N, fft_type, data_type));
    Vector U(gr, dim, "U");
    DeviceTimeVector dU(gr, dim, "dU");

    FFTt fft(gr);

    std::shared_ptr<SamplerZ> smp(new SamplerZ());
    smp->sync();
    std::shared_ptr<InitialData<dim>> data = sphinx::Data::Factory(smp,
                                                                   gr,
                                                                   "vortex_sheet"
                                              );
    io::Solution<Grd, Snapshot<Vector>> sol(gr, sphinx::name, true, true);

    smp->set_idx(0);
    data->init();
    eval<dim>(U, *data);

    int R = 5;
    bool pixely = true;
    int maxstep = std::numeric_limits<int>::max();
    bool verbose = true;

    operators::extra::set_strucutre_options(R, pixely, maxstep, verbose);

    int repeats = 1;
    if( sphinx::options.has("nsim") ) {
        repeats = sphinx::options.as<int>("nsim");
    }
    double D;

    for(int i = 0; i < repeats; ++i) {
#if USE_CUDA
        operators::HostToDevice(U, dU);

        D = operators::extra::compute_structure(dU);
#else // if !USE_CUDA
        D = operators::extra::compute_structure(U);
#endif // !USE_CUDA
    }

    operators::extra::reset_structure();

    debug::test_message(mpi::world, "Structure: ", D);

    double ex = 0.0025406480156057024;

    return testunit::report(std::abs(D - ex) < 0.01, "Structure test within tolerance!");
}
