/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include <algorithm>

#include "sphinx.hpp"

int projection_test(void) {
    const unsigned int dim  = NDIMS;

    std::array<std::ptrdiff_t, dim> N;

    std::copy_n(sphinx::options.as<std::vector<int>>("mesh").begin(), dim, N.begin());

    Set<1> data_type = Interlaced | R2CFFT | DestroyInput | DoublePrecision;
    FFTType fft_type = DeviceTimeVector::fft_type;
    if( sphinx::options.has("fft_inplace")
        && sphinx::options.as<bool>("fft_inplace") ) {
         data_type |= InPlace;
    }

    std::shared_ptr<Grd> gr(new Grd(N, fft_type, data_type));
    double padding = 3./2.;
    if( sphinx::options.has("padding") ) {
        padding = sphinx::options.as<double>("padding");
    }
    std::shared_ptr<Grd> gr_pad(new Grd(N*padding, fft_type, data_type));

    HostDeviceVector<Vector, DeviceTimeVector> BU;
    HostDeviceVector<Vector, DeviceTimeVector> Bw;
    std::shared_ptr<Vector> U = std::make_shared<Vector>(gr, dim);
    std::shared_ptr<Vector> w = std::make_shared<Vector>(gr, curl_dim(dim));

    BU.setHost(U);
    Bw.setHost(w);
    U->name = "U";
    w->name = "w";

    DeviceFreqVector Uhat_d(gr, dim),
            Vhat_d(gr, dim), what_d(gr, curl_dim(dim));

    FFTt fft(gr);

    fft.plan(*BU.getWriteDevice(), Uhat_d);

    std::shared_ptr<SamplerZ> smp(new SamplerZ());
    smp->sync(sphinx::master_seed());
    smp->set_idx(0);
    std::shared_ptr<InitialData<dim>> data =
            sphinx::Data::Factory(smp, gr,
                                  options.as<std::string>("InitialData.name")
                                  );

    data->init();

    eval<dim>(*BU.getWriteHost(), *data);

    Solution sol(gr, sphinx::name, true, true);

//    set(*w, 0);
//    fft.forward(*BU.getReadDevice(), Uhat_d);
//    curl(Uhat_d, what_d);
//    fft.backward(Uhat_d, *BU.getWriteDevice());
//    fft.backward(what_d, *Bw.getWriteDevice());

    auto snp1 = sol.newSnapshot(0);
    snp1->pushData(*BU.getReadHost(), range<dim>());
    snp1->pushData(*Bw.getReadHost(), range<curl_dim(dim)>());
    snp1->writeData();

//    BU.getReadDevice()->spy();
    fft.forward(*BU.getReadDevice(), Uhat_d);

    projection(Uhat_d, Vhat_d);
    divergence(Vhat_d, what_d);

    fft.backward(Uhat_d, *BU.getWriteDevice());
    // Here scaled output is normal if FFT sclases
    fft.backward(what_d, *Bw.getWriteDevice());
    // Here scaled output is normal if FFT sclases

    double err = norm(*BU.getReadDevice());
//    double erru = norm(*Bw.getReadDevice());

    debug::test_message(mpi::world, "Cleaned P(U) = ", err);
    if((1 - err) > testunit::tolerance()) {
        testunit::report(false, "Error exceeds tolerance for || P(U) || == 1");
    }

    auto snp15 = sol.newSnapshot(1);
    snp15->pushData(*BU.getReadHost(), range<dim>());
    snp15->pushData(*Bw.getReadHost(), range<curl_dim(dim)>());
    snp15->writeData();

    fft.forward(*BU.getReadDevice(), Uhat_d);

    projection(Uhat_d, Vhat_d);
    axpy(Uhat_d, Vhat_d, -1);
    curl(Uhat_d, what_d);

    // Here scaled output is normal if FFT scales
    fft.backward(Vhat_d, *BU.getWriteDevice());
    fft.backward(what_d, *Bw.getWriteDevice());

    err = norm(*Bw.getReadDevice());

    debug::test_message(mpi::world, "Cleaned curl (Q(U)) = ", err);
    if(err > testunit::tolerance()) {
        testunit::report(false, "Error exceeds norm of curl(Q(U)).");
    }

    auto snp2 = sol.newSnapshot(2);
    snp2->pushData(*BU.getReadHost(), range<dim>());
    snp2->pushData(*Bw.getReadHost(), range<curl_dim(dim)>());
    snp2->writeData();

    sol.writeXml();

    return testunit::success();
}
