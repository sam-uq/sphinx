/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
//
// Created by filippo on 03/10/17.
//

#include <iostream>
#include <array>
#include <cmath>

#ifdef USE_STRAND
// Remove "random" include
#else
#  include <random>
#endif
#include <climits>

#include "config.hpp"
#include "data/initialdata/all_initialdata.hpp"

#if USE_FFTW && HAVE_MPI
#  include <fftw3-mpi.h>
//#if myUSE_MKL
//#include <fftw3-mpi_mkl.h>
//#endif // myUSE_MKL
#else // Serial fftw
#  include <fftw3.h>
#endif

#if USE_OMP
#  include <omp.h>
#endif

#include "sphinx.hpp"

#if USE_FFTW
#endif

#if USE_CUDA
#  include "utilities/cuda_tools.hpp"

#include <mpi.h>
#include <mpi-ext.h>

#  if defined(MPIX_CUDA_AWARE_SUPPORT) && !MPIX_CUDA_AWARE_SUPPORT
//static_assert(false, "No CUDA Aware MPI support!");
#   warning "Using CUDA aware MPI"
#  elif !defined(MPIX_CUDA_AWARE_SUPPORT)
#    warning "Cannot determine if CUDA Aware MPI support!"
#  endif // CUDA MPI AWARE
#endif

//#ifdef HAVE_PYTHON
//BOOST_PYTHON_MODULE(SamplerModule)
//{
//    boost::python::class_<SamplerZ>("Sampler")
//        .def("sync", &SamplerZ::sync)
////        .def("get", &SamplerZ::operator())
////                .def("greet", &World::greet)
////                .def("set", &World::set)
//            ;
//}
//#endif

namespace sphinx {

std::string timestamp;
std::string name;

unsigned int mSeed;

sphinx::mpi::IntraDomainComm sphinx::mpi::world;
sphinx::mpi::IntraDomainComm sphinx::mpi::self;

unsigned int omp_threads = 1;

unsigned int master_seed() {
    return mSeed;
}

int init(int argc, char **argv) {
    // Read options, store in global class
//    options.read(argc, argv);

    // Setup MPI if present, with threads if fresent
#if HAVE_MPI
    int provided;

    // Startup mpi with threads
    mpiErrchk(MPI_Init_thread(&argc, &argv, MPI_THREAD_FUNNELED, &provided));

    int mpi_size, mpi_rank;
    mpiErrchk(MPI_Comm_size(MPI_COMM_WORLD, &mpi_size));
    mpiErrchk(MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank));

    // Wrapper for MPI_COMM_WRODL
    sphinx::mpi::world = sphinx::mpi::IntraComm(
            MPI_COMM_WORLD, mpi_rank, mpi_size, "world", 0, 1);
    // Wrapper for MPI_COMM_SELF
    sphinx::mpi::self = sphinx::mpi::IntraComm(
            MPI_COMM_SELF, 0, 1, "self", mpi_rank, mpi_size);

    std::stringstream ss_init;
    ss_init << "Initializing sphinx...";
    debug::status(ss_init.str(), sphinx::mpi::world, sphinx::mpi::Policy::Collective);

    std::stringstream ss_mpi;
    ss_mpi << "MPI , with " << mpi_size << " ranks.";
    debug::status(ss_mpi.str(), sphinx::mpi::world, sphinx::mpi::Policy::Collective);
#endif // HAVE_MPI

    // Here read options?
    options.read(argc, argv);


#if USE_OMP
    std::stringstream ss;
    ss << "Max OMP threads: " << omp_get_max_threads() << ".";
    debug::status(ss.str(), sphinx::mpi::world, sphinx::mpi::Policy::Collective);

    omp_threads = omp_get_num_threads();
#endif // USE_OMP

    // Setup FFTW with threads, if present
#if USE_FFTW && (USE_OMP || HAVE_THREADS)
    bool threads_ok = true;
#if HAVE_MPI
    threads_ok = provided >= MPI_THREAD_FUNNELED;
#endif // HAVE_MPI
    if (threads_ok) {
        threads_ok = fftw_init_threads();
    }
#if USE_OMP
    fftw_plan_with_nthreads(omp_get_max_threads());

    std::stringstream ss_omp;
    ss_omp << "Planning FFTW with OmpenMP threads, with " << omp_get_max_threads() << " threads.";
    debug::status(ss_omp.str(), sphinx::mpi::world, sphinx::mpi::Policy::Collective);
#endif // USE_OMP
#endif // USE_FFTW && (USE_OMP || HAVE_THREADS)

    // Initialize CUDA, if needed
#if USE_CUDA
    sphinx::cuda::cuda_init(argc, argv);
#endif // USE_CUDA

#if HAVE_MPI && USE_CUDA
//    assert(MPIX_Query_cuda_support() && "You must have CUDA-MPI support!");
#endif

    // Initialize FFTW to use MPI
#if USE_FFTW && HAVE_MPI
    fftw_mpi_init();
#endif // HAVE_MPI

#if USE_ACCFFT
    //accfft_init(omp_get_max_threads());
    accfft_init();
#endif

    // Initialize profiler
    profiler::init();

    // Broadcast this timestamp to the world, make sure everyone is in sync
    auto current_time = std::time(nullptr);
    mpiErrchk(MPI_Bcast(&current_time, 1, MPI_INT, 0, MPI_COMM_WORLD));

    timestamp = std::to_string(current_time);
#if TIMESTAMP_SOLUTION
    if( sphinx::options.has("name") )
        name = sphinx::options.as<std::string>("name") + "_"+ timestamp;
    else
        name = "unnamed_" + timestamp;
#else
    if( sphinx::options.has("name") )
        name = sphinx::options.as<std::string>("name");
    else
        name = "unnamed";
#endif // TIMESTAMP_SOLUTION
    if( sphinx::options.has("master_seed")) {
        mSeed = sphinx::options.as<int>("master_seed");
    } else {
        // Generate randomness
#ifdef USE_SRAND
        srand(time(NULL) + getpid());
        int myRand = rand();
#else
        std::random_device device;
        std::seed_seq seed{device(), device(), device()};
        std::mt19937 engine(seed);
        std::uniform_int_distribution<> distribution{0,INT_MAX};
        int myRand = distribution(engine);
#endif
        mSeed = (MASTER_SEED == -1) ? myRand : MASTER_SEED;
    }

//#ifdef HAVE_PYTHON
//    int err = PyImport_AppendInittab("sampler", &PyInit_SamplerModule);
//    if(err != 0) {
//        debug::warning("Failed to inject 'sampler' module into Python bindings.", sphinx::mpi::world);
//    } else {
//        debug::message("Injected 'sampler' module into Python bindings.", sphinx::mpi::world);
//    }
//#endif // HAVE_PYTHON

    return 0;
}

int terminate(int ret) {
    {
        std::stringstream ss_init;
        ss_init << "Finalizing sphinx...";
        debug::status(ss_init.str(), sphinx::mpi::world, sphinx::mpi::Policy::Collective);
    }

    // Finalize  profiler
    profiler::finalize();

    // Finalize CUDA, if used
#if USE_CUDA
    sphinx::cuda::cuda_term();
#endif // USE_CUDA

#if USE_FFTW && (USE_OMP || HAVE_THREADS)
    fftw_cleanup_threads();
#endif // USE_OMP || HAVE_THREADS
    fftw_cleanup();
#if USE_FFTW && HAVE_MPI
    fftw_mpi_cleanup();
#endif // HAVE_MPI

#if USE_ACCFFT
    //accfft_cleanup();
#endif

#if HAVE_MPI
    mpiErrchk(MPI_Finalize());
#endif // HAVE_MPI

    return ret;
}

int abort(int errcode = EXIT_FAILURE) {
    terminate(errcode);
    abort();
    return errcode;
}

//! \brief Generate a random number
//! https://xkcd.com/221/
//! \return true random number
int getRandomNumber() {
    return 4; // chosen by fair dice roll
    // guaranteed to be random
}

} // END: namespace sphinx