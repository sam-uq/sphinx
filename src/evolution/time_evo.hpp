/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include <memory>

#include "utilities/profiler.hpp"
#include "operators/host_functions/host_operators.hpp"

namespace sphinx {
namespace evolution {

template<class Solution, class Grid, class FFT, class TimeVec, class FreqVec>
class timeEvo {
public:
    constexpr  static unsigned int dim = TimeVec::dim;

    timeEvo(std::shared_ptr<Grid> gr, std::shared_ptr<Solution> sol,
            TimeVec & U,
            int threshold, double eps, int aliasing, double cfl)
        : fft(gr), Uhat(gr, dim), sol(sol)
        , T(gr, sym_tensor_dim(dim)), w(gr, curl_dim(dim))
        , That(gr, sym_tensor_dim(dim)), what(gr, curl_dim(dim))
        , threshold(threshold), eps(eps), aliasing(aliasing), c(cfl) {
        T.name = "T";
        w.name = "w";
        Uhat.name = "Uhat";
        That.name = "That";
        what.name = "what";

        fft.plan(U, Uhat);
    }

    TimeVec & operator()( TimeVec & U) {
        profiler::start_stage("Phi");

        sym_tensorize(U, T);

        fft.forward(U, Uhat); // can be inplace
        vector_laplace(Uhat, Uhat, threshold, eps);

        fft.forward(T, That); // can be inplace
        sym_vector_divergence(That, That);
        projection(That,That);
        axpy(Uhat, That, -1);
        alias(Uhat, aliasing);
        fft.backward(Uhat, U); // can be inplace

        profiler::end_stage();
        return U;
    }

    void pre() {}

    void post() {}

    void snapshot(TimeVec & U, double t, bool = true) {
        auto snp = sol->newSnapshot(t);
        fft.forward(U,Uhat);
        alias(Uhat, aliasing); // remove weird things
        curl(Uhat,what);
        fft.backward(Uhat, U);
        fft.backward(what, w);
        normalize(U);
        normalize(w);

        snp->pushData(U, range<dim>());
        snp->pushData(w, range<curl_dim(dim)>());
        snp->writeData();
    }


    double cfl(TimeVec & U, double h, double & sup) {
        sup = norm(U, INFINITY); // * std::pow(3/2.,NDIMS); // UGLYNESS
        return h / sup * c;
    }

    FFT fft;

    FreqVec Uhat;
private:
    std::shared_ptr<Solution> sol;

    TimeVec T;
    TimeVec w;
    FreqVec That;
    FreqVec what;

    int threshold;
    double eps;
    int aliasing;
    double c;
};

} // END namespace evolution
} // END namespace sphinx
