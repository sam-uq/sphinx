/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include <memory>

namespace sphinx {
//! \namespace evolution
//! Contains all the evolution operators (i.e. the stepping methods).
namespace evolution {

//! \brief A class that provides an evolution operator for the frequency space.
//!
//!
//!
//!
//! \tparam Solution
//! \tparam Grid
//! \tparam FFT
//! \tparam TimeVec
//! \tparam FreqVec
template<class Solution, class Grid, class FFT, class TimeVec, class FreqVec>
class freqEvo {
public:
    ////////////////
    //// ALIASES ///
    ////////////////

    //! Alias for simensionality of the problem.
    constexpr static unsigned int dim = TimeVec::dim;
    //! The forcing function type alias.
    using ForcingFunction = std::function<data_t<dim>(const coord_t<dim> &, double)>;

    //! \brief Allocate all temporary vectors, give names and plan the FFT.
    //!
    //!
    //!
    //! \param gr
    //! \param sol
    //! \param U
    //! \param threshold
    //! \param eps
    //! \param aliasing
    //! \param cfl
    freqEvo(std::shared_ptr<Grid> gr, std::shared_ptr<Solution> sol,
           TimeVec & U,
           int threshold, double eps, int aliasing, double cfl)
       : fft(gr)
       , Uhat(gr, dim), what(gr, curl_dim(dim)), sol(sol)
       , U(gr, dim), T(gr, sym_tensor_dim(dim)), w(gr, curl_dim(dim))
       , That(gr, sym_tensor_dim(dim))
       , threshold(threshold), eps(eps), aliasing(aliasing), c(cfl) {

        Uhat.name = "Uhat";
        That.name = "That";
        what.name = "what";
        U.name = "U";
        w.name = "w";

        fft.plan(U, Uhat);
    }

    //! Perform a single step in frequency space
    //!
    //! The input vector is modified and returned.
    //!
    //! \param[in,out] Uhat_  Storage for velocity in frequency space.
    //! \return               Reference to Uhat_ for chaining.
    FreqVec & operator()( FreqVec & Uhat_, double t = 0 ) {
        profiler::start_stage("Phi");

        vector_laplace(Uhat_, Uhat, threshold, eps);

        alias(Uhat_, aliasing);
        fft.backward(Uhat_, U); // can be inplace
        sym_tensorize(U, T);
        fft.forward(T, That); // can be inplace

        sym_vector_divergence(That, Uhat_);
        projection(Uhat_, Uhat_);
        axpy(Uhat_, Uhat, -1);
        scale(Uhat_, -1);

        // Apply forcing to solution
        if( has_forcing ) {
            apply_forcing(U, t);
            fft.forward(U, Uhat);
            axpy(Uhat_, Uhat, -1);
        }

//        alias(Uhat_, aliasing);

        profiler::end_stage();
        return Uhat_;
    }


    //! Function that is executed only once before the simulation.
    void pre() {}

    //! Function that is executed only once before the simulation.
    void post() {}

    //! Applies forcing function to input vector at specified time.
    //!
    //! Wrapper around forcing member that directly calls the eval_func
    //! kernel. The forcing member is on its own a wrapper around
    //! the initial data class's forcing member.
    //!
    //! \param U  Value to update.
    //! \param t  Current time of the simulation.
    inline void apply_forcing(TimeVec & U, double t) {
        eval_func<dim>(U,
               [this, t] (const coord_t<dim> & coord) {
                   return forcing(coord, t);
               }
        );
    }

    //!
    //! \param forcing_
    void set_forcing(const ForcingFunction & forcing_) {
        has_forcing = true;
        forcing = forcing_;
    }

    //! \brief Callback for called when taking a snapshot of the solution.
    //!
    //!
    //!
    //! \param Uhat_
    //! \param t
    //! \param frameincrement
    void snapshot(FreqVec & Uhat_, double t, bool frameincrement = true) {
        auto snp = sol->newSnapshot(t);
//        alias(Uhat_, aliasing);
        curl(Uhat_,what);

        fft.backward(Uhat_, U);
        fft.backward(what, w);

        normalize(U);
        normalize(w);

        snp->pushData(U, range<dim>());
        snp->pushData(w, range<curl_dim(dim)>());
        snp->writeData();
    }

    //! \brief Return CFL multiplier for adaptive timestepping.
    //!
    //!
    //! \param Uhat_
    //! \param h
    //! \param sup
    //! \return
    double cfl(FreqVec & Uhat_, double h, double & sup) {
        sup = norm(Uhat_, 1); // * std::pow(3/2.,NDIMS); // UGLYNESS

        return h / sup * c;
    }

    //! FFT handle for forward and backward substitution.
    FFT fft;

    //! Handle for velocity in frequency space.
    FreqVec Uhat;
    //! Handle for vorticity in fequency space.
    FreqVec what;
private:
    //! Solution handle with all the data.
    std::shared_ptr<Solution> sol;

    //! Temporary storage for velocity
    TimeVec U;
    //! Temporary storage for nonlinear tensor in time space.
    TimeVec T;
    //! Temporary storage for vorticity.
    TimeVec w;
    //! Temporary storage for nonlinear tensor product in freq. space.
    FreqVec That;

    //! Threshold for truncation of modes in spectral viscosity.
    int threshold;
    //! Multiplier for spectral viscosity.
    double eps;
    //! Highest mode after which anti-aliasing is performed.
    int aliasing;
    //! ?
    double c;

    //! Does the model have forcing? If true then apply_forcing is valid.
    bool has_forcing = false;
    //! The forcing to apply to the velocity given the time.
    ForcingFunction forcing;
};

} // END namespace evolution
} // END namespace sphinx
