/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "geometry/grid.hpp"

#include "initialdata.hpp"

#include "data/initialdata/noise/brownian.h"
#include "data/initialdata/noise/perlin.h"

using namespace sphinx::Geometry;

namespace sphinx {
namespace Data {

template <class Sampler, class Grid>
class brownian : public virtual initialdata<Sampler, Grid> {
    using super = initialdata<Sampler, Grid>;
public:
    enum class Noise {
        Brownian, Perlin
    } noise;

    using G_p = std::shared_ptr<Grid>;
    using Sampler_p = std::shared_ptr<Sampler>;

    brownian(Sampler_p sp, G_p gp)
       : initialdata<Sampler, Grid>(sp, gp) {
    }

    void init() {
        super::init();

//        basefield_x = [] (const coord_t<2> & ) -> double { return 1; };
//        basefield_y = [] (const coord_t<2> & ) -> double { return 1; };
//        basefield_ix = [] (const pos_t<2> & ) -> double { return 1; };
//        basefield_iy = [] (const pos_t<2> & ) -> double { return 1; };

        if ( sphinx::options.has("InitialData.H") ) {
            hurst = sphinx::options.as<double>("InitialData.H");
        }

        std::string noise_name = "brownian";
        if ( sphinx::options.has("InitialData.noise") ) {
            noise_name = sphinx::options.as<std::string>("InitialData.noise");
        }


//        if ( sphinx::options.has("InitialData.N") )
//            N = sphinx::options.as<int>("InitialData.N");
        assert(this->gr->timeN[0] == this->gr->timeN[1]
//               && this.gr.timeN[0] == this.gr.timeN[2]
               && "Needed a mesh with powers of two and with same values in both directions.");
        assert(this->sampler && "Void sampler was passed to the constructor!");

        this->need_cleansing = true;

        if(noise_name == "perlin") {
            noise = Noise::Perlin;
            size = next_pow_of_two(this->gr->timeN[0]) / 8;
            this->sampler->reserve(2*size*size);

            data.resize(2);
            this->sampler->select_stream(0,0);
            data[0] = generateUnitVectorMap(size, this->sampler);
            this->sampler->select_stream(0,1);
            data[1] = generateUnitVectorMap(size, this->sampler);

        } else {
            //! This means size has neever been initialized or has changed
            if(initialdata<Sampler, Grid>::previous_index == -1
               || initialdata<Sampler, Grid>::previous_index != initialdata<Sampler, Grid>::current_index) {
                size = next_pow_of_two(this->gr->timeN[0]) * 2;
            }

                      noise = Noise::Brownian;
            this->sampler->reserve(size*size);

            data.resize(2);
            this->sampler->select_stream(0,0);
            data[0] = fractionalBrownianMotion(size, hurst, this->sampler);
            this->sampler->select_stream(0,1);
            data[1] = fractionalBrownianMotion(size, hurst, this->sampler);
            std::cout << initialdata<Sampler, Grid>::previous_index << "#####################"
                      << size << " " << initialdata<Sampler, Grid>::current_index
                      << std::endl << " --- " << data[0][777] << " --- " << data[1][777]<< " --- "
                    << (*this->sampler)(0,0,0) << " --- ";
        }
    }

    void evalat(const coord_t<2> & index, data_t<2> & val) const {
        int index0, index1;
        switch (noise) {
            default:
            case Noise::Brownian:
                index0 = std::floor((index[0] / 1) * (size));
                index1 = std::floor((index[1] / 1) * (size));

                val[0] = /* basefield_x(index) + */ data[0].at(index0 + (size+1) * index1) * this->scalingFactor;
                val[1] = /* basefield_y(index) + */ data[1].at(index0 + (size+1) * index1) * this->scalingFactor;
                break;
            case Noise::Perlin:
                val[0] = perlin(index[0] * size, index[1] * size, data.at(0), size);
                val[1] = perlin(index[0] * size, index[1] * size, data.at(1), size);
                break;
        }
    }

    // Grab index exatly as is, b(set indexed() to return true below), but must take care of periodicity and yada yada
    void evalat_idx(const pos_t<2> & index, data_t<2> & val) const {

        val[0] = /* basefield_ix(index) + */  data[0].at(index[0] + size * index[1]) * this->scalingFactor;
        val[1] = /* basefield_iy(index) + */ data[1].at(index[0] + size * index[1]) * this->scalingFactor;
    }

private:
    double hurst = 0.5;

//    std::function<double(const coord_t<2>)> basefield_x, basefield_y;

//    std::function<double(const pos_t<2>)> basefield_ix, basefield_iy;

    std::vector<std::vector<double>> data;
    //! Size used to generate noise, it is cached between
    // two different solves with same index (but shan't change, replace with map)
    static int size;
public:
    virtual bool indexed() const { return false; }
};


template <class Sampler, class G>
int brownian<Sampler, G>::size = -1;

} // END namespace Data
} // END namespace sphinx
