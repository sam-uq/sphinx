/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "../../geometry/grid.hpp"

#include "initialdata.hpp"

using namespace sphinx::Geometry;

namespace sphinx {
namespace Data {

//!
//! \tparam Sampler
//! \tparam Grid
template <class Sampler, class Grid>
class taylor_green
        : public virtual initialdata<Sampler, Grid> {
    using super = initialdata<Sampler, Grid>;
public:
    using G_p = std::shared_ptr<Grid>;
    using Sampler_p = std::shared_ptr<Sampler>;

    taylor_green(Sampler_p sp, G_p gp)
       : initialdata<Sampler, Grid>(sp, gp) {
    }

    void init() {
        super::init();
    }

    virtual void evalat(const coord_t<3> & coord,
                data_t<3> & val) const {
        val = {
                std::sin(M_PI*2*coord[0]) * std::cos(M_PI*2*coord[1]) *
                std::cos(M_PI*2*coord[2]),
                -std::cos(M_PI*2*coord[0]) * std::sin(M_PI*2*coord[1]) *
                std::cos(M_PI*2*coord[2]),
                0
         };
    }

    virtual void evalat(const coord_t<2> &,
                data_t<2> &) const {
        throw NotImplementedException();
    }

    virtual void evalat(const coord_t<2> &,
                data_t<1> &) const {
        throw NotImplementedException();
    }

private:
public:
    virtual bool indexed() const { return false; }
};

} // END namespace Data
} // END namespace sphinx
