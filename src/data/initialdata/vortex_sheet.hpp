/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

//! \file vortex-sheet.hpp
//! \brief Contains the implementation of the vortex_sheet initial data.
//!
//! \author Filippo Leonardi
//! \date 2017-01-01

#include "initialdata.hpp"

#include "utilities/options.hpp"

namespace sphinx {
namespace Data {

//!  \brief Specification for the vortex-sheet initial data.
//!
//! The factory name for this data is "vortex_sheet".
//!
//! \ref id-vortexsheet
//!
//! \tparam Sampler
//! \tparam G
template <class Sampler, class G>
class vortex_sheet : public virtual initialdata<Sampler, G> {
    //! Alias for base calss
    using super = initialdata<Sampler, G>;
public:
    //! Alias for non-owned pointer to grid.
    using G_p = std::shared_ptr<G>;
    //! Alias for non-owned pointer to sampler
    using Sampler_p = std::shared_ptr<Sampler>;

    //! \brief Initialise base.
    //!
    //! Does not initialise the class.
    //!
    //! \param smp
    //! \param gp
    vortex_sheet(Sampler_p smp, G_p gp)
        : initialdata<Sampler, G>(smp, gp) {
    }

    virtual ~vortex_sheet() = default;

    //! \brief Initialise class.
    //!
    //! Grabs options and initialise super.
    void init(void) {
        super::init();

        if (sphinx::options.has("InitialData.rho")) {
            rho = sphinx::options.as<double>("InitialData.rho");
        }
        if(rho <= 0) {
            discontinuous = true;
        }
        if ( sphinx::options.has("InitialData.delta") ) {
            delta = sphinx::options.as<double>("InitialData.delta");
        }
    }

    //! \brief Evaluate the initial data at the given coordinate. 2d vector Variant.
    //!
    //! \param coord
    //! \param val
    void evalat(const coord_t<2> & coord, data_t<2> & val) const {
        val = {
                discontinuous ? (
                        (coord[1] > 0.75 || coord[1] < 0.25)
                            ? 1.
                            : -1.
                    ) : (
                        coord[1] > 0.5
                            ? tanh((coord[1] - 0.75) / rho)
                            : tanh((- coord[1] + 0.25) / rho)
                    ),
                delta * sin(2 * M_PI * coord[0])
        };
    }

    //! \brief Evaluate the initial data at the given coordinate. 3d vector Variant.
    //!
    //! \param coord
    //! \param val
    void evalat(const coord_t<3> & coord, data_t<3> & val) const {
        val = {
                discontinuous ? (
                        (coord[1] > 0.75 || coord[1] < 0.25)
                            ? 1.
                            : -1.
                    ) : (
                        coord[1] > 0.5
                            ? tanh((coord[1] - 0.75) / rho)
                            : tanh((- coord[1] + 0.25) / rho)
                    ),
                delta * sin(2 * M_PI * coord[0]),
                1
        };
    }

private:
    //! Is the sheet discontinuous? (Avoid division by 0.)
    bool discontinuous = false;

    //! Perturbation amplitude
    double delta = 0.05;
    //! Smoothness parameter
    double rho = 0.05;

public:
    //! Name to be used to create this class in factory, unique identifier name.
    static constexpr std::string factory_name() { return "vortex_sheet"; }
    //! Is the layout of the data based on indices?
    virtual bool indexed() const { return false; }
};

} // END namespace Data
} // END namespace sphinx
