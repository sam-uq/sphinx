/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "geometry/grid.hpp"

#include "initialdata.hpp"

namespace sphinx {
namespace Data {

template <class Sampler, class G>
class debug_checkerboard : public initialdata<Sampler, G> {
    using super = initialdata<Sampler, G>;
public:
    using G_p = std::shared_ptr<G>;
    using Sampler_p = std::shared_ptr<Sampler>;

    debug_checkerboard(Sampler_p smp, G_p gp)
        : initialdata<Sampler, G>(smp, gp) {
    }

    void init(void) {
        super::init();
        throw NotImplementedException();
    }

//    data_t<2> operator()(const coord_t<2> &)  const {
//        return { 44*this->gr->comm.rank + this->sampler->current_index,
//                    this->gr->comm.color };
//    }
public:
    virtual bool indexed() const { return false; };
};

} // END namespace Data
} // END namespace sphinx
