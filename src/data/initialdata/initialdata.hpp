/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include <functional>
#include <memory>

#include "utilities/types.hpp"

#include "utilities/options.hpp"
#include "utilities/utilities.hpp"

//! \file initialdata.hpp
//! \brief Contains the implementation of base class for all initial data types.
//!
//! \author Filippo Leonardi
//! \date 2017-01-01

namespace sphinx {

//! \namespace Data
//! \brief Contains all types and utilities concerning initial data, including the data itself.
//!
//! \author Filippo Leonardi
//! \date 2017-01-01
namespace Data {

using namespace sphinx::types;

//! \brief An abstract class providing basic methods for initial datum types.
//!
//! The initial data is specified via a derived class of initialdata and contains informations
//! about how to generate the initial data (for the various simulations) and other informations
//! that are scrictly relative to the initial data.
//!
//! Within the \ref Options context, initial data is mostly controlled within the section InitialData.
//!
//! \see \ref sphinx::Sampler, \ref sphinx::Geometry::Grid, \ref sphinx::Options, \ref available-initialdata,
//! \ref vortex_sheet for an example. The factory to construct the initialdata is found in \ref Data::Factory.
//!
//! \tparam Sampler    Type of the sampler, providing randomness for random initial data
//! \tparam G          Type of Grid which used to hold the initial data.
template <class Sampler, class G>
class initialdata {
public:
    ///////////////////////////////////////////////////////////
    //////// TYPEDEFS AND ALIASES /////////////////////////////
    ///////////////////////////////////////////////////////////

    //! Pointer to grid, can't be nullptr.
    using G_p = std::shared_ptr<G>;
    //! Pointer to sampler, can be nullptr.
    using Sampler_p = std::shared_ptr<Sampler>;

    //! Contains information about the last used index for the sim.
    static int previous_index;
    //! Used to update the previous_index tracked
    static int current_index;

    ///////////////////////////////////////////////////////////
    //////// (DE)CONSTRUCTION AND INITIALISATION //////////////
    ///////////////////////////////////////////////////////////

    //! \brief Initializes Grid and Sampler
    //!
    //! The sampler is a non-owned pointer and determines all the non-deterministic parameters
    //! that might be required by the simulation.
    //!
    //! The grid \p gr will be a non-owned pointer an will tell the initial data informations about
    //! the geometry of the mesh, which will be used to properly initialize the grid-vectors.
    //!
    //! \param sampler    A pointer to a Sampler type (e.g. \ref sphinx::Sampler).
    //! \param gr         A pointer to a G type (e.g. \ref sphinx::Geometry::Grid).
    initialdata(Sampler_p sampler, G_p gr)
            : sampler(sampler), gr(gr) {
    }

    //! \breif Perform initialization of the initial data.
    //!
    //! Sets up the indices from the sampler, reads some option and perform some sanity checks.
    //!
    //! This is called to setup the initial data.
    virtual void init() {
        is_data_init = true;

        previous_index = current_index;
        current_index = sampler->current_index;

#ifdef HAVE_OPENMP
        if(!parallel()) {
            debug::warning("Initial data cannot run on parallel MP threads!",
                           mpi::world);
        }
#endif
        if ( sphinx::options.has("InitialData.scalingFactor") ) {
            scalingFactor = sphinx::options.as<double>("InitialData.scalingFactor");
        }
    }

    virtual ~initialdata() = default;

    //! Accessor for is_data init.
    //!
    //! This is true when data is initialized.
    //!
    //! \return true or false depending if data has been initialized (init() has been called).
    bool is_init() const {
        return is_data_init;
    }

    ///////////////////////////////////////////////////////////
    //////// COORDINATE EVALUATION ////////////////////////////
    ///////////////////////////////////////////////////////////

    //! \brief Evaluate the initial data at the given coordinate. 3d vector Variant.
    //!
    //! \warning Placeholder that throws in case it is not implmemented in the derived class.
    virtual void evalat(const coord_t<3> &, data_t<3> &) const {
        throw NotImplementedException();
    }

    //! \brief Evaluate the initial data at the given coordinate. 2d vector Variant.
    //!
    //! \warning Placeholder that throws in case it is not implmemented in the derived class.
    virtual void evalat(const coord_t<2> &, data_t<2> &) const {
        throw NotImplementedException();
    }

    //! \brief Evaluate the initial data at the given coordinate. 2d scalar Variant.
    //!
    //! \warning Placeholder that throws in case it is not implmemented in the derived class.
    virtual void evalat(const coord_t<2> &, data_t<1> &) const {
        throw NotImplementedException();
    }

    ///////////////////////////////////////////////////////////
    ///// INDEX EVALUATORS ////////////////////////////////////
    ///////////////////////////////////////////////////////////

    //! \brief Evaluate the initial data at the given index. 2d vector Variant.
    //!
    //! \warning Placeholder that throws in case it is not implmemented in the derived class.
    virtual void evalat_idx(const pos_t<2> &, data_t<2> &) const {
        throw NotImplementedException();
    }

    //! \brief Evaluate the initial data at the given index. 2d scalar Variant.
    //!
    //! \warning Placeholder that throws in case it is not implmemented in the derived class.
    virtual void evalat_idx(const pos_t<2> &, data_t<1> &) const {
        throw NotImplementedException();
    }

    //! \brief Evaluate the initial data at the given index. 3d vector Variant.
    //!
    //! \warning Placeholder that throws in case it is not implmemented in the derived class.
    virtual void evalat_idx(const pos_t<3> &, data_t<3> &) const {
        throw NotImplementedException();
    }

    //! \brief Returns true if provides initial data only via "index"-coordinate.
    //!
    //! This will be used for convenience for those types of initial data that
    //! are generated on an "index" based manner rather than a "coordinate" based manner.
    //!
    //! \warning Care has to be taken care if this returns true in a MLMC (or multilevel) context.
    //!
    //! \return True if data only generated via index.
    virtual bool indexed() const = 0;
    //! \brief Can the data be generated run in parallel?
    //!
    //! Some simulation can generated exclusively in series, as they are implemented
    //! s.t. they are generated serially.
    //!
    //! \return True if indeed it can.
    virtual bool parallel() const { return true; }

    //! \brief Returns true if initial data returns only a vorticity and not a velocity.
    //!
    //! This is used e.g. in the vortex patch, to force the vorticity automatically.
    //!
    //! \return True if the voriticty is the only supported initial data.
    virtual bool only_vorticity() const { return false; }

    //! \brief Returns true if the initial data provides a forcing.
    //!
    //! \warning Must return true if data provides some initial data.
    //!
    //! \return
    virtual bool has_forcing() const { return false; }

    //! \brief The actual forcing function. 2d Variant.
    //!
    //! Must be overridden.
    //!
    //! \param coord  2d coordinate.
    //! \param t      Time.
    virtual data_t<2> forcing(const coord_t<2> &, double) {
        return {0, 0};
    }

    //! \brief The actual forcing function. 3D Variant.
    //!
    //! Must be overridden.
    //!
    //! \param coord  3d coordinate.
    //! \param t      Time.
    virtual data_t<3> forcing(const coord_t<3> &, double) {
        return {0, 0, 0};
    }

    //! Does the initial data need an initial cleansing (i.e. it is non-div-free)?
    bool need_cleansing = false;
protected:
    //! Shared pointer to a \ref Sampler (must be synced and initialized,
    //! can be nullptr if the data is nonrandom).
    const Sampler_p sampler;
    //! Pointer to grid, cannot be nullptr and must be initialized with the class.
    const G_p gr;
    //! Factor that is read from the options and provides an overall scaling of the entire intial data.
    double scalingFactor = 1.;
    //! True whenever init() has been called (to avoid annoying bugs).
    bool is_data_init = false;
};

//// Static members initializations
template <class Sampler, class G>
int initialdata<Sampler, G>::current_index = -1;
template <class Sampler, class G>
int initialdata<Sampler, G>::previous_index = -1;

} // END namespace Data
} // END namespace sphinx
