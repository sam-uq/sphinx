/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "../../geometry/grid.hpp"

#include "initialdata.hpp"

#include "./noise/brownian.h"
#include "./noise/perlin.h"

using namespace sphinx::Geometry;

namespace sphinx {
namespace Data {

template <class basefield, class Sampler, class Grid>
class brownian_additive : public brownian<Sampler,Grid>, public basefield {
    using super = brownian<Sampler,Grid>;
public:

    using G_p = std::shared_ptr<Grid>;
    using Sampler_p = std::shared_ptr<Sampler>;

    brownian_additive(Sampler_p sp, G_p gp)
       : initialdata<Sampler, Grid>(sp, gp)
       , brownian<Sampler,Grid>(sp, gp)
       , basefield(sp, gp) {
    }

    void init() {
        super::init();
        basefield::init();
    }

    void evalat(const coord_t<2> & index, data_t<2> & val) const {
        basefield::evalat(index, val);
        data_t<2> val2;
        super::evalat(index, val2);
        val += val2;
    }

    // Grab index exatly as is, b(set indexed() to return true below), but must take care of periodicity and yada yada
    void evalat_idx(const pos_t<2> &, data_t<2> &) const {
        throw NotImplementedException();
    }

    virtual bool indexed() const { return false; }
};

} // END namespace Data
} // END namespace sphinx
