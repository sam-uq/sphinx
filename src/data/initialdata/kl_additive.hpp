/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "kl.hpp"

namespace sphinx {
namespace Data {

template <class basefield, class Sampler, class G>
class kl_additive
        : public basefield
        , private kl<Sampler, G> {
    using super = kl<Sampler, G>;
public:
    //! Pointer to grid, can't be nullptr.
    using G_p = std::shared_ptr<G>;
    //! Pointer to samples, can be nullptr.
    using Sampler_p = std::shared_ptr<Sampler>;

    kl_additive(Sampler_p sp, G_p gp)
       : initialdata<Sampler, G>(sp, gp)
       , basefield(sp, gp)
       , kl<Sampler, G>(sp, gp) {
    }

    void init() {
        super::init();
        basefield::init();
    }

    void evalat(const coord_t<2> & coord, data_t<1> & val) const {
        double add = kl<Sampler, G>::expansion(coord);
        basefield::evalat(coord, val);
        val[0] += add;
    }

    void evalat(const coord_t<3> & coord, data_t<3> & val) const {
        double add = kl<Sampler, G>::expansion(coord);
        basefield::evalat(coord, val);
        val[0] += add;
    }

    virtual bool indexed() const { return false; }
private:
};

} // END namespace Data
} // END namespace sphinx

