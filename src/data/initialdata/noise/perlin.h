/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

//
// Created by filippo on 15/05/17.
//
// Adapted from

// Function to linearly interpolate between a0 and a1
// Weight w should be in the range [0.0, 1.0]
inline double lerp(double a0, double a1, double w) {
    return (1.0 - w) * a0 + w*a1;
}

// Computes the dot product of the distance and gradient vectors.
inline double dotGridGradient(int ix, int iy,
                       double x, double y,
                       const std::vector<double> & Gradient, int size) {

    // Precomputed (or otherwise) gradient vectors at each grid node

    // Compute the distance vector
    double dx = x - (double) ix;
    double dy = y - (double) iy;

    // Compute the dot-product
    return (
            dx*Gradient[iy + 2*(size * ix)]
            + dy*Gradient[1 + 2*(iy + size * ix)]
    );
}

// Compute Perlin noise at coordinates x, y (mapped from [0,1]x[0,1])
inline double perlin(double x, double y,
                     const std::vector<double> & gradient, int size) {
    // Determine grid cell coordinates
    int x0 = std::floor(x);
    int x1 = x0 + 1;
    int y0 = std::floor(y);
    int y1 = y0 + 1;

    // Determine interpolation weights
    // Could also use higher order polynomial/s-curve here
    double sx = x - (double) x0;
    double sy = y - (double) y0;
    // Interpolate between grid point gradients
    double n0, n1, ix0, ix1, value;
    n0 = dotGridGradient(x0, y0, x, y, gradient, size);
    n1 = dotGridGradient(x1, y0, x, y, gradient, size);
    ix0 = lerp(n0, n1, sx);
    n0 = dotGridGradient(x0, y1, x, y, gradient, size);
    n1 = dotGridGradient(x1, y1, x, y, gradient, size);
    ix1 = lerp(n0, n1, sx);
    value = lerp(ix0, ix1, sy);

    return value;
}

template <class Sampler>
inline std::vector<double> generateUnitVectorMap(int size,
                                                 std::shared_ptr<Sampler> sampler) {
    std::vector<double> ret(2*size*size);

    for(int i = 0; i < size; ++i) {
        for(int j = 0; j < size; ++j) {
            ret[0 + 2*(i + size*j)] = sampler->next();
            ret[1 + 2*(i + size*j)] = sampler->next();
            double norm = std::sqrt(ret[0 + 2*(i + size*j)]*ret[0 + 2*(i + size*j)]
                                    + ret[1 + 2*(i + size*j)]*ret[1 + 2*(i + size*j)]);
            ret[0 + 2*(i + size*j)] /= norm;
            ret[1 + 2*(i + size*j)] /= norm;
        }
    }

    return ret;
}