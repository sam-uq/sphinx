/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

//
// Created by filippo on 15/05/17.
//
// Adapted from code by Kjetil Olsen Lye, originally generated from:
// This is found in the paper
//   Brouste, A., Istas, J., & Lambert-Lacroix, S. (2007).
//   On Fractional Gaussian Random Fields Simulations. Journal of Statistical Software, 23(1), 1–23.
//   http://doi.org/http://dx.doi.org/10.18637/jss.v023.i01
// in the section 2.3

inline double variance_fBm(double hurst, double n) {
    return std::sqrt(
            (1-std::pow(2, 2*hurst-2)) /
            std::pow(2, 2*n*hurst)
    );
}

template <class Sampler>
std::vector<double> fractionalBrownianMotion(int size, double hurst,
                                             std::shared_ptr<Sampler> sampler) {
    double n = std::log2(size);
    // End recursion by returning zero
    if(size == 1) {
        return {0, 0, 0, 0};
    }
    std::vector<double> dPrev = fractionalBrownianMotion(size/2, hurst, sampler);

    // Interpolate to refined grid, add ghost nodes at end
    std::vector<double> d((size+1)*(size+1));
    int size2 = size/2;
    // Actual mesh with padding
    int size2p1 = size2+1;
    int sizep1 = size + 1;

    // ZERO BOUNDARY
    // NEED (size-1)^2 random numbers
//    auto outerFineIndex = [sizep1] (int i, int j) {
//        return i+sizep1*j;
//    };
//    auto outerCoarseIndex = [size2p1] (int i, int j) {
//        return i+size2p1*j;
//    };
//    int start_i = 1;
//    int start_j = 1;
//    int end_i = size2;
//    int end_j = size2;

    // PERIODIC BOUNDARY
    // NEED (size-1)^2 random numbers
    int start_i = 0;
    int start_j = 0;
    int end_i = size2;
    int end_j = size2;
    auto outerFineIndex = [sizep1, size] (int i, int j) {
        if(i == size) i = 0;
        if(j == size) j = 0;
        return i+sizep1*j;
    };
    auto outerCoarseIndex = [size2p1] (int i, int j) {
        return i+size2p1*j;
    };

    for (int j = start_j; j < end_j; ++j) {
        for (int i = start_i; i < end_i; ++i) {
            d[outerFineIndex(2*i, 2*j)] = dPrev[outerCoarseIndex(i,j)];
        }
    }

    for (int j = start_j; j < end_j; ++j) {
        for (int i = start_i; i < end_i; ++i) {
            int iCenter = 2*i+1;
            int jCenter = 2*j;
            int ICenter = outerFineIndex(iCenter, jCenter);

            int iLeft  = 2*i;
            int iRight = 2*(i+1);
            int ILeft = outerFineIndex(iLeft, jCenter);
            int IRight = outerFineIndex(iRight, jCenter);

            d[ICenter] = 0.5*(d[ILeft]+d[IRight]) + variance_fBm(hurst, n) * sampler->next();

            iCenter = 2*i;
            jCenter = 2*j+1;
            ICenter = outerFineIndex(iCenter, jCenter);

            int jLeft  = 2*j;
            int jRight = 2*(j+1);
            ILeft = outerFineIndex(iCenter, jLeft);
            IRight = outerFineIndex(iCenter, jRight);

            d[ICenter] = 0.5*(d[ILeft]+d[IRight]) + variance_fBm(hurst, n) * sampler->next();

            iCenter = 2*i+1;
            jCenter = 2*j+1;
            ICenter = outerFineIndex(iCenter, jCenter);

            jLeft  = 2*j;
            jRight = 2*(j+1);
            iLeft = 2*i;
            iRight = 2*(i+1);
            int ILeftLeft = outerFineIndex(iLeft, jLeft);
            int IRightRight = outerFineIndex(iRight, jRight);
            int ILeftRight = outerFineIndex(iLeft, jRight);
            int IRightLeft = outerFineIndex(iRight, jLeft);

            d[ICenter] = (d[ILeftLeft]+d[ILeftRight]+d[IRightLeft]+d[IRightRight]) / 4.0
                       + variance_fBm(hurst, n) * sampler->next();
        }
    }

    return d;
}
