/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "initialdata.hpp"

namespace sphinx {
namespace Data {

template <class Sampler, class G>
class random_initialdata
        : public virtual initialdata<Sampler, G> {
    using super = initialdata<Sampler, G>;
public:
    using G_p = std::shared_ptr<G>;
    using Sampler_p = std::shared_ptr<Sampler>;

    random_initialdata(Sampler_p sp, G_p gp)
        : initialdata<Sampler, G>(sp, gp) {
        assert(this->sampler
               && "No sampler was passed to the constructor!");
    }

    void init(void) {
        super::init();

        this->sampler->reserve(N);

        if ( sphinx::options.has("InitialData.N") ) {
            N = sphinx::options.as<int>("InitialData.N");
        }
        if ( sphinx::options.has("InitialData.alpha") ) {
            alpha = sphinx::options.as<double>("InitialData.alpha");
        }
        if ( sphinx::options.has("InitialData.shift") ) {
            shift = sphinx::options.as<double>("InitialData.shift");
        }

        vampl = std::vector<double>(N, 1);
        vshift = std::vector<double>(N, 0);

        if( sphinx::options.has("InitialData.vshift") ) {
            auto v = sphinx::options.as<std::vector<double>>("InitialData.vshift");
            std::copy(v.begin(), v.end(), vshift.begin());
        }
        if( sphinx::options.has("InitialData.ashift") ) {
            auto v = sphinx::options.as<std::vector<double>>("InitialData.ashift");
            std::copy(v.begin(), v.end(), vampl.begin());
        }


        this->need_cleansing = true;
    }

    //! \breif Applies an "interface" perturbation to the coordinate a
    //! 
    //! Modifies only the second component of a, based on the first component of a:
    //! \f$ a_2 = a_2 + \sum_{i = 0}^N \alpha \omega_{0,0,i} * \sin(2\pi (i+1) (a_0 - shift) - \omega_{0,1,s}\f$.
    //!
    //! 
    [[deprecated]] coord_t<2> interface(const coord_t<2> &a) const {

        coord_t<2> intf = a;
        for(int s = 0; s < N; s++) {
            intf[1] += alpha * vampl[s] * (*this->sampler)(0,0,s) *
                    sin( 2 * M_PI * ((s+1) * (a[0] - shift - vshift[s]) - (*this->sampler)(0,1,s)));
        }
        return intf;
    }

    //! \brief Applies interface to a single scalar.
    //!
    //! \warning No 2 * pi factor, i.e. periodic in a[1] with period \f$2 \pi\f$.
    //! \warning Output on elem index 0.
    //!
    //! \param a    Scalar to perturb.
    //! \return     Perturbed scalar.
    real_t interface_single(const coord_t<2> &a) const {
        auto r = a[0];
        for (int s = 0; s < N; s++) {
            r += alpha * vampl[s] * (*this->sampler)(0, 0, s) *
                 sin(((s + 1) * (a[1] - shift - vshift[s]) - (*this->sampler)(0, 1, s)));
        }
        return r;
    }

    virtual bool indexed() const { return false; }
protected:
    //! Perturbation amplitude
    double alpha = 0.01;
    //! Shifting of the data along x-axis.
    double shift = 0.0;
    //! Horizontal and amplitude shift *per-node*
    std::vector<double> vshift, vampl;
    //! Number of modes of the perturbation
    int N = 10;
};

} // END namespace Data
} // END namespace sphinx

