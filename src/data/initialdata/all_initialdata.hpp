/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

//! \file all_initialdata.hpp Convenient header including all
//! initial data types and containing a factory function.

#include <memory>

#include "utilities/debug.hpp"
#include "initialdata.hpp"

// Standard benchmarks
#include "data/initialdata/taylor_green.hpp"
#include "data/initialdata/vortex_sheet.hpp"
#include "data/initialdata/blob.hpp"
#include "data/initialdata/patch.hpp"
#include "data/initialdata/debug_checkerboard.hpp"
// Kissing vortex
#include "data/initialdata/kissing_vortices.hpp"

// Random initial data
#include "data/initialdata/random_vortex_sheet.hpp"
#include "data/initialdata/random_patch.hpp"
#include "data/initialdata/kl_additive.hpp"
#include "data/initialdata/brownian.hpp"
#include "data/initialdata/brownian_additive.hpp"

// Test data
#include "data/initialdata/dummy.hpp"
#include "data/initialdata/debug_initialdata.hpp"

#include "data/initialdata/python_initialdata.hpp"
#include "data/initialdata/lua_initialdata.hpp"

namespace sphinx {
namespace Data {

//! \brief Creates the correct initial data given the corresponding name.
//!
//!
//!
//!
//! \see \ref initialdata, \ref available-initialdata
//!
//! \tparam Sampler
//! \tparam G
//! \param smp
//! \param grid
//! \param name
//! \return
template <class Sampler, class G>
std::shared_ptr<initialdata<Sampler, G>> Factory(std::shared_ptr<Sampler> smp,
                                                 std::shared_ptr<G> grid,
                                                 std::string name) {
    initialdata<Sampler, G> * ptr;

    std::map<std::string, std::function<initialdata<Sampler, G>*(void)>> initialDatas = {
            {   vortex_sheet<Sampler, G>::factory_name(), [&smp, &grid] () {
                return new vortex_sheet<Sampler, G>(smp, grid);
            }
        },
            {   "debug" , [&smp, &grid] () {
                return new debug_initialdata<Sampler, G>(smp, grid);
            }
        },
            {   "taylor_green" , [&smp, &grid] () {
                return new taylor_green<Sampler, G>(smp, grid);
            }
        },
            {   "random_taylor_green" , [&smp, &grid] () {
                return new kl_additive<sphinx::Data::taylor_green<Sampler, G>, Sampler, G>(smp, grid);
            }
        },
            {   "constant" , [&smp, &grid] () {
                return new constant<Sampler, G>(smp, grid);
            }
        },
            {   "blob" , [&smp, &grid] () {
                return new blob<Sampler, G>(smp, grid);
            }
        },
            {   "patch" , [&smp, &grid] () {
            return new patch<Sampler, G>(smp, grid);
        }
        },
            {   "debug_checkerboard" , [&smp, &grid] () {
                return new debug_checkerboard<Sampler, G>(smp, grid);
            }
        },
            {   "random_vortex_sheet" , [&smp, &grid] () {
                return new random_vortex_sheet<Sampler, G>(smp, grid);
            }
        },
            {   "random_additive_blob" , [&smp, &grid] () {
                return new kl_additive<sphinx::Data::blob<Sampler, G>, Sampler, G>(smp, grid);
            }
        },
            {   "random_patch", [&smp, &grid]() {
                return new random_patch<Sampler, G>(smp, grid);
            }
        },
            {   "random_additive_constant", [&smp, &grid] () {
                return new kl_additive<sphinx::Data::constant<Sampler, G>, Sampler, G>(smp, grid);
            }
        },
            {   "vortex_core", [&smp, &grid] () {
                return new kissing_vortices<Sampler, G>(smp, grid);
            }
        },
            {   "brownian", [&smp, &grid] () {
            return new brownian<Sampler, G>(smp, grid);
        }
        },
            {   "vortex_sheet_brownian", [&smp, &grid] () {
            return new brownian_additive<vortex_sheet<Sampler, G>, Sampler, G>(smp, grid);
        }
        },
#ifdef HAVE_PYTHON
            {"python", [&smp, &grid]() {
            return new python_initialdata<Sampler, G>(smp, grid);
        }
        },
#endif // HAVE_PYTHON
#ifdef HAVE_LUA
            {   "lua", [&smp, &grid] () {
            return new lua_initialdata<Sampler, G>(smp, grid);
        }
        },
#endif // HAVE_PYTHON
    };

    if(initialDatas.count(name) > 0) {
        ptr = initialDatas[name]();
    } else {
        debug::error<true>("Choose a valid initial data, one of:",
                           sphinx::mpi::world);
        for(auto keyval: initialDatas) {
            std::stringstream ss;
            ss << "-" << keyval.first;
            debug::error<true>(ss.str(), sphinx::mpi::world);
        }
        debug::error("...or else.",
                     sphinx::mpi::world);
        ptr = nullptr;
    }

    return std::shared_ptr<initialdata<Sampler, G>>(ptr);
}

} // END namespace Data
} // END namespace sphinx
