/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "geometry/grid.hpp"

#include "initialdata.hpp"

#ifdef HAVE_PYTHON
#  include <boost/python.hpp>
#  include <boost/ref.hpp>
#endif // HAVE_PYTHON

//! \file python_initialdata.hpp Loads initial data as a python file.

#ifdef HAVE_PYTHON
namespace sphinx {

extern unsigned int omp_threads;

namespace Data {

namespace py = boost::python;

//! Define this to toggle safe Python calls
#define USE_SAFE_PYTHON_CALLS

#ifdef USE_SAFE_PYTHON_CALLS
#  define PYTHON_SAFE_CALL(x) \
try { \
x \
} catch(...) { \
if (PyErr_Occurred()) { \
std::cout << handle_pyerror(); \
} \
py::handle_exception(); \
PyErr_Clear(); \
}
#else
#  define PYTHON_SAFE_CALL(x) {x}
#endif

//! \brief Converts Python exceptions to strings.
//!
//! From http://stackoverflow.com/questions/1418015/how-to-get-python-exception-text
//! Decode a Python exception into a string
//!
//! \return A human readable string.
inline std::string handle_pyerror() {
    using namespace boost::python;
    using namespace boost;

    PyObject *exc, *val, *tb;

    object formatted_list, formatted;

    PyErr_Fetch(&exc, &val, &tb);
    PyErr_NormalizeException(&exc, &val, &tb);

    handle<> hexc(exc), hval(allow_null(val)), htb(allow_null(tb));

    object traceback(import("traceback"));

    if (!tb) {
        object format_exception_only(traceback.attr("format_exception_only"));
        formatted_list = format_exception_only(hexc, hval);
    } else {
        object format_exception(traceback.attr("format_exception"));
        formatted_list = format_exception(hexc, hval, htb);
    }

    formatted = str("\n").join(formatted_list);

    return extract<std::string>(formatted);
}


//! \brief Initial data which reads a file in Python format.
//!
//! Loads python functions and varriables.
//!
//! \tparam Sampler
//! \tparam G
template<class Sampler, class G>
class python_initialdata
        : public virtual initialdata<Sampler, G> {


    using super = initialdata<Sampler, G>;
public:
    //! Pointer to grid, can't be nullptr.
    using G_p = std::shared_ptr<G>;
    //! Pointer to samples, can be nullptr.
    using Sampler_p = std::shared_ptr<Sampler>;

    python_initialdata(Sampler_p smp, G_p gp)
            : initialdata<Sampler, G>(smp, gp) {
        Py_Initialize();

        boost::python::class_<Sampler>("Sampler")
                .def("next", &Sampler::next)
                .def("select_stream", &Sampler::select_stream)
                .def("reserve", &Sampler::reserve)
                .def("get", &Sampler::operator());

        assert(sphinx::omp_threads == 1 && "No concurrent Python initialdata.");

        std::string input_name = default_input_name;
        if(options.has("InitialData.input")) {
            input_name = options.as<std::string>("InitialData.input");
        }

        PYTHON_SAFE_CALL(
            main_module = py::import("__main__");
            main_namespace = main_module.attr("__dict__");

                main_namespace["smp"] = py::object(py::ptr(smp.get()));

            file = py::exec_file(input_name.c_str(), main_namespace);

            // Export functions
                main_namespace["check_python"]();
                init_py = main_namespace["init_py"];
            eval_at_w_2d = main_namespace["eval_at_w_2d"];
            eval_at_u_2d = main_namespace["eval_at_u_2d"];
            forcing_2d = main_namespace["forcing_2d"];
            forcing_3d = main_namespace["forcing_2d"];

        );
    }

    //! \brief Sets the parameters up
    //!
    //!
    void init(void) {
        super::init();

        init_py();
    }

    //! \brief Evaluation operator for data with 1-dof.
    //!
    //! Generates a constant vector. Disabled if f != 1.
    //!
    //! \param[in] a coordinate to which evaluate the initial data.
    //! \return value of initial data at a.
    void evalat(const coord_t<2> & a, data_t<1> & b) const {
        PYTHON_SAFE_CALL(
                b[0] = py::extract<double>(
                        eval_at_w_2d(a[0], a[1])
                );
        );
//        , boost::ref(*super::sampler)
    }

    //! \brief Evaluation operator for data with 2-dof.
    //!
    //! Generates a constant vector. Disabled if f != 2.
    //!
    //! \param[in] a coordinate to which evaluate the initial data.
    //! \return value of initial data at a.
    void evalat(const coord_t<2> & a, data_t<2> & b) const {
        PYTHON_SAFE_CALL(
            py::tuple b_ = py::extract<py::tuple>(eval_at_u_2d(a[0], a[1]));
            b[0]  = py::extract<double>(b_[0]);
            b[1]  = py::extract<double>(b_[1]);
            );
    }

    //! \brief The actual forcing function. 2d Variant.
    //!
    //! Must be overridden.
    //!
    //! \param coord  2d coordinate.
    //! \param t      Time.
    virtual data_t<2> forcing(const coord_t<2> & coord, double t) {
        data_t<2> ret;
        PYTHON_SAFE_CALL(
                py::tuple b_ = py::extract<py::tuple>(
                        forcing_2d(coord[0], coord[1], t)
                );
                ret[0]  = py::extract<double>(b_[0]);
                ret[1]  = py::extract<double>(b_[1]);
        );
        return ret;
    }

    //! \brief The actual forcing function. 3D Variant.
    //!
    //! Calls the luad function "forcing_3d"
    //!
    //! \param coord  3d coordinate.
    //! \param t      Time.
    virtual data_t<3> forcing(const coord_t<3> & coord, double t) {
        data_t<3> ret;
        PYTHON_SAFE_CALL(
                py::tuple b_ = py::extract<py::tuple>(
                        forcing_3d(coord[0], coord[1], coord[2], t)
                );
                ret[0]  = py::extract<double>(b_[0]);
                ret[1]  = py::extract<double>(b_[1]);
                ret[2]  = py::extract<double>(b_[2]);
        );
        return ret;
    }

    py::object main_module;
    py::object main_namespace;

    py::object file;

    py::object init_py;

    py::object eval_at_w_2d;
    py::object eval_at_u_2d;

    py::object forcing_2d;
    py::object forcing_3d;

    //! \brief Evaluation operator for data with 3-dofs.
    //!
    //! Generates a constant vector. Disabled if f != 2.
    //!
    //! \param[in] a coordinate to which evaluate the initial data.
    //! \return    value of initial data at a.
    void evalat(const coord_t<3> &, data_t<3> &) const {
        throw NotImplementedException();
    }

    //! \brief Returns true if initial data returns only a vorticity and not a velocity.
    //!
    //! This is used e.g. in the vortex patch, to force the vorticity automatically.
    //!
    //! \return True if the voriticty is the only supported initial data.
    virtual bool only_vorticity() const {
        return py::extract<bool>(main_namespace["only_vorticity"]);
    }

    //! \brief Returns true if the initial data provides a forcing.
    //!
    //! \warning Must return true if data provides some initial data.
    //!
    //! \return
    virtual bool has_forcing() const {
        return py::extract<bool>(main_namespace["has_forcing"]);
    }
private:
    //! Default name to load.
    std::string default_input_name = "initialdata.py";

    //! Scale the size of the initial data, this increases the speed of
    //! the flow
    double magnitude = 1.;
public:
    virtual bool indexed() const { return false; };
//    virtual bool parallel() const { return false; };
};

} // END namespace Data
} // END namespace sphinx

#endif //HAVE_PYTHON
