/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "patch.hpp"
#include "random_initialdata.hpp"

//! \file random_patch.hpp Contains implementation for the randomly
//! perturbed vortex patch.

namespace sphinx {
namespace Data {

//!
//! \tparam Sampler
//! \tparam G
template<class Sampler, class G>
class random_patch
        : public patch<Sampler, G>
          , public random_initialdata<Sampler, G> {
    using super = initialdata<Sampler, G>;
public:
    using G_p = std::shared_ptr<G>;
    using Sampler_p = std::shared_ptr<Sampler>;

    random_patch(Sampler_p sp, G_p gp)
            : initialdata<Sampler, G>(sp, gp)
              , patch<Sampler, G>(sp, gp)
              , random_initialdata<Sampler, G>(sp, gp) {
    }

    void init() {
        patch<Sampler, G>::init();
        random_initialdata<Sampler, G>::init();
    }

    //! \brief Compute initially perturbed vorticity for 2d case.
    //!
    //! Centers coordinate to (0,0), and then apply perturbation radially
    //! for every angle.
    //!
    //! \param coord    Cartesian coordinate to evaluate.
    //! \param val      Value of vorticity at coord
    void evalat(const coord_t<2> &coord, data_t<1> &val) const {
#if __cpp_structured_bindings
        auto[r, theta] = cart2pol(coord - patch<Sampler, G>::center);
#else
	auto coordn = cart2pol(coord - patch<Sampler, G>::center);
	auto r = coordn[0];
	auto theta = coordn[1];
#endif
        // Unfortunately interface operates on the permuted vector
        r = random_initialdata<Sampler, G>::interface_single({r, theta});

        patch<Sampler, G>::evalat(pol2cart({r, theta}) + patch<Sampler, G>::center, val);
    }

    void evalat(const coord_t<2> &, data_t<2> &) const {
        throw NotImplementedException();
    }

private:

public:
    virtual bool indexed() const { return false; }
};

} // END namespace Data
} // END namespace sphinx

