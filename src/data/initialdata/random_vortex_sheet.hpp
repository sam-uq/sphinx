/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "vortex_sheet.hpp"
#include "random_initialdata.hpp"

namespace sphinx {
namespace Data {

template <class Sampler, class G>
class random_vortex_sheet
        : public vortex_sheet<Sampler, G>
        , public random_initialdata<Sampler, G> {
    using super = initialdata<Sampler, G>;
public:
    using G_p = std::shared_ptr<G>;
    using Sampler_p = std::shared_ptr<Sampler>;

    random_vortex_sheet(Sampler_p sp, G_p gp)
       : initialdata<Sampler, G>(sp, gp)
       , vortex_sheet<Sampler, G>(sp, gp)
       , random_initialdata<Sampler, G>(sp, gp) {
    }

    void init() {
        vortex_sheet<Sampler, G>::init();
        random_initialdata<Sampler, G>::init();
    }

    void evalat(const coord_t<2> & coord, data_t<2> & val) const {
        coord_t<2> a_ = random_initialdata<Sampler, G>::interface(coord);
        vortex_sheet<Sampler, G>::evalat(a_, val);
    }

private:

public:
    virtual bool indexed() const { return false; }
};

} // END namespace Data
} // END namespace sphinx

