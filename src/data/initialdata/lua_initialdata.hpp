/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "geometry/grid.hpp"

#include "initialdata.hpp"

#ifdef HAVE_LUA
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wunused-but-set-parameter"
#include <selene.h>
#pragma GCC diagnostic pop
#endif // HAVE_LUA


//! \file lua_initialdata.hpp A class that contain the lua_initialdata class.

#ifdef HAVE_LUA
namespace sphinx {

extern unsigned int omp_threads;

namespace Data {

//! \brief Manages initial data specified via a LUA script.
//!
//! Reads the file named "initialdata.lua" or the filename specified by the "InitialData.input"
/// parameters, whenever specified.
//!
//! Exposes the following objects to the LUA interpreter:
//!  - the sampler, via the "sampler" object.
//!
//! TODO: export arguments of "InitialData.args"
//!
//! \tparam Sampler     Must have a few methods for the generation of random numbers.
//! \tparam Grid
template <class Sampler, class Grid>
class lua_initialdata
        : public virtual initialdata<Sampler, Grid> {
    using super = initialdata<Sampler, Grid>;
public:
    //! Pointer to grid, can't be nullptr.
    using G_p = std::shared_ptr<Grid>;
    //! Pointer to samples, can be nullptr.
    using Sampler_p = std::shared_ptr<Sampler>;

    sel::State state;

    lua_initialdata(Sampler_p smp, G_p gp)
        : initialdata<Sampler, Grid>(smp, gp), state(true) {

        assert(sphinx::omp_threads == 1 && "No concurrent LUA initialdata.");

        std::string input_name = default_input_name;
        if(options.has("InitialData.input")) {
            input_name = options.as<std::string>("InitialData.input");
        }

        state.Load(input_name);
        state["check_lua"]();

        // Read and expose arguments to LUA
        std::vector<std::string> args;
        if(options.has("InitialData.args")) {
            args = options.as<std::vector<std::string>>("InitialData.args");
        }
        int I = 0;
        for(std::string str: args) {
            state["args"][I++] = str;
        }

        // Expose "sampler" to LUA.
        state["sampler"].SetObj(*smp,
                                "select_stream", &Sampler::select_stream,
                                "next", &Sampler::next_nonconst,
                                "reserve", &Sampler::reserve);
    }

    //! \brief Sets the parameters up
    //!
    //! \warning Must be called after the sample has been setup.
    void init(void) {
        super::init();

        state["init"]();
    }

    //! \brief Evaluation operator for data with 1-dof (vorticity).
    //!
    //! Generates a constant vector. Disabled if f != 1.
    //!
    //! \param[in] a coordinate to which evaluate the initial data.
    //! \param[out] b the data evaluated at coordinate a
    void evalat(const coord_t<2> & a, data_t<1> & b) const {
        b[0] = state["eval_at_w_2d"](a[0], a[1]);
    }

    //! \brief Evaluation operator for data with 2-dof.
    //!
    //! Generates a constant vector. Disabled if f != 2.
    //!
    //! \param[in] a coordinate to which evaluate the initial data.
    //! \param[out] b the data evaluated at coordinate a
    void evalat(const coord_t<2> & a, data_t<2> & b) const {
        sel::tie(b[0], b[1])
                = state["eval_at_u_2d"](a[0], a[1]);
    }

    //! \brief Evaluation operator for data with 2-dof.
    //!
    //! Generates a constant vector. Disabled if f != 2.
    //!
    //! \param[in] a coordinate to which evaluate the initial data.
    //! \return value of initial data at a.
    void evalat(const coord_t<3> & coord, data_t<3> & val) const {
        sel::tie(val[0], val[1], val[2])
                = state["eval_at_u_3d"](coord[0], coord[1], coord[2]);

    }

    //! \brief Returns true if initial data returns only a vorticity and not a velocity.
    //!
    //! This is used e.g. in the vortex patch, to force the vorticity automatically.
    //!
    //! \return True if the voriticty is the only supported initial data.
    virtual bool only_vorticity() const {
        return state["only_vorticity"];
    }

    //! \brief Returns true if the initial data provides a forcing.
    //!
    //! \warning Must return true if data provides some initial data.
    //!
    //! \return
    virtual bool has_forcing() const {
        return state["has_forcing"];
    }

    //! \brief The actual forcing function. 2d Variant.
    //!
    //! Must be overridden.
    //!
    //! \param coord  2d coordinate.
    //! \param t      Time.
    virtual data_t<2> forcing(const coord_t<2> & coord, double t) {
        data_t<2> temp;
        sel::tie(temp[0], temp[1], temp[2])
                = state["forcing_2d"](coord[0], coord[1], t);
        return temp;
    }

    //! \brief The actual forcing function. 3D Variant.
    //!
    //! Calls the luad function "forcing_3d"
    //!
    //! \param coord  3d coordinate.
    //! \param t      Time.
    virtual data_t<3> forcing(const coord_t<3> & coord, double t) {
        data_t<3> temp;
        sel::tie(temp[0], temp[1], temp[2])
                = state["forcing_3d"](coord[0], coord[1], coord[2], t);
        return temp;
    }

private:
    //! Default name to load.
    std::string default_input_name = "initialdata.lua";

public:
    virtual bool indexed() const { return false; };
    virtual bool parallel() const { return false; };
};

} // END namespace Data
} // END namespace sphinx

#endif //HAVE_LUA
