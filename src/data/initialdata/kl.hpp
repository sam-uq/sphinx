/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "initialdata.hpp"

namespace sphinx {
namespace Data {

template <class Sampler, class G>
class kl : public virtual initialdata<Sampler, G> {
    using super = initialdata<Sampler, G>;
public:
    //! Pointer to grid, can't be nullptr.
    using G_p = std::shared_ptr<G>;
    //! Pointer to samples, can be nullptr.
    using Sampler_p = std::shared_ptr<Sampler>;

    kl(Sampler_p sp, G_p gp)
        : initialdata<Sampler, G>(sp, gp) {
        assert(this->sampler && "No sampler was passed to the constructor!");
    }

    void init(void) {
        super::init();

        this->sampler->reserve(N*N);

        if ( sphinx::options.has("InitialData.N") )
            N = sphinx::options.as<int>("InitialData.N");
        if ( sphinx::options.has("InitialData.alpha") )
            alpha = sphinx::options.as<double>("InitialData.alpha");
    }

    double expansion(const coord_t<2> & a) const {
        double val = 0.;
        for(int s = 0; s < N; s++) {
            val += alpha * std::pow((s+1), -2.)* (*this->sampler)(0,0,s) *
                    sin( 2 * M_PI * ((s+1) * a[0])) *
                    sin( 2 * M_PI * ((s+1) * a[1]));
        }
        return val;
    }

    double expansion(const coord_t<3> & a) const {
        double val = 0.;
        for(int s = 0; s < N; s++) {
            val += alpha * std::pow((s+1), -2.)* (*this->sampler)(0,0,s) *
                    sin( 2 * M_PI * ((s+1) * a[0])) *
                    sin( 2 * M_PI * ((s+1) * a[1])) *
                    sin( 2 * M_PI * ((s+1) * a[2]));
        }
        return val;
    }

protected:
    //! Magnitude of randomness
    double alpha = 10;

    //! Dimensionality of random space
    int N = 10;
public:
    virtual bool indexed() const { return false; }
};

} // END namespace Data
} // END namespace sphinx

