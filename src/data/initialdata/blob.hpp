/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "../../geometry/grid.hpp"

#include "initialdata.hpp"

//! \file blob.hpp Contains implementation of vortex blob initial data.

namespace sphinx {
namespace Data {

//! Initial data providing a blob as data.
//! If applied to vorticity is a benchmark for simple, rotating vortex.
//! \tparam Sampler type of the sampler, providing randomness for random initial data
//! \tparam G type of Grid, used to compute coordinates
//! \tparam f degrees of freedom provided by evaluation operators
template <class Sampler, class G>
class blob : public virtual initialdata<Sampler, G> {
    using super = initialdata<Sampler, G>;
public:
    //! Pointer to grid, can't be nullptr.
    using G_p = std::shared_ptr<G>;
    //! Pointer to samples, can be nullptr.
    using Sampler_p = std::shared_ptr<Sampler>;

    //! \brief Constructor's only job is to call init.
    //! \param[in] smp a sampler to generate random numbers (may be nullptr)
    //! \param[in] grid a grid contect to compute coordinate transforms
    blob(Sampler_p smp, G_p gp)
            : initialdata<Sampler, G>(smp, gp) {
    }

    //! \brief Sets the parameters up
    virtual void init(void) {
        super::init();

        if ( sphinx::options.has("InitialData.lambda") )
            lambda = sphinx::options.as<double>("InitialData.lambda");
        if ( sphinx::options.has("InitialData.magnitude") )
            magnitude = sphinx::options.as<double>("InitialData.magnitude");
    }

    //! \brief Evaluation operator for data with 1-dof.
    //! Makes a vorticity blob. Disabled if f != 1.
    //! \param[in] a coordinate to which evaluate the initial data.
    //! \return value of initial data at a.
    void evalat(const coord_t<2> & coord, data_t<1>& b) const {
        b = {
            magnitude *
            std::pow(coord[0]*(1.-coord[0])*coord[1]*(1.-coord[1]), lambda)
        };
    }

    //! \brief Evaluation operator for data with 2-dof.
    //! Makes a blob for each dof. Disabled if f != 2.
    //! \param[in] a coordinate to which evaluate the initial data.
    //! \return value of initial data at a.
    void evalat(const coord_t<2> & coord, data_t<2> & b) const {
        b =  {
            magnitude *
            std::pow(coord[0]*(1.-coord[0])*coord[1]*(1.-coord[1]), lambda),
            magnitude *
            std::pow(coord[0]*(1.-coord[0])*coord[1]*(1.-coord[1]), lambda)
        };
    }
    void evalat(const coord_t<3> &, data_t<3> &) const {
        throw NotImplementedException();
    }


private:
    //! A coefficient making the data behave like
    //! \f$ abs(x^2 - 1)^\lambda \f$.
    double lambda = 1;

    //! Scale the size of the initial data, this increases the speed
    //! of the flow
    double magnitude = 100.;
public:
    virtual bool indexed() const { return false; }
};

} // END namespace Data
} // END namespace sphinx
