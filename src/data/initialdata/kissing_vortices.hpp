/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "initialdata.hpp"

#include "utilities/options.hpp"

namespace sphinx {
namespace Data {

/**
 * @brief normsq Utility to compute l-2 norm of an array.
 *
 * Use SFINAE to unroll the loop.
 * @tparam
 * @tparam
 * @return
 */
template <class T, long unsigned dim, unsigned int depth,
          typename std::enable_if<depth == 0, bool>::type = true>
T normsq(const std::array<T, dim> &) {
    return 0;
}

template <class T, long unsigned dim, unsigned int depth = dim,
          typename std::enable_if<depth != 0, bool>::type = true>
T normsq(const std::array<T, dim> & arr) {
    return normsq<T, dim, depth - 1>(arr) + arr[depth-1]*arr[depth-1];
}

template <class T, long unsigned dim>
T norm(const std::array<T, dim> &arr) {
    return std::sqrt(normsq(arr));
}

template <class T, long unsigned dim>
inline std::array<T, dim> operator-(const std::array<T, dim> & a,
                                    const std::array<T, dim> & b) {
    std::array<T, dim> ret;
    for(unsigned int i = 0; i < dim; ++i) {
        ret[i] = a[i] - b[i];
    }
    return ret;
}


template <class T, long unsigned dim>
inline std::array<T, dim>& operator+=(std::array<T, dim> & a,
                                      const std::array<T, dim> & b) {
    for(unsigned int i = 0; i < dim; ++i) {
        a[i] += b[i];
    }
    return a;
}

template <unsigned int dim>
struct polar_coord_t { };

template <>
struct polar_coord_t<3> {
    real_t r, phi, omega;
};

template <>
struct polar_coord_t<2> {
    real_t r, phi;
};

enum class Reflection {
    Horizontal, Vertical, DUMMY, None, HorizontalAndVertical
};

template <unsigned int dim>
struct Core {
    coord_t<dim> center;
    real_t radius;
    Reflection flip;
};


inline polar_coord_t<2> cart2polar(coord_t<2> coord, Reflection refl) {
    switch(refl) {
    default:
    case Reflection::None:
        return {norm(coord), std::atan2(coord.at(0), coord.at(1))};
    case Reflection::Horizontal:
        return {norm(coord), std::atan2(-coord.at(0), coord.at(1))};
    case Reflection::Vertical:
        return {norm(coord), std::atan2(coord.at(0), -coord.at(1))};
    case Reflection::HorizontalAndVertical:
        return {norm(coord), std::atan2(-coord.at(0), -coord.at(1))};
    }
}


template <class Sampler, class G>
class kissing_vortices
        : public virtual initialdata<Sampler, G> {

    using super = initialdata<Sampler, G>;
public:
    using G_p = std::shared_ptr<G>;
    using Sampler_p = std::shared_ptr<Sampler>;

    /**
     * @brief Grabs option from command line to setup parameters.
     * 
     * @param smp p_smp:...
     * @param gp p_gp:...
     */
    kissing_vortices(Sampler_p smp, G_p gp)
        : initialdata<Sampler, G>(smp, gp) {
        }

    ~kissing_vortices() {}

    void init(void) {
        super::init();

        if ( sphinx::options.has("InitialData.rho") )
            rho = sphinx::options.as<double>("InitialData.rho");

        if( rho == 0 ) discontinuous = true;
    }

    const std::array<Core<2>,2> core = std::array<Core<2>,2> {
        Core<2>{ coord_t<2>{0.333333, 0.5}, 0.166666, Reflection::None },
        Core<2>{ coord_t<2>{0.666666, 0.5}, 0.166666, Reflection::Horizontal }
    };


    std::array<real_t, 2> orthogonalize(std::array<real_t, 2> array) const {
        return {-array[1], array[0]};
    }

    //!
    //! \brief evalat returns the value of the velocity for the Kissing vortex model.
    //!
    //! The velocity is integrated analytically from the vorticity expression, and
    //! is derived from the core member.
    //! \param coord
    //! \param val
    //!
    void evalat(const coord_t<2> & coord, data_t<2> & val) const {
        set(val, 0);
        for(unsigned int i = 0; i < core.size(); ++i) {
            polar_coord_t<2> polar = cart2polar(coord - core[i].center, core[i].flip);
            polar.r /= core[i].radius;
            if( polar.r >= 0.25 && polar.r <= 0.5 ) {
                real_t magnitude = (polar.r - 0.25) * 2. * M_PI;
                val += orthogonalize(coord - core[i].center) * magnitude;
            } else if ( discontinuous && polar.r >= 0.5 && polar.r <= 1) {
                real_t magnitude = M_PI / 2;
                val += orthogonalize(coord - core[i].center) * magnitude;
            } else if ( !discontinuous && polar.r >= 0.5) {
                real_t magnitude = M_PI / 2 * (tanh((-polar.r + 1) / rho ) + 1) * 0.5;
                val += orthogonalize(coord - core[i].center) * magnitude;
            }

        }
//        val[1] *= -1;
    }


    /**
     * @brief evalat
     *
     * @param  empty paramter
     *
     * @param  empty paramter
     *
     */
    void evalat(const coord_t<3> &, data_t<3> &) const {
        assert(false && "3D vortex core is not implemented!");
    }

private:
    //! Is the sheet discontinuous (avoid division by 0)
    bool discontinuous = false;

    //! Smoothness parameter
    double rho = 0.05;
public:
    virtual bool indexed() const { return false; };
};

} // END namespace Data
} // END namespace sphinx
