/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "sphinx.hpp"
#include "utilities/performance.hpp"

#include "timestepping/ssprk3.hpp"
#include "timestepping/ssprk2.hpp"
#include "timestepping/fe.hpp"
#include "timestepping/stepper.hpp"

#include "utilities/debug.hpp"

#include "prime_seeker.hpp"

//! \file Implements the class genericSolver, providing an implementation of a generic spectral solver.

//! \namespace solver Contains generic solvers used in
//! bigger contexts (convergence study, MC/MLMC, ...)
//!
namespace solver {

//! \brief Implements a generic spectral solver
//!
//! Needs to be configured with a grid, a sampler, and an id (name is optionsl)
//!
//! \tparam Sampler type for the sampler
//! \tparam G type for the grid
template <class Sampler, class G>
class genericSolver {
public:
    //! Smart pointer to grid
    using G_p = std::shared_ptr<G>;
    //! Smart pointer to sampler
    using Sampler_p = std::shared_ptr<Sampler>;
    //!
    using InitialData_p = std::shared_ptr<InitialData<dim>>;

    //! \brief Constructor initializes solution and grabs shared pointers.
    //!
    //!
    genericSolver(Sampler_p smp,
                  G_p grid,
                  InitialData_p data,
                  unsigned int id, std::string name)
     : smp(smp), gr(grid), data(data), id(id), name(name)
     , output_on_push(sphinx::options.has("Save.individual") &&
                      sphinx::options.as<bool>("Save.individual")
                      )
     , sol_p(new io::Solution<G, Snpshot>(gr, name + "_" + std::to_string(id),
                                          /* store_components */ true,
                                          output_on_push)) {

    }

    //! \brief Starts the actual simulations
    //!
    //!
    //!
    void start(void) {
        std::stringstream ss;
        ss << "Simulation with id " << id
           << " will now be initiated, please stand by...";
        debug::message(ss.str(), gr->comm);

        profiler::timestamp("Solver_" + std::to_string(id) + "::start");

        // Defines options for FFT
        Set<1> data_type = Interlaced | R2CFFT |
                DestroyInput | DoublePrecision;
        FFTType fft_type =  DeviceTimeVector::fft_type;
        if( sphinx::options.has("fft_inplace") &&
                sphinx::options.as<bool>("fft_inplace") ) {
             data_type |= InPlace;
        }

        double padding = 3./2.;
        if( sphinx::options.has("padding") )
            padding =
                    sphinx::options.as<double>("padding");
        auto pad = gr->timeN*padding;
        if( sphinx::options.has("autopad")) {
            double ratio;
            const long long MAX_NUM = 20000;
            std::vector<long long int> primes =
                    sphinx::options.as<std::vector<long long int>>("autopad");
            std::vector<long long int> min_exp = {1};

            auto numbers = sphinx::extra::primes::find_all_numbers(
                    primes, MAX_NUM, min_exp, true, true, false, true);

            int I = 0;
            for(auto& n: pad) {
                int oldn = n, copyn = n;
                n = sphinx::extra::primes::find_closest_good_number(
                        (long long int) n,
                        numbers, primes, false, ratio, 1, false);

                std::stringstream ss2;
                if(ratio > MAX_PADDING_RATIO) {
                    ss2 << "Auto-padding would generate a too "
                            "large padding, that will likely not be more efficient"
                        << " (new number of modes = " << n << ", old number = "
                        << copyn << ", ratio = " << ratio << ").";
                    n = copyn;
                } else {
                    ss2 << "Auto-padding set padding to " << n
                        << " from " << oldn << " (" << ratio
                        << "% of difference) for dimension " << I++ << ", primes: ";
                    for (auto prime: primes) {
                        ss2 << prime << ", ";
                    }
                }
                debug::message(ss2.str(), sphinx::mpi::world);
            }
        }
        // Setups bigger grid for aliasing
        std::shared_ptr<G> gr_pad(new G(pad,
                                        fft_type,
                                        data_type, false));
        gr_pad->setup(gr->comm);

        // Setups working vectors
        DeviceTimeVector U(gr_pad, dim),
                w(gr_pad, curl_dim(dim));

        DeviceFreqVector Uhat(gr_pad, dim),
                Vhat(gr_pad, dim),
                what(gr_pad, curl_dim(dim));
        U.name = "U";
        Uhat.name = "Uhat";
        Vhat.name = "Vhat";
        w.name = "w";
        what.name = "what";

        // Vectors for output (small grid)
        HostDeviceVector<Vector,DeviceTimeVector> BUs, Bws;
        std::shared_ptr<DeviceTimeVector> ws(
                    new DeviceTimeVector(gr,
                                         curl_dim(dim)));
        std::shared_ptr<DeviceTimeVector> Us(
                    new DeviceTimeVector(gr, dim));
        Us->name = "Usmall";
        ws->name = "wsmall";

        if (data->only_vorticity()
            || sphinx::options.as<bool>("InitialData.force_vorticity")) {
            // EVAL VORTICITY
            Bws.setDevice(ws);
        }
        BUs.setDevice(Us);

        // Troughout the sampler we have a toggle for
        // detecting if we force the vorticity as IV
        DeviceFreqVector Uhats(gr, dim), whats(gr, curl_dim(dim));

        // Copy some option to context
        int threshold = std::sqrt(gr_pad->timeN[0]);
        double eps = 1./20.; //4. / (gr_pad->timeN[0]);
        // This option is really sensible
        int aliasing = gr_pad->timeN[0] / 3;
//        std::cout << "Aliasing: " << aliasing << std::endl;
        if( sphinx::options.has("threshold") )
            threshold = sphinx::options.as<int>("threshold");
        if( sphinx::options.has("eps") )
            eps = sphinx::options.as<double>("eps");
        if( sphinx::options.has("aliasing") )
            aliasing = sphinx::options.as<int>("aliasing");
        double cfl = 0.5;
        if( sphinx::options.has("cfl") )
            cfl = sphinx::options.as<double>("cfl");

        bool do_initial_cleansing = true;
        if( sphinx::options.has("do_initial_cleansing") ) {
            do_initial_cleansing =
                    sphinx::options.as<bool>("do_initial_cleansing");
        }
        if( do_initial_cleansing && !data->need_cleansing ) {
            debug::warning("Initial data doesn't require initial"
                           " cleansing but you did that!", mpi::world);
        } else if(!do_initial_cleansing && data->need_cleansing) {
            debug::warning("Initial data requires initial cleansing but "
                           "you didn't select the option.", mpi::world);
        }

        // Creates evolution context
        std::shared_ptr<Evo> evo_p =
                std::shared_ptr<Evo>(
                        new Evo(gr_pad, sol_p,
                                U, threshold, eps, aliasing, cfl
                        )
                );

        // Create and plan fft
        fft_s = std::shared_ptr<FFTt>(new FFTt(gr));

        if(sphinx::options.has("FFT.max_time")) {
            fft_s->set_planning_time(
                        sphinx::options.as<double>("FFT.max_time")
                        );
        }
        if(sphinx::options.has("FFT.load_wisdom")
                && sphinx::options.as<bool>("FFT.load_wisdom")) {
            fft_s->load_wisdom();
        }
        if(sphinx::options.has("FFT.mode")) {
            fft_s->set_planning_mode(
                            sphinx::options.as<std::string>("FFT.mode")
                        );
        }

        fft_s->plan(*BUs.getWriteDevice(), Uhats);
        fft_s->plan(*ws, whats);

        if(sphinx::options.has("FFT.save_wisdom")
                && sphinx::options.as<bool>("FFT.save_wisdom")) {
            fft_s->save_wisdom();
        }

        data->init();
        // Fill initial vector
        if (data->only_vorticity()
            || sphinx::options.as<bool>("InitialData.force_vorticity")) {
            // EVAL VORTICITY
            eval<curl_dim(dim)>(*Bws.getWriteHost(), *data);
            fft_s->forward(*Bws.getReadDevice(), whats);
            anti_curl(whats, Uhats);
        } else {
            // EVAL VELOCITY
            eval<dim>(*BUs.getWriteHost(), *data);
            fft_s->forward(*BUs.getReadDevice(), Uhats);
        }

        // Do initial cleansing if needed
        if(do_initial_cleansing) {
            projection(Uhats, Uhats);
        }

        // Setup forcing in Evo if needed.s
        if( data->has_forcing() && sphinx::options.has("use_forcing")) {
            evo_p->set_forcing(
                    [this] (const coord_t<dim> & coord, double t) -> data_t<dim> {
                        return data->forcing(coord, t);
                    }
            );

//                    std::bind(static_cast<data_t<2>(InitialData<dim>::*)(const pos_t<2> &, double)>(&InitialData<dim>::forcing), data.get(), _1, _2));
        }

        zero_padding(Uhats, Uhat);

#if FREQEVO == false
        evo_p->fft.backward(Uhat, U); // Remove if EVOFREQ
#endif // FREQEVO == false

#if FREQEVO == true
        auto & Uinit = Uhat; // Remove if EVOTIME
#else
        auto & Uinit = U; // Remove if EVOFREQ
#endif // FREQEVO == true

        unsigned int frame = 0;

        auto snpF = [&U, &w, &what,
                &Uhat, this, &frame, &aliasing,
                evo_p, &BUs, &Uhats, &ws, &whats]
                (EvolutionVector & U_, double t, bool dump_xdmf = false)
                -> void {

#if FREQEVO == false
            evo_p->fft.forward(U_, Uhat); // Remove if EVOFREQ
            truncate(Uhat, Uhats); // Remove if EVOFREQ
#endif // FREQEVO == false

            ///// NEW PART
#if FREQEVO == true
            truncate(U_, Uhats); // Remove if EVOTIME
#endif // FREQEVO == true
            curl(Uhats, whats);
            fft_s->backward(Uhats, *BUs.getWriteDevice());
            fft_s->backward(whats, *ws);
            normalize(*BUs.getReadWriteDevice());
            normalize(*ws);

            std::shared_ptr<Snapshot<Vector>>
                    snp = sol_p->newSnapshot(t, frame);
            snp->pushData(*BUs.getReadHost(), range<dim>());
            snp->pushData(*ws, range<curl_dim(dim)>());

            if( output_on_push ) snp->writeData();
            frame++;

            if(dump_xdmf) {
                sol_p->reset();
                sol_p->writeXml(false, "_f" + std::to_string(frame - 1), true);
            }
        };

        auto cflF = [this, evo_p]
                (EvolutionVector & V, double h, double & sup)
                -> double {
            return evo_p->cfl(V, h, sup);
        };

        auto Step = ssp_rk3_step<EvolutionVector, decltype(*evo_p)>;

        profiler::timestamp("Solver_" + std::to_string(id) + "::stepper_start");
        stepper<EvolutionVector>(Uinit, Step,
                                 *evo_p, snpF, cflF, gr_pad->comm);
        profiler::timestamp("Solver_" + std::to_string(id) + "::stepper_end");


        if( sphinx::options.has("Save.individual") &&
                sphinx::options.as<bool>("Save.individual") ) {
            sol_p->reset();
            sol_p->writeXml();
        }
        profiler::timestamp("Solver_" + std::to_string(id) + "::end");

    }
private:
    //! Handler for the solver provided to the solver
    Sampler_p smp;

    //! Handler for the (smaller) grid managed by the solver
    G_p gr;

    //! Handler for intial data
    std::shared_ptr<InitialData<dim>> data;

    //! Unique identifier for this simulation
    unsigned int id;

    //! A name for this simulation
    std::string name;

    //! When a snapshot is pushed back to the solution, choose if output or not
    bool output_on_push = true;

public:
    //! Save the smaller FFT for later use (FIXME: remove, use vector instead)
    std::shared_ptr<FFTt> fft_s;
    //! Stores all the data
    std::shared_ptr<io::Solution<G, Snpshot>> sol_p;

    double MAX_PADDING_RATIO = 0.1;
};

}
