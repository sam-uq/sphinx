/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

//! \file test_config.hpp Contains includes, aliases and definitions that simplify the work in the main application.
//!
//!

#define NORM_TOL 10e-6

#include "config.hpp"

#include "sphinx_internal.hpp"

#include "utilities/debug.hpp"

#include "data/initialdata/all_initialdata.hpp"

#include "evolution/time_evo.hpp"
#include "evolution/freq_evo.hpp"

#include "containers/fftw_time_vec.hpp"

#if USE_CUDA
#   include "containers/cuda_device_time_vec.hpp"
#endif // USE_CUDA

#include "containers/host_device_vector.hpp"

#if USE_CUDA
#   if USE_ACCFFT
#  include "../operators/acc_fft.hpp"
#   else // USE_CUDAFFT
#       include "operators/fft/cuda_fft.hpp"
#   endif // END NO USE_ACCFFT
#endif // USE_CUDA

#if myUSE_MKL
#include "operators/mkl_fft.hpp"
#else // NO myUSE_MKL
#   include "operators/fft/fftw_fft.hpp"
#endif // myUSE_MKL

#include "io/solution.hpp"
#include "io/snapshot.hpp"

#if USE_CUDA
#   include "operators/device_functions/device_operators.hpp"
#   include "operators/device_functions/device_overloaded_operators.hpp"
#   include "operators/device_functions/device_structure.hpp"
#endif // END USE CUDA
#include "operators/host_functions/host_structure.hpp"

#include "operators/host_functions/host_structure.hpp"
#include "operators/host_functions/host_operators.hpp"
#include "operators/host_functions/host_overloaded_operators.hpp"

#include "sampler/sampler.hpp"
#include "sampler/engine_well512a.hpp"

//!< Use data in a contiguous memory chunk (may not work)
constexpr bool use_contiguous = false;
//!< Use data in a interlaced memory layout (may not work)
constexpr bool use_interlaced = false;

const unsigned int dim = NDIMS;
#if USE_CUDA
using Vector = FFTWTimeVec<dim, use_contiguous, use_interlaced>;
using HostFreqVector = FFTWFreqVec<dim, use_contiguous, use_interlaced>;
using DeviceTimeVector = CUDADeviceTimeVec<dim, use_contiguous, use_interlaced>;
using DeviceFreqVector = CUDADeviceFreqVec<dim, use_contiguous, use_interlaced>;
#   if USE_ACCFFT
using FFTt = operators::ACCFFT<dim, use_contiguous, use_interlaced>;
#   else // NO USE_ACCFFT
using FFTt = operators::CUDAFFT<dim, use_contiguous, use_interlaced>;
#   endif // USE_ACCFFT
#else // NO USE_CUDA
using Vector = FFTWTimeVec<dim, use_contiguous, use_interlaced>;
using HostFreqVector = FFTWFreqVec<dim, use_contiguous, use_interlaced>;
using DeviceTimeVector = FFTWTimeVec<dim, use_contiguous, use_interlaced>;
using DeviceFreqVector = FFTWFreqVec<dim, use_contiguous, use_interlaced>;
# if myUSE_MKL
using FFTt = operators::MKLFFT<dim, use_contiguous, use_interlaced>;
# else // NO myUSE_MKL
using FFTt = operators::FFTWFFT<dim, use_contiguous, use_interlaced>;
# endif // NO myUSE_MKL
#endif // NO USE_CUDA

using Grd = Grid<dim, use_contiguous, use_interlaced>;
using Snpshot = Snapshot<Vector>;
using Solution = sphinx::io::Solution<Grd, Snpshot>;
using Solution_p = std::shared_ptr<Solution>;

#if FREQEVO == true
using EvolutionVector = DeviceFreqVector;
using Evo = evolution::freqEvo<Solution, Grd, FFTt, DeviceTimeVector, DeviceFreqVector>;
#else
using EvolutionVector = DeviceTimeVector;
using Evo = evolution::timeEvo<Solution, Grd, FFTt, DeviceTimeVector, DeviceFreqVector>;
#endif // FREQEVO

#if RNG_ENGINE == 0
using EngineZ = sphinx::sampler::engine::well512a;
#elif RNG_ENGINE == 1
using EngineZ = std::default_random_engine;
#endif // RNG_ENGINE
using SamplerZ = sphinx::Sampler<1, 2, EngineZ>;

template <unsigned int dofs>
using InitialData = sphinx::Data::initialdata<SamplerZ, Grd>;

//! \namespace
//!
//!
namespace sphinx {

extern unsigned int omp_threads;

//! \brief Startup MPI, FFTW (THREADS and MPI), CUDA and profiler
int init(int argc, char **argv);

//! \brief Shut down profiler, terminate FFTW (order: threads, MPI, serial)
//!
//! \param ret  ErrorCode until now.
//! \return     ErrorCode
int terminate(int ret);

}
