/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once
//
// Created by filippo on 03/10/17.
//

#include <limits>

#define THROW_ON_FAILED_TEST false

namespace sphinx {
//! \brief
namespace testunit {

//!
//! \return
inline int success() {
    return EXIT_SUCCESS;
}

//!
//! \param pass
//! \return
inline int report(bool pass, std::string msg = "") {
    if (pass) {
        debug::test_message(sphinx::mpi::world, "Test completed successfully!");
        debug::test_message(sphinx::mpi::world, msg);
        return success();
    } else {
        debug::test_message(sphinx::mpi::world, "Test failed!");
        debug::test_message(sphinx::mpi::world, msg);
        if (THROW_ON_FAILED_TEST) {
            throw new TestFailureException(msg);
        }
        return EXIT_FAILURE;
    }
}

double constexpr tolerance() {
        return 10 * std::numeric_limits<scalar_t>::epsilon();
};

} // END namespace testunit
} // END namespace sphinx