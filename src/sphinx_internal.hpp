/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include <string>

#include "config.hpp"

namespace sphinx {

//! \namespace sphinx Main namepace enclosing all the structures/functions/routines (TODO).

//! Timestamp shared by all processes
extern std::string timestamp;
extern std::string name;

unsigned int master_seed();

#if HAVE_MPI
// MPI Tags bases
const unsigned int padding_tag_left = 10001;
const unsigned int padding_tag_right = 20001;
const unsigned int fix_tag = 30001;
const unsigned int remote_grid_tag = 40001;
const unsigned int send_vec_tag = 50001;
const unsigned int ghost_tag = 60001;
// MPI subtag
const unsigned int mean_subtag = 1001;
const unsigned int var_subtag = 2001;

inline unsigned int generate_tag(unsigned int master_tag,
                                 unsigned int component,
                                 unsigned int frame) {
    return master_tag + 100*component + 10*frame;
}
#endif

} // END: namespace sphinx
