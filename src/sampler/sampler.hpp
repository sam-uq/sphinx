/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

//
// Created by filippo on 01/01/14.
//

#include <functional>
#include <array>
#include <vector>
#include <random>

#include "utilities/mpi.hpp"

namespace sphinx {

using master_generator = std::default_random_engine;
using master_disribution = std::uniform_int_distribution<int>;

using default_generator = std::default_random_engine;
using default_distribution = std::uniform_real_distribution<double>;

//! \brief This class represents a generic RNG, which is tied to specific simulations in order to ensure reproducibility.
//!
//! This class is also exposed to LUA and Python and to all the initial data types.
//!
//! \tparam nstreams
//! \tparam ndims
//! \tparam engine
//! \tparam distribution
template <unsigned int nstreams, unsigned int ndims,
        class engine = default_generator, class distribution = default_distribution>
class Sampler {
public:
    Sampler() : mD(0, RAND_MAX), D(-1., 1.) {}

    //! \brief Initialize the random number generator and syncronise all ranks.
    //!
    //! Tells the sampler to syncronyse amongst ranks and to initialize its internal data.
    //!
    //! This must be called (at least), and must be called before any other method.
    //!
    //! \param master_seed
    void sync(int master_seed = -1) {
//        dice = std::bind(E, D);

        mpiErrchk(MPI_Bcast(&master_seed, 1, MPI_INTEGER, 0, sphinx::mpi::world.comm));

        std::stringstream ss;
        ss << "Initializing sampler with master seed " << master_seed;
        debug::message(ss.str(), sphinx::mpi::world);

        this->master_seed = master_seed;
        mG.seed((unsigned int) master_seed);
        counter = 0;
        selected_dim = 0;
        selected_stream = 0;
    }

    int get_seed(int idx) {
        std::size_t size = seeds.size();
        if ((int) size <= idx) {
            seeds.resize(idx + 1);
            mG.seed((unsigned int) master_seed);
            for(int i = 0; i < idx + 1; ++i) {
                seeds.at(i) = mD(mG);
            }
        }
        return seeds.at(idx);
    }

    void set_idx(int new_idx) {
        initialized = true;
        counter = 0;
        selected_dim = 0;
        selected_stream = 0;

        current_index = new_idx;
        current_seed = get_seed(current_index);
        E.seed(current_seed);

        generate();

        std::stringstream ss;
        ss << "Changing sampler index, to " << new_idx << ", new seed: " << current_seed;
        debug::status(ss.str(), sphinx::mpi::world, sphinx::mpi::Policy::Self);
    }

    double operator()(unsigned int stream,
                      unsigned int dim, int node) const {
        assert(stream < nstreams && dim < ndims && "Too many streams or dimensions!");
        if(!initialized) {
            std::stringstream ss;
            ss << "Requesting random generation, but seed was NOT initialized!!!!"
                    << " stream = " << stream
                    << " dim = " << dim
                    << " node = " << node;
            debug::warning(ss.str(), sphinx::mpi::world, sphinx::mpi::Policy::Self);
        }
        auto& sd = entropy[stream][dim];
        int size = sd.size();
        assert(node < size && "Did not reserve enough space!");
//        if( size <= node ) {
//            for(auto& ens: entropy) {
//                for(auto& ensd: ens) {
//                    ensd.resize(node+1);
//                    for(int d = size; d < node+1;++d) {
//                        ensd.at(d) = D(E);
//                    }
//                }
//            }
//        }
        return entropy.at(stream).at(dim).at(node);
    }

    void reserve(int nnodes) {
        auto& sd = entropy[0][0];
        int size = sd.size();
        if( size <= nnodes ) {
            for(auto& ens: entropy) {
                for(auto& ensd: ens) {
                    ensd.resize(nnodes);
                }
            }
        }
        // Reset seed!
        current_seed = get_seed(current_index);
        E.seed(current_seed);
        generate();
    }

    void generate() {
        assert(current_index != -1
               && "Sampler must be initialized before generating!");
        for(auto& ens: entropy) {
            for(auto& ensd: ens) {
                for(auto& ensda: ensd) {
                    ensda = D(E);
                }
            }
        }
    }

    //! \brief Set the stream and dimension to be used when calling next()
    //! \param stream
    //! \param dim
    void select_stream(int stream, int dim) {
        selected_stream = stream;
        selected_dim = dim;
        counter = 0;
    }

    //! Hack for LUA exposure
    //! \return
    double next_nonconst() {
        return this->next();
    }

    //! Grab next number, as internally stored.
    //! \return
    double next() const {
        return (*this)(selected_stream, selected_dim, counter++);
    }

//private:
    int current_index = -1, current_seed = -1;

private:
    //! True whenever the seed has ee initialized (called set_idx)
    bool initialized = false;
    //! The seed shared globally among all ranks, providing seed for simulations
    int master_seed = -1;

    std::vector<int> seeds;

    // Pre-generated random numbers
    std::array<std::array<std::vector<double>, ndims>, nstreams> entropy;

    // Distribution to generate seeds
    master_disribution mD;
    // Engine to generate seeds
    master_generator mG;

    // Any RNG engine
    engine E;
    // Any RNG distribution
    distribution D;

    // Internal counter
    mutable int counter = 0;
    mutable int selected_stream = 0;
    mutable int selected_dim = 0;
};

} // END namespace sphinx
