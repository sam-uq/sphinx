/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include <array>
#include <memory>

#include "config.hpp"

#include "mc.hpp"

#include "../utilities/mpi.hpp"

#include "../geometry/remote_grid.hpp"
#include "../geometry/grid.hpp"

//! \file mlmc.hpp Contains implementation of class template MLMC.

namespace sphinx {

//! \namespace Integration contains all classes and
//! routines relatives to statistical integration.
namespace Integration {

//! From the size of the space grid, we estimate
//! the work needed for a single simulation.
//! \tparam Array
//! \param N
//! \return
template <class Array>
double get_space_work(const Array & N) {
    double pN = prod(N);
    return pN*std::log(pN);
}

template <class Array>
double get_time_work(const Array & N) {
    double pN = N[0]; // TODO: max
    return pN;
}

//!
//! \brief get_simulations_n returns the number of simulation
//! of a certain level
//! \param base_sims is the number of samples on the finest level
//! \param level is the level for which you want to get the
//! number of simulations
//! \param nlevels is the total number of levels of the
//! entire MLMC simulation
//! \return the (TODO: optimal) simulations that is selected for the level
//!
inline int get_simulations_n(int base_sims, int level, int nlevels) {
    int alpha = 1; // this should depend on the convergence of the method
    return base_sims * std::pow(2,alpha * (nlevels-level-1));
}

//! \brief Class handling running and collection of Integration::MC simulations needed for MLMC approximations.
//! This class allocates rank to levels is a suitable manner for parallel MLMC, creates and runs the correct
//! MC simulations and collects all the data on the domain containing the root of the finest simulation.
//! \tparam Sampler Type of the sampler used to generate random numbers.
//! \tparam d Dimension (used in grid, etc.)
template <class Sampler, unsigned int d>
class MLMC {
public:
    //! Underlying phase space dimension
    static constexpr unsigned int dim  = d;
    //! Maximal number of levels allowed in MLMC simulations
    static constexpr unsigned int max_levels = 20;

    using MC_ = MC<Sampler, dim>;
    using MC_p = std::shared_ptr<MC_>;

    using Sampler_p = std::shared_ptr<Sampler>;

    using Grid_p = std::shared_ptr<Grid<dim, use_contiguous, use_interlaced>>;
    using RemoteGrid_p = std::shared_ptr<RemoteGrid<dim>>;

    //! \brief Setups the MLMC framework by splitting the communicator and sharing the grids amongs processes
    //! \param[in] base_sims Number of simulations for the coarsest level
    MLMC(unsigned int base_sims) {

        // Gather options for MLMC
        std::array<std::ptrdiff_t, dim> N;
        std::copy_n(
                    sphinx::options.as<std::vector<int>>("mesh")
                    .begin(), dim, N.begin()
                    );

        if( sphinx::options.has("levels") ) {
             nlevels = sphinx::options.as<unsigned int>("levels");
        }

        // Reserve size for containers
        nsimulations.resize(nlevels);
        work_per_level.resize(nlevels);
        ranks.resize(nlevels);
        start_rank_in_parent.resize(nlevels);

        // Safety/Sanity checks
        assert(base_sims > 0 &&
               "You must provide a positive number of base simulations");
        assert(nlevels >= 1 && nlevels < max_levels &&
               "Either you have too few or to many levels planned.");
        assert(nlevels <= (unsigned int) sphinx::mpi::world.size &&
               "Currently levels can only be parallel.");

        // Find out the number of simulations for each level and max of simulations for each level
        for(unsigned int l = 0; l < nlevels; ++l) {
            nsimulations.at(l) = get_simulations_n(base_sims, l, nlevels);
            // FIXME: Change?
        }
        int max_sim_num = nsimulations.front();

        // Approximate the expected work per level
        for(unsigned int l = 0; l < nlevels; ++l) {
            double space_work = (l == 0 ? get_space_work(N) :
                                          get_space_work(N * std::pow(2,l))+get_space_work(N * std::pow(2,l+1)));
            double time_work = (l == 0 ? get_time_work(N) :
                                         get_time_work(N * std::pow(2,l)) + get_time_work(N * std::pow(2,l+1)));
            work_per_level.at(l) = nsimulations.at(l) * space_work * time_work;
            tot_work += work_per_level.at(l);
        }

        // Try and assign ranks to levels base on work
        int ranks_left = mpi::world.size;
        for(unsigned int l = 0; l < nlevels; ++l) {
            double work_per_level_perc_at_l = (double) work_per_level.at(l) / tot_work;
            ranks.at(l) = std::max(1, (int) std::floor(work_per_level_perc_at_l*mpi::world.size));
            ranks_left -= ranks.at(l);
        }

        // Balance the above assignment of ranks, making sure assignment is feasible
        while( ranks_left != 0 ) {
            // WARNING: DO NOT DO UNSIGNED L!!!
            for(int l = nlevels - 1; ranks_left != 0 && l >= 0; --l) {
                if( ranks_left > 0 ) {
                    ranks.at(l) += 1;
                    ranks_left -= 1;
                } else {
                    if( ranks.at(l) > 1 ) {
                        ranks.at(l) -= 1;
                        ranks_left += 1;
                    }
                    // FIXME: What if wont work?
                }
            }
        }

        // Group ranks toghether based on level
        start_rank_in_parent.at(0) = 0;
        for(unsigned int l = 0; l < nlevels; ++l) {
            if(l > 0) {
                start_rank_in_parent.at(l) = ranks.at(l-1) + start_rank_in_parent.at(l-1);
            }
        }

        // Find out level of this rankG
        level = 0;
        for(unsigned int l = 0; l < nlevels; ++l) {
            if( mpi::world.rank < start_rank_in_parent.at(l) ) {
                level = l-1;
                break;
            }
        }
        if( mpi::world.rank >= start_rank_in_parent.back() ) { // Fix, for when you are in last level
            level = nlevels-1;
        }

        print();
        save_allocaion_info();

        // Find out options for FFT
        // FIXME: deprecate, must find a better way to do that.
        Set<1> data_type = Interlaced | R2CFFT | DestroyInput | DoublePrecision;
        FFTType fft_type = DeviceTimeVector::fft_type;
        if( sphinx::options.has("fft_inplace") && sphinx::options.as<bool>("fft_inplace") ) {
             data_type |= InPlace;
        }

        // Find out dimension of corse grid for this process
        auto baseN = level == 0 ? N : N * std::pow(2,level-1);

        // Create grids and if necessary create finer grid
        grid = Grid_p(new Grid<dim, use_contiguous, use_interlaced>(baseN, fft_type, data_type, false));
        if( level != 0 ) {
            finer_grid = Grid_p(
                        new Grid<dim, use_contiguous, use_interlaced>(baseN*2, fft_type, data_type, false));
        }

        // Create a sampler that will be synced accros all ranks participating in MLMC
        std::shared_ptr<Sampler> smp(new Sampler());
        smp->sync(sphinx::master_seed());

        // TODO: get the minimal amount of parallel ranks from the grid
        int min_ranks = finer_grid->getMinRanks();

        // FIXME: ugly as hell, fix this one
        // Generate a communicator for communication between roots
        mpi::IntraLevelComm IntraLevelComm(MPI_COMM_NULL, -1, -1, "ilc", level, nlevels);
        MPI_Comm_split(mpi::world.comm, level,
                       mpi::world.rank, &IntraLevelComm.comm);
        MPI_Comm_rank(IntraLevelComm.comm, &IntraLevelComm.rank);
        MPI_Comm_size(IntraLevelComm.comm, &IntraLevelComm.size);

        // Generate a communicator for communication from ranches to roots
        mpi::IntraLevelComm InterLevelComm(mpi::world.comm, mpi::world.rank,
                                           mpi::world.size, "inlc", level, nlevels);

        // Assign an ID where the MC simulation will be placed:
        // make sure the id allows a sufficient number of simulations
        id_start =  level*max_sim_num;

        auto data = sphinx::Data::Factory(smp,
                                          grid,
                                          options.as<std::string>("InitialData.name")
                                          );

        // Generate a MC simulation contect for coarse grid
        mc_coarser = MC_p(new MC_(smp, grid, data,
                                  IntraLevelComm, nsimulations.at(level),
                                  sphinx::name + "_coarse_lvl" + std::to_string(level),
                                  id_start, min_ranks));

        // Generate a context for finer MC simulations. If level is 0, let the finer level to be the same
        // as the coarse level
        if( level == 0 ) {
            mc_finer = mc_coarser;
            finer_grid = grid;
        } else {
            mc_finer = MC_p(new MC_(smp, finer_grid, data,
                                    IntraLevelComm, nsimulations.at(level),
                                    sphinx::name + "_fine_lvl" + std::to_string(level),
                                    id_start, min_ranks));
        }

        // Generate communicators for root processes for each level
        for(unsigned int l = 0; l < nlevels; ++l) {
            InterLevelComm.addDomain(mpi::IntraDomainComm(MPI_COMM_NULL,
                                                    level == l ? IntraLevelComm.rank : -1,
                                                    level == l ? mc_coarser->IntraLevelComm.intra_domain_comms.at(0).size : -1, "idc",
                                                    l, nlevels,
                                                    start_rank_in_parent.at(l), IntraLevelComm.size));
        }
        InterLevelComm.handshake();

        // Extra sanity checks
        assert( mc_coarser->is_root == mc_finer->is_root && "Roots of MC do not match!");

        print_after_mc();

        // If rank is not a root, then we are finished here
        if( !mc_coarser->is_root ) return;

        // Setup inter-root communication
        if( level == nlevels -1 ) {
            // If you are a master root, create coarse grids to hold received data
            auto Nx = N;
            for(unsigned int l = 0; l < nlevels-1; ++l) {
                coarse_grids.push_back(Grid_p(
                                          new Grid<dim, use_contiguous, use_interlaced>(Nx, fft_type, data_type, false)));
                coarse_grids.back()->setup(mc_coarser->intraDomComm);
                Nx *= 2;
            }

            // If you are a master root, send you mesh to everybody and receive all the root remote grids
            for(unsigned int l = 0; l < nlevels-1; ++l) {
                coarse_grids.at(l)->send_to(InterLevelComm, l); // NASTY BUG: .at(0) instead of .at(l)!!!

                remote_grids.push_back(std::shared_ptr<RemoteGrid<dim>>(new RemoteGrid<dim>()));
                remote_grids.back()->recv_from(InterLevelComm, l);
                remote_grids.back()->tot_sim = nsimulations.at(l);
            }
        } else {
            // If you are a normal root, send yor grid to the master root after receiving the master grid
            master_grid = std::shared_ptr<RemoteGrid<dim>>(new RemoteGrid<dim>());
            master_grid->recv_from(InterLevelComm, nlevels -1);
            master_grid->tot_sim = nsimulations.at(nlevels -1);

            finer_grid->send_to(InterLevelComm, nlevels -1);
        }
    }

    //! \brief Start all MC simulations, communicate the data ad the end.
    //! Runs the coarse and fine MC simulations, outputs the data if requested, communicate between roots and output
    //! the final result.
    void start(void) {
        mc_coarser->start();

        // All levels start the fine resolution simulation, and collect the result in the latter.
        if( level != 0 ) {
            mc_finer->start();

            if( mc_coarser->is_root ) {
                mc_finer->mav->difference(mc_coarser->mav);

                // Rename and output the difference in the MC simulations
                if( sphinx::options.has("Save.diff") && sphinx::options.as<bool>("Save.diff") ) {
                    mc_finer->mav->mean->prefix = sphinx::name + "_diff_lvl" + std::to_string(level) + "_mean";
                    mc_finer->mav->M2->prefix = sphinx::name + "_diff_lvl" + std::to_string(level) + "_var";
                    mc_finer->mav->write();
                }
            }
        }

        // The job of non-roots is now ended
        if( !mc_coarser->is_root ) return;

        // Root level receives all the data and writes the output
        if( level == nlevels-1 ) {
            mc_finer->mav->mean->reset();
            mc_finer->mav->M2->reset();
            mc_finer->mav->recv_from_levels(remote_grids, coarse_grids);
            mc_finer->mav->mean->prefix = sphinx::name + "_lvl" + std::to_string(level) + "_mean";
            mc_finer->mav->M2->prefix = sphinx::name + "_lvl" + std::to_string(level) + "_var";
            mc_finer->mav->write();
        // Level 0 sens his coarse simulation
        } else if(level == 0) {
            mc_coarser->mav->send_to_root(master_grid);
        // Everybody else sends the difference
        } else if( level != nlevels-1 ) {
            mc_finer->mav->send_to_root(master_grid);
        }
    }

    //! Output some information about the MLMC class
    void print() {

        // Global information
        std::stringstream ss;
        ss << "MLMC ready to start: " << nlevels
           << " levels will be used, splitted across " << mpi::world.size << " ranks. Please stand by...";
        debug::message(ss.str(), mpi::world);


        // Some info for each level
        for(unsigned int l = 0; l < nlevels; ++l) {
            std::stringstream ss_level;
            ss_level << "Level " << l << " of " <<  nlevels << " gets "
                     << nsimulations.at(l) << " simulations using "
                     << ranks.at(l)
                     << " starting at rank " << start_rank_in_parent.at(l)
                     << " (predicted work = " << work_per_level.at(l) << " of " << tot_work << ".";
            debug::verbose(ss_level.str(), mpi::world);
        }
    }
    //! Output some information about the MLMC class after MC has ben prepared
    void print_after_mc() {
        // Information from this rank
        std::stringstream ss_self;
        ss_self << "This rank is assigned to level " << level
                << " and to color " << mc_coarser->intra_domain_color
                << " (root = " << mc_coarser->is_root << ").";
        debug::verbose(ss_self.str(), mpi::world, mpi::Policy::Self);
    }

    template <int, class Array, char vert_sep = '|'>
    void table_push(std::ofstream & file, Array /* widths */) {
        file << vert_sep << "\n";
    }

    template <int offset = 0, char vert_sep = '|', class First, class ...Args>
    void table_push(std::ofstream & file,
               const std::array<int, sizeof...(Args) + 1 + offset> & widths,
               First && first, Args&&... args) {
        file << vert_sep << " "
             << std::setw(widths[offset]) << first
             << " ";
        table_push<offset+1>(file, widths, args...);
    }

    void save_allocaion_info() {
        if(mpi::world.rank == 0) {
            std::array<int, 6> widths{10,10,10,10,10,10};

            std::ofstream file(sphinx::name + ".alloc");
            file << "------------------------------" << std::endl
                 << "--- Level information: -------" << std::endl
                 << "------------------------------" << std::endl;

            table_push(file, widths, "l/L", "m", "R", "R_s", "W", "W_p");
            for(unsigned int l = 0; l < nlevels; ++l) {
                table_push(file, widths,
                           std::to_string(l) + "/" + std::to_string(nlevels),
                           nsimulations.at(l),
                           ranks.at(l), start_rank_in_parent.at(l),
                           work_per_level.at(l), work_per_level.at(l) / (double) tot_work);
            }
            file << "Legend:" << std::endl
                 << " - l/L: level/tot. number of levels" << std::endl
                 << " - m:   number of simulations assigned to this level" << std::endl
                 << " - R:   number of ranks assigned to this level" << std::endl
                 << " - R_s: starting rank index" << std::endl
                 << " - W:   estimated work for level" << std::endl
                 << " - W_p: estimated work for level (%)" << std::endl;
            file << "Total estimated work: " << tot_work << std::endl;
        }
    }

private:
    //! Level associated to this rank
    unsigned int level = -1;
    //! Number of MLMC levels
    unsigned int nlevels = -1;
    //! An unique id providing sufficient space for the MC simulations with the selected amount of simulations
    int id_start;

    // Information for each level
    // TODO: put into a class
    //! Simulations per level
    std::vector<int> nsimulations;
    //! Predicted work per level
    std::vector<double> work_per_level;
    //! Total work predicted (just a number)
    double tot_work = 0;
    //! Assigment of ranks per level
    std::vector<int> ranks;
    //! First rank assigned to level
    std::vector<int> start_rank_in_parent;

    //! Places where the master-root places the data communicated to it.
    //! Empty for non-roots and for non-master-roots
    std::vector<std::shared_ptr<Grid<dim, use_contiguous, use_interlaced>>> coarse_grids;

    //! Remote grids of non-master-roots, may be empty if this is a master-root or non-root
    std::vector<std::shared_ptr<RemoteGrid<dim>>> remote_grids;

    //! A remote grid present at the master root for other roots, may be null if non-root or master-root
    std::shared_ptr<RemoteGrid<dim>> master_grid;

    //! Grids associateds to coarse and finer MC contexts
    std::shared_ptr<Grid<dim, use_contiguous, use_interlaced>> grid, finer_grid;

    //! Coarse MC contect, fine MC context (may be null)
    MC_p mc_coarser, mc_finer;
};

} // END namespace Integration
} // END namespace sphinx
