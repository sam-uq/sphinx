/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include <array>
#include <memory>

#include "config.hpp"

#if HAVE_MPI
#include <mpi.h>
#endif

#include "../utilities/mpi.hpp"

#include "../representation/mav.hpp"

#include "../solver/genericSolver.hpp"

#include "../sampler/sampler.hpp"

#include "../geometry/remote_grid.hpp"
#include "../geometry/grid.hpp"

namespace sphinx {
namespace Integration {

//! \brief Class handling a single level MC simulation.
//! Distributes the work among processors and runs the correct number of simulations.
//! \tparam Sampler Type of the sampler used to generate random numbers.
//! \tparam d Dimension (used in grid, etc.)
template <class Sampler, unsigned int d>
class MC {
public:
    //! Underlying phase space dimension
    static constexpr unsigned int dim  = d;
    using Sampler_p = std::shared_ptr<Sampler>;

    using Grid_ = Grid<dim, use_contiguous, use_interlaced>;
    using RemoteGrid_ = RemoteGrid<dim>;

    using Grid_p = std::shared_ptr<Grid_>;
    using RemoteGrid_p = std::shared_ptr<RemoteGrid_>;

    using Solver_ = solver::genericSolver<Sampler, Grid_>;

    using MaV_ = representation::MaV<Grid_, RemoteGrid_>;
    using MaV_p = std::shared_ptr<MaV_>;

    using InitialData_p = std::shared_ptr<sphinx::Data::initialdata<Sampler, Grid_>>;

    //! \brief Distributes the work among processes assigned to this.
    //!
    MC(Sampler_p smp, Grid_p grid,
       InitialData_p data,
       const mpi::Comm & intra_level_comm,
       int n_sims_tot, std::string prefix,
       int base_id = 0, int min_ranks = 1 )
        : smp(smp), grid(grid)
        , data(data)
        , n_sims_tot(n_sims_tot), prefix(prefix) {

        assert(n_sims_tot > 0 && "Dear user, according to our information, your call to MC requested 0 simulations. "
                                 "This may be due to a insufficient number of simulations of an MC simulation, or to "
                                 "a failed MLMC parallel splitting. Please, consider adressing this problem. "
                                 "Unfortunately we have no idea how to deal with this issue, so we will abort "
                                 "the simulation. Kind regards, S.");

        int level = 0;
        int levels = 0;
        // FIXME: something fishy with the intralevelcomm
        IntraLevelComm = mpi::IntraLevelComm(intra_level_comm.comm,
                                             intra_level_comm.rank, intra_level_comm.size,
                                             "ilc", level, levels);

        // If we need more ranks than provided, complain
        if( min_ranks > IntraLevelComm.size ) {
            debug::warning("Warning: not enough ranks (simulations may be too big)!", IntraLevelComm);
            par_sims = 1;
            intra_domain_color = 0;
            IntraLevelComm.intra_domain_comms.resize(1);
            // Else, try and parallelize as much as you can, assign domains to each rank
        } else {
            par_sims = IntraLevelComm.size / min_ranks; // FIXME
            par_sims = std::min(par_sims, n_sims_tot); // We do not do more parallel
            // Simulations than the number of simulations :)
//            debug::warning("Warning: overkilled parallelism?!", IntraLevelComm);

            // Number of ranks in each IntraDomainComm
            int size = IntraLevelComm.size / par_sims;
            // If size does not split evenly, just increase it until
            // we will the left over ranks
            if(IntraLevelComm.size % par_sims != 0) {
                ++size;
            }
            if( size == 0 || (size == 1 && IntraLevelComm.size % par_sims != 0) ) {
                debug::error("Error: too few simulations! within a intra domain comm.", IntraLevelComm);
                // FIXME: fall back if fails
            }

            int numberOfBigDomains = IntraLevelComm.size % par_sims;

            // Color assigned to IntraDomainComm
            int color = 0;
            // Loop over all domains
            for(int i = 0; i < IntraLevelComm.size; i += size) {
                // Reduce size if we need to use smaller splits
                if(numberOfBigDomains != 0 && i == numberOfBigDomains * size) {
                    --size;
                }

                int rankInDomain = -1;
                // This rank belongs to domain
                if(i <= IntraLevelComm.rank && IntraLevelComm.rank  < i + size) {
                    // Assign a rank in the domain
                    rankInDomain = IntraLevelComm.rank - i;
                    // Assign color to this rank
                    intra_domain_color = color;
                }

                IntraLevelComm.addDomain(
                        mpi::IntraDomainComm(MPI_COMM_NULL, rankInDomain,
                                             size, "idc", color,
                                             par_sims, i, IntraLevelComm.size));
                std::stringstream ss;
                ss << "New domain with rank (in domain)" << rankInDomain
                   << " of " << size << " with color " << color << " of " << par_sims
                   << " with parent rank " << i << " of " << IntraLevelComm.size;
                debug::verbose(ss.str(), IntraLevelComm, mpi::Policy::Self);

                color++;
            }
        }

        // Decide if this rank is a root
        is_root = intra_domain_color == 0;

        // Add domain identifier to the name of the simulation
        prefix += "_ps" + std::to_string(intra_domain_color);

        // FIXME: Cleanup this
        //        IntraLevelComm.print();

        // Create a communicator for these domains
        MPI_Comm intra_domain_comm;
//        std::cout << "Splitting DomainDommunicator color = " << intra_domain_col+or
//                  << "rank_in_level=" << IntraLevelComm.rank << std::endl;
        MPI_Comm_split(IntraLevelComm.comm, intra_domain_color, IntraLevelComm.rank, &intra_domain_comm);

        // Figure out the number of simulations
        n_sims_this = n_sims_tot / par_sims + ((intra_domain_color < n_sims_tot % par_sims) ? 1 : 0);

        std::stringstream ss;
        ss << "This rank is doing "
                  << par_sims << " par. sims " << "(grid min ranks=" << min_ranks
                  << ", color=" << intra_domain_color << ", tot. sim.=" << n_sims_tot
                  << ", sim. this=" << n_sims_this << ")";
        debug::message(ss.str(), sphinx::mpi::world, sphinx::mpi::Policy::Self, debug::componentEnum::mc);

        // Generate a communicator to setup the grid
        auto & thisComm =  IntraLevelComm.intra_domain_comms.at(intra_domain_color);
        thisComm.comm = intra_domain_comm;
        MPI_Comm_rank(intra_domain_comm, &thisComm.rank);
        MPI_Comm_size(intra_domain_comm, &thisComm.size);
        intraDomComm = thisComm;

        grid->setup(intraDomComm);

        // Share grids within level
        if( intraDomComm.color == 0 ) { // if root
            // Root sends his grid to everybody, receives grid from everybody
            for(int col = 1; col < intraDomComm.ncolors; ++col) {
                grid->send_to(IntraLevelComm, col);

                remote_grids.push_back(RemoteGrid_p(new RemoteGrid_()));
                remote_grids.back()->recv_from(IntraLevelComm, col);
                remote_grids.back()->tot_sim = n_sims_tot / intraDomComm.ncolors +
                        ((col < n_sims_tot % intraDomComm.ncolors) ? 1 : 0);
            }
        } else {
            // Branches receive root grid send their grid to root
            remote_grids.push_back(RemoteGrid_p(new RemoteGrid_()));
            remote_grids.back()->recv_from(IntraLevelComm, 0);
            remote_grids.back()->tot_sim = n_sims_tot / intraDomComm.ncolors +
                    ((0 < n_sims_tot % intraDomComm.ncolors) ? 1 : 0);

            grid->send_to(IntraLevelComm, 0);
        }

        // Figure out where the simulation starts for this domain
        sim_id_start = base_id + (n_sims_tot / par_sims + 1)*intra_domain_color;

    }

    //! \brief Creates a representation context and starts the simulations
    //! he needs to do.
    void start(void) {
        mav = MaV_p(new MaV_(grid, prefix));

        // Do as many simulations as requested
        for(int s = 0; s < n_sims_this; ++s) {
            smp->set_idx(sim_id_start + s);
            Solver_ solver(smp, grid, data,
                           sim_id_start + s, prefix);
            solver.start();
            for(auto& snp: solver.sol_p->snapshots_p) {
                mav->pushSnapshot(snp);
            }
        }

        // Receive data from the branches (if you are root)
        mav->collect(intraDomComm, remote_grids);
        // Rescale mean and variance
        mav->finalize();

        // If saving is requested, output your data
        if( sphinx::options.has("Save.mc") && sphinx::options.as<bool>("Save.mc") ) {
            if(intra_domain_color == 0) mav->write();
        }
    }

private:
    //! Grids of branches (if root) of root (if branch)
    std::vector<RemoteGrid_p> remote_grids;

    //! A copy of the sampler
    const Sampler_p smp;
    //! A copy of the grid
    const Grid_p grid;
    //! A copy of the initial data
    InitialData_p data = nullptr;

    //! The simulation id assigned to this domain
    int sim_id_start;

    //! The number of simulations requested in total
    int n_sims_tot;
    //! The number of simulations carried out by this domain
    int n_sims_this;

    //! Number of parallel MC simulations
    int par_sims;

    std::string prefix;
public:
    //! Intra domain color assigned to this
    int intra_domain_color = MPI_UNDEFINED;

    //! Representation of MaV
    MaV_p mav;

    //! Is this rank in a root of MC?
    bool is_root;

    //! An intra level communicator as provided by the parent
    mpi::IntraLevelComm IntraLevelComm;
    //! An intra domain communicator of this processor
    mpi::IntraDomainComm intraDomComm;
};

} // END namespace Integration
} // END namespace sphinx

