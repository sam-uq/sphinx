/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include <memory>

#include "config.hpp"

#include "io/snapshot.hpp"
#include "io/solution.hpp"

#include "containers/fftw_time_vec.hpp"

#include "utilities/mpi.hpp"

//! \file mav.hpp Contains implementation of "mean and variacne" class.

namespace representation {

//! \namepsace representation Namespace containing data representation handlers, such as "mean and variance",
//! "snapshots" (TODO) and "solutions" (TODO).

const unsigned int dim = NDIMS;
using Vector = FFTWTimeVec<dim, use_contiguous, use_interlaced>;

//! \brief
//!
//!
//! \tparam G
//! \tparam RG
template<class G, class RG>
class MaV {
public:
    using G_p = std::shared_ptr<G>;
    using R_p = std::shared_ptr<RG>;
    using Snp_p = std::shared_ptr<Snapshot<Vector>>;

    // store snapshot data, and (False) do not write when get)
    MaV(G_p grid_p, std::string prefix)
            : mean(new io::Solution<G, Snapshot<Vector>>(grid_p, prefix + "_mean", true))
              , M2(new io::Solution<G, Snapshot<Vector>>(grid_p, prefix + "_var", true)) {
    }

    //! \brief
    //! \param snp
    void pushSnapshot(Snp_p snp) {
        unsigned int frame = snp->frame;
        Snp_p mean_snp = mean->getSnapshot(snp, frame);
        Snp_p M2_snp = M2->getSnapshot(snp, frame);

        count.resize(std::max((unsigned int) count.size(), frame + 1), 0);
        count[frame]++;
        Snp_p delta = Snp_p(new Snapshot<Vector>(*snp));

        delta->setZero();
        // delta = x - mean
        // mean += delta/n
        //  M2 += delta*(x - mean)

        axpbypz(*delta, *snp , *mean_snp, -1, 1);
        axpy(*mean_snp, *delta, 1. / count[frame]);
        xmytz(*delta, *snp, *mean_snp);
        axpy(*M2_snp, *delta, -1);
    }

    //!
    //!
    //!
    void finalize() {
        for(unsigned int f = 0; f < count.size(); ++f) {
            if(count[f] < 2) {
                M2->getSnapshot(f)->setZero();
            } else {
                M2->getSnapshot(f)->rescale(1. / (count[f] - 1));
            }
        }
    }

    //!
    //!
    //!
    //! \param intraDomainComm
    //! \param remote_grids
    void collect(const mpi::IntraDomainComm & intraDomainComm, std::vector<R_p> & remote_grids) {
        // CHECK SAME FRAME COUNT

        for(unsigned int f = 0; f < count.size(); ++f) {
            glue(mean->getSnapshot(f), M2->getSnapshot(f), count.at(f), intraDomainComm, remote_grids, f);
        }
    }

    //!
    //!
    //!
    //!
    //! \param othermav
    void difference(std::shared_ptr<MaV<G,RG>> othermav) {
        G_p thisgr = mean->grid_p;
        G_p othergr = othermav->mean->grid_p;
        int maxdof = mean->getSnapshot(0)->maxdof;
        int othermaxdof = othermav->mean->getSnapshot(0)->maxdof;

        for(unsigned int f = 0; f < count.size(); ++f) {
            Vector bigV(thisgr, maxdof);
            Vector::FreqVec bighatV(thisgr, maxdof);
            Vector smallV(othergr, othermaxdof);
            Vector::FreqVec smallhatV(othergr, othermaxdof);

            std::cout << maxdof << std::endl;
            bigV.plan(bighatV);
            smallV.plan(smallhatV);
            for(unsigned int c = 0; c < mean->getSnapshot(f)->components.size(); ++c) {
                Vector & meanV = mean->getSnapshot(f)->components.at(c);
                Vector & otherMeanV = othermav->mean->getSnapshot(f)->components.at(c);
                Vector & M2V = M2->getSnapshot(f)->components.at(c);
                Vector & otherM2V = othermav->M2->getSnapshot(f)->components.at(c);

                otherMeanV.forward(smallhatV);
                zero_padding(smallhatV, bighatV);
                bighatV.backward(bigV);
                scale(bigV, std::pow(2, NDIMS)); // FIXME
                axpy(meanV, bigV, -1);

                otherM2V.forward(smallhatV);
                zero_padding(smallhatV, bighatV);
                bighatV.backward(bigV);
                scale(bigV, std::pow(2, NDIMS)); // FIXME !!!
                axpy(M2V, bigV, -1);
            }
        }
    }

    //!
    //!
    //!
    //!
    //! \param master_grid
    void send_to_root(R_p master_grid) {
        for(unsigned int f = 0; f < count.size(); ++f) {
            for(unsigned int c = 0; c < mean->getSnapshot(f)->components.size(); ++c) {
                send(mean->getSnapshot(f)->components.at(c), master_grid, generate_tag(sphinx::mean_subtag, c, f));
                send(M2->getSnapshot(f)->components.at(c), master_grid, generate_tag(sphinx::var_subtag, c, f));
            }
        }
    }

    //! \brief Called by master root, receives all the data from the levels (roots).
    //!
    //!
    //!
    //! \param remote_grids
    //! \param coarse_grids
    void recv_from_levels(std::vector<R_p> remote_grids, std::vector<G_p> coarse_grids) {
        G_p thisgr = mean->grid_p;
        int maxdof = mean->getSnapshot(0)->maxdof;

        for(unsigned int l = 0; l < remote_grids.size(); ++l) {
            G_p othergr = coarse_grids.at(l);

            for(unsigned int f = 0; f < count.size(); ++f) {

                for(unsigned int c = 0; c < mean->getSnapshot(f)->components.size(); ++c) {
                    Vector V(othergr, mean->getSnapshot(f)->components.at(c).dof);
                    Vector V_ref(thisgr, mean->getSnapshot(f)->components.at(c).dof);
                    Vector::FreqVec Vhat(othergr, mean->getSnapshot(f)->components.at(c).dof);
                    Vector::FreqVec Vref_hat(thisgr, mean->getSnapshot(f)->components.at(c).dof);

                    std::cout << maxdof << std::endl;

                    V.plan(Vhat);
                    V_ref.plan(Vref_hat);

                    recv(V, remote_grids.at(l), generate_tag(sphinx::mean_subtag, c, f));

                    V.forward(Vhat);
                    zero_padding(Vhat, Vref_hat);
                    Vref_hat.backward(V_ref);
                    scale(V_ref, std::pow(4,remote_grids.size()-l)); // FIXME

                    axpy(mean->getSnapshot(f)->components.at(c), V_ref, 1);

                    recv(V, remote_grids.at(l), generate_tag(sphinx::var_subtag, c, f));

                    V.forward(Vhat);
                    zero_padding(Vhat, Vref_hat);
                    Vref_hat.backward(V_ref);
                    scale(V_ref, std::pow(4, remote_grids.size() - l)); // FIXME;

                    axpy(M2->getSnapshot(f)->components.at(c), V_ref, 1);
                }
            }
        }
    }

    //!
    //!
    //!
    void write() {
        mean->write();
        M2->write();
    }

    //!
    std::unique_ptr<io::Solution<G,Snapshot<Vector>>> mean;
    //!
    std::unique_ptr<io::Solution<G,Snapshot<Vector>>> M2;
private:
    //!
    std::vector<unsigned int> count;
};

} // END namespace representation
