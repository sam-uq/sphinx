/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "config.hpp"

#include "fftw_freq_vec.hpp"

#include "device_vec.hpp"
#include "freq_vec.hpp"

//!
//!
//!
//! \tparam d
//! \tparam contiguous
//! \tparam interlaced
template <unsigned int d, bool contiguous, bool interlaced>
class CUDADeviceFreqVec
        : public DeviceVec<cuda_device_freq_t, d, contiguous, interlaced>
          , public FreqVec<cuda_device_freq_t, d, contiguous, interlaced> {
public:
    using base_t = Vec<cuda_device_freq_t, d, contiguous, interlaced>;
    using FFT_t = sphinx::operators::CUDAFFT<d, contiguous, interlaced>;
    static const unsigned int dim = d;

    CUDADeviceFreqVec(const std::shared_ptr<Grid<d,
            contiguous, interlaced>> grid_p,
                      const unsigned int dof,
                      std::string name = "unknown")
            : Vec<cuda_device_freq_t, d,
            contiguous, interlaced>(grid_p, dof, name)
              , DeviceVec<cuda_device_freq_t, d,
                    contiguous, interlaced>(grid_p, dof,
                                            name)
              , FreqVec<cuda_device_freq_t, d,
                    contiguous, interlaced>(grid_p, dof, name) {
        init();
    }

    CUDADeviceFreqVec(
            const CUDADeviceFreqVec<d, contiguous, interlaced> & other,
            sphinx::container::Data /* copy_type */ =
            sphinx::container::Data::ShapeOnly)
            : Vec<cuda_device_freq_t, d, contiguous, interlaced>(other)
              , DeviceVec<cuda_device_freq_t, d, contiguous, interlaced>(other)
              , FreqVec<cuda_device_freq_t, d, contiguous, interlaced>(other) {
        init();
        *this = other;
    }

    CUDADeviceFreqVec(const FFTWFreqVec<d, contiguous, interlaced> & other,
                      sphinx::container::Data copy_type =
                      sphinx::container::Data::ShapeOnly)
            : Vec<cuda_device_freq_t, d, contiguous, interlaced>(other.grid_p,
                                                                 other.dof)
              , DeviceVec<cuda_device_freq_t, d,
                    contiguous, interlaced>(other.grid_p,
                                            other.dof)
              , FreqVec<cuda_device_freq_t, d,
                    contiguous, interlaced>(other.grid_p,
                                            other.dof) {
        init();
        operators::HostToDevice(other, *this);
    }

    CUDADeviceFreqVec(const FFTWTimeVec<d, contiguous, interlaced> & other)
            : Vec<cuda_device_freq_t, d, contiguous, interlaced>(other.grid_p,
                                                                 other.dof)
              , DeviceVec<cuda_device_freq_t, d,
                    contiguous, interlaced>(other.grid_p,
                                            other.dof)
              , FreqVec<cuda_device_freq_t, d,
                    contiguous, interlaced>(other.grid_p,
                                            other.dof) {
        init();
    }


    CUDADeviceFreqVec(const CUDADeviceTimeVec<d, contiguous, interlaced> &other)
            : CUDADeviceFreqVec(other.grid_p, other.dof, other.name) {
        init();
    }

    void init(void) {
        DeviceVec<cuda_device_freq_t, d, contiguous, interlaced>::init();
        FreqVec<cuda_device_freq_t, d, contiguous, interlaced>::init();

        const unsigned int dof = FreqVec<cuda_device_freq_t,
                d, contiguous, interlaced>::dof;

//        std::cout << "Allocating " << this->size << std::endl;
        for(unsigned int f = 0; f < dof; ++f) {
            gpuErrchk(cudaMalloc(&this->_data[f],
                                 this->size * 2 * sizeof(double)));
        }
        gpuErrchk(cudaMemcpy(this->device_data, this->_data.data(),
                             dof *  sizeof(typename CUDADeviceFreqVec::data_t*),
                             cudaMemcpyHostToDevice) );

        std::array<unsigned int, 3> max_shape;
        if(dim == 2) {
            max_shape = {32u, 16u, 1u};
        } else {
            max_shape = {8u, 8u, 8u};
        }

        this->dimBlock.x = std::min(max_shape[0],
                                    (unsigned int) this->grid_p->freqN[0]);
        this->dimBlock.y = std::min(max_shape[1],
                                    (unsigned int) this->grid_p->freqN[1]);
        this->dimGrid.x = this->grid_p->freqN[0] / this->dimBlock.x
                          + ((this->grid_p->freqN[0] % this->dimBlock.x==0)?0:1);
        this->dimGrid.y = this->grid_p->freqN[1] / this->dimBlock.y
                          + ((this->grid_p->freqN[1] % this->dimBlock.y==0)?0:1);

        if( dim >= 3) {
            this->dimBlock.z = std::min(max_shape[2],
                                        (unsigned int) this->grid_p->freqN[2]);
            this->dimGrid.z = this->grid_p->freqN[2] / this->dimBlock.z
                              + ((this->grid_p->freqN[2] % this->dimBlock.z==0)?0:1);
        }

        unsigned int buffer[3];
        for(unsigned int f = 0; f < dim; ++f) {
            buffer[f] = (unsigned int) this->grid_p->freqN[f];
        }
        gpuErrchk(cudaMemcpy(this->device_size, buffer,
                             dim * sizeof(unsigned int),
                             cudaMemcpyHostToDevice) );
        gpuErrchk(cudaMemcpy(this->device_size_pad, buffer,
                             dim * sizeof(unsigned int),
                             cudaMemcpyHostToDevice) );
        buffer[0] = this->ksize;
        buffer[1] = this->jsize;
        buffer[2] = this->isize;
        gpuErrchk(cudaMemcpy(this->local_size, buffer,
                             dim * sizeof(unsigned int),
                             cudaMemcpyHostToDevice) );
        buffer[0] = this->kstart;
        buffer[1] = this->jstart;
        buffer[2] = this->istart;
        gpuErrchk(cudaMemcpy(this->local_start, buffer,
                             dim * sizeof(unsigned int),
                             cudaMemcpyHostToDevice) );
    }

    virtual ~CUDADeviceFreqVec() {
        for(unsigned int f = 0; f < this->dof; ++f) {
            gpuErrchk(cudaFree(this->_data[f]) );
        }
    }

    void plan(CUDADeviceTimeVec<d, contiguous, interlaced> & time) {
        this->grid_p->require_device_fft();
        this->grid_p->fftd->plan(time, *this);
    }

    void backward(CUDADeviceTimeVec<d, contiguous, interlaced> & time) {
        this->grid_p->require_device_fft();
        this->grid_p->fftd->backward(*this, time);
    }
};
