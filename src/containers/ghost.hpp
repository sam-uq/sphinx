/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include <vector>
#include <memory>
#include <cassert>

#include <fmt/format.h>

#include <mpi.h>
#include <utilities/utilities.hpp>

namespace sphinx {
namespace containers {

//! The type of data in neighbouring ranks.
enum class GhostType {
    None = 0,
    Wrap,
    Buffer,
};

//! \brief Manages transfer of Ghost points amongst partitioned domain.
//!
//!
//! \tparam Vec
//! \tparam read_only
template<class Vec, bool read_only = true>
class Ghost {
public:
    //! cv-Qualified vector
    using cvVec = typename std::conditional<read_only, const Vec, Vec>::type;
    //! External handler
    using VectorType = cvVec;
    //! Pointer to grid
    using pGrid = std::shared_ptr<typename Vec::grid_t>;

    static_assert(Vec::dim == 2, "Only implemented for 2d vectors!");
    static_assert(Vec::interlaced_data == false, "Interlaced format not implemented!");
    static_assert(Vec::contiguous_data == false, "Contiguous format not implemented!");

    //!
    //!
    //! TODO: take a grid. Maybe a size and a vector?
    //!
    Ghost() {
        uuid = count++;

        debug::verbose(fmt::format("New ghost with id = {}", uuid), mpi::world);
    }

    //! \brief Returns the type of neighbor of the current rank.
    //!
    //! \param is_end
    //! \param periodic
    //! \param ranks
    //! \return
    GhostType get_ghost_type(bool is_end, bool periodic, bool ranks) {
        if (is_end) {
            if (periodic) {
                if (ranks == 1) {
                    return GhostType::Wrap;
                } else {
                    return GhostType::Buffer;
                }
            } else {
                return GhostType::None;
            }
        } else {
            return GhostType::Buffer;
        }

    }

    //! \breif Set grid which will be used to Ghost a vector
    //!
    //! TODO make private and put in constructor
    //!
    //! \param new_grid
    void set_grid(pGrid new_grid) {
        grid = new_grid;

        int rank = grid->comm.rank;

        rank_left = (rank - 1 < 0 and periodic)
                    ? grid->comm.size - 1
                    : rank - 1;
        rank_right = (rank + 1 >= grid->comm.size and periodic)
                     ? 0
                     : rank + 1;

        left_type = get_ghost_type(rank_left < 0, periodic, grid->comm.size);
        right_type = get_ghost_type(rank_right >= grid->comm.size, periodic, grid->comm.size);
    }

    //! \brief Sets container which will be ghosted.
    //!
    //! \param v
    void set_container(cvVec *v) {
        vec = v;

        left_buffer.resize(vec->dof);
        right_buffer.resize(vec->dof);
    }

    //! \brief Set size of ghost slab.
    //!
    //! \param new_size
    void set_size(int new_size) {
        assert(grid && "Grid not set, you must set the grid via 'set_grid'!");
        assert(vec != nullptr && "Vector not set, you must set the vector via 'set_container'");

        size = new_size;

        slab_size = grid->timeNpad[1];

        data_count = size * slab_size;

        if (left_type == GhostType::Buffer) {
            for (auto &lb: left_buffer) {
                lb.resize((std::size_t) data_count);
            }
        }
        if (right_type == GhostType::Buffer) {
            for (auto &rb: right_buffer) {
                rb.resize((std::size_t) data_count);
            }
        }

        // Check that we have enough data to send, otherwise we must abort.
        if (grid->local_n0 < size) {
            debug::error("Stencil too big for this partitioning, not implemented!", mpi::world);

            throw NotImplementedException();
        }
    }

    //! \brief Start transfer of Ghosted data.
    //!
    void update_begin() {
        if (size == 0) {
            debug::warning("Ghost size is 0, either you forgot to call 'set_size' or you are using an empty stencil.",
                           mpi::world);
            return;
        }

        int dof = vec->dof;
        req.resize((std::size_t) 4 * dof);
        int I = 0;

        int global_tag = get_tag();

        // Send and receive ghost information
        for (int f = 0; f < dof; ++f) {
            int local_tag = global_tag + 2 * f;

            // Left boundary (send to the left and receive from the left)
            if (left_type == GhostType::Buffer) {
                debug::verbose(fmt::format("Send/Recv from left: {}, count = {}", rank_left, data_count), mpi::world);
                MPI_Isend(vec->_data[f], data_count, MPI_DOUBLE, rank_left, local_tag, grid->comm.comm,
                          &req[I++]); // send left
                MPI_Irecv(left_buffer[f].data(), data_count, MPI_DOUBLE, rank_left, local_tag + 1, grid->comm.comm,
                          &req[I++]); // recv left
            } else {
                debug::verbose(fmt::format("Not sending to left = {} (no nhbd.)", rank_left), mpi::world);
            }
            // Right boundary (send to the right and receive from the right)
            if (right_type == GhostType::Buffer) {
                debug::verbose(fmt::format("Send/Recv from right: {}, count = {}", rank_right, data_count), mpi::world);
                int offset = (grid->local_n0 - size) * slab_size;

                MPI_Isend(vec->_data[f] + offset, data_count, MPI_DOUBLE, rank_right, local_tag + 1, grid->comm.comm,
                          &req[I++]); // send right
                MPI_Irecv(right_buffer[f].data(), data_count, MPI_DOUBLE, rank_right, local_tag, grid->comm.comm,
                          &req[I++]); // recv right
            } else {
                debug::verbose(fmt::format("Not sending to right = {} (no nhbd.)", rank_right), mpi::world);
            }

            // Setup a new tag for the next dow
        }

        req.resize((std::size_t) I);
    }

    //! \brief Waits for MPI transfer to complete. Returns once done.
    //!
    //! Performs error checking.
    //!
    void update_end() {
        std::vector<MPI_Status> statuses(req.size());

        // Wait for completion of all requests
        MPI_Waitall((int) req.size(), req.data(), statuses.data());

        // Check for errors
        for (MPI_Status &stat: statuses) {
            if (stat.MPI_ERROR != MPI_SUCCESS) {
                debug::error(fmt::format("MPI Send/Recv issued an error {}.", stat.MPI_ERROR), mpi::world);
            } else {
                debug::verbose("Sending/Recv OK!", mpi::world);
            }
        }
    }

    //! \brief Fast unchecked ghost access.
    //!
    //!
    //! \param dof
    //! \param l_modes
    //! \return
    inline double ghost_access(int dof, const pos_t<2> & l_modes) {

        auto lm1 = periodic_remap_cpu<1>(*vec, l_modes[1]);

        if (l_modes[0] < 0 && left_type == GhostType::Buffer) {
            return left_buffer[dof][vec->nj * (l_modes[0] + size) + lm1];
        } else if (l_modes[0] >= vec->kend && right_type == GhostType::Buffer) {
            return right_buffer[dof][vec->nj * (l_modes[0] - vec->kend) + lm1];
        } else {
            int Ix = vec->nj * periodic_remap_cpu<0>(*vec, l_modes[0]) + periodic_remap_cpu<1>(*vec, lm1);
            return vec->_data[dof][Ix];
        }
    }

    //! \brief Safe variant of ghost acces, throws or asserts on error.
    //!
    //! Performs many checks, might be slow.
    //!
    //! \param dof
    //! \param l_modes
    //! \return
    inline double ghost_access_throw(int dof, const pos_t<2> & l_modes) {
        assert(dof < (int) vec->dof && "Requested Dof. too high!");

        if (l_modes[0] < -size && left_type != GhostType::Wrap) {
            debug::error("Index out of bounds (< -size)!", mpi::world);
        } else if (l_modes[0] >= vec->kend + size && right_type != GhostType::Wrap) {
            debug::error("Index out of bounds (>= nk + size)!", mpi::world);
        }

        auto lm1 = periodic_remap_cpu<1>(*vec, l_modes[1]);

        if (l_modes[0] < 0 && left_type == GhostType::Buffer) {
            return left_buffer.at(dof).at(vec->nj * (l_modes[0] + size) + lm1);
        } else if (l_modes[0] < 0 && left_type == GhostType::None) {
            debug::error("Accessing data on the left ghost area, which in neither Buffered nor Wrapped.", mpi::world);
            return 0;
        } else if (l_modes[0] >= vec->kend && right_type == GhostType::Buffer) {
            return right_buffer.at(dof).at(vec->nj * (l_modes[0] - vec->kend) + lm1);
        } else if (l_modes[0] >= vec->kend && right_type == GhostType::None) {
            debug::error("Accessing data on the right ghost area, which in neither Buffered nor Wrapped.", mpi::world);
            return 0;
        } else {
            int Ix = vec->nj * periodic_remap_cpu<0>(*vec, l_modes[0]) + lm1;
            return vec->_data.at(dof)[Ix];
        }
    }

    //! \brief Provides access to internal storage.
    //!
    //! \return
    Vec *get() const {
        return vec;
    }

    //! \breif Used in kernels that accept Vector-like objects
    //!
    //!
    //! \return
    explicit operator VectorType &() const {
        return *vec;
    }

protected:
    //! \brief Return a tag for use by MPI
    //!
    //! The tag is unique for this ghost and for each call to get_tag.
    //!
    //! \return
    int get_tag() const {
        // Return a unique tag for this transaction, tag is valid from returned value to returned value + 10
        return tag + 1000 * uuid + 10 * tag_counter++;
    }

private:
    //// EXTERNAL INFO

    //!  Access to grid
    pGrid grid;
    //! Non owned access to vector
    cvVec *vec = nullptr;

    //// COUNTERS

    //! Tag for mpi communication
    const int tag = sphinx::ghost_tag;
    //! Unique identifier
    mutable int uuid;
    //! A counter that increments at each call of get_tag
    mutable int tag_counter = 0;
    //! Static unique identifier counter incremented after each creation
    static int count;

    //// BUFFERS

    //! The data in the left, resp right. part of the domain.
    std::vector<std::vector<double>> left_buffer, right_buffer;

    //// STATE

    //! Pending requests
    std::vector<MPI_Request> req;

    //// CACHED DATA AND OPTIONS

    //! Use periodic boundaries?
    bool periodic = true;
    //! The stencil size of the Ghost
    int size = 0;
    //! Ranks to the left and right (according to topology)
    int rank_left = -1, rank_right = -1;
    //! Data to send
    int data_count = 0;
    //! The size of a slab: the product of all dimensions along which partitioning is NOT performed, including padding.
    int slab_size = 0;
    //! Type of data in left and right buffer.
    GhostType left_type, right_type;
};

template<class Vec, bool read_only>
int Ghost<Vec, read_only>::count = 0;

} // END namespace containers
} // END namespace sphinx