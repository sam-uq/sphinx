/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "config.hpp"

#include "device_vec.hpp"
#include "time_vec.hpp"
#include "host_vec.hpp"

#include "operators/device_functions/device_functions.hpp"

// Forward declare device frequency vector
template <unsigned int d, bool contiguous, bool interlaced>
class CUDADeviceFreqVec;

//!
//! \tparam d
//! \tparam contiguous
//! \tparam interlaced
template <unsigned int d, bool contiguous, bool interlaced>
class CUDADeviceTimeVec
        : public DeviceVec<cuda_device_time_t, d, contiguous, interlaced>
          , public TimeVec<cuda_host_time_t, d, contiguous, interlaced> {
public:
    using FreqVec = CUDADeviceFreqVec<d, contiguous, interlaced>;

    using base_t = Vec<cuda_device_time_t, d, contiguous, interlaced>;
    using FFT_t = sphinx::operators::CUDAFFT<d, contiguous, interlaced>;
    static const unsigned int dim = d;

    CUDADeviceTimeVec(
            const std::shared_ptr<Grid<d, contiguous, interlaced>> grid_p,
            const unsigned int dof, std::string name = "unknown")
            : Vec<cuda_device_time_t, d, contiguous, interlaced>(grid_p,
                                                                 dof, name)
              , DeviceVec<cuda_device_time_t, d, contiguous, interlaced>(grid_p,
                                                                         dof, name)
              , TimeVec<cuda_device_time_t, d, contiguous, interlaced>(grid_p,
                                                                       dof, name) {
        init();
    }

    CUDADeviceTimeVec(
            const CUDADeviceTimeVec<d, contiguous, interlaced> & other,
            sphinx::container::Data copy_type =
            sphinx::container::Data::ShapeOnly)
            : Vec<cuda_device_time_t, d, contiguous, interlaced>(other)
              , DeviceVec<cuda_device_time_t, d, contiguous, interlaced>(other)
              , TimeVec<cuda_device_time_t, d, contiguous, interlaced>(other) {
        init();
        *this = other;
    }

//    template <class T>
//    CUDADeviceTimeVec(const TimeVec<T, d, contiguous, interlaced> & other)
//        : Vec<cuda_device_time_t, d, contiguous, interlaced>(other)
//        , DeviceVec<cuda_device_time_t, d, contiguous, interlaced>(other.grid_p, other.dof)
//        , TimeVec<cuda_device_time_t, d, contiguous, interlaced>(other) {
//        init();
//    }

    template <class T>
    CUDADeviceTimeVec(const HostVec<T, d, contiguous, interlaced> & other,
                      sphinx::container::Data /* copy_type */ =
                      sphinx::container::Data::ShapeOnly)
            : Vec<cuda_host_time_t, d, contiguous, interlaced>(other)
              , DeviceVec<cuda_host_time_t, d, contiguous,
                    interlaced>(other.grid_p, other.dof)
              , TimeVec<cuda_host_time_t, d, contiguous,
                    interlaced>(other.grid_p, other.dof) {
        init();
        sphinx::operators::HostToDevice(other, *this);
    }

    void init(void) {
        DeviceVec<cuda_device_time_t, d, contiguous, interlaced>::init();
        TimeVec<cuda_device_time_t, d, contiguous, interlaced>::init();

        const unsigned int dof =
                TimeVec<cuda_device_time_t, d, contiguous, interlaced>::dof;

//        std::cout << "Allocating " << this->size << std::endl;
        for(unsigned int f = 0; f < dof; ++f) {
            gpuErrchk(cudaMalloc(&(this->_data[f]), this->size * sizeof(double)));
        }
        gpuErrchk(cudaMemcpy(this->device_data, this->_data.data(),
                             dof * sizeof(typename CUDADeviceTimeVec::data_t*),
                             cudaMemcpyHostToDevice) );

//        std::cout << this->_data.data() << " " << this->_data[0] << std::endl;

        std::array<unsigned int, 3> max_shape;
        if(dim == 2) {
            max_shape = {32u, 16u, 1u};
        } else {
            max_shape = {8u, 8u, 8u};
        }

        this->dimBlock.x = std::min(max_shape[0],
                                    (unsigned int) this->grid_p->timeNpad[0]);
        this->dimBlock.y = std::min(max_shape[1],
                                    (unsigned int) this->grid_p->timeNpad[1]); // FIXME
        this->dimGrid.x = this->grid_p->timeNpad[0] / this->dimBlock.x
                          + ((this->grid_p->timeNpad[0] % this->dimBlock.x==0)?0:1);
        this->dimGrid.y = this->grid_p->timeNpad[1] / this->dimBlock.y
                          + ((this->grid_p->timeNpad[1] % this->dimBlock.y==0)?0:1);

        if( dim >= 3) {
            this->dimBlock.z = std::min(max_shape[2],
                                        (unsigned int) this->grid_p->timeNpad[2]); // FIXME
            this->dimGrid.z = this->grid_p->timeNpad[2] / this->dimBlock.y
                              + ((this->grid_p->timeNpad[2] % this->dimBlock.y==0)?0:1);
        }

        unsigned int buffer1[3], buffer2[3];
        for(unsigned int f = 0; f < dim; ++f) {
            buffer1[f] = (unsigned int) this->grid_p->timeN[f];
            buffer2[f] = (unsigned int) this->grid_p->timeNpad[f];
        }
        gpuErrchk(cudaMemcpy(this->device_size, buffer1,
                             dim * sizeof(unsigned int),
                             cudaMemcpyHostToDevice) );
        gpuErrchk(cudaMemcpy(this->device_size_pad, buffer2,
                             dim * sizeof(unsigned int),
                             cudaMemcpyHostToDevice) );
        buffer1[0] = this->ksize;
        buffer1[1] = this->jsize;
        buffer1[2] = this->isize;
        gpuErrchk(cudaMemcpy(this->local_size, buffer1,
                             dim * sizeof(unsigned int),
                             cudaMemcpyHostToDevice) );
        buffer2[0] = this->kstart;
        buffer2[1] = this->jstart;
        buffer2[2] = this->istart;
        gpuErrchk(cudaMemcpy(this->local_start, buffer2,
                             dim * sizeof(unsigned int),
                             cudaMemcpyHostToDevice) );
    }

    virtual ~CUDADeviceTimeVec() {
        for(unsigned int f = 0; f < TimeVec<cuda_device_time_t, d,
                contiguous, interlaced>::dof; ++f) {
            gpuErrchk(cudaFree((this->_data[f])) );
        }
    }


    void plan(CUDADeviceFreqVec<d, contiguous, interlaced> & freq) {
        this->grid_p->require_device_fft();
        this->grid_p->fftd->plan(*this, freq);
    }

    void forward(CUDADeviceFreqVec<d, contiguous, interlaced> & freq) {
        this->grid_p->require_device_fft();
        this->grid_p->fftd->forward(*this, freq);
    }
};
