/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "vec.hpp"

//! Virtual derived of Vec, representing a Vecotr in frequency space
template <class T, unsigned int d, bool contiguous, bool interlaced>
class FreqVec : public virtual Vec<T, d, contiguous, interlaced> {
public:
    FreqVec(const std::shared_ptr<Grid<d, contiguous, interlaced>> grid_p,
            const unsigned int dof, std::string name = "unknown")
        : Vec<T, d, contiguous, interlaced>(grid_p, dof, name = "unknown") {

        Vec<T, d, contiguous, interlaced>::parpad = 2;
    }

    FreqVec(const FreqVec & other)
        : Vec<T, d, contiguous, interlaced>(other) {
        Vec<T, d, contiguous, interlaced>::parpad = 2;
    }

    virtual ~FreqVec() {}

    void init(void) {
#if USE_ACCFFT
        this->kstart = this->grid_p->frequency_local_start[0];
        this->jstart = this->grid_p->frequency_local_start[1];
        this->istart = this->grid_p->frequency_local_start[2];
        this->ksize = this->grid_p->frequency_local_size[0];
        this->jsize = this->grid_p->frequency_local_size[1];
        this->isize = this->grid_p->frequency_local_size[2];
        this->kend = this->grid_p->frequency_local_size[0] + this->kstart;
        this->jend = this->grid_p->frequency_local_size[1] + this->jstart;
        this->iend = this->grid_p->frequency_local_size[2] + this->istart;
#else
        this->kend = this->grid_p->local_n0_trsp;
        this->jend = this->grid_p->freqN[0];
        this->iend = this->grid_p->freqN[2];
        this->ksize = this->grid_p->local_n0_trsp
                + this->grid_p->local_0_start_trsp;
        this->jsize = this->grid_p->freqN[1];
        this->isize = this->grid_p->freqN[2];
        this->kstart = this->grid_p->local_0_start_trsp;
        this->jstart = 0;
        this->istart = 0;
#endif
        this->nk = this->grid_p->freqN[1];
        this->nj = this->grid_p->freqN[0];
        this->ni = this->grid_p->freqN[2];

        this->size = this->grid_p->alloc_freq;

        std::stringstream ss;
        ss << this->kstart << "-" << this->kend << "." << this->nk << ","
           << this->jstart << "-" << this->jend << "." << this->nj << ","
           << this->istart << "-" << this->iend << "." << this->ni << ","
           << "ALLOC:" << this->size;

        debug::verbose(ss.str(), mpi::self);
    }

    inline pos_t<2> getRealModes(const pos_t<2> & l_modes) const {
         return {
             l_modes[1] > this->nj/2+1 ? l_modes[1] - this->nj : l_modes[1],
             l_modes[0] + this->kstart
         };
     }

     inline pos_t<3> getRealModes(const pos_t<3> & l_modes) const {
         index_t k = l_modes[0] + this->kstart;
         return {
             l_modes[1] > this->nj/2+1 ? l_modes[1] - this->nj : l_modes[1],
             k > this->nk/2+1 ? k - this->nk : k,
             l_modes[2]
         };
     }

};

