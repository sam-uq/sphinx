/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "config.hpp"

#include "vec.hpp"

#include "utilities/include_fftw.hpp"

#if USE_CUDA
#include "device_vec.hpp"
#endif
#include "host_vec.hpp"
#include "freq_vec.hpp"

#include "operators/operators.hpp"

template <unsigned int d, bool contiguous, bool interlaced>
class FFTWTimeVec;

template <unsigned int d, bool contiguous, bool interlaced>
class FFTWFreqVec :
        public HostVec<fftw_freq_t, d, contiguous, interlaced>,
        public FreqVec<fftw_freq_t, d, contiguous, interlaced> {
public:
    FFTWFreqVec(const std::shared_ptr<Grid<d, contiguous, interlaced>> grid_p,
                const unsigned int dof, std::string name = "unknown")
        : Vec<fftw_freq_t, d, contiguous, interlaced>(grid_p, dof, name)
        , HostVec<fftw_freq_t, d, contiguous, interlaced>(grid_p, dof, name)
        , FreqVec<fftw_freq_t, d, contiguous, interlaced>(grid_p, dof, name)
    {
        init();
    }

    FFTWFreqVec(const FFTWFreqVec<d, contiguous, interlaced> & other,
                sphinx::container::Data copy_type = sphinx::container::Data::Copy)
        : Vec<fftw_freq_t, d, contiguous, interlaced>(other)
        , HostVec<fftw_freq_t, d, contiguous, interlaced>(other)
        , FreqVec<fftw_freq_t, d, contiguous, interlaced>(other) {

        switch(copy_type) {
        case sphinx::container::Data::Copy:
        default:
            init();
            *this = other;
            break;
        case sphinx::container::Data::Share:
            this->share(other);
            break;
        }
    }

#if USE_CUDA
    template <class T>
    FFTWFreqVec(const DeviceVec<T, d, contiguous, interlaced> & other)
        : Vec<fftw_freq_t, d, contiguous, interlaced>(other.grid_p, other.dof)
        , HostVec<fftw_freq_t, d, contiguous, interlaced>(other.grid_p, other.dof)
        , FreqVec<fftw_freq_t, d, contiguous, interlaced>(other.grid_p, other.dof) {
        init();
        operators::DeviceToHost(other, *this);
    }
#endif // USE_CUDA

    FFTWFreqVec(const FFTWTimeVec<d, contiguous, interlaced> & other,
                sphinx::container::Data copy_type = sphinx::container::Data::ShapeOnly)
        : Vec<fftw_freq_t, d, contiguous, interlaced>(other.grid_p, other.dof)
        , HostVec<fftw_freq_t, d, contiguous, interlaced>(other.grid_p, other.dof)
        , FreqVec<fftw_freq_t, d, contiguous, interlaced>(other.grid_p, other.dof) {

        switch(copy_type) {
        case sphinx::container::Data::ShapeOnly:
            init();
            break;
        case sphinx::container::Data::Share:
            this->share(other);
            break;
        case sphinx::container::Data::Copy:
            debug::error("Nonsense copy data from freq to tome!", this->grid_p->comm);
            break;
        }
    }

    ~FFTWFreqVec() { dealloc(); }

    template <class T,
              bool interlaced_ = interlaced, typename std::enable_if<interlaced_ == true, bool>::type = true>
    void share(const HostVec<T, d, contiguous, interlaced> & other) {
        this->share_count = other.share_count;
        ++*(this->share_count);

        this->_data = reinterpret_cast<fftw_freq_t*>(other._data);
    }


    template <class T,
              bool interlaced_ = interlaced, typename std::enable_if<interlaced_ == false, int>::type = 1>
    void share(const HostVec<T, d, contiguous, interlaced> & other) {
        this->share_count = other.share_count;
        ++*(this->share_count);

        for(unsigned int f = 0; f < this->dof; ++f) {
            this->_data.at(f) = reinterpret_cast<fftw_freq_t*>(other._data.at(f));
        }
    }

    template <bool interlaced_ = interlaced, typename std::enable_if<interlaced_ == true, bool>::type = true>
    void init(void) {
        static_assert(contiguous == true, "Need contiguous!");

        FreqVec<fftw_freq_t, d, contiguous, interlaced>::init();

#if myUSE_MKL
        this->_data = reinterpret_cast<fftw_freq_t*>(malloc(this->dof * this->size * sizeof(std::complex<double>)));
#else
        this->_data = reinterpret_cast<fftw_freq_t*>(fftw_alloc_complex(this->dof * this->size));
#endif
    }

    template <bool interlaced_ = interlaced, typename std::enable_if<interlaced_ == false, int>::type = 1>
    void init(void) {
        static_assert(contiguous == false, "Not yet implemented!");

        FreqVec<fftw_freq_t, d, contiguous, interlaced>::init();

        for(auto& dt: this->_data) {
#if myUSE_MKL
            dt = reinterpret_cast<fftw_freq_t*>(malloc(this->size * sizeof(std::complex<double>)));
#else
            dt = reinterpret_cast<fftw_freq_t*>(fftw_alloc_complex(this->size));
#endif
        }
    }

    template <bool interlaced_ = interlaced, typename std::enable_if<interlaced_ == true, bool>::type = true>
    void dealloc() {
        if( *(this->share_count) > 1 ) return;
        else --*(this->share_count);

#if myUSE_MKL
        free(this->_data);
#else
        fftw_free(this->_data);
#endif
    }

    template <bool interlaced_ = interlaced, typename std::enable_if<interlaced_ == false, int>::type = 1>
    void dealloc() {
        if( *(this->share_count) > 1 ) return;
        else --*(this->share_count);

        for(auto dt: this->_data) {
#if myUSE_MKL
            free(dt);
#else
            fftw_free(dt);
#endif
        }
    }

    void plan(FFTWTimeVec<d, contiguous, interlaced> & time) {
        this->grid_p->require_host_fft();
        this->grid_p->ffth->plan(*this, time);
    }

    void backward(FFTWTimeVec<d, contiguous, interlaced> & time) {
        this->grid_p->require_host_fft();
        this->grid_p->ffth->backward(*this, time);
    }

    using base_t = Vec<fftw_freq_t, d, contiguous, interlaced>;
#if myUSE_MKL
    using FFT_t = operators::MKLFFT<d, contiguous, interlaced>;
#else
    using FFT_t = operators::FFTWFFT<d, contiguous, interlaced>;
#endif
};
