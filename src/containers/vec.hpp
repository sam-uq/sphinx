/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

//! \file vec.hpp
//! \brief File containing the definitions of the \ref sphinx::Vec class.
//!
//!
//!
//! \author Filippo Leonardi
//! \date 2017-01-01

#include <tuple>

#include <array>
#include <vector>
#include <cassert>
#include <complex>
#include <memory>

#include "utilities/profiler.hpp"
#include "utilities/utilities.hpp"
#include "utilities/types.hpp"

#include "geometry/grid.hpp"

namespace sphinx {

using namespace Geometry;
using namespace types;

//// Forward declarations
namespace operators {

// Forward declaration of FFTW FFT context
template <unsigned int d, bool contiguous, bool interlaced>
class FFTWFFT;

// Forward decalartion of CUDA FFT context
template <unsigned int d, bool contiguous, bool interlaced>
class CUDAFFT;

#if myUSE_MKL
// Forward decalartion of MKL FFT context
template <unsigned int d, bool contiguous, bool interlaced>
class MKLFFT;
#endif // myUSE_MKL

} // END namespace operators

//! \brief Namespace for container-related data.
//!
//! Containers are grid vectors in either time or frequency space.
namespace container {

//! \brief Enum for specification of options of copy.
//!
//! When copying, we can perform a deep copy (Data::Copy),
//! a shallow copy (only pointers) (Data::Share)
//! or only the shape (Data::ShapeOnly).
enum class Data { Copy, Share, ShapeOnly };

} // END namespace container

//! \brief Generic container for multidimensional device or host vector with more degrees of freedom.
//!
//! Takes care of memory allocation and memory layout. Provides basic indexing routines. Initialization
//! must be given by a grid/dof pair or by copy construction.
//!
//! The class is abstract an must be inherited from.
//!
//! \tparam T            Scalar type of the vector.
//! \tparam d            Number of dimension the vector is in.
//! \tparam contiguous   True if data is stored contiguously (only if interlaced is false).
//! \tparam interlaced   True if data the \ref Dofs are interlaced, false otherwise.
template <class T, unsigned int d, bool contiguous, bool interlaced>
class Vec {
public:
    ///////////////////////////////////////////////////////////
    /////////////// MEMBER DATA TYPES AND ALIASES /////////////
    ///////////////////////////////////////////////////////////

    //! Data type pointed by the vector (also referred as "scalar type")
    using data_t = T;
    //! A type for a d-dimensional vector of scalar type.
    using array_t = std::array<T, d>;
    //! Alias for real numbers
    using real_t = double;
    //! Reference type for data_t
    using ref_data_t = T&;

    //! \brief Data structure holding all the \ref Dofs of a vector,
    //! either contiguously or not (scattered).
    //!
    //! Depending on whether the date is store contiguously or not, we use a
    //! pointer or a vector of pointers.
    using container_data_t = typename std::conditional<
        contiguous,
        data_t*,
        std::vector<data_t*>
    >::type;

    //! Grab a reference to template parameter "contiguous"
    static constexpr bool contiguous_data = contiguous;
    //! Grab a reference to template parameter "interlaced"
    static constexpr bool interlaced_data = interlaced;

    //! \brief Grid type for this vector.
    //!
    //! Contains data layout and placement, and the type depends
    //! on whether the data is stored contiguously/interlaced and on
    //! the dimensionality \p d of the problem.
    using grid_t = Grid<d, contiguous, interlaced>;

    //! \brief Alias to expose dimensions "d" to user.
    static constexpr unsigned int dim = d;

    /////////////////////////////////////////////////////////////
    /////////////// CONSTRUCTION AND INITIALISATION /////////////
    /////////////////////////////////////////////////////////////

    //! \brief Standard constructor, takes a grid and dof pair, copies both.
    //!
    //! The grid and dof are required to setup the memory allocation.
    //!
    //! \param[in]  grid_p   A pointer to an underlying grid. Non-owned.
    //! \param[in]  dof      Number of \ref Dofs. of the vector.
    //! \param[in]  name     A name for the vector, should be a unique identifier.
    Vec(const std::shared_ptr<grid_t> grid_p,
        const unsigned int dof, std::string name = "unnamed")
        : grid_p(grid_p), dof(dof), name(name) {
        share_count = std::shared_ptr<int>(new int);
        *share_count = 1;
        init();
    }

    //! \brief Copy constructor (shape only copy).
    //!
    //! Creates a new vector with same shape as \p other.
    //!
    //! The \p other vector is used only to copy the shape.
    //!
    //! \param[in]   other   Vector from which grid information and dofs are copied.
    Vec(const Vec<T,d, contiguous, interlaced> & other)
        : grid_p(other.grid_p), dof(other.dof) {
        share_count = std::shared_ptr<int>(new int);
        *share_count = 1;
        name = other.name; // + "_copy";
        init();
    }

    virtual ~Vec() {}

    //! \brief Initialize vector (called by constructor).
    //!
    //! Non-contiguous variant.
    //!
    //! TODO: make protected
    //! TODO: does nothing, make virtual?
    //!
    //! \tparam contiguous_      Placeholder for SFINAE.
    template <bool contiguous_ = contiguous,
              typename std::enable_if<contiguous_ == true, bool>::type =
              false>
    void init(void) {

    }

    //! \brief Initialize vector (called by constructor).
    //!
    //! Contiguous variant. Makes shure the vector for the data has enough elements.
    //!
    //! TODO: make protected
    //! TODO: make virtual?
    //!
    //! \tparam contiguous_      Placeholder for SFINAE.
    template <bool contiguous_ = contiguous,
              typename std::enable_if<contiguous_ == false, int>::type = 1>
    void init(void) {
        this->_data.resize(this->dof);
    }

    //! \brief Copy vector on the right into vector on the left.
    //!
    //! TODO: Just a placeholder, does nothing.
    //!
    //! \param[in]   other  R.h.s. vector.
    //! \return             A reference to l.h.s.
    Vec<T,d, contiguous, interlaced> & operator=(const Vec<T,d, contiguous,
                                                 interlaced> &) {
//        copy(*this, from);
        return *this;
    }

    //////////////////////////////////////////////
    /////////////// INDEXING MEMBERS /////////////
    //////////////////////////////////////////////

    //! \brief Given the index (along the partitioned domain axis), returns the corresponding rank.
    //!
    //! This routine is used to find out to which processor does the specified grid coordinate belong
    //! to. Since we decompose along 1d slabs, we only require the index along which the
    //! domain is decomposed.
    //! \param    idx   Index for which we want to know the rank (usually along the "k"-index).
    //! \return         Global rank to which the data at \p idx belongs.
    inline int find_rank(int idx) const {
        assert(idx >= 0 && idx < nk && "Index must be >= 0 and < nk!");
        int a = 0;
        int b = this->grid_p->comm.size;
        if( b == 1 ) return 0;
        int m = b / 2;
        while(true) {
//            std::cout << a << " " << b << " " << m << " " << idx << " "
//                      << this->grid_p->comm.rank << " "
//                      << grid_p->partitioning[4*m+2] << " " << grid_p->partitioning[4*(m+1)+2] << std::endl;
            if(m == b-1) return m;
            if(grid_p->partitioning[4*m+2] <= idx &&
                    idx < grid_p->partitioning[4*(m+1)+2] ) return m;
            else if( idx < grid_p->partitioning[4*m+2]) b = m;
            else if( grid_p->partitioning[4*(m+1)+2] <= idx) a = m+1;
            else abort();
            m = a+(b-a)/2;
        }
    }

    //! \brief Read-only accessor for entries of the vector.
    //!
    //! Returns the value of the vector at the dof \p f and at the outer index \p Ix.
    //!
    //! This variant is for the non contiguous layout.
    //!
    //! \see \ref Dofs, \ref indexing
    //!
    //! \tparam    contiguous_ Placeholder for SFINAE.
    //! \param[in] f           dof at which the data is accessed.
    //! \param     Ix          Outer index at which the data is accessed.
    //! \return                Value of this at dof \p f and index \p Ix.
    template <bool contiguous_ = contiguous,
              typename std::enable_if<contiguous_ == false, bool>::type = false>
    //! Operator to access to the data, need a dof index and position index (1D)
    inline data_t operator()(unsigned int f, size_t Ix) const {
        assert(f < this->dof && Ix < this->size && "Array access out of bounds");
        return _data[f][Ix];
    }

    //! \brief Read/write accessor for entries of the vector.
    //!
    //! Returns the reference of the entry of the vector at the dof \p f and at the outer index \p Ix.
    //!
    //! This variant is for the non contiguous layout.
    //!
    //! \see \ref Dofs, \ref indexing
    //!
    //! \tparam    contiguous_ Placeholder for SFINAE.
    //! \param[in] f           dof at which the data is accessed.
    //! \param     Ix          Outer index at which the data is accessed.
    //! \return                Reference to the value of this at dof \p f and index \p Ix.
    template <bool contiguous_ = contiguous,
              typename std::enable_if<contiguous_ == false, bool>::type = false>
    inline ref_data_t operator()(unsigned int f, size_t Ix) {
        assert(f < this->dof && Ix < this->size && "Array access out of bounds");
        return _data[f][Ix];
    }

    //! \brief Read-only accessor for entries of the vector.
    //!
    //! Returns the value of the vector at the dof \p f and at the outer index \p Ix.
    //!
    //! This variant is for the contiguous and non-interlaced layout.
    //!
    //! \see \ref Dofs, \ref indexing
    //!
    //! \tparam    contiguous_ Placeholder for SFINAE.
    //! \param[in] f           dof at which the data is accessed.
    //! \param     Ix          Outer index at which the data is accessed.
    //! \return                Value of this at dof \p f and index \p Ix.
    template <bool contiguous_ = contiguous,
              typename std::enable_if<contiguous_ == true &&
                                      interlaced == false, bool>::type = true>
    inline data_t operator()(unsigned int f, size_t Ix) const {
        assert(f < this->dof && Ix < this->size && "Array access out of bounds");
        return _data[f*this->size+Ix];
    }

    //! \brief Read/write accessor for entries of the vector.
    //!
    //! Returns the reference of the entry of the vector at the dof \p f and at the outer index \p Ix.
    //!
    //! This variant is for the contiguous and non-interlaced layout.
    //!
    //! \see \ref Dofs, \ref indexing
    //!
    //! \tparam    contiguous_ Placeholder for SFINAE.
    //! \param[in] f           dof at which the data is accessed.
    //! \param     Ix          Outer index at which the data is accessed.
    //! \return                Reference to the value of this at dof \p f and index \p Ix.
    template <bool contiguous_ = contiguous,
              typename std::enable_if<contiguous_ == true &&
                                      interlaced == false, bool>::type = true>
    //! Operator to access to the reference to the data,
    //! need a dof index and position index (1D)
    inline ref_data_t operator()(unsigned int f, size_t Ix) {
        assert(f < this->dof && Ix < this->size && "Array access out of bounds");
        return _data[f*this->size+Ix];
    }

    //! \brief Read-only accessor for entries of the vector.
    //!
    //! Returns the value of the vector at the dof \p f and at the outer index \p Ix.
    //!
    //! This variant is for the contiguous and interlaced layout.
    //!
    //! \see \ref Dofs, \ref indexing
    //!
    //! \tparam    contiguous_ Placeholder for SFINAE.
    //! \param[in] f           dof at which the data is accessed.
    //! \param     Ix          Outer index at which the data is accessed.
    //! \return                Value of this at dof \p f and index \p Ix.
    template <bool interlaced_ = interlaced,
              typename std::enable_if<interlaced_ == true, int>::type = 1>
    //! Operator to access to the data, need a dof index and position index (1D)
    inline data_t operator()(unsigned int f, size_t Ix) const {
        assert(f < this->dof && Ix < this->size && "Array access out of bounds");
        return _data[this->dof*Ix+f];
    }

    //! \brief Read/write accessor for entries of the vector.
    //!
    //! Returns the reference of the entry of the vector at the dof \p f and at the outer index \p Ix.
    //!
    //! This variant is for the contiguous and interlaced layout.
    //!
    //! \see \ref Dofs, \ref indexing
    //!
    //! \tparam    contiguous_ Placeholder for SFINAE.
    //! \param[in] f           dof at which the data is accessed.
    //! \param     Ix          Outer index at which the data is accessed.
    //! \return                Reference to the value of this at dof \p f and index \p Ix.
    template <bool interlaced_ = interlaced,
              typename std::enable_if<interlaced_ == true, int>::type = 1>
    //! Operator to access to the reference to the data,
    //! need a dof index and position index (1D)
    inline ref_data_t operator()(unsigned int f, size_t Ix) {
        assert(f < this->dof && Ix < this->size && "Array access out of bounds");
        return _data[this->dof*Ix+f];
    }

//protected:
    //! \brief Generic indexing mapping 2D coordinates (k,j) to a 1D index.
    //!
    //! Given the \ref logical-modes, it returns the \ref outer-index.
    //! Transforms a \f$2\f$-dimensional loop index into a 1-dimensional index.
    //!
    //! \param[in] l_modes   Current logical mode.
    //! \return              Outer index corresponding to the \p l_modes.
    inline size_t outerIndex(const pos_t<2> & l_modes) const {
        return nj * l_modes[0] + l_modes[1];
    }

    //! \brief Generic indexing mapping 3D coordinates (k,j) to a 1D index.
    //!
    //! Given the \ref logical-modes, it returns the \ref outer-index.
    //! Transforms a \f$3\f$-dimensional loop index into a 1-dimensional index.
    //!
    //! \param[in] l_modes   Current logical mode.
    //! \return              Outer index corresponding to the \p l_modes.
    inline size_t outerIndex(const pos_t<3> & l_modes) const {
        return ni * (nj * l_modes[0] +  l_modes[1]) + l_modes[2];
    }

    //! \brief Generic indexing mapping 2D coordinates (k,j) of another vector to a 1D index for this vector.
    //!
    //! This is for obtaining the \ref outer-index of this vector, but assuming that the \ref logical-modes
    //! are the ones of a second vector \p other.
    //!
    //! That is, retrieve the outer index that need to be applied to
    //! *this (which assumed to be the bigger vector) in order to obtain the correct value from evaluation
    //! operators.
    //!
    //! In this case we need to take care of some transformation. In particular we need to take care
    //! of padding and domain decomposition.
    //!
    //! 2D version.
    //!
    //! \warning Works only frequency domain!!!!!
    //!
    //! \see \ref outerIndex.
    //!
    //! \tparam S               Type of the other vector, should be similar (or the same as type of this).
    //! \param[in] l_modes      The logical modes of the vector \p S.
    //! \param[in] other        The other vector, for which \p l_modes are
    //!                         the logical modes. Assumed to be smaller than *this.
    //! \return                 The outer index for *this* vector.
    template <class S>
    inline size_t outerIndexRelative(const pos_t<2> & l_modes,
                                     const S & other) const {
        return nj * (l_modes[0])
        // in 2D this dimension is the one truncated (i.e. with real padding)
                + (l_modes[1] < other.nj/2+1 ?
                    l_modes[1] : l_modes[1] + nj - other.nj);
    }

    //! \brief Generic indexing mapping 3D coordinates (k,j,i) of another vector to a 1D index for this vector.
    //!
    //! This is for obtaining the \ref outer-index of this vector, but assuming that the \ref logical-modes
    //! are the ones of a second vector \p other.
    //!
    //! That is, retrieve the outer index that need to be applied to
    //! *this (which assumed to be the bigger vector) in order to obtain the correct value from evaluation
    //! operators.
    //!
    //! In this case we need to take care of some transformation. In particular we need to take care
    //! of padding and domain decomposition.
    //!
    //! 3D version.
    //!
    //! \warning Works only frequency domain!!!!!
    //!
    //! \see \ref outerIndex.
    //!
    //! \tparam S               Type of the other vector, should be similar (or the same as type of this).
    //! \param[in] l_modes      The logical modes of the vector \p S.
    //! \param[in] other        The other vector, for which \p l_modes are
    //!                         the logical modes. Assumed to be smaller than *this.
    //! \return                 The outer index for *this* vector.
    template <class S>
    inline size_t outerIndexRelative(const pos_t<3> & l_modes, const S & other) const {
        return  ni * ( nj * l_modes[0]
        // the buffer already takes into account splitting
        +  (l_modes[1] < other.nj/2+1 ?
        l_modes[1] : l_modes[1] + nj - other.nj))
        + l_modes[2];
    } // FIXME: ni already divided

    //! \brief Generic indexing mapping 2D coordinates (k,j)
    //! relative to this to a 1D index for the receiving padding buffer.
    //!
    //! The receviging padding buffer is the member recvbuffer of type buffer_t of the class \ref redistribute:
    //! this class is the class
    //! which had received data from \p other and has overall a similar shape as \p other).
    //!
    //! This is for obtaining the \ref outer-index used to loop over the receiving buffer for the zero-padding,
    //! assuming that the \ref logical-modes
    //! are the ones of this.
    //!
    //! The data layout in the receiving buffer follows the data layout of \p other (tough it is different
    //! as the data had to be redistributed across many ranks).
    //!
    //! That is, we retrieve the outer index that need to be applied to the receiving buffer for the
    //! zero-padding (following the layout of \p other, which assumed to be the bigger vector)
    //! in order to obtain the correct value from evaluation
    //! operators. The logical indices are assumed to the ones of this.
    //!
    //! In this case we need to take care of some transformation. In particular we need to take care
    //! of padding and domain decomposition.
    //!
    //! 2D version.
    //!
    //! \warning Works only frequency domain and correctly sized \ref redistribute!!!!!
    //!
    //! \see \ref zero_padding_kern, \ref outerIndex, \ref redistribute.
    //!
    //! \tparam S               Type of the other vector, should be smaller than *this.
    //! \param[in] l_modes      The current logical modes of *this.
    //! \param[in] other        The other vector. Assumed to be smaller than *this.
    //! \return                 The outer index for accessing the receiving buffer.
    template <class S>
    inline size_t outerIndexPaddingBuffer(const pos_t<2> & l_modes,
                                          const S & other) const {
        return other.nj * (l_modes[0])
        // in 2D this dimension is the one truncated (i.e. with real padding)
                + l_modes[1];
    }

    //! \brief Generic indexing mapping 3D coordinates (k,j,i)
    //! relative to this to a 1D index for the receiving padding buffer.
    //!
    //! The receviging padding buffer is the member recvbuffer of type buffer_t of the class \ref redistribute:
    //! this class is the class
    //! which had received data from this and has the same shape as \p other).
    //!
    //! This is for obtaining the \ref outer-index used to loop over the receiving buffer for the zero-padding,
    //! assuming that the \ref logical-modes
    //! are the ones of this.
    //!
    //! The data layout in the receiving buffer follows the data layout of \p other.
    //!
    //! That is, we retrieve the outer index that need to be applied to the receiving buffer for the
    //! zero-padding (following the layout of \p other, which assumed to be the bigger vector)
    //! in order to obtain the correct value from evaluation
    //! operators. The logical indices are assumed to the ones of this.
    //!
    //! In this case we need to take care of some transformation. In particular we need to take care
    //! of padding and domain decomposition.
    //!
    //! 3D version.
    //!
    //! \warning Works only frequency domain and a correctly sized \ref redistribute!!!!!
    //!
    //! \see \ref zero_padding_kern, \ref outerIndex, \ref redistribute.
    //!
    //! \tparam S               Type of the other vector, should be smaller than *this.
    //! \param[in] l_modes      The current logical modes of *this.
    //! \param[in] other        The other vector. Assumed to be smaller than *this.
    //! \return                 The outer index for accessing the receiving buffer.
    template <class S>
    inline size_t outerIndexPaddingBuffer(const pos_t<3> & l_modes,
                                          const S & other) const {
        return other.ni * (other.nj * (l_modes[0])
                + l_modes[1]) + l_modes[2];
    }

    //! \brief Generic indexing mapping 2D coordinates (k,j)
    //! relative to this to a 1D index for the receiving truncation buffer.
    //!
    //! The receivging truncate buffer is the member recvbuffer of type buffer_t of the class \ref redistribute:
    //! this class is the class
    //! which had received data from \p other and has a similar shape as \p other).
    //!
    //! This is for obtaining the \ref outer-index used to loop over the receiving buffer for the truncation,
    //! assuming that the \ref logical-modes
    //! are the ones of *this.
    //!
    //! The data layout in the receiving buffer follows, overall, the data layout of \p other.
    //!
    //! That is, we retrieve the outer index that need to be applied to the receiving buffer for the
    //! truncation (following the layout of \p other, which assumed to be the smaller vector)
    //! in order to obtain the correct value from evaluation
    //! operators. The logical indices are assumed to the ones of *this.
    //!
    //! In this case we need to take care of some transformation. In particular we need to take care
    //! of padding and domain decomposition.
    //!
    //! 2D version.
    //!
    //! \warning Works only frequency domain and a correctly sized \ref redistribute!!!!!
    //!
    //! \see \ref truncate_kern, \ref outerIndex, \ref redistribute.
    //!
    //! \tparam S               Type of the other vector, should be smaller than *this.
    //! \param[in] l_modes      The current logical modes of *this.
    //! \param[in] other        The other vector. Assumed to be bigger than *this.
    //! \return                 The outer index for accessing the receiving buffer.
    template <class S>
    inline size_t outerIndexTruncateBuffer(const pos_t<2> & l_modes,
                                           const S & other) const {
        return other.nj * l_modes[0]
        // in 2D this dimension is the one truncated (i.e. with real padding)
                + (l_modes[1] < nj/2+1 ? l_modes[1] : l_modes[1]
                + other.nj - nj);
    }

    //! \brief Generic indexing mapping 3D coordinates (k,j,i)
    //! relative to this to a 1D index for the receiving truncation buffer.
    //!
    //! The receivging truncate buffer is the member recvbuffer of type buffer_t of the class \ref redistribute:
    //! this class is the class
    //! which had received data from \p other and has a similar shape as \p other).
    //!
    //! This is for obtaining the \ref outer-index used to loop over the receiving buffer for the truncation,
    //! assuming that the \ref logical-modes
    //! are the ones of *this.
    //!
    //! The data layout in the receiving buffer follows, overall, the data layout of \p other.
    //!
    //! That is, we retrieve the outer index that need to be applied to the receiving buffer for the
    //! truncation (following the layout of \p other, which assumed to be the smaller vector)
    //! in order to obtain the correct value from evaluation
    //! operators. The logical indices are assumed to the ones of *this.
    //!
    //! In this case we need to take care of some transformation. In particular we need to take care
    //! of padding and domain decomposition.
    //!
    //! 3D version.
    //!
    //! \warning Works only frequency domain and a correctly sized \ref redistribute!!!!!
    //!
    //! \see \ref truncate_kern, \ref outerIndex, \ref redistribute.
    //!
    //! \tparam S               Type of the other vector, should be smaller than *this.
    //! \param[in] l_modes      The current logical modes of *this.
    //! \param[in] other        The other vector. Assumed to be bigger than *this.
    //! \return                 The outer index for accessing the receiving buffer.
    template <class S>
    inline size_t outerIndexTruncateBuffer(const pos_t<3> & l_modes,
                                           const S & other) const {
        return other.ni * (other.nj * l_modes[0]
                +  (l_modes[1] < nj/2+1 ? l_modes[1] : l_modes[1] + other.nj - nj))
                    + (l_modes[2]);
    }

    /////////////////////////////////////////////////////
    //////////// DEBUGGING METHODS //////////////////////
    /////////////////////////////////////////////////////

    //! \brief Debug methods to print data of the vector.
    //!
    //! Dumps all the data in a human readable manner. Used only for debugging purposes.
    void spy(void) {
        for(unsigned int f = 0; f < dof; ++f) {
//            std::cout << "NEW DOF" << std::endl;
//            for(unsigned int k = 0; k < kend; ++k) {
//                std::cout << "NEW RANK 3" << std::endl;
            for(unsigned int k = 0; k < kend; ++k) {
                for(unsigned int j = 0; j < jend; ++j) {
                    pos_t<2> I = {k,j};
//                        std::cout << (*this)(f, this->outerIndex(I)) << " ";
                }
//                    std::cout << std::endl;
            }
//            }
        }
    }

public: // TODO privatize:
    /////////////////////////////////////////////////
    //////////// INTERNAL DATA //////////////////////
    /////////////////////////////////////////////////

    //! Data may be storeed scaled (cf. FFT), this keeps track of it
    real_t scaling_factor = 1;

    //! Number of elements of type T allocated
    size_t size;
    //! Size of vector space T w.r.t to scalar type (i.e.
    //! complex has dim 2 on reals etc.)
    const size_t data_size = sizeof(typename Vec<T,d, contiguous,
                                    interlaced>::data_t) / sizeof(scalar_t);
    //! Pointer to grid, shared with other Vec's, not owned
    const std::shared_ptr<grid_t> grid_p;
    const unsigned int dof;
    //! Start of first index (the one which is split
    //! amongst different processors)
    index_t kstart;
    index_t jstart, istart; // ONLY ACCFFT distribution
    //! Sizes of the loops (slower first)
    index_t kend, jend, iend;
    //! Local sizes of data
    index_t ksize, jsize, isize;
    //! Actual size of the data
    index_t nk, nj, ni;

    //! Human readable name of the vector
    std::string name = "unknown";

    //! Return pointer to the data, used for backward compatibility
    container_data_t data(void) const { return _data; }
    //! Pointer to each dof of the vector
    container_data_t _data;

//    Vec<T,d>& operator=(const Vec<T,d> & from);

    int parpad = -1;

    //! Points to a counter determining how many shared instances there are
    std::shared_ptr<int> share_count;
protected:
};

} // END namespace sphinx
