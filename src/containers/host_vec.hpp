/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "vec.hpp"

//! Virtual derived of Vec, representing a Vector in the host memory
template <class T, unsigned int d,
          bool contiguous, bool interlaced>
class HostVec : public virtual Vec<T, d, contiguous, interlaced> {
public:
    HostVec(const std::shared_ptr<Grid<d, contiguous, interlaced>> grid_p,
            const unsigned int dof, std::string name = "unknown")
        : Vec<T, d, contiguous, interlaced>(grid_p, dof, name) {}

    HostVec(const HostVec & other)
        : Vec<T, d, contiguous, interlaced>(other) {}

    virtual ~HostVec() {}

    HostVec<T,d, contiguous, interlaced>&
    operator-=(const HostVec<T,d, contiguous, interlaced> & right);

    HostVec<T,d, contiguous, interlaced>&
    operator+=(const HostVec<T,d, contiguous, interlaced> & right);

    HostVec<T,d, contiguous, interlaced>&
    operator=(const HostVec<T,d, contiguous, interlaced> & from);


    static const FFTType fft_type;
};

template <class T, unsigned int d,
          bool contiguous, bool interlaced>
const FFTType HostVec<T, d, contiguous, interlaced>::fft_type = MPIFFT;

template <class T, unsigned int d,
          bool contiguous, bool interlaced>
HostVec<T,d, contiguous, interlaced>&
HostVec<T,d, contiguous, interlaced>::operator=(
        const HostVec<T,d, contiguous, interlaced> & from) {
    copy(from, *this);
    return *this;
}
