/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "config.hpp"
#include "vec.hpp"

#include "utilities/types.hpp"
#include "utilities/cutilities.hpp"
#include "utilities/cuda_tools.hpp"

#include <cuda_runtime.h>
#include <cufft.h>

//! Virtual derived of Vec, representing a Vector in the device memory
template <class T, unsigned int d, bool contiguous, bool interlaced>
class DeviceVec : public virtual Vec<T, d, contiguous, interlaced> {
public:
    static const unsigned int dim = d;

    DeviceVec(const std::shared_ptr<Grid<d, contiguous, interlaced>> grid_p,
              unsigned int dof, std::string name = "unknown")
        : Vec<T, d, contiguous, interlaced>(grid_p, dof, name) {
        init();
    }

    void init(void) {
        static_assert(contiguous == false && interlaced == false, "Not yet implemented!");
        gpuErrchk((cudaMalloc(&device_data, Vec<T, d, contiguous, interlaced>::dof*sizeof(T*))));
        gpuErrchk((cudaMalloc(&device_size, dim*sizeof(unsigned int))));
        gpuErrchk((cudaMalloc(&device_size_pad, dim*sizeof(unsigned int))));
        gpuErrchk((cudaMalloc(&local_start, dim*sizeof(unsigned int))));
        gpuErrchk((cudaMalloc(&local_size, dim*sizeof(unsigned int))));
    }

    virtual ~DeviceVec() {
        gpuErrchk(cudaFree(device_data));
        gpuErrchk(cudaFree(device_size));
        gpuErrchk(cudaFree(device_size_pad));
        gpuErrchk(cudaFree(local_start));
        gpuErrchk(cudaFree(local_size));
    }

    DeviceVec<T,d, contiguous, interlaced>& operator-=(
            const DeviceVec<T,d, contiguous, interlaced> & right);

    DeviceVec<T,d, contiguous, interlaced>& operator+=(
            const DeviceVec<T,d, contiguous, interlaced> & right);

    DeviceVec<T,d, contiguous, interlaced>& operator=(
            const DeviceVec<T,d, contiguous, interlaced> & from);

    static const FFTType fft_type;

public:
    T **device_data;

    unsigned int* device_size;
    unsigned int* device_size_pad;
    unsigned int* local_start;
    unsigned int* local_size;

    dim3 dimBlock;
    dim3 dimGrid;

//    int modes_pad_thr, modes_pad_blk;
};

template <class T, unsigned int d, bool contiguous, bool interlaced>
DeviceVec<T,d, contiguous, interlaced>&
DeviceVec<T,d, contiguous, interlaced>::operator=(const DeviceVec<T,d, contiguous, interlaced> & from) {
    copy(from, *this);
    return *this;
}

template <class T, unsigned int d, bool contiguous, bool interlaced>
const FFTType DeviceVec<T, d, contiguous, interlaced>::fft_type = GPUFFT;
