/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

//! \file time_vec.hpp
//! \brief File containing the definitions of the \ref sphinx::TimeVec class.
//!
//!
//!
//! \author Filippo Leonardi
//! \date 2017-01-01

#include "vec.hpp"

//! \brief Virtual derived of Vec representing a Vector in time-space.
//!
//!
//!
//! \tparam T            Scalar type of the vector.
//! \tparam d            Number of dimension the vector is in.
//! \tparam contiguous   True if data is stored contiguously (only if interlaced is false).
//! \tparam interlaced   True if data the \ref Dofs are interlaced, false otherwise.
template <class T, unsigned int d, bool contiguous, bool interlaced>
class TimeVec : public virtual Vec<T, d, contiguous, interlaced> {
public:
    //! \brief
    //!
    //!
    //!
    //! \param grid_p
    //! \param dof
    //! \param name
    TimeVec(const std::shared_ptr<Grid<d, contiguous, interlaced>> grid_p,
            const unsigned int dof, std::string name = "unknown")
        : Vec<T, d, contiguous, interlaced>(grid_p, dof, name) {
        Vec<T, d, contiguous, interlaced>::parpad = 0;
    }

    //! \brief
    //!
    //!
    //!
    //! \param other
    TimeVec(const TimeVec & other)
        : Vec<T, d, contiguous, interlaced>(other) {
        Vec<T, d, contiguous, interlaced>::parpad = 0;
    }

    virtual ~TimeVec() {}

    //! \brief
    //!
    //!
    void init(void) {
#if USE_ACCFFT
        this->kstart = this->grid_p->time_local_start[0];
        this->jstart = this->grid_p->time_local_start[1];
        this->istart = this->grid_p->time_local_start[2];
        this->ksize = this->grid_p->time_local_size[0];
        this->jsize = this->grid_p->time_local_size[1];
        this->isize = this->grid_p->time_local_size[2];
        this->kend = this->grid_p->time_local_size[0] + this->kstart;
        this->jend = this->grid_p->time_local_size[1] + this->jstart;
        this->iend = this->grid_p->time_local_size[2] + this->istart;
#else
        this->kend = this->grid_p->local_n0;
        this->jend = this->grid_p->timeN[1];
        this->iend = this->grid_p->timeN[2];
        this->ksize = this->grid_p->local_n0 + this->grid_p->local_0_start;
        this->jsize = this->grid_p->timeN[1];
        this->isize = this->grid_p->timeN[2];
        //0; // Maybe wrong? Is this used?
        this->kstart = this->grid_p->local_0_start;
        this->jstart = 0;
        this->istart = 0;
#endif
        this->nk = this->grid_p->timeNpad[0];
        this->nj = this->grid_p->timeNpad[1];
        this->ni = this->grid_p->timeNpad[2];

        this->size = this->grid_p->alloc_time;

        std::stringstream ss;
        ss << this->kstart << "-" << this->kend << "." << this->nk << ","
           << this->jstart << "-" << this->jend << "." << this->nj << ","
           << this->istart << "-" << this->iend << "." << this->ni << ","
           << "ALLOC:" << this->size;

        debug::verbose(ss.str(), mpi::self);
    }


    inline pos_t<2> getRealModes(const pos_t<2> & l_modes) const {
         return l_modes;
     }

     inline pos_t<3> getRealModes(const pos_t<3> & l_modes) const {
         return l_modes;
     }
};

