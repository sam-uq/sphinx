/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "operators/operators.hpp"

#if USE_CUDA

#  include "operators/device_functions/device_functions.hpp"

#endif // USE_CUDA

//! \brief Class that handles cached transfer between host and device.
//!
//!
//! \tparam HostT
//! \tparam DeviceT
template <class HostT, class DeviceT>
class HostDeviceVector {
public:
    //! Alias for host vector type
    using Host = HostT;
    //! Alias for device vector type
    using Device = DeviceT;
    //! Pointer to host vector type
    using Host_p = std::shared_ptr<Host>;
    //! Pointer to device vector type
    using Device_p = std::shared_ptr<Device>;

    //! Set container for host
    void setHost(Host_p H_) { H = H_; device_uptodate=false; }

    //! Set contianer for device
    void setDevice(Device_p D_) { D = D_; device_uptodate=true; }

    HostDeviceVector() = default;

    virtual ~HostDeviceVector() = default;


    HostDeviceVector(const HostDeviceVector<HostT,DeviceT> & other) {
        if( other.H ) H = Host_p(new Host(*other.H));
        if( other.D ) D = Device_p(new Device(*other.D));
    }

    HostDeviceVector<HostT, DeviceT>& operator=(const HostDeviceVector<HostT,DeviceT> &) = delete;

    Host_p getReadHost() {
        if( device_uptodate ) toHost();
        return getHost();
    }

    Device_p getReadDevice() {
        if( !device_uptodate ) toDevice();
        return getDevice();
    }

    Host_p getWriteHost() {
        device_uptodate = false;
        return getHost();
    }

    Device_p getWriteDevice() {
        device_uptodate = true;
        return getDevice();
    }

    Host_p getReadWriteHost() {
        if( !device_uptodate ) toHost();
        device_uptodate = false;
        return getHost();
    }

    Device_p getReadWriteDevice() {
        if( !device_uptodate ) toDevice();
        device_uptodate = true;
        return getDevice();
    }

    void toDevice(void) {
        device_uptodate = true;
//        assert((D && H) && "Must have both containers!");
        getHost();
        getDevice();
        sphinx::operators::HostToDevice(*H, *D);
    }

    void toHost(void) {
        device_uptodate = false;
//        assert((D && H) && "Must have both containers!");
        getHost();
        getDevice();
        sphinx::operators::DeviceToHost(*D, *H);
    }

    bool device_uptodate = false;

    operator Host&(void) { return *getReadWriteHost(); }

    operator const Host&(void) { return *getReadHost(); }

    template <typename Host_ = Host, typename std::enable_if<!std::is_same<Host_,Device>::value, bool>::type = true>
    operator Device&(void) { return *getReadWriteDevice(); }

    template <typename Host_ = Host, typename std::enable_if<!std::is_same<Host_,Device>::value, bool>::type = true>
    operator const Device&(void) { return *getReadDevice(); }
private:
    template <typename Host_ = Host>
    typename std::enable_if<std::is_same<Host_,Device>::value, Host_p>::type getHost() {
        assert((D || H) && "Must have one of the two containers!");
        if(H) return H;
        else if(D) {
            H = D;
            return H;
        }
        return nullptr;
    }

    template <typename Host_ = Host>
    typename std::enable_if<!std::is_same<Host_,Device>::value, Host_p>::type getHost() {
        assert((D || H) && "Must have one of the two containers!");
        if(H) return H;
        else if(D) {
            H = Host_p(new Host(*D));
            return H;
        }
        return nullptr;
    }

    template <typename Host_ = Host>
    typename std::enable_if<std::is_same<Host_,Device>::value, Device_p>::type getDevice() {
        assert((D || H) && "Must have one of the two containers!");
        if(D) return D;
        else if(H) {
            D = H;
            return D;
        }
        return nullptr;
    }

    template <typename Host_ = Host>
    typename std::enable_if<!std::is_same<Host_,Device>::value, Device_p>::type getDevice() {
        assert((D || H) && "Must have one of the two containers!");
        if(D) return D;
        else if(H) {
            D = Device_p(new Device(*H));
            return D;
        }
        return nullptr;
    }

    Host_p H;
    Device_p D;
};
