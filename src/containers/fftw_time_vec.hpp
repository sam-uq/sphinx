/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

//! \file fttw_time_vec.hpp
//! \brief File containing the definitions of the \ref sphinx::FFTWTimeVec class.
//!
//!
//!
//! \author Filippo Leonardi
//! \date 2017-01-01

#include "config.hpp"

#include "vec.hpp"

#include "utilities/include_fftw.hpp"

#include "host_vec.hpp"
#include "time_vec.hpp"

#include "operators/operators.hpp"

#if USE_CUDA
#  include "operators/device_functions/device_operators.hpp"
#endif // USE_CUDA

#include "operators/host_functions/host_operators.hpp"

#include "fftw_freq_vec.hpp"

//! \brief A "time" space vector that performs FFTs on the CPU using fftw.
//!
//! This type of vector takes care of aligned allocation, correct storage and
//! layout of the data, such that the data can be readily used by FFTW.
//!
//! \tparam d           Number of dimensions.
//! \tparam contiguous  true if the data is stored contiguously (false only in not interlaced)
//! \tparam interlaced  true if the data is stored interlaced
template <unsigned int d, bool contiguous, bool interlaced>
class FFTWTimeVec :
        public HostVec<fftw_time_t, d, contiguous, interlaced>,
        public TimeVec<fftw_time_t, d, contiguous, interlaced> {
public:
    //! Alias for the corresponding frequency vector.
    using FreqVec = FFTWFreqVec<d, contiguous, interlaced>;

    //! Alias for base class
    using base_t = Vec<fftw_time_t, d, contiguous, interlaced>;

    //! Alias for corresponding FFT class.
#if myUSE_MKL
    using FFT_t = operators::MKLFFT<d, contiguous, interlaced>;
#else
    using FFT_t = operators::FFTWFFT<d, contiguous, interlaced>;
#endif

    /////////////////////////////////////////////////////////////
    /////////////// CONSTRUCTION AND INITIALISATION /////////////
    /////////////////////////////////////////////////////////////

    //! \brief Builds and initialise a vector compatible with the given grid.
    //!
    //! The constructor also allocates the memory according to data layout,
    //! grid information, \ref Dofs and type of FFT.
    //!
    //! \param[in] grid_p  Non-owned pointer to geometry.
    //! \param[in] dof     Number of \ref Dofs.
    //! \param[in] name    Unique name for I/O and referencing.
    FFTWTimeVec(const std::shared_ptr<Grid<d, contiguous, interlaced>> grid_p,
                const unsigned int dof, std::string name = "unknown")
        : Vec<fftw_time_t, d, contiguous, interlaced>(grid_p, dof, name)
        , HostVec<fftw_time_t, d, contiguous, interlaced>(grid_p, dof, name)
        , TimeVec<fftw_time_t, d, contiguous, interlaced>(grid_p, dof, name) {
        init();
    }

    //! \brief Special copy constructor that initializes the memory and (potentially) the data.
    //!
    //! There are three options of copy, specified by \ref container::Data specified via \p copy_type:
    //!  - ShapeOnly: copies only the shape of the data;
    //!  - Share: shallow copy of pointers to data: data will be shared and counted amongst vectors;
    //!  - Copy: copies the data, as well as the geometry (deep copy).
    //!
    //! \param[in] other       Vector to copy.
    //! \param[in] copy_type   Type of copy to perform
    FFTWTimeVec(const FFTWTimeVec<d, contiguous, interlaced> & other,
                sphinx::container::Data copy_type =
            sphinx::container::Data::Copy)
        : Vec<fftw_time_t, d, contiguous, interlaced>(other)
        , HostVec<fftw_time_t, d, contiguous, interlaced>(other)
        , TimeVec<fftw_time_t, d, contiguous, interlaced>(other) {

        switch(copy_type) {
            case sphinx::container::Data::Copy:
            default:
                init();
                *this = other;
                break;
            case sphinx::container::Data::Share:
                this->share(other);
                break;
        }
    }

#if USE_CUDA
    //! \brief Copy data from a Device vector.
    //!
    //!
    //! \tparam    T           Scalar type of other vector.
    //! \param[in] other       Vector to copy.
    template <class T>
    FFTWTimeVec(const DeviceVec<T, d, contiguous, interlaced> & other)
        : Vec<fftw_time_t, d, contiguous, interlaced>(other)
        , HostVec<fftw_time_t, d, contiguous, interlaced>(
              other.grid_p, other.dof)
        , TimeVec<fftw_time_t, d, contiguous, interlaced>(
              other.grid_p, other.dof) {
        init();
        sphinx::operators::DeviceToHost(other, *this);
    }
#endif // USE_CUDA

    //! \brief Special copy constructor that initializes the memory
    //! and (potentially) the data from a frequency vector.
    //!
    //! Creates a new vector that grabs information from another vector, but in frequency space.
    //! Data can be shared, copied or created anew. Beware that sharing data amongs time and frequency
    //! vectors means you are sharing two different data layouts (data cannot be read from another
    //! frequency vector in a meaningful way). Only the data buffer is shared.
    //!
    //! See \ref container::Data and \ref FFTWTimeVec.
    //!
    //! \param[in] other       Vector to copy.
    //! \param[in] copy_type   Type of copy to perform
    FFTWTimeVec(const FreqVec & other,
                sphinx::container::Data copy_type =
            sphinx::container::Data::ShapeOnly)
        : Vec<fftw_time_t, d, contiguous, interlaced>(
              other.grid_p, other.dof)
        , HostVec<fftw_time_t, d, contiguous, interlaced>(
              other.grid_p, other.dof)
        , TimeVec<fftw_time_t, d, contiguous, interlaced>(
              other.grid_p, other.dof) {

        switch(copy_type) {
        case sphinx::container::Data::ShapeOnly:
            init();
            break;
        case sphinx::container::Data::Share:
            this->share(other);
            break;
        case sphinx::container::Data::Copy:
            debug::error("Nonsense copy data from freq to tome!",
                         this->grid_p->comm);
            break;
        }
    }

    ~FFTWTimeVec() { dealloc(); }

    //! \brief Share data buffer between another vector. Interlaced version.
    //!
    //! Keeps internal counter of number of sharers for memory cleanup.
    //!
    //! \tparam     T             Type of other vector.
    //! \tparam     interlaced_   Alias for SFINAE enabling.
    //! \param[in]  other         Vector which will be shared with this.
    template <class T,
              bool interlaced_ = interlaced, typename std::enable_if<interlaced_ == true, bool>::type = true>
    void share(const HostVec<T, d, contiguous, interlaced> & other) {
        this->share_count = other.share_count;
        ++*(this->share_count);

        this->_data = reinterpret_cast<fftw_time_t*>(other._data);
    }

    //! \brief Share data buffer between another vector. Deinterlaced version.
    //!
    //! Keeps internal counter of number of sharers for memory cleanup.
    //!
    //! \tparam     T             Type of other vector.
    //! \tparam     interlaced_   Alias for SFINAE enabling.
    //! \param[in]  other         Vector which will be shared with this.
    template <class T,
              bool interlaced_ = interlaced, typename std::enable_if<interlaced_ == false, int>::type = 1>
    void share(const HostVec<T, d, contiguous, interlaced> & other) {
        this->share_count = other.share_count;
        ++*(this->share_count);

        for(unsigned int f = 0; f < this->dof; ++f) {
            this->_data.at(f) = reinterpret_cast<fftw_time_t*>(other._data.at(f));
        }
    }

    //! \breif Initialize memory of the vector. Interlaced variant.
    //!
    //! Calls FFTW or MKL aligned allocators to create sufficient storage for the
    //! vector data.
    //!
    //! \tparam interlaced_  Alias for SFINAE.
    template <bool interlaced_ = interlaced,
              typename std::enable_if<interlaced_ == true, bool>::type = true>
    void init(void) {
        static_assert(contiguous == true, "Not possible to have uncontiguous data and interlaced!");

        TimeVec<fftw_time_t, d, contiguous, interlaced>::init();

#if myUSE_MKL
        this->_data = (double*) malloc(this->dof * this->size * sizeof(double));
#else
        this->_data = fftw_alloc_real(this->dof * this->size);
#endif
    }

    //! \breif Initialize memory of the vector. Deinterlaced variant.
    //!
    //! Calls FFTW or MKL aligned allocators to create sufficient storage for the
    //! vector data.
    //!
    //! \tparam interlaced_  Alias for SFINAE.
    template <bool interlaced_ = interlaced,
              typename std::enable_if<interlaced_ == false, int>::type = 1>
    void init(void) {
        static_assert(contiguous == false, "Not yet implemented!");

        TimeVec<fftw_time_t, d, contiguous, interlaced>::init();

        for(auto& dt: this->_data) {
#if myUSE_MKL
            dt = (double*) malloc(this->size * sizeof(double));
#else
            dt = fftw_alloc_real(this->size);
#endif
        }
    }

    //! \brief Called only by destructor, destroy all data. Interlaced variant.
    //!
    //! Frees up memory (except if shared and positive count).
    //!
    //! \tparam interlaced_  Alias for SFINAE.
    template <bool interlaced_ = interlaced, typename std::enable_if<interlaced_ == true, bool>::type = true>
    void dealloc() {
        if( *(this->share_count) > 1 ) return;
        else --*(this->share_count);

#if myUSE_MKL
        free(this->_data);
#else
        fftw_free(this->_data);
#endif
    }

    //! \brief Called only by destructor, destroy all data. Deinterlaced variant.
    //!
    //! Frees up memory (except if shared and positive count).
    //!
    //! \tparam interlaced_  Alias for SFINAE.
    template <bool interlaced_ = interlaced, typename std::enable_if<interlaced_ == false, int>::type = 1>
    void dealloc() {
        if( *(this->share_count) > 1 ) return;
        else --*(this->share_count);

        for(auto dt: this->_data) {
#if myUSE_MKL
            free(dt);
#else
            fftw_free(dt);
#endif
        }
    }

    //! \brief Returns real modes from logical modes.
    //!
    //!
    //! \param l_modes
    //! \return
    inline pos_t<2> getRealModes(const pos_t<2> & l_modes) const {
        return {l_modes[0] + this->grid_p->local_0_start, l_modes[1]};
    }

    //! \brief Returns real modes from logical modes.
    //!
    //!
    //! \param l_modes
    //! \return
    inline pos_t<3> getRealModes(const pos_t<3> & l_modes) const {
        throw NotImplementedException();

        return l_modes;
    }

    //! \brief Plans the grid FFT.
    //!
    //! Uses the grid's FFT to make a plan (if needed).
    //!
    //! \param[in] freq    We need an output vector just to plan the transform.
    void plan(FFTWFreqVec<d, contiguous, interlaced> & freq) {
        this->grid_p->require_host_fft();
        this->grid_p->ffth->plan(*this, freq);
    }

    //! \brief Forward the data to
    //!
    //! Forward data onto \p freq using the grid's FFT.
    //!
    //! \param[in,out] freq   Output vector, input geometry.
    void forward(FFTWFreqVec<d, contiguous, interlaced> & freq) {
        this->grid_p->require_host_fft();
        this->grid_p->ffth->forward(*this, freq);
    }

public:
    static bool is_time_vector() { return true; }
};
