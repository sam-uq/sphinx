/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

//! \file ssprk2.hpp This file contains an implementation of SSPRK(2,2)

#include "utilities/profiler.hpp"
#include "utilities/options.hpp"

//! \brief Function implementing one second order SSP RK2 timestepping.
//!
//! TODO: Step frequency modes
//!
//! Implements SSPRK(2,2) as follows:
//! \f[ u^1 = u^n + \Delta t L(t^n, u^n) \f]
//! \f[ u^{n+1} = \frac{1}{2} u^n + \frac{1}{2} u^1 + \frac{1}{2} \Delta t L(t^n + \Delta t, u^1) \f]
//! Which can be recasted as single step RK method:
//! \f[ k_1 = L(t^n, u^n) \f]
//! \f[ k_2 = L(t^n + \Delta t, u^n + \Delta t k_1) \f]
//! \f[ u^{n+1} = u^n + \frac{1}{2} \Delta t k_1 + \frac{1}{2} \Delta t k_2 \f]
//! This has butcher tableau:
//! \f[ \begin{array}{c|cc} 0 & 0 & \\ 1 & 1 & 0 \\ \hline \\ & \frac{1}{2} & \frac{1}{2} \end{array} \f]
//!
//! \see \cite ShuSSP2001
//!
//! \tparam vec     Type of data to forward
//! \tparam func    Functor for the evolution of a snapshot (U_t = L(U))
//! \param[in] prev Previous timestep \f$U^2\f$, WARNING: may be destroyed
//! \param[in,out]  Next timestep \f$U^{n+1}\f$
//! \param[in] L    Functor implementing U_T = L(U), signature vec & L(vec U)
//! \param[in] dt   Time-step size
template<class vec, class func>
void ssp_rk2_step(vec & prev, vec & next, func& L, double dt, double t = 0) {

    profiler::start_stage("ssp_rk2");

    // store u^n for later
    next = prev;

    // SSPRK2 requires 1 buffer
    vec temp(prev);
    // temp = prev;

    axpy(prev, L(temp, t + dt), dt);
    // prev += dt * L(temp);
    // prev = u(1), temp = garbage, next = u(n)

    next += prev;
    axpy(next, L(prev, t + dt), dt);
    // next += dt * L(prev);
    // next = u^n + u^1 + L(u^1) dt

    scale(next, 0.5);
    // next = u^{n+1}

    profiler::end_stage();
}
