/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

//! \file ssprk3.hpp This file contains an implementation of SSPRK(3,3)

#include "utilities/profiler.hpp"
#include "utilities/options.hpp"

//! \brief Function implementing one third order SSP RK3 timestepping.
//!
//! TODO: Step frequency modes.
//!
//! Implements SSPRK(3,3) as follows:
//! \f[ u^1 = u^n + \Delta t L(t^2, u^n) \f]
//! \f[ u^2 = \frac{3}{4} u^n + \frac{1}{4}  u^1 + \frac{1}{4} \Delta t L(t^n + \Delta t, u^1) \f]
//! \f[ u^{n+1} = \frac{1}{3} u^n + \frac{2}{3} u^2 + \frac{2}{3} \Delta t L(t^n + \frac{1}{2} \Delta t, u^2) \f]
//! Which can be recasted as single step RK method:
//! \f[ k_1 = L(t^n, u^n) \f]
//! \f[ k_2 = L(t^n + \Delta t, u^n + \Delta t k_1) \f]
//! \f[ k_3 = L(t^n + \frac{1}{2} \Delta t, u^n + \frac{1}{4} \Delta t k_1 + \frac{1}{4} \Delta t k_2) \f]
//! \f[ u^{n+1} = u^n + \frac{1}{6} \Delta t k_1 + \frac{1}{6} \Delta t k_2 + \frac{2}{3} \Delta t k_3\f]
//! This has butcher tableau:
//! \f[ \begin{array}{c|ccc} 0 & 0 & & \\ 1 & 1 & 0 & \\ \frac{1}{2} & \frac{1}{4} & \frac{1}{4} & 0 \\ \hline \\ & \frac{1}{6} & \frac{1}{6} & \frac{2}{3} \end{array} \f]
//!
//! \see \cite ShuSSP2001
//!
//! \tparam vec     Type of data to forward
//! \tparam func    Functor for the evolution of a snapshot (U_t = L(U))
//! \param[in] prev Previous timestep \f$U^2\f$, WARNING: may be destroyed
//! \param[in,out]  Next timestep \f$U^{n+1}\f$
//! \param[in] L    Functor implementing U_T = L(U), signature vec & L(vec U)
//! \param[in] dt   Time-step size
template<class vec, class func>
void ssp_rk3_step(vec & prev, vec & next, func& L, double dt, double t = 0) {
    profiler::start_stage("ssp_rk3");

    // SSPRK3 requires 2 buffers
    vec temp(prev);
    vec temp2(prev);

    next = prev;
    axpy(prev, L(next, t + 0. * dt), dt);
    // prev += dt * L(next);
    // next now contains u^n
    // prev = u(1), next = garbage, temp = u^n, temp2 = u^n

    next = prev;
    scale(temp, 3./4.);
    axpy(temp, next, 1./4.);
    // next now contains u^1
    // temp now contains 3/4 u^n + 1/4 u^1, temp2 = u^n

    axpy(temp, L(prev, t + 1. * dt), dt/ 4.);
    // temp += (dt/ 4.) * L(prev);
    // temp = u(2), next = 1/4*u(1), temp2 = u^n, prev = garbage

    scale(temp2, 1./3.);
    next = temp2;
    // next = (1/3.) * temp2 = u^n / 3;

    prev = temp;
    axpy(next, prev, 2./3.);
    // next += (2./3.)*prev;
    // prev = u^2, next = 2/3 u^2 + u^n / 3, temp = u^2, temp2 = u^n

    axpy(next, L(temp, t + 0.5 * dt), 2./3.*dt);
    // next += (2./3.*dt)*L(temp);
    // next = u^{n+1}

    profiler::end_stage();
}
