/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

//! \file fe.hpp Contains implementation for Forward Euler timestepping.

#include "utilities/profiler.hpp"
#include "utilities/options.hpp"

//! \brief Function implementing one Forward Euler timesteping.
//!
//! Unstable, use at your own risk (debug only):
//! \f[ u^{n+1} = u^n + \Delta t L(t^n, u^n) \f]
//!
//! TODO: Step frequency modes
//!
//! \tparam vec     type of data to forward
//! \tparam func    functor for the evolution of a snapshot (U_t = L(U))
//! \param[in] prev previous timestep \f$U^2\f$, WARNING: may be destroyed
//! \param[in,out]  next timestep \f$U^{n+1}\f$
//! \param[in] L    functor implementing U_T = L(U), signature vec & L(vec U)
//! \param[in] dt   time-step size
template<class vec, class func>
void fe_step(vec & prev, vec & next, func& L, double dt, double t = 0) {
    profiler::start_stage("fe");

    next = prev;
    axpy(next, L(prev, t + 0. * dt), dt);

    profiler::end_stage();
}
