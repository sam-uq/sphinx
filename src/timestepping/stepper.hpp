/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

//! \dir timestepping Contains all utilities to perform timestepping.

//! \file stepper.hpp Contains the function that executes a single step of the evolution.

#include "utilities/profiler.hpp"
#include "utilities/options.hpp"
#include "utilities/mpi.hpp"

//! Minimum dt value (blow up detecting)
#define EPS_TOL 1e-14
//! Blow up detecting value
#define HUGE_TOL 1e14

using namespace sphinx;

//! \brief
//!
//!
//!
//!
//!
//!
//! \tparam evovec
//! \tparam vec
//! \tparam step
//! \tparam func
//! \tparam snpfunc
//! \tparam cflfunc
//! \param  init
//! \param  S
//! \param  L
//! \param  snpF
//! \param  comm
template <class evovec, class vec, class step, class func, class snpfunc, class cflfunc>
void stepper(vec & init, step & S, func & L,
             snpfunc & snpF, cflfunc &, const mpi::IntraDomainComm & comm = mpi::world) {
    profiler::start_stage("stepper");

    int max_itr = 1000000;
    if( sphinx::options.has("maxit") ) {
        max_itr = sphinx::options.as<int>("maxit");
    }

    double sup = 0;

    double h = 0;
    h = static_cast<evovec&>(init).grid_p->h;

    double dt = 0;
    double t = 0.;
    double T = 1.;
    if( sphinx::options.has("T") ) T = sphinx::options.as<double>("T");
    double adaptive = true;
    if( sphinx::options.has("adaptive") ) adaptive = sphinx::options.as<bool>("adaptive");
    int snapshot_counter = 0;
    bool snapshot_force = false;
    if( sphinx::options.has("snapshot_force") ) snapshot_force = sphinx::options.as<bool>("snapshot_force");
    bool snapshot_save_fps = false;
    double snapshot_fps = 0.2;
    if( sphinx::options.has("snapshot_fps") ) {
        snapshot_save_fps = true;
        snapshot_fps = sphinx::options.as<double>("snapshot_fps");
    }
//    std::list<double> snapshots = {0.01, 0.02, 0.03, 0.5, 0.7, 0.4, 0.05};
    std::list<double> snapshots;
    if( sphinx::options.has("snapshot_list") ) {
        const auto& list = sphinx::options.as<std::vector<int>>("snapshots_list");
        snapshots.resize(list.size());
        std::copy(list.begin(), list.end(), snapshots.begin());
    }


    if( snapshot_save_fps && snapshot_fps > 0 ) {
        for(double ti = snapshot_fps; ti < T; ti += snapshot_fps) {
            snapshots.push_back(ti);
        }
    }
    // Add last snapshot if not already present
    if( sphinx::options.has("Save.final") &&
            sphinx::options.as<bool>("Save.final") &&
            snapshots.back() != T ) {
        snapshots.push_back(T);
    }
    for(auto it = snapshots.begin(); std::next(it) != snapshots.end(); ++it) {
        if( std::abs(*it - *(std::next(it))) < EPS_TOL ) snapshots.erase(std::next(it));
    }
    snapshots.sort();
    for(auto it = snapshots.begin(); it != prev(snapshots.end()); ++it ) {
        if( *it == *next(it) ) {
            snapshots.erase(next(it));
        }
    }
    std::stringstream ss;
    ss << "The following points have been selected for snapshot saving: ";
    for(auto t: snapshots ) {
        ss << t << ", ";
    }
    debug::message(ss.str(), comm, mpi::Policy::Collective, debug::ts);

    // Print an Xdmf at each step
    bool dump_xdmf = false;
    if(sphinx::options.has("dump_xdmf")) {
        dump_xdmf = sphinx::options.as<bool>("dump_xdmf");
    }

    if( sphinx::options.has("Save.initial") &&
            sphinx::options.as<bool>("Save.initial") ) {
        profiler::start_stage("snapshot");
        snpF(init, t, dump_xdmf); // add no save init
        profiler::end_stage();
    }

    // When a step is shortened, make sure the next step is
    // placed exactly at the right place (if true)
    bool next_snapshot_must_be_enforced = false;

    vec owned(init);
    vec * prev = &init;
    vec * next = &owned; // init
    for(int i = 0; i < max_itr; ++i) {
        profiler::start_stage("cfl");
        if(adaptive) {
            dt = L.cfl(*prev, h, sup);
        } else {
            L.cfl(*prev, h, sup);
            dt = h*1.;
        }
//        std::cout << dt << " " << sup << std::endl;
        bool snapshot_save = false;
        if( next_snapshot_must_be_enforced ) {
            // Prevents weird stuff when shortening steps
            next_snapshot_must_be_enforced = false;
            dt = snapshots.front() - t;
            snapshot_save = true;
            snapshots.pop_front();
        } else if( !snapshots.empty() && t + dt >= snapshots.front() ) {
            if( snapshot_force ) {
                debug::warning("Shortening step to force snapshot time!",
                               comm, mpi::Policy::Collective, debug::ts);
                dt = snapshots.front() - t;
            }
            snapshot_save = true;
            snapshots.pop_front();
        } else if( !snapshots.empty() && t + dt * 3. / 2. >= snapshots.front() && snapshot_force ) {
            dt = (snapshots.front() - t) / 2.;
            debug::warning("Shortening 2 steps to force snapshot time!",
                           comm, mpi::Policy::Collective, debug::ts);
            next_snapshot_must_be_enforced = true;
        }
        profiler::end_stage();
        t += dt;

        std::stringstream os;
        os << "[STEP " << i << "] " << "t = " << t << " dt = " << dt << ", sup = " << sup <<
              " [" << profiler::current_stage()->runtime() << " s]";
        debug::message(os.str(), comm, mpi::Policy::Collective, debug::ts, "\r");
        debug::verbose("", comm, mpi::Policy::Collective, debug::ts, "\n");

        if( dt < EPS_TOL || sup > HUGE_TOL ) {
            if(BLOW_UP_DUMP) {
                snpF(*prev, t, dump_xdmf); // FIXME: add xmdf dump too
            }
            debug::error("Blow up detected!", comm, mpi::Policy::Collective, debug::ts);
        }
        profiler::start_stage("step");
        S(*prev, *next, L, dt, t);
        profiler::end_stage();

        if( snapshot_save ) {
            profiler::start_stage("snapshot");
            std::stringstream fname;
            fname << "out_f" << snapshot_counter++;
            snpF(*next, t, dump_xdmf);
            profiler::end_stage();
//            next->output(fname.str(), true);
        }

        auto temp = next;
        next = prev;
        prev = temp;
//        prev->output("ts");
        if( t >= T ) {
            debug::message("Final time reached.", comm, mpi::Policy::Collective, debug::ts);
            break;
        }
    }
//    profiler::start_stage("snapshot");
//    snpF(*prev, t);
//    profiler::end_stage();

    profiler::end_stage();
}

