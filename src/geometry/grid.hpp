/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include <iostream>
#include <array>
#include <sstream>
#include <memory>

#include "../utilities/debug.hpp"
#include "../utilities/tools.hpp"
#include "../utilities/utilities.hpp"

#include "../sphinx_internal.hpp"
#include "../utilities/mpi.hpp"
#include "../utilities/performance.hpp"

#if myUSE_MKL
#include "../utilities/mkl_tools.hpp"
#elif USE_ACCFFT
//#include <accfft_common.h>
#include "../utilities/accfft_tools.hpp"
#else // myUSE_FFTW
#include "../utilities/fftw_tools.hpp"
#include "../utilities/mpi_tools.hpp"

#endif

//! Enum/Set-like type that controls how the data is structure for containers
//! using the grid
// TODO: remove or clean
using DataType = Set<1>;
const DataType Interlaced(1);
const DataType R2CFFT(2);
const DataType InPlace(4);
const DataType DestroyInput(8);
const DataType DoublePrecision(16);

//! Enum/Set-like type that controls what type of FFT is used for data
//! using this grid
using FFTType = Set<2>;
const FFTType ThreadedFFT(1);
const FFTType MPIFFT(2);
const FFTType GPUFFT(4);

//! Type for real numbers
// TODO: put into types.hpp
using real_t = double;

//! \namespace Geometry Manages data layout and distribution.

namespace sphinx {

namespace operators {

// Forward declare FFTs on host
template <unsigned int dim, bool contiguous, bool interlaced>
class FFTWFFT;
// Forward declare FFTs on device
template <unsigned int dim, bool contiguous, bool interlaced>
class CUDAFFT;

#if myUSE_MKL
// Forward declare FFTs on device
template <unsigned int dim, bool contiguous, bool interlaced>
class MKLFFT;
#endif
#if USE_ACCFFT
// Forward declare FFTs on device
template <unsigned int dim, bool contiguous, bool interlaced>
class ACCFFT;
#endif

} // END namespace operators

namespace Geometry {

//! \brief Contains layout and distribution in memory of the data.
//!
//! \tparam dim Dimension of the phase space
template <unsigned int dim, bool contiguous, bool interlaced>
struct Grid {
    using pos_t = std::ptrdiff_t;
    using index_t = std::array<pos_t, dim>;
    using coord_t = std::array<real_t, dim>;

    Grid(index_t size, FFTType _fft_type = FFTType::None,
         DataType data_type = Interlaced | R2CFFT,
         bool presetup = true)
        : fft_type(_fft_type), data_type(data_type), timeN(size) {

        timeNpad = timeN;
        freqN = timeN;
        freqN[dim-1] = freqN[dim-1] / 2 +1;

        if( data_type >= InPlace || fft_type >= MPIFFT ) {
            debug::status("Using padded data.", mpi::world,
                          mpi::Policy::Collective);
            pad = 2-(timeN[dim-1] % 2);
        } else {
            pad = 0;
        }

        timeNpad[dim-1] += pad;

        if( presetup ) setup();
    }

    ~Grid() {
        delete partitioning;
    }

    //! Given index (as local index), returns the coordinate at the index
    inline coord_t coord(const index_t & idx) const {
        coord_t c;
        c[0] = (real_t) (idx[0] + .0) / timeN[0];
        for(unsigned int d = 1; d < dim; ++d) {
            c[d] = (real_t) (idx[d] + .0) / timeN[d];
        } // torus with nodes from 0,0
        return c;
    }

    void print() {
        std::stringstream ss1, ss2, ss3, ss4, ss5, ss6, ss7;
        ss1 << "Mesh of size:                           "
            << timeN[0] << "x" << timeN[1];
        if(dim >= 3) ss1 << "x" << timeN[2];
        ss2 << "Padded mesh (padding = " << pad << "):  "
            << timeNpad[0] << " " << timeNpad[1];
        if(dim >= 3) ss2 << " " << timeNpad[2];
        ss3 << "Fourier modes:                          "
            << freqN[0] << " " << freqN[1];
        if(dim >= 3) ss3 << " " << freqN[2];
        ss4 << "Local n0:                               "
            << local_n0 << " " << local_0_start;
        ss5 << "Local n0 trsp:                          "
            << local_n0_trsp << " " << local_0_start_trsp;
        ss6 << "Modes and alloc:                        "
            << modes << " " <<  alloc_time << " " << alloc_freq;
        ss7 << "Delta information:                        "
            << dx << " " <<  dy << " " << dz << " " << h;

        debug::message(ss1.str(), comm);
        debug::message(ss2.str(), comm);
        debug::message(ss3.str(), comm);
        debug::status(ss4.str(), comm, mpi::Policy::Self);
        debug::status(ss5.str(), comm, mpi::Policy::Self);
        debug::status(ss6.str(), comm, mpi::Policy::Self);
        debug::status(ss7.str(), comm, mpi::Policy::Self);

        do_grid_check(timeN, comm);
    }

    unsigned int getMinRanks(void) {
        return 1; // TODO
    }

    void setup(mpi::IntraDomainComm newcomm = mpi::world) {
        comm = newcomm;

#if USE_ACCFFT
        accfft_create_comm_wrapper(comm.comm,
                           std::array<int,2>{ comm.size, 1 }, // TODO
                           &acc_comm);
#endif

        local_n0 = timeN[0];
        local_0_start = 0;
        local_n0_trsp = freqN[1];
        local_0_start_trsp = 0;

        modes = prod(timeN);
        alloc_time = prod(timeNpad);
        alloc_freq = prod(freqN);

#if myUSE_MKL
        mkl_get_alloc(alloc_time, alloc_freq,
                       local_0_start, local_n0,
                       local_0_start_trsp, local_n0_trsp,
                       timeN, comm.comm, mkl_descriptor);
#elif USE_ACCFFT
        accfft_get_alloc(alloc_time, alloc_freq,
                         time_local_start, time_local_size,
                         frequency_local_start, frequency_local_size,
                         timeN, acc_comm);
#elif USE_FFTW || USE_CUDA // CHECK: CUDA=
        // experimental
        fftw_get_alloc(alloc_time, alloc_freq,
                           local_0_start, local_n0,
                           local_0_start_trsp, local_n0_trsp,
                           freqN, comm.comm);
#endif // no mUSE_MKL

        dx = 1. / timeN[0]; // FIXME
        dy = 1. / timeN[1]; // FIXME
        if(dim >= 3) dz = 1. / timeN[2]; // FIXME
        else dz = 1;
        h = std::min(dx,std::min(dy,dz));

        const unsigned int size = 4;
        int sendarray[size] = {(int) local_0_start,
                               (int) local_n0,
                               (int) local_0_start_trsp,
                               (int) local_n0_trsp};

        partitioning = (int *) malloc(comm.size*size*sizeof(int));

        std::stringstream ss;
        ss << "Gathering partitioning information..." << std::endl;
        debug::verbose(ss.str(), comm);

        mpiErrchk(MPI_Allgather(sendarray, size, MPI_INT,
                      partitioning, size, MPI_INT, comm.comm));

        print();
    }

    //! Contains degrees of freedom to access externally
    static const int dims = dim;

    //! Sends grid information to other ranks on the same level, to avoid proper
    //! transfer of information. Receiver ranks, should have a RemoteGrid, ready to
    //! receive the information that has been sent.
    //! \param intra_level_comm The intra-level communicator. Infomration
    //! is sent to subset intra_level_color withing this communicator.
    //! \param intra_level_color The color within the level. 0 is the root.
    void send_to(const mpi::IntraLevelComm & intra_level_comm, unsigned int intra_level_color) {
        std::vector<int> intbuffer(3*dim + 1 + comm.size*4);
        std::vector<double> doublebuffer(4);

        std::copy(timeN.begin(), timeN.end(), intbuffer.begin());
        std::copy(timeNpad.begin(), timeNpad.end(), intbuffer.begin()+dim);
        std::copy(freqN.begin(), freqN.end(), intbuffer.begin()+2*dim);
        intbuffer[3*dim] = pad;
        for(int i = 0; i < comm.size*4 ;++i) {
            intbuffer[3*dim+1+i] = partitioning[i];
        }
        doublebuffer[0] = dx;
        doublebuffer[1] = dy;
        doublebuffer[2] = dz;
        doublebuffer[3] = h;

        int tag = 11111;

       auto & levelReceivingComm = intra_level_comm.intra_domain_comms.at(intra_level_color);


        for(int c = 0; c < levelReceivingComm.size; ++c) {

            std::stringstream ss;
            ss << "Sending RemoteGrid information to intra-level color "
               << intra_level_color << " at (global) rank "
               << levelReceivingComm.start_rank_in_parent + c
               << "(local rank " << c << ")" <<std::endl;
            debug::verbose(ss.str(), intra_level_comm);

            // Receiving rank is the c-th one after the domain root.
            mpiErrchk(MPI_Send(intbuffer.data(), intbuffer.size(), MPI_INTEGER,
                               levelReceivingComm.start_rank_in_parent + c,
                     tag, intra_level_comm.comm));
            mpiErrchk(MPI_Send(doublebuffer.data(), doublebuffer.size(), MPI_DOUBLE,
                               levelReceivingComm.start_rank_in_parent + c,
                     tag, intra_level_comm.comm));
        }
    }

    void require_host_fft(void) {
        if( ffth ) return;
#if myUSE_MKL
        ffth = std::shared_ptr<operators::MKLFFT<dim, contiguous,
                interlaced>>(
                    new operators::MKLFFT<dim, contiguous, interlaced>(this)
                    );
#else
        ffth = std::shared_ptr<operators::FFTWFFT<dim, contiguous,
                interlaced>>(
                    new operators::FFTWFFT<dim, contiguous, interlaced>(this)
                    );
#endif
    }

    void require_device_fft(void) {
        if( fftd ) return;
#if USE_ACCFFT
        fftd = std::shared_ptr<operators::ACCFFT<dim, contiguous,
                interlaced>>(
                    new operators::ACCFFT<dim, contiguous, interlaced>(this)
                    );
#else
        fftd = std::shared_ptr<operators::CUDAFFT<dim, contiguous,
                interlaced>>(
                    new operators::CUDAFFT<dim, contiguous, interlaced>(this)
                    );
#endif
    }

    //!
    FFTType fft_type;
    DataType data_type;
public:
#if myUSE_MKL
    std::shared_ptr<operators::MKLFFT<dim, contiguous, interlaced>> ffth;
#else
    std::shared_ptr<operators::FFTWFFT<dim, contiguous, interlaced>> ffth;
#endif
#if USE_ACCFFT
    std::shared_ptr<operators::ACCFFT<dim, contiguous, interlaced>> fftd;
#else
    std::shared_ptr<operators::CUDAFFT<dim, contiguous, interlaced>> fftd;
#endif

    index_t timeN;
    index_t timeNpad;
    index_t freqN;

    mpi::IntraDomainComm comm;

    pos_t modes;
    pos_t alloc_time, alloc_freq;

    // DEPRECADED for CUDA MKL AND FFTW, useless for ACCFFT
    pos_t local_n0, local_0_start, local_n0_trsp, local_0_start_trsp;
    // NEW: only ACCFFT
    index_t time_local_size, time_local_start;
    index_t frequency_local_size, frequency_local_start;

    int pad;

    int *partitioning;

    double dx, dy, dz, h;

#if myUSE_MKL
    DFTI_DESCRIPTOR_DM_HANDLE mkl_descriptor;
#endif

public:
    // ACC COMM
    MPI_Comm acc_comm;
};

} // END namespace Geometry
} // END namespace sphinx
