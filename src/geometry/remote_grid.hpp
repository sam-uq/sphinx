/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include <array>
#include <vector>
#include <sstream>

#include "../sphinx_internal.hpp"
#include "../utilities/types.hpp"
#include "../utilities/tools.hpp"
#include "../utilities/utilities.hpp"
#include "../utilities/mpi.hpp"
#include "../utilities/debug.hpp"
#include "../utilities/performance.hpp"

#if myUSE_MKL
#include "../utilities/mkl_tools.hpp"
#elif USE_ACCFFT
//#include <accfft_common.h>
#include "../utilities/accfft_tools.hpp"
#else // myUSE_FFTW
#include "../utilities/fftw_tools.hpp"
#endif

#include "grid.hpp"

using namespace sphinx;

template <unsigned int dim>
struct RemoteGrid {

    using pos_t = std::ptrdiff_t;
    using index_t = std::array<pos_t, dim>;

    using coord_t = std::array<real_t, dim>;

    RemoteGrid() = default;

    //! Contains degrees of freedom to access externally
    static const int dims = dim;

    void recv_from(const mpi::IntraLevelComm & intra_level_comm_,
                   unsigned int intra_level_color)  {
        intra_level_comm = intra_level_comm_;
        comm = intra_level_comm.intra_domain_comms.at(intra_level_color);

        std::vector<int> intbuffer(3*dim + 1 + comm.size*4);
        std::vector<double> doublebuffer(4);

        int tag = 11111;

        std::stringstream ss;
        ss << "Receiving Remote Grid information for level color "
           << intra_level_color
           << " with size " << comm.size << " from rank "
           << intra_level_comm.intra_domain_comms.at(intra_level_color).start_rank_in_parent;
        debug::verbose(ss.str(), intra_level_comm, mpi::Policy::Self);

        mpiErrchk(MPI_Recv(intbuffer.data(), intbuffer.size(), MPI_INTEGER,
                           intra_level_comm
                                   .intra_domain_comms
                                   .at(intra_level_color).start_rank_in_parent,
                           tag, intra_level_comm.comm, &status[0]));
        mpiErrchk(MPI_Recv(doublebuffer.data(), intbuffer.size(), MPI_DOUBLE,
                           intra_level_comm.intra_domain_comms
                                   .at(intra_level_color)
                                   .start_rank_in_parent,
                           tag, intra_level_comm.comm, &status[1]));

        mpiStatuschk(status[0]);
        mpiStatuschk(status[1]);

        std::copy(intbuffer.begin(), intbuffer.begin()+dim, timeN.begin());
        std::copy(intbuffer.begin()+dim, intbuffer.begin()+2*dim, timeNpad.begin());
        std::copy(intbuffer.begin()+2*dim, intbuffer.begin()+3*dim, freqN.begin());
        pad = intbuffer[3*dim];
        partitioning = new int[comm.size*4];
        for(int i = 0; i < comm.size*4 ;++i) {
            partitioning[i] = intbuffer[3*dim + 1 +i];
        }
        dx = doublebuffer[0];
        dy = doublebuffer[1];
        dz = doublebuffer[2];
        h = doublebuffer[3];
    }

    MPI_Status status[2];
public:


    index_t timeN;
    index_t timeNpad;
    index_t freqN;

    mpi::IntraLevelComm intra_level_comm;
    mpi::IntraDomainComm comm;

    int pad;

    int *partitioning;

    double dx, dy, dz, h;

    unsigned int tot_sim;
};
