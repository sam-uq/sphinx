# Usage: source this file ". filename.sh"
[ "$0" = "$BASH_SOURCE" ] && { echo "File must be sourced!"; exit; }

export HOST_TYPE="brutus"
echo "Setting up environment for \"$HOST_TYPE\", please stand by..."

echo "Unloading autoloaded modules..."
module unload open_mpi
module unload cmake
module unload boost
module unload gcc

echo "Loading modules..."
if [ "$COMPILER" = "intel" ]; then
    echo "Setting up compiler, using intel..."
    module load intel/14.0.0
else
    echo "Setting up compiler, using gcc..."
    module load gcc/4.8.2
fi

module load hdf5
module load open_mpi
module load boost/1.54.0
module load cmake/2.8.12
module load openblas
module load fftw/3.3.3
echo "Done!"
