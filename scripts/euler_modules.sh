# Usage: source this file ". filename.sh"
[ "$0" = "$BASH_SOURCE" ] && { echo "File must be sourced!"; exit; }

export HOST_TYPE="euler"

export SPHINX_SCRIPTS_PATH=`dirname "$BASH_SOURCE"`
export SPHINX_SRC_PATH="$(dirname "$SPHINX_SCRIPTS_PATH")"

export SPHINX_INSTALL_PATH=$SCRATCH/sphinx
echo "Setting up environment for \"$HOST_TYPE\", please stand by..."

echo "Unloading autoloaded modules..."
module unload gcc
module unload open_mpi
module unload cmake
module unload boost

echo "Loading modules..."
if [ "$COMPILER" = "intel" ]; then
    echo "Setting up compiler, using intel..."
    module load intel
else
    echo "Setting up compiler, using gcc..."
    module load gcc/5.2.0
fi
module load hdf5
if [ "$MPI" = "impi" ]; then
    echo "Setting up compiler, using Intel MPI..."
    module load impi
elif [ "$MPI" = "mvapich" ]; then
    echo "Setting up compiler, using mvapich (untested)..."
    module load mvapich
else
    echo "Setting up compiler, using Open MPI..."
    module load open_mpi
fi
module load mkl
module load boost/1.59.0
echo "Boost PATH: $BOOST_LIBRARYDIR"
module load cmake/3.3.1
module load openblas
if [ "$MPI" = "impi" ]; then
    echo "Won't load FFTW due to Intel MPI incompatibility, use MKL instead."
else
    echo "Loading FFTW..."
    module load fftw/3.3.4
fi

echo "Loading optional modules..."
#module load paraview

chmod +x $SPHINX_SCRIPTS_PATH/pre_scripts.sh
$SPHINX_SCRIPTS_PATH/pre_scripts.sh

echo "Done!"

