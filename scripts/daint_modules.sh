# Usage: source this file ". filename.sh"
[ "$0" = "$BASH_SOURCE" ] && { echo "File must be sourced!"; exit; }

# FIXME: modules are incorrect
export HOST_TYPE="daint"
echo "Setting up environment for \"$HOST_TYPE\", please stand by..."

echo "Unloading autoloaded modules..."
module unload cmake
module unload boost
module unload PrgEnv-cray


echo "Loading modules..."
if [ "$COMPILER" = "intel" ]; then
    echo "Setting up compiler, using intel..."
    module load intel
    module load PrgEnv-intel
    module load gcc/4.9.3
else
    echo "Setting up compiler, using gcc..."
    module load gcc/5.1.0
    module load PrgEnv-gnu
fi
module load cudatoolkit/7.0.28-1.0502.10742.5.1

module load cray-hdf5
module load cray-mpich
module load boost/1.58.0
module load cmake/3.3.2
module load openblas
module load fftw/3.3.4.4
echo "Done!"
