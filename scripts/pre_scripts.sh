#!/bin/sh

echo "Module setup completed, now it's time to compile and run."

if [ $SPHINX_INSTALL_PATH ]; then
    echo " -> Sphinx will be installed in $SPHINX_INSTALL_PATH."
fi

echo " -> Now you should run \"cmake $SPHINX_SRC_PATH\" here."
echo " -> You can install \"sphinx\" using \"make install\"."
echo " -> You can run sphinx by cd to \"$SPHINX_INSTALL_PATH/bin\" and use \"./batch_launcher.sh\"."

