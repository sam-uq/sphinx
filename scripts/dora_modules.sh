# Usage: source this file ". filename.sh"
[ "$0" = "$BASH_SOURCE" ] && { echo "File must be sourced!"; exit; }

# FIXME: modules are incorrect
export HOST_TYPE="dora"
echo "Setting up environment for \"$HOST_TYPE\", please stand by..."

echo "Unloading autoloaded modules..."
module unload open_mpi
module unload cmake
module unload boost

echo "Loading modules..."
if [ "$COMPILER" = "intel" ]; then
    echo "Setting up compiler, using intel..."
    module load intel
else
    echo "Setting up compiler, using gcc..."
    module load gcc/5.2.0
fi
module load hdf5
module load open_mpi
module load boost/1.59.0
module load cmake/3.3.1
module load openblas
module load fftw/3.3.4
echo "Done!"
