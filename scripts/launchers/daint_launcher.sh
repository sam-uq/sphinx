#!/bin/bash

# Config
account="s366"
# Executables name
mpi_exec="aprun"
batch_exec="sbatch"

# Parse options
. ./opt_parser.sh

# SETUP MPI: Check if MPI needs to be used
if [ "$nproc" -gt "1" ];
then
  echo "Running program WITH MPI and $nproc processors.";
  mpi_cmd="$mpi_exec -n $nproc";
else
  echo "Running program WITHOUT MPI.";
  mpi_cmd="$mpi_exec";
fi

# SETUP BATCH: local no batch is needed
batch_cmd="$batch_exec -A $account --nodes="$((($nproc-1)/$cpu_per_node+1))" --time=$wtime:00 --job-name=$name
--output=./$name.out --error=./$name.err"

# SETUP GLOBAL: gather all commands + options
cmd="echo -e \"#! /bin/bash\n$mpi_cmd $exec $opts\" | $batch_cmd"
