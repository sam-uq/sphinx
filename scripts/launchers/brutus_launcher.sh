#! /bin/bash

# Config
# mem="2000"
# cpu="Opteron6174"
# tile="48"

mpi_exec="mpirun"
batch_exec="bsub"

# Parse options
. ./opt_parser.sh

# SETUP MPI: Check if MPI needs to be used
if [ "$nproc" -gt "1" ];
then
  echo "Running program WITH MPI and $nproc processors.";
  mpi_cmd="$mpi_exec -np $nproc";
else
  echo "Running program WITHOUT MPI.";
  mpi_cmd="";
fi

# SETUP BATCH: local no batch is needed
if [ "$email" = true ];
then
  bsub_opt=$bsub_opt" -B -N"
fi
if [ -n "$cpu" ];
then
  bsub_opt=$bsub_opt" -R \"select[model==$cpu]\""
fi
if [ -n "$mem" ];
then
  bsub_opt=$bsub_opt" -R \"rusage[mem=$mem]\""
fi
if [ -n "$tile" ];
then
  bsub_opt=$bsub_opt" -R \"span[ptile=$tile]\""
fi
batch_cmd="$batch_exec -n $nproc -W $wtime -o $name.out -J $name $bsub_opt"

# SETUP GLOBAL: gather all commands + options
cmd="$batch_cmd $mpi_cmd $exec $opts"
