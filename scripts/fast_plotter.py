#! /usr/bin/env python
# Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>

import poseidon.base.data
from poseidon.base.tools import *
import numpy as np
import sys

for i in range(0,60):
    try:
        dt = poseidon.base.data.Data(sys.argv[1], comps=[0,1,2], frame=i)
    except FrameNotFoundException:
        print("No other frame! Bye...")
        exit(0)
    except AttributeError:
        print("Maybe no more than " + str(i) + " frames.....")
        exit(0)
    dt.plot(sys.argv[1] + "_f" + str(i) + ".png")

#dt = poseidon.base.data.Data(sys.argv[1], comps=[0,1,2], frame=0)
#dt.plot(sys.argv[1] + "_f" + str(0) + ".png")
    
print(dt.shape)
print(np.isnan(np.min(dt.data)))
