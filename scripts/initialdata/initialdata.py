#!/usr/bin/env python3
# Copyright 2015-2018 ETH Zurich, Filippo Leonardi

"""
Example of initial data through Python bindings

Here a Sampler instance is exposed with name "smp" and with access to the methods:
 - "reserve", call only in "init_py"
 - "get",
 - "next",
 - "select_stream"
"""

# Legacy export, do not use
#import sampler

# Set to true if data provides only vorticity
only_vorticity=True
# Set to true if data has forcing
has_forcing=True

def check_python():
    """
    Do not delete this function.
    """
    print("Python is up and running. Hi from Python!")
    return True


def inti_py():
    """

    :return:
    """
    # Reserve enough space for the sampler
    smp.reserve(4000)

def eval_at_u_2d(x, y):
    """
    Returns the value of the velocity, given the samper and the coordinates.
    :param x: x-coordinate
    :param y: y-coordinate
    :return: a tuple with the velocity value at (x,y)
    """
    return x, y

def eval_at_w_2d(x, y):
    ## Example: simple patch
    x -= 0.5
    y -= 0.5
    r = 0.25

    if x**2+y**2>r**2:
        return 1
    else:
        return 0
        #return 1

def forcing_2d(x, y, t):
    """
    Forcing to apply at coordinate (x,y,t)
    """

    return 1, 0

def forcing_3d(x, y, t):
    """
    Forcing to apply at coordinate (x,y,t)
    """

    return 1, 0, 0
