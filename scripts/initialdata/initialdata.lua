#!/usr/bin/env lua
-- Copyright 2015-2018 ETH Zurich, Filippo Leonardi

-- Example of LUA initial data.
--
-- * The "sampler" table is available as instance
--   with some of its methods exposed to LUA.
--   It exposes: reserve, next, select_stream.
-- * The global "args" is a map that will contain
--   generic arguments as string, indexed by argument number
--   the index is an integer aka vector (indices start at 1)
--   and is passed via the option "InitialData.args".

-- This function must always be there to ensure that the initialdata file
-- is conforming and available.
function check_lua()
    print("LUA is up and running, hi from Lua.")
end

-- This function is called once, before the initial
-- data is set.
function init()
    
    if args ~= nil then
        print("Initial data arguments:")
        for key, value in pairs(args) do
            print(key .. ":" .. value)
        end
    else
        print("No args passed to LUA")
    end

    sampler.reserve(50000)
end

-- Set to true if you want to use always the vorticity
only_vorticity = false

-- Input is the current coordinate as a pair of doubles.
-- This function must return a pair of doubles, the value
-- of the initial data at this coordinate, velocity in 2d.
function eval_at_u_2d(x, y)
--     Debugging
--     z = args[1]
--     print(x .. " " .. y .. " " .. sampler.next() .. " - " .. z)
    
    return x + y, x*x+y*y
end

-- Input is the current coordinate as a pair of doubles.
-- This function must return a pair of doubles, the value
-- of the initial data at this coordinate, vorticity in 2d.
function eval_at_w_2d(x, y)
--     Debugging
--     z = args[1]
--     print(x .. " " .. y .. " " .. sampler.next() .. " - " .. z)
    
    -- EXAMPLE: PATCH
--     x = x - 0.5
--     y = y - 0.5
--     r = 0.25
--     if x*x + y*y > r*r then return 0 else return 1 end

    return math.cos(x) + math.cos(y)
end

-- Set to true if forcing is to be used.
has_forcing = false

-- Forging function (2d variant).
-- Input is current coordinate and current time.
-- Returns value of forcing at current coordinate and time.
function forcing_2d(x, y, t)
    return 0, 0
    
    -- EXAMPLE: advect to right by 1
--     return 1, 0
end

-- Forging function (3d variant).
-- Input is current coordinate and current time.
-- Returns value of forcing at current coordinate and time.
function forcing_3d(x, y, z, t)
    return 0, 0, 0
end
