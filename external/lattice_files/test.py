#! /usr/bin/env python

import numpy as np
from scipy import integrate

# u = lambda x, y: np.array([np.cos(x), np.cos(y)])

rho = 0.05
delta = 0.05


def u(x, y):
    u2 = delta * np.sin(2 * np.pi * x)
    if y > 0.5:
        return np.array([np.tanh((y - 0.75) / rho), u2])
    else:
        return np.array([np.tanh((-y + 0.25) / rho), u2])


p = 2.
# r = 0.01
M = 512.
r = 5.
h = r / M

S = lambda x, y, r, phi: np.power(np.linalg.norm(u(x, y) - u(x + r * np.sin(phi), y + r * np.cos(phi))), 2.)

I = integrate.tplquad(lambda x, y, phi: S(x, y, h, phi), 0, 2 * np.pi, lambda x: 0, lambda x: 1, lambda x, y: 0,
                      lambda x, y: 1)

print(I, I[0] / 2. / np.pi)
