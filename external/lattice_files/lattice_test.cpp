/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "path_utilities.hpp"

#include <iostream>
#include <limits>

//
// Created by filippo on 23/11/17.
//

int main(int argc, char **argv) {

    using vector = std::vector<int>;

    int I = 0;

    int R = 5;
    if (argc > ++I) {
        R = std::stoi(argv[I]);
    }

    bool pixely = false;
    if (argc > ++I) {
        pixely = std::stoi(argv[I]);
    }

    int maxstep = std::numeric_limits<int>::max();
    if (argc > ++I) {
        maxstep = std::stoi(argv[I]) == -1 ? maxstep : std::stoi(argv[I]);
    }

    bool verbose = true;
    if (argc > ++I) {
        verbose = std::stoi(argv[I]);
    }

    double tol = 1.;

    vector pathx, pathy;

    // TEST of distance
    std::tie(pathx, pathy) = get_lattice_approx<vector>(R, pixely, maxstep, verbose);

    for (int i = 0; i < pathx.size(); ++i) {
        if (verbose) {
            std::cout << "(" << pathx[i] << "," << pathy[i] << ") - dist = "
                      << std::sqrt(pathx[i] * pathx[i] + pathy[i] * pathy[i]) << "\n";
        }

        if (std::abs(std::sqrt(pathx[i] * pathx[i] + pathy[i] * pathy[i]) - R) > tol) {
            return EXIT_FAILURE;
        }
    }
    std::cout << "Found path wit length " << pathx.size() << ".\n";

    // TEST of equality
    std::tie(pathx, pathy) = get_lattice_approx<vector>(5, false, maxstep, verbose);

    vector pathx_ex = {5, 5, 5, 4, 3, 2, 1, 0, -1, -2, -3, -4, -5, -5, -5, -5, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 5};
    vector pathy_ex = {0, 1, 2, 3, 4, 5, 5, 5, 5, 5, 4, 3, 2, 1, 0, -1, -2, -3, -4, -5, -5, -5, -5, -5, -4, -3, -2, -1};

    for (int i = 0; i < pathx.size(); ++i) {
        if (pathx_ex[i] != pathx[i] || pathy_ex[i] != pathy[i]) {
            return EXIT_FAILURE;
        }
    }

    return EXIT_SUCCESS;
}