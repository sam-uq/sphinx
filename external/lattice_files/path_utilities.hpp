/*
 * Copyright 2015-2017 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include <cassert>
#include <exception>
#include <cmath>
#include <tuple>
#include <array>
#include <vector>
#include <limits>
#include <iostream>

//
// Created by filippo on 23/11/17.
//

template<class T = int>
class point {
public:
    T x, y;

    point() = default;

    point(const T &x, const T &y) : x(x), y(y) {}

    point<bool> operator==(const T &val) const {
        return point<bool>(x == val, y == val);
    }

    point<bool> operator==(const point<> &other) const {
        return point<bool>(x == other.x, y == other.y);
    }

    bool all() const {
        return x && y;
    }

    bool any() const {
        return x || y;
    }

    const T &operator[](int i) const {
        return (i == 0) ? x : y;
    }

    T &operator[](int i) {
        return (i == 0) ? x : y;
    }

    double norm() const {
        return std::sqrt(x * x + y * y);
    }

    point<T> operator+(const point<T> &other) const {
        return point<T>(x + other.x, y + other.y);
    }

    template<class S>
    friend std::ostream &operator<<(std::ostream &o, const point<S> &p);
};

template<class T>
std::ostream &operator<<(std::ostream &o, const point<T> &p) {
    o << "(" << p.x << "," << p.y << ")";
    return o;
}

class PathError : std::exception {
public:
    std::string msg;

    explicit PathError(std::string msg) : msg(msg) {};

    virtual ~PathError() throw() = default;

    const char *what() const throw() {
        return msg.c_str();
    }
};

template<class vector>
std::tuple<vector, vector> get_lattice_approx(int R, bool pixely = false,
                                              int maxstep = std::numeric_limits<int>::max(), bool verbose = false) {

    int max_dist = 5;

    assert(std::sqrt(std::numeric_limits<int>::max()) > R && "Radius is too big!");

    auto up = point<>(0, 1);
    auto down = point<>(0, -1);
    auto left = point<>(-1, 0);
    auto right = point<>(1, 0);

    auto upleft = point<>(-1, 1);
    auto upright = point<>(1, 1);
    auto downleft = point<>(-1, -1);
    auto downright = point<>(1, -1);

    std::array<std::vector<point<>>, 4> avail;
    if (pixely) {
        avail = {
                std::vector<point<>>{up, left},
                std::vector<point<>>{left, down},
                std::vector<point<>>{down, right},
                std::vector<point<>>{right, up}
        };
    } else {
        avail = {
                std::vector<point<>>{up, upleft, left},
                std::vector<point<>>{left, downleft, down},
                std::vector<point<>>{down, downright, right},
                std::vector<point<>>{right, upright, up}
        };
    }

    struct Path : public std::tuple<vector, vector> {
        void append(const point<int> &P) {
            std::get<0>(*this).push_back(P[0]);
            std::get<1>(*this).push_back(P[1]);
        }
    } path;

    auto start = point<>(R, 0);

    int quadrant = 0;

    auto current = start;
    path.append(start);

    int step = 0;

    constexpr int max_quadrants = 4;

    while (step < maxstep) {
        point<> min_next;
        double min_dist = INFINITY;

        if (verbose) {
            std::cout << "####### step " << step << "\n";
        }

        for (auto &next: avail[quadrant]) {
            double dist = std::abs((current + next).norm() - R);

            if (verbose) {
                std::cout << "Trying " << current + next << " (delta is "
                          << next << ") with distance " << dist << ".\n";
            }

            if (dist < min_dist) {
                min_next = current + next;
                min_dist = dist;
            } else if (dist > max_dist) {
                throw PathError("Maximal distance reached!");
            }
        }
        if (verbose) {
            std::cout << " --> Selected " << min_next << " (delta is "
                      << min_next << ") with distance " << min_dist << ".\n";
        }

        if ((min_next == 0).any()) {
            if (verbose) {
                std::cout << " --> Change of quadrant!\n";
            }

            if (++quadrant >= max_quadrants) {
                if (!(min_next == start).all()) {
                    throw PathError("Non matching end-points!");
                }
                break;
            }
        }
        current = min_next;
        path.append(min_next);

        step++;
    }

    return path;
};
