#! /usr/bin/env python

import matplotlib.pyplot as plt
import numpy as np


def get_lattice_approx(R, pixely=False, maxstep=np.inf, verbose=False):
    up = np.array([0, 1])
    down = np.array([0, -1])
    left = np.array([-1, 0])
    right = np.array([1, 0])

    upleft = np.array([-1, 1])
    upright = np.array([1, 1])
    downleft = np.array([-1, -1])
    downright = np.array([1, -1])

    if pixely:
        avail = [
            [],
            [up, left],
            [left, down],
            [down, right],
            [right, up]
        ]
    else:
        avail = [
            [],
            [up, upleft, left],
            [left, downleft, down],
            [down, downright, right],
            [right, upright, up]
        ]

    path = []

    start = np.array([R, 0])
    quadrant = 1

    this = start
    path.append(this)

    step = 0

    while step < maxstep:
        min_next = None
        min_dist = np.inf
        if verbose:
            print("####### step {}".format(step))
        for next in avail[quadrant]:
            dist = np.abs(np.linalg.norm(this + next) - R)
            if verbose:
                print("Trying {} (delta is {}) with distance {}.".format(this + next, next, dist))
            if min_next is None or dist < min_dist:
                min_next = this + next
                min_dist = dist

        if (min_next == 0).any():
            if verbose:
                print("Change of quadrant!")
            quadrant += 1
            if quadrant > 4:
                break

        this = min_next
        path.append(this)

        step += 1

    pathx = [x[0] for x in path]
    pathy = [y[1] for y in path]

    if verbose:
        print(pathx)
        print(pathy)

    return pathx, pathy


R = 5
angle = np.linspace(0, 2 * np.pi)
x = np.arange(-10, 10)
X, Y = np.meshgrid(x, x)

plt.figure()

plt.plot(R * np.cos(angle), R * np.sin(angle))
plt.plot(np.hstack(X), np.hstack(Y), ".")

pathx, pathy = get_lattice_approx(R, True)

plt.plot(pathx, pathy)

plt.show()
