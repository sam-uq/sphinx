/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include <iostream>
#include <cmath>
#include <fstream>
#include <iterator>
#include <vector>
#include <algorithm>
#include <iomanip>

namespace sphinx {
namespace extra {
namespace primes {

    using factor_t = long long;
    using primes_t = std::vector<factor_t>;
    using numbers_t = std::pair<factor_t, primes_t>;

    bool operator<(const std::pair<factor_t, primes_t> &A, const std::pair<factor_t, primes_t> &B);

    bool cmp(const std::pair<factor_t, primes_t> &A, const factor_t &B);

    bool cmp2(const factor_t &A, const std::pair<factor_t, primes_t> &B);

    //! Find all "good" numbers that are less than \max_num
    //! \param primes A list of prim (not necessarily*) numbers. Only
    //! numbers with those factors will be considered.
    //!
    //! *e.g. You can add 4 to the list of factors, this way you will guarantee to have 4 as factor,
    //! which might be useful in many FFTs. Not ideal since you will not be able to have in-betweens.
    //! See \min_exp below.
    //! \param max_num The Maximum number to list.
    //! \param min_exp if \min_exp is specified, it defines the minimum exponent for give to the corresponging
    //! number in primes (primes[i] corresponds to min_exp[p]). If the lenght is insufficient we fall
    //! back to 0 as minimum exponent.
    //!
    //! * useful if you want to enforce a prime/non-prime factor to show up a least once.
    //! \param sorted Sort the list.
    //! \param add_anyways Fuzzy search, add number even if number surpasses \max_num
    //! This is done because we use some euristic to filter large number.
    //! \param remember_factors Store exponents to factorization in return argument. (Otherwise ->second)
    //! will be empty.
    //! \param silent Supress most output.
    //! \return List of numbers, with optional factorization.
    std::vector<numbers_t> find_all_numbers(std::vector<factor_t> primes,
                                            factor_t max_num, std::vector<factor_t> min_exp = {},
                                            bool sorted = true, bool add_anyways = true,
                                            bool remember_factors = false,
                                            bool silent = true);

    //! Within a list of sorted "good" numbers, we select the bes
    //! \param num The number we want to approximate.
    //! \param numbers A list of good numbers, including a factorization
    //! (second argument) (exponent), relevant for output only (!silent).
    //! \param primes A list of primes from which we selected the inputs, only relevant for output (!silent)
    //! \param remember_factors Is the factorization of \numbers relevant? Only relevant if !silent
    //! \param[out] ratio The relative error of the choosen number vs. the provided one (in [0,1]).
    //! \param direction Direction on which to search good numbers, one of -1 (force down), 0 (ignore), +1 (force up)
    //! \param silent Suppresses (most) output.
    //! \return The choosen number.
    factor_t find_closest_good_number(factor_t num,
                                      std::vector<numbers_t> &numbers,
                                      const primes_t &primes, bool remember_factors, double &ratio,
                                      int direction = 1,
                                      bool silent = false
    );

    void print_number(const numbers_t &all, factor_t num,
                      const primes_t &primes, bool remember_factors);

    void print_closest_good_numbers(factor_t num,
                                        std::vector<numbers_t> &numbers,
                                        const primes_t &primes, double perc,
                                        bool remember_factors);

}
}
}