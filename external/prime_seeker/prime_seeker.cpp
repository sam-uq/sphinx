/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include <iostream>
#include <cmath>
#include <fstream>
#include <iterator>
#include <vector>
#include <algorithm>
#include <iomanip>

namespace sphinx {
namespace extra {
namespace primes {

    using factor_t = long long;
    using primes_t = std::vector<factor_t>;
    using numbers_t = std::pair<factor_t, primes_t>;

    bool operator<(const std::pair<factor_t, primes_t> &A, const std::pair<factor_t, primes_t> &B) {
        return A.first < B.first;
    }

    bool cmp(const std::pair<factor_t, primes_t> &A, const factor_t &B) {
        return A.first < B;
    }

    bool cmp2(const factor_t &A, const std::pair<factor_t, primes_t> &B) {
        return A < B.first;
    }

    std::vector<numbers_t> find_all_numbers(std::vector<factor_t> primes,
                                            factor_t max_num, std::vector<factor_t> min_exp,
                                            bool sorted, bool add_anyways,
                                            bool remember_factors, bool silent) {

        std::vector<factor_t> max_exp;
        max_exp.reserve(4);
        std::cout << "Max. exponents: " << std::endl;
        for (factor_t prime: primes) {
            factor_t maxe = std::floor(std::log(max_num) / std::log(prime));
            max_exp.push_back(maxe);
            std::cout << maxe << "-";
        }
        std::cout << std::endl;

        std::vector<factor_t> exp(primes.size());

        for (unsigned int p = 0; p < primes.size(); ++p) {
            exp[p] = min_exp.size() > p ? min_exp[p] : 0;
        }

        std::vector<numbers_t> numbers; // TODO product reserve
        while (true) {
            factor_t num = 1;
            for (unsigned int q = 0; q < primes.size(); ++q) {
                num *= std::pow(primes[q], exp[q]);
            }
            if (num < max_num || add_anyways) {
                for (unsigned int q = 0; q < primes.size(); ++q) {
                    if (!silent) {
                        std::cout << primes[q] << "^" << exp[q];
                        if (q != primes.size() - 1) std::cout << " * ";
                    }
                }
                primes_t exp_copy;
                if (remember_factors) {
                    exp_copy = exp;
                }
                numbers.push_back(std::pair<factor_t, primes_t>(num, exp_copy));
                if (!silent) {
                    std::cout << " = " << num << std::endl;
                }
            }

            int q = primes.size() - 1;
            for (; q >= 0; --q) {
                if (++(exp[q]) <= max_exp[q]) {
                    break;
                } else {
                    exp[q] = min_exp.size() > (unsigned int) q? min_exp[q] : 0;
                }
            }
            if (q < 0) break;
        }
        if (!silent) {
            std::cout << "Good numbers found: " << numbers.size() << std::endl;
        }

        if (sorted) {
            std::sort(numbers.begin(), numbers.end());
        }

        return numbers;
    }

    factor_t find_closest_good_number(factor_t num,
                                      std::vector<numbers_t> &numbers,
                                      const primes_t &primes, bool remember_factors, double &ratio,
                                     int direction = 1, bool silent = true) {
        std::vector<numbers_t>::const_iterator lb = std::lower_bound(numbers.begin(), numbers.end(), num, cmp);
        if (!silent) {
            std::cout << (lb - 1)->first << " < " << num << " <= " << lb->first << std::endl;
        }
        std::vector<numbers_t>::const_iterator found;
        if(direction == 0) {
            if (std::abs((lb - 1)->first - num) < std::abs(lb->first - num)) {
                found = lb - 1;
            } else {
                found = lb;
            }
        } else {
            // If a direction is specified we force lb to be on that side.
            found = lb + (direction > 0 ? 0 : -1);
        }
        factor_t found_n = found->first;
        factor_t dist = std::abs(found_n - num);
        ratio = (double) dist / num;

        if (!silent) {
            std::cout << " - selected: " << found_n << std::endl
                      << " - distance: " << dist << std::endl
                      << " - percentage: " << std::setprecision(2) << dist * 100. / num << "%" << std::endl;
            if (remember_factors) {
                std::cout << " - factors: ";
                primes_t fac = found->second;
                for (unsigned int q = 0; q < primes.size(); ++q) {
                    std::cout << primes[q] << "^" << fac[q];
                    if (q != primes.size() - 1) std::cout << " * ";
                }
                std::cout << std::endl;
            }
            std::cout << std::endl;
        }

        return found->first;
    }

    void print_number(const numbers_t &all, factor_t num,
                      const primes_t &primes, bool remember_factors) {
        std::cout << all.first << " = ";
        if (remember_factors) {
            primes_t fac = all.second;
            for (unsigned int q = 0; q < primes.size(); ++q) {
                std::cout << primes[q] << "^" << fac[q];
                if (q != primes.size() - 1) std::cout << " * ";
            }
        }
        std::cout << " --- " << std::abs(all.first - num)
                  << " (" << std::abs(all.first - num) * 100. / num << "%)"
                  << std::endl;
    }

    void print_closest_good_numbers(factor_t num,
                                    std::vector<numbers_t> &numbers,
                                    const primes_t &primes, double perc_below, double perc_above,
                                    bool remember_factors) {

        factor_t a = static_cast<factor_t>(std::floor(num * (1. - perc_below)));
        factor_t b = static_cast<factor_t>(std::ceil(num * (1. + perc_above)));
        std::vector<numbers_t>::iterator ln = std::upper_bound(numbers.begin(), numbers.end(), a, cmp2);
        std::vector<numbers_t>::iterator un = std::lower_bound(numbers.begin(), numbers.end(), b, cmp);
        while (ln < un) {
            print_number(*ln, num, primes, remember_factors);
            print_number(*un, num, primes, remember_factors);

            ++ln;
            --un;
        }
    }

    void print_closest_good_numbers(factor_t num,
                                    std::vector<numbers_t> &numbers,
                                    const primes_t &primes, double perc,
                                    bool remember_factors) {
        print_closest_good_numbers(num, numbers, primes, perc, perc, remember_factors);
    }

}
}
}