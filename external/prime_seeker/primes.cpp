/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include <iostream>
#include <cmath>
#include <fstream>
#include <iterator>
#include <vector>
#include <algorithm>
#include <iomanip>

#include "./prime_seeker.hpp"

using namespace sphinx::extra::primes;

int main(int argc, char** argv) {
    const factor_t MAX_NUM = 20000;
    const bool pad = true;
    const bool remember_factors = false;
    
    std::vector<factor_t> primes = {2,3,4,5};
    std::vector<factor_t> min_exp = {0,0,1,0};


    // TODO: cap max number size
    // TODO: break if sum too big
    std::vector<numbers_t> numbers = find_all_numbers(primes, MAX_NUM, min_exp, true, false);
    
    std::ofstream good_numbers_file("./good_numbers.txt");
    for(numbers_t number: numbers) {
        good_numbers_file << number.first << std::endl;
    }
    good_numbers_file.close();
    
    if(argc > 1) {
        std::cout << "Found: ";
        factor_t num = std::stoi(argv[1]);
        if(pad) num = (num * 3) / 2;
        
        std::cout << "#### CLOSEST GOOD NUMBERS ####" << std::endl;
        double ratio;
        find_closest_good_number(num, numbers, primes, remember_factors, ratio);
        
        std::cout << "#### NEIGHBOURING FACTORIZATIONS ####" << std::endl;
        print_closest_good_numbers(num, numbers, primes, 0.1, remember_factors);
    }
    
    std::ofstream closest_good_numbers_file("./closest_good_numbers.txt");
    for(factor_t i = 1; i < MAX_NUM; ++i) {
        double perc;
        factor_t best_factor = find_closest_good_number(i, numbers, primes, remember_factors, perc, 0, true);
        closest_good_numbers_file << i 
                          << std::setw(15) << best_factor 
                          << std::setw(15) << std::setprecision(2) << perc 
                          << std::endl;
    }
    closest_good_numbers_file.close();
}

