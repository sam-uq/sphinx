#! /usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>

import numpy as np
import matplotlib.pyplot as plt

numbers = np.genfromtxt("closest_good_numbers.txt")

maxn = 1000

plt.figure()
plt.plot(numbers[:maxn,0], numbers[:maxn,1])
plt.savefig("num.png")

plt.figure()
plt.plot(numbers[:maxn,0], numbers[:maxn,2])
plt.savefig("perc.png")
