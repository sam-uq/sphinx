file(GLOB SCRIPTS *.cpp)

#if(USE_CUDA AND HAVE_CUDA)
#    include_directories(${INCLUDES})
#    message(STATUS ${INCLUDES})
#    add_executable(cuda_malloc cuda_malloc.cpp)
#    target_link_libraries(cuda_malloc ${LIBRARIES})
#endif()

if(USE_ACCFFT AND HAVE_ACCFFT)
    add_executable(accfft accfft.cpp)
    target_link_libraries(accfft ${LIBRARIES})
endif()

if(USE_CUDA)
    cuda_add_executable(cuda-mpi cuda-mpi.cu)
#    add_executable(cuda-mpi cuda-mpi.cpp)
    target_link_libraries(cuda-mpi ${LIBRARIES})
endif(USE_CUDA)

#set(LOG_REDIRECT "&>> output.log")
set(LOG_REDIRECT "")

set(EXEC_NAME sphinx)
if(USE_CUDA)
    set(CUDA_OPTIONAL optirun)
endif()

macro(add_auto_test name nprocs config)
    message(STATUS "Creating test ${name}...")
    file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/${config}.ini DESTINATION ${CMAKE_CURRENT_BINARY_DIR})
    if(${nprocs} GREATER 1)
        add_test(NAME ${name} COMMAND mpirun -np 2 ${CUDA_OPTIONAL} ${CMAKE_BINARY_DIR}/${EXEC_NAME} -c ${config}.ini ${LOG_REDIRECT})
    else()
        add_test(NAME ${name} COMMAND ${CUDA_OPTIONAL}  ${CMAKE_BINARY_DIR}/${EXEC_NAME} -c ${config}.ini ${LOG_REDIRECT})
    endif()
endmacro(add_auto_test)

enable_testing()

# IO tests
add_auto_test(io_test 1 io_test)

# Padding tests
add_auto_test(basic_test 1 basic_test)
add_auto_test(speed_test 1 speed_test)
if(NOT USE_CUDA)
    add_auto_test(mpi_speed_test 2 speed_test)
endif(NOT USE_CUDA)

# FFT Bench tests
add_auto_test(fftwbench_test 1 fftwbench_test)

# Projection tests
add_auto_test(proj_test 1 proj_test)

# Structure tests
add_auto_test(structure_test 1 structure_test)


if(USE_CUDA)
    add_test(NAME cuda-mpi COMMAND mpirun -np 2 optirun cuda-mpi)
endif()