/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include <cassert>
#include <iostream>
#include <cstddef>

#include <cuda.h>
#include <cuda_runtime.h>

#include <mpi.h>

#define VALUE 11

int rank, size;

enum Memory { Host, Device };

template <typename T>
int cuda_malloc(T*& buffer, std::ptrdiff_t size, Memory mem = Device) {
    cudaError_t err;
    switch(mem) {
    case Device:
        err = cudaMalloc((void**)&buffer, size*sizeof(T));
        assert(err == CUDA_SUCCESS);
        std::cout << "Allocated Device buffer at : " << buffer << "." << std::endl;
        return err;
    case Host:
        buffer = new T[size];
        std::cout << "Allocated Host buffer at : " << buffer << "." << std::endl;
        return 0;
    }
}

template <typename T>
int cuda_free(T* buffer, Memory mem = Device) {
    cudaError_t err;
    switch(mem) {
        case Device:
            std::cout << "Freed Device buffer at : " << buffer << "." << std::endl;
            err = cudaFree(buffer);
            assert(err == CUDA_SUCCESS);
            return err;
        case Host:
            std::cout << "Freed Host buffer at : " << buffer << "." << std::endl;
            delete buffer;
            return 0;
    }
}

int mpi_info(void) {
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    std::cout << "Rank/size: " << rank << "/" << size << "." << std::endl;
}

template <typename T>
int print_first(T*& host_buffer, T*& device_buffer) {
    cudaMemcpy(host_buffer, device_buffer, 1, cudaMemcpyDeviceToHost);
    std::cout << "Rank " << rank << " has value " << host_buffer[0] << "." << std::endl;
    
    return 0;
}

template <typename T>
int mem_test(T*& host_buffer, T*& device_buffer) {
    
    std::cout << "No init value: " << std::endl;
    print_first(host_buffer, device_buffer);
    
    MPI_Barrier(MPI_COMM_WORLD);
    
    if(rank == 0) {
        host_buffer[0] = rank + VALUE;
    }
    cudaMemcpy(device_buffer, host_buffer, 1, cudaMemcpyHostToDevice);
    std::cout << "Rank 1 write value: " << std::endl;
    print_first(host_buffer, device_buffer);
    
    MPI_Barrier(MPI_COMM_WORLD);
    
    if(rank != 0) {
        host_buffer[0] = rank + VALUE;
    }
    cudaMemcpy(device_buffer, host_buffer, 1, cudaMemcpyHostToDevice);
    
    std::cout << "Rank !1 write value: " << std::endl;
    print_first(host_buffer, device_buffer);
    
    return 0;
}

int main(int argc, char* argv[]) {
    MPI_Init(&argc, &argv);
    
    mpi_info();
    int* buffer, * host_buffer;
    std::ptrdiff_t size = 6;
    cuda_malloc<int>(buffer, size);
    cuda_malloc<int>(host_buffer, size, Host);
    
    mem_test(host_buffer, buffer);
    
    cuda_free<int>(buffer);
    cuda_free<int>(host_buffer, Host);
    
    MPI_Finalize();
    
    return 0;
}
