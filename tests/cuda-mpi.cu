/*
 * Copyright 2015-2018 ETH Zurich, Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include <iostream>
#include <algorithm>
#include <numeric>

#include <cuda.h>

#include <mpi.h>
#include <mpi-ext.h>

#include <thrust/device_vector.h>
#include <thrust/reduce.h>

int main(int argc, char** argv) {
    int provided;
    MPI_Init_thread(&argc, &argv, MPI_THREAD_FUNNELED, &provided);

#ifdef MPIX_CUDA_AWARE_SUPPORT
    std::cout << "MPIX_CUDA_AWARE_SUPPORT = " << MPIX_CUDA_AWARE_SUPPORT << std::endl;
#else
    std::cout << "'MPIX_CUDA_AWARE_SUPPORT' not defined.\n";
#endif // PIX_CUDA_AWARE_SUPPORT

    int query = MPIX_Query_cuda_support();
    std::cout << "MPI query cuda support: " << query << std::endl;

    int N = 512;
    int ranks;
    MPI_Comm_size(MPI_COMM_WORLD, &ranks);

    thrust::device_vector<int> device_data(N), device_data_out(N);

    std::vector<int> host_data(N), host_data_out(N);

    for(int i = 0; i < N; ++i) {
        host_data[i] = i;
    }
    thrust::copy(host_data.begin(), host_data.end(), device_data.begin());

    int sum = std::accumulate(host_data.begin(), host_data.end(), 0);
    int sum_d = thrust::reduce(device_data.begin(), device_data.end());

    std::cout << "Before sum: host = " << sum << ", device = " << sum_d << ".\n";

    std::cout << "Allreduce on Host data:\n";
    int err = MPI_Allreduce(host_data.data(),
                            host_data_out.data(),
                            N, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
    std::cout << "Error code = " << err << "\n";

    int sum_out = std::accumulate(host_data_out.begin(), host_data_out.end(), 0);
    int sum_d_out = thrust::reduce(device_data_out.begin(), device_data_out.end());

    std::cout << "After sum: host = " << sum_out
              << ", device = " << sum_d_out
              << " (should be " << sum * ranks << ").\n";

    std::cout << "Allreduce on Device data:\n";
    err = MPI_Allreduce(device_data.data().get(),
                        device_data_out.data().get(),
                        N, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
    std::cout << "Error code = " << err << "\n";


    MPI_Finalize();
}
