# - Find the ACCFFT library
#
# Usage:
#   find_package(ACCFFT [REQUIRED] [QUIET] )
#
# It sets the following variables:
#   ACCFFT_FOUND               ... true if fftw is found on the system
#   ACCFFT_LIBRARIES           ... full path to fftw library
#   ACCFFT_INCLUDES            ... fftw include directory
#
# The following variables will be checked by the function
#   ACCFFT_USE_STATIC_LIBS    ... if true, only static libraries are found
#   ACCFFT_ROOT               ... if set, the libraries are exclusively searched
#                               under this path
#   ACCFFT_LIBRARY            ... fftw library to use
#   ACCFFT_INCLUDE_DIR        ... fftw include directory
#

# message(STATUS "Looking for ACCFFT")

#If environment variable ACCFFT_DIR is specified, it has same effect as ACCFFT_ROOT
if( NOT ACCFFT_ROOT AND ACCFFT_DIR )
    set( ACCFFT_ROOT "${ACCFFT_DIR}" )
elseif( NOT ACCFFT_ROOT AND ENV{ACCFFT_DIR} )
    set( ACCFFT_ROOT $ENV{ACCFFT_DIR} )
endif()

# Check if we can use PkgConfig
find_package(PkgConfig)

#Determine from PKG
if( PKG_CONFIG_FOUND AND NOT ACCFFT_ROOT )
  pkg_check_modules( PKG_ACCFFT QUIET "accfft" )
endif()

#find libs
find_library(ACCFFT_LIB NAMES "accfft" "accfft_utils" PATHS ${ACCFFT_ROOT} ${PKG_ACCFFT_LIBRARY_DIRS} ${LIB_INSTALL_DIR} PATH_SUFFIXES "lib" "lib64")
#message(STATUS "${ACCFFT_LIB} ${ACCFFT_ROOT}")
find_library(ACCFFT_GPU_LIB NAMES "accfft_gpu" "accfft_utils_gpu" PATHS ${ACCFFT_ROOT} ${PKG_ACCFFT_LIBRARY_DIRS} ${LIB_INSTALL_DIR} PATH_SUFFIXES "lib" "lib64" NO_DEFAULT_PATH)

#find includes
find_path(ACCFFT_INCLUDES NAMES "accfft.h" "accfft_gpu.h" "accfftf.h" "accfft_gpuf.h" PATHS ${ACCFFT_ROOT} ${PKG_ACCFFT_LIBRARY_DIRS} ${LIB_INSTALL_DIR}
                                                        PATH_SUFFIXES "include" "include/accfft")
                                                        
set(ACCFFT_LIBRARIES ${ACCFFT_LIB} ${ACCFFT_GPU_LIB})

set(CMAKE_FIND_LIBRARY_SUFFIXES ${CMAKE_FIND_LIBRARY_SUFFIXES_SAV} )

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(ACCFFT DEFAULT_MSG
                                  ACCFFT_INCLUDES ACCFFT_LIBRARIES)

mark_as_advanced(ACCFFT_INCLUDES ACCFFT_LIBRARIES)
