# - Find the MKL libraries
# Modified from Armadillo's ARMA_FindMKL.cmake, modified from
# This module takes as input:
#  ENV{MKLROOT_PATH}, set to hint mkl lib location
#  ENV{MKLROOT}, set to hint to mkl lib lobarion
#  USE_MKL_32BIT_LIB, set true if wanna force 32 bit library
#  MKL_MPI_TYPE, choose BLACS MPI backend: between nothing, openmpi, intelmpi, sgimpt
#  MKL_THREAD_TYPE, choose between: gnu, intel, pgi, tbb, if want to force select a threading type
#  MKL_FIND_QUIETLY, set to true if wanna suppress oputput
# This module defines
#  MKL_INCLUDE_DIR, the directory for the MKL headers
#  MKL_LIB_DIR, the directory for the MKL library files
#  MKL_COMPILER_LIB_DIR, the directory for the MKL compiler library files
#  MKL_LIBRARIES, the libraries needed to use Intel's implementation of BLAS & LAPACK.
#  MKL_FOUND, If false, do not try to use MKL; if true, the macro definition USE_MKL is added.
# Available components: BLACS and CDFT (TODO: fftw)

# Set the include path, try to find at /opt/intel/mkl, ENV{MKLROOT}, or ENV{MKLROOT_PATH} (reverse order)
# In windows, try to find MKL at C:/Program Files (x86)/Intel/Composer XE/mkl
if ( WIN32 )
  if(NOT DEFINED ENV{MKLROOT_PATH})
    set(MKLROOT_PATH "C:/Program Files (x86)/Intel/Composer XE/mkl" CACHE PATH "Where the MKL are stored")
  endif(NOT DEFINED ENV{MKLROOT_PATH})
else( WIN32 )
    if( DEFINED ENV{MKLROOT_PATH} )
        set(MKLROOT_PATH $ENV{MKLROOT_PATH} CACHE PATH "Where the MKL are stored")
    elseif( DEFINED ENV{MKLROOT} )
        set(MKLROOT_PATH $ENV{MKLROOT} CACHE PATH "Where the MKL are stored")
    else( DEFINED ENV{MKLROOT}  )
        set(MKLROOT_PATH "/opt/intel/mkl" CACHE PATH "Where the MKL are stored")
    endif( DEFINED ENV{MKLROOT_PATH} )
endif( WIN32 )

# Check if MKL is found at provided directory
if (EXISTS ${MKLROOT_PATH})
    set(MKL_FOUND TRUE)

    # Detect MKL bit size
    message(STATUS "MKL is found at ${MKLROOT_PATH}")
    if(CMAKE_SIZEOF_VOID_P EQUAL 8)
        message(STATUS "MKL uses 64 bit")
        set( USE_MKL_64BIT On )
        set(MKL_BIT_TYPE ilp64)
    else(CMAKE_SIZEOF_VOID_P EQUAL 8)
        message(STATUS "MKL uses 32 bit")
        set( USE_MKL_64BIT Off )
        set(MKL_BIT_TYPE lp64)
    endif(CMAKE_SIZEOF_VOID_P EQUAL 8)
else (EXISTS ${MKLROOT_PATH})
    set(MKL_FOUND FALSE)
endif (EXISTS ${MKLROOT_PATH})

# Force usage of 32 bit lib on 64 bit system
if(USE_MKL_32BIT_LIB)
    set(MKL_USE_BIT_TYPE lp64)
else(USE_MKL_32BIT_LIB)
    set(MKL_USE_BIT_TYPE ilp64)
endif(USE_MKL_32BIT_LIB)


# Auto set threads if not set
list (FIND MKL_FIND_COMPONENTS "threads" _index)
if(NOT MKL_THREAD_TYPE AND ${_index} GREATER -1)
    set(MKL_USE_THREADS ON)
    if(WIN32)
        set(MKL_THREAD_TYPE "intel")
    else(WIN32) # NOT Win32
        set(MKL_THREAD_TYPE "gnu")
    endif(WIN32)
endif(NOT MKL_THREAD_TYPE AND ${_index} GREATER -1)
if(MKL_USE_THREADS)
set(MKL_threads_LIB mkl_${MKL_THREAD_TYPE}_thread)
else()
    set(MKL_threads_LIB mkl_sequential)
endif()

find_library(MKL_CDFT_LIB NAMES "mkl_cdft_core"
    PATHS ${MKLROOT_PATH}/lib/intel64 ${MKLROOT_PATH}/lib/ia32)
find_library(MKL_BLACS_LIB NAMES "mkl_blacs_ilp64" "mkl_blacs_lp64"
    PATHS ${MKLROOT_PATH}/lib/intel64 ${MKLROOT_PATH}/lib/ia32)
find_library(MKL_BLAS_LIB NAMES "mkl_blas95_ilp64" "mkl_blas95_lp64"
    PATHS ${MKLROOT_PATH}/lib/intel64 ${MKLROOT_PATH}/lib/ia32)
find_path(MKL_FFTW_LIB NAMES "fftw3_mkl.h"
    PATHS ${MKLROOT_PATH}/include/fftw)

list (FIND MKL_FIND_COMPONENTS "CDFT" _index)
if (${_index} GREATER -1)
    list(APPEND MKL_FIND_COMPONENTS "BLACS")
endif()

# Define components
IF( MKL_FIND_COMPONENTS )
  FOREACH(comp ${MKL_FIND_COMPONENTS})
    if(NOT MKL_${comp}_LIB)
      SET(MKL_${comp}_FOUND FALSE)
      IF(MKL_FIND_REQUIRED_${comp})
        MESSAGE(FATAL_ERROR "MKL ${comp} not available.")
      ENDIF()
    ELSE()
      SET(MKL_${comp}_FOUND 1)
    ENDIF()
  ENDFOREACH()
ENDIF()

if (MKL_FOUND)
    set(MKL_INCLUDE_DIR "${MKLROOT_PATH}/include")
    if( MKL_FFTW_FOUND )
        list(APPEND MKL_INCLUDE_DIR "${MKLROOT_PATH}/include/fftw")
    endif()

    add_definitions(-DUSE_MKL)

    if ( USE_MKL_64BIT )

        set(MKL_LIB_DIR "${MKLROOT_PATH}/lib/intel64")
        set(MKL_COMPILER_LIB_DIR "${MKLROOT_PATH}/compiler/lib/intel64")
        set(MKL_COMPILER_LIB_DIR ${MKL_COMPILER_LIB_DIR} "${MKLROOT_PATH}/lib/intel64")

        # Enable FFT support
        if( MKL_CDFT_FOUND )
            if( NOT MKL_BLACS_FOUND )
                message(FATAL_ERROR "No BLACS component found (required by CDFT).")
            endif( NOT MKL_BLACS_FOUND )
            set(MKL_LIBRARIES ${MKL_LIBRARIES} mkl_cdft_core)
        endif( MKL_CDFT_FOUND )

        # Link against correct Blacs MPI shared library
        if( MKL_BLAS_FOUND )
            set(MKL_LIBRARIES ${MKL_LIBRARIES} mkl_blas95_${MKL_USE_BIT_TYPE})
        endif()

        # Link against correct Blacs MPI shared library
        if( MKL_CDFT_FOUND OR MKL_BLASC_FOUND )
            if( MKL_MPI_TYPE )
                set(MKL_LIBRARIES ${MKL_LIBRARIES} mkl_blacs_${MKL_MPI_TYPE}_${MKL_USE_BIT_TYPE})
            else()
                set(MKL_LIBRARIES ${MKL_LIBRARIES} mkl_blacs_${MKL_USE_BIT_TYPE})
            endif()
        endif( MKL_CDFT_FOUND OR MKL_BLASC_FOUND )

        set(MKL_LIBRARIES ${MKL_LIBRARIES} mkl_intel_${MKL_USE_BIT_TYPE})

    else( USE_MKL_64BIT )
        set(MKL_LIB_DIR "${MKLROOT_PATH}/lib/ia32")
        set(MKL_COMPILER_LIB_DIR "${MKLROOT_PATH}/compiler/lib/ia32")
        set(MKL_COMPILER_LIB_DIR ${MKL_COMPILER_LIB_DIR} "${MKLROOT_PATH}/lib/ia32")
        if ( WIN32 )
            set(MKL_LIBRARIES ${MKL_LIBRARIES} mkl_intel_c)
        else ( WIN32 )
            set(MKL_LIBRARIES ${MKL_LIBRARIES} mkl_intel)
        endif ( WIN32 )
    endif( USE_MKL_64BIT )

    set(MKL_LIBRARIES ${MKL_LIBRARIES} ${MKL_threads_LIB})

    if(WIN32)
        set(MKL_LIBRARIES ${MKL_LIBRARIES} libiomp5md)
    endif (WIN32)

    set(MKL_LIBRARIES ${MKL_LIBRARIES} mkl_core)

    IF(NOT MKL_FIND_QUIETLY)
        MESSAGE(STATUS "Found MKL libraries: ${MKL_LIBRARIES}")
        MESSAGE(STATUS "MKL_INCLUDE_DIR: ${MKL_INCLUDE_DIR}")
        MESSAGE(STATUS "MKL_LIB_DIR: ${MKL_LIB_DIR}")
        MESSAGE(STATUS "MKL_COMPILER_LIB_DIR: ${MKL_COMPILER_LIB_DIR}")
    ENDIF(NOT MKL_FIND_QUIETLY)

    INCLUDE_DIRECTORIES(${MKL_INCLUDE_DIR})
    LINK_DIRECTORIES(${MKL_LIB_DIR} ${MKL_COMPILER_LIB_DIR})
endif(MKL_FOUND)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(MKL DEFAULT_MSG
                                  MKL_FOUND MKL_INCLUDE_DIR MKL_LIBRARIES MKL_LIB_DIR)

set(MKL_INCLUDE_DIRS ${MKL_INCLUDE_DIR})
set(MKL_LIBRARY ${MKL_LIBRARIES})

mark_as_advanced(MKL_INCLUDE_DIR MKL_INCLUDE_DIRS MKL_LIBRARY MKL_LIBRARIES MKL_LIB_DIR)
